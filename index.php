<?php
	/** 
	* Main page
	* @author: ROUMEGUE Jérémy - 360sc
	*/
	
	/**
	* Starting a session
	*/
	session_start();

	/** 
	* Require algorithm
	*/
	require "./algorithm/vigenereCypher.php"; 
	define("SERVERROOT",'/360'); 
	
	/***************************
	******** Variables *********
	***************************/

	function connection_db() {
		try {
			//return new PDO("mysql:host=82.97.21.144;port=10026;dbname=360sc_cms;charset=utf8", "bdd_special", "360BacASable");
			return new PDO("mysql:host=localhost;port=3306;dbname=360sc_cms;charset=utf8", "root", "");
		} catch (Exception $e){
			die('Erreur : ' . $e->getMessage());
		}
	};

	
	/**
	* Get the main link of the direction of files
	*/
	function get_link() { 
		return '/360/'; 
	};
	


	/**
	* connect and redirect variable
	*/
	$_SESSION['connect']=false;
	$_SESSION['redirect']=false;
	
	/**
	* Check connection
	*/
	if(isset($_SESSION['client_id']) AND strlen($_SESSION['client_id']) != 0 ){
		$_SESSION['connect'] = true;
	}
	

	
		require "./algorithm/outils.php";

	/***************************
	****** Initialisation ******
	***************************/
	
    // In case one is using PHP 5.4's built-in server
    $filename = __DIR__ . preg_replace('#(\?.*)$#', '', $_SERVER['REQUEST_URI']);
	if (php_sapi_name() === 'cli-server' && is_file($filename)) {
		return false;
	}

    // Include the Router class
    require_once __DIR__ . '/./Router/Router.php';

    // Create a Router
    $router = new Router\Router();

    // Custom 404 Handler
    $router->set404(function () {
        header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
        require "./404.php";
    });
	
    // Before Router Middleware
    $router->before('GET', '/.*', function () {
        header('X-Powered-By: router');
    });



	/************************************************************************************************************
	*************************************************** TABLE ROUTE *********************************************
	************************************************************************************************************/


    require_once __DIR__ . '/./Router/RouteTable.php';


	/***************************
	******** Search Page *******
	***************************/
	$router->get('/search/', function () {
		/**
		* We display and set deconnection page.
		*/
		$_SESSION['page'] = "search";
		require "./php/search.php";
	});

	/***************************
	******* Search Page ********
	***************************/
	$router->post('/search/', function () {
		/**
		* We display and set deconnection page.
		*/
		$_SESSION['page'] = "search";
		require "./php/search.php";
	});
  
	 
	
	/***************************
	***** Connection page ******
	***************************/
	$router->get('/connexion/', function () {
		/**
		* We display and set connection page.
		*/
		$_SESSION['page'] = "connexion";
		require "./php/connexion.php";
	});

	$router->get('/_connexion/', function () {
		/**
		* We display and set connection page.
		*/
		$_SESSION['page'] = "_connexion";
		require "./php/_connexion.php";
	});
	
	/***************************
	**** Deconnection page *****
	***************************/
	$router->get('/deconnexion/', function () {
		/**
		* We display and set deconnection page.
		*/
		session_destroy();
		header('Location: '.get_link().'connexion/');
	});
	
	/***************************
	**** Connection request ****
	***************************/
	$router->post('/connexion/', function () {
		/**
		* We display and set connection page.
		*/
		$_SESSION['page'] = "connexion";
		require "./php/connexion.php";
	});
	$router->post('/_connexion/', function () {
		/**
		* We display and set connection page.
		*/
		$_SESSION['page'] = "connexion";
		require "./php/_connexion.php";
	});
 
	
	/***************************
	********* Add page *********
	***************************/
	$router->get('/add/', function () {
		/**
		* We display and set add page.
		*/
		$_SESSION['page'] = "add";
		require "./php/add.php";
	});
	 
	
	/***************************
	******* Import Page ********
	***************************/
	$router->get('/import/', function () {
		/**
		* We display and set import page.
		*/
		$_SESSION['page'] = "import";
		require "./php/import.php";
	});
	  
	

	

	/*
	// IMPORTATION (work but useless)
	$router->post('/import/', function () {
		include "./php/PHPExcel.php";
		include "./php/PHPExcel/IOFactory.php";
		//$objet = new PHPExcel_Reader_Excel2007();
		$objet = PHPExcel_IOFactory::createReader('Excel2007');
		$excel = $objet->load('360sc-tags.xlsx');
		$sheet = $excel->getSheet(0);
		// On boucle sur les lignes
		foreach($sheet->getRowIterator() as $row) {
		   // On boucle sur les cellule de la ligne
		   foreach ($row->getCellIterator() as $cell) {
			 if('B' == $cell->getColumn()) {
				print_r($cell->getValue());
			 } else if('C' == $cell->getColumn()) {
				print_r($cell->getCalculatedValue());
			 }
		   }
		   echo '<br/>';
		}
	});
	*/
	 
	/***************************
	****** URLe was call *******
	***************************/
	$router->get('/(\w+)', function ($id) {
		
		//if($_SERVER['HTTP_HOST'] == "360sc.yt" OR $_SERVER['HTTP_HOST'] == "localhost"){
			$key = get_client_key();
		/*} else {
			$key = get_domain_key($_SERVER['HTTP_HOST']);
		}*/
		/**
		* Read and decrypt URL
		*/
		$cVigenere = new classVigenere($key);
		$key = $cVigenere->decrypt(trim(htmlentities($id)));
		
		//echo $key;
		
		/**
		* We open the database for the SQL request.
		*/
		$bdd = connection_db();
		
		/**
		* We prepare SQL request for searching URLs.
		*/
		$target = $bdd->prepare('SELECT * FROM 360sc_iz.yourls WHERE ShortURL=:url');
		$target->execute(array('url'=>$key));
		
		/*****************************
		****Redirect to the good url**
		****Modified by Jorge Luis****
		*****************************/
 
		
		while($donnee = $target->fetch()) {
			$url = $donnee['OriginalURL'];
			if ($donnee['OriginalURL'] == 'http://www.360sc.yt/webApp/FingWa_MCInfo'){
				$url = get_link().'webApp/FingWa_MCInfo/'.$key;
			}
			if ($donnee['OriginalURL'] == 'http://www.360sc.yt/webApp/FingerWA_MiseenServiceGeoloc'){
				$objetQuery = $bdd->prepare('SELECT * FROM 360sc_cms.objet_tags WHERE yourlsID = :yourlsID');
				$objetQuery->bindValue(':yourlsID',$donnee['YourlsID']);
				$objetQuery->execute();
				while($row = $objetQuery->fetch()){
					$url = get_link().'webApp/FingerWA_MiseenServiceGeoloc/'.$row['objetID'];		
				}				
			}
			if ($donnee['OriginalURL'] == 'http://www.360sc.yt/webApp/FingerWA_Selecteur'){
				$objetQuery = $bdd->prepare('SELECT * FROM 360sc_cms.objet_tags WHERE yourlsID = :yourlsID');
				$objetQuery->bindValue(':yourlsID',$donnee['YourlsID']);
				$objetQuery->execute();
				while($row = $objetQuery->fetch()){
					$url = get_link().'webApp/FingerWA_Selecteur/'.$row['objetID'];		
				}				
			}
			if ($donnee['OriginalURL'] == 'http://www.360sc.yt/fingerwademosalonmpb'){
				$objetQuery = $bdd->prepare('SELECT * FROM 360sc_cms.objet_tags WHERE yourlsID = :yourlsID');
				$objetQuery->bindValue(':yourlsID',$donnee['YourlsID']);
				$objetQuery->execute();
				while($row = $objetQuery->fetch()){
					$url = get_link().'fingerwademosalonmpb/'.$row['objetID'];		
				}				
			}
			header('Location: '.$url);
		}
		$target->closeCursor();
	});




    // Launch redirection.
    $router->run();
?>