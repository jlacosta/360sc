<{extends file="showNotification.tpl"}> 
<{block name=body append}>
<div class="row">
  <div class="columns large-12 small-12">
    <div class="panel">
      <p>
        <!-- Form for encrypt an URLs in many tags -->
        <form action="<{$postPage}>" method="post">
          <div class="form-group">
            <label for="url">URL cible</label>
            <input id="url" type="text" name="url" value="http://"/>
          </div>
          <div class="form-group">
            <label for="tagName">Nom du tag</label>
            <input id="tagName" type="text" name="tagName" />
          </div>
          <div class="form-group">
            <label for="nb_tags">Nombre de tags souhaités</label>
            <input id="nb_tags" type="text" name="nb_tags" value="1"/>
          </div>
          <input class="medium button" type="submit" value="Envoyer" />
        </form>
      </p>
    </div>
  </div>      
</div>
<{/block}> 