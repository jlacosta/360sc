<{extends file="noNotification.tpl"}> 
<{block name=body append}>
<div class="row">
  <div class="columns large-12 small-12">
    <div class="panel">
      <p>
        Voici les <{$paginate.total}> URLe(s) dont vous disposez.<br/> 
        <div class="paginateNav" >
          <{paginate_prev id="urles"}> 
          <{paginate_middle id="urles"}> 
          <{paginate_next id="urles"}>
        </div>
        <div class="tableContainer">
          <table>
            <tr><th>MCs</th></tr>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>ShortURL</th>
              <th>URL de sortie</th>
              <th>UID</th>
            </tr>
          
           <{section name=key loop=$MCs}> 
            <tr>
              <td><{$MCs[key].TagID}></td>
              <td><{$MCs[key].TagName}></td>
              <td><{$MCs[key].urle}></td>
              <td><{$MCs[key].OriginalURL}></td>
              <td><{$MCs[key].UniqueTagID}></td>
            </tr>
              
          <{/section}>
          </table>
        </div>
        <div class="paginateNav" >
          <{paginate_prev id="urles"}> 
          <{paginate_middle id="urles"}> 
          <{paginate_next id="urles"}>
        </div>
      </p>
    </div>
  </div>      
</div>
<{/block}> 