<{extends file="showNotification.tpl"}>  
 

<{block name=body append}>
<div class="row">

  <div class="columns large-12 small-12">
    <div class="panel">
      <p>Modifier le MC</p>
      <form action="<{$postPage}>" method="post" >
        <div class="form-group">
          <label for="name">Nom du MC :</label>
          <input name="name" value="<{$MC.name}>"  type="text" />
        </div> 
        <div class="form-group">
          <label for="desc">Description :</label>
          <input id="desc" type="text" name="desc" value="<{$MC.description}>"/>
        </div>
        <div class="form-group">
          <label for="alphaId">URL de sortie :</label> 
            <input id="url" type="text" name="url" value="<{$MC.url}>"/>
        </div>
        <div class="form-group">
          <label for="owner">Propiétaire</label>
          <select id='owner' name='owner' >
            <{html_options options=$societies selected=$MC.societyId}>
          </select>
        </div> 
        <div class="form-group">
          <label for="active" style="display:inline">Active :</label>
          <{if $MC.active}>
            <input id="active" type="checkbox" name="active" value="<{$MC.active}>" checked/>
          <{else}>
            <input id="active" type="checkbox" name="active" value="<{$MC.active}>" />
          <{/if}>
        </div> 
                    
          <input class="medium button" type="submit" value="Valider" />
          <a href="<{$serverRoot}>" class="button" style="text-decoration:none;float:right">Annuler</a>
      </form>
    </div>
  </div>

</div>
<{/block}>