﻿<{extends file="noNotification.tpl"}> 

<{block name=jsZone append}>
  <script>
        $(document).ready(function(){

          $("#checkAll").click(function(){
            if($(this).is(":checked")){
              $("tr.MC-id").addClass("selected");
              $("input[type='checkbox']").prop("checked",true);
            }else{
              $("tr.MC-id").removeClass("selected");
              $("input[type='checkbox']").prop("checked",false);
            }
          });
 

          $("tr.MC-id").on('click', function(e){
            if($(this).hasClass("selected")) 
                $(this).removeClass("selected");
              else
                $(this).addClass("selected");
            if($(e.target).closest('input[type="checkbox"]').length > 0){

            }else{
              
              $(this).find("input[type='checkbox']").each(function(){
                $(this).prop("checked",!$(this).prop("checked"));
              }); 
            }
          });
        });
        </script> 
<{/block}>

<{block name=body append}>
<div class="row">
  <div class="columns large-12 small-12">
    <div class="panel">
      <p>
        Liste des MCs déjà attribués :<br/>  
        <{paginate_prev id="gMCs"}> 
        <{paginate_middle id="gMCs"}> 
        <{paginate_next id="gMCs"}>
        <form action="<{$postPage}>" method="post" enctype="multipart/form-data">
        <table style="width:100%">
          <tr>
            <th><input type="checkbox" id="checkAll" /></th>
            <th>ID</th>
            <th>Nom</th>
            <th>Description</th> 
            <th>Entreprise</th> 
          </tr>
        
         <{section name=key loop=$MCs}> 
          <tr id="<{$MCs[key].TagID}>" class="MC-id">
            <td><input type="checkbox" id="checkbox-<{$MCs[key].TagID}>" name="selectedMCs[]" value="<{$MCs[key].TagID}>"/></td>
            <td><{$MCs[key].TagID}></td>
            <td><{$MCs[key].TagName}></td>
            <td><{$MCs[key].description}></td> 
            <td><{$MCs[key].name}></td> 
          </tr>

        <{/section}>
        </table>
        <{paginate_prev id="gMCs"}> 
        <{paginate_middle id="gMCs"}> 
        <{paginate_next id="gMCs"}>

        <div class="form-group">
            <label for="owner">*Entreprise</label>
            <select id='owner' name='owner' >
              <{html_options options=$societies selected=$accountSociety}>
            </select>
          </div>
          <input class="button" type="submit" value="Envoyer" />
        </form>
      </p>
    </div>
  </div>      
</div>
<{/block}> 