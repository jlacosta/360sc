<{extends file="showNotification.tpl"}> 
<{block name=jsZone append}>

<script type="text/javascript">

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#imgPreview').attr('src', e.target.result);
                $('#imgPreview').css('display', 'inline-block'); 
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

  $(document).ready(function(){        

    $('input#newAccountImage').change(function(){
        readURL(this);
    }); 

    $("form").submit(function(){
      if($("#confirm").val()=="" || $("#password").val()=="" || $("#confirm").val()!==$("#password").val()){
        alert("Votre mot de passe et votre confirmation sont différents, ou sont vides");
        alert($("#confirm").val()+" "+$("#password").val()) ;
        return false;
      }else{
        return;
      }
    });

  });

</script>
<{/block}>

<{block name=body append}>
<div class="row">
  <div class="columns large-12 small-12">
    <div class="panel">
      <p>
        
        <!-- Form for create an account -->
        <form action="<{$postPage}>" method="post" enctype="multipart/form-data">
          <fieldset>
            <legend>Identifiants</legend>
            <div class="form-group">
              <label for="login">* Identifiant :</label>
              <input name="login" type="text" id="login" />
            </div>
            <div class="form-group">
              <label for="name">* Nom :</label>
              <input name="name" type="text" id="name" />
            </div>
            <div class="form-group">
              <label>* Entreprise :</label>
              <select name="society" id="society">
                <{html_options options=$societies}>
              </select>
            </div>
            <div class="form-group">
              <label for="password">* Mot de Passe :</label>
              <input type="password" name="password" id="password" />
            </div>
            <div class="form-group">
              <label for="confirm">* Confirmer le mot de passe :</label>
              <input type="password" name="confirm" id="confirm" />
            </div>
            

          </fieldset>
          <fieldset>
            <legend>Contact</legend>
            <div class="form-group">
              <label for="name">* Droit du compte :</label>
              <{include file='permissions-checkboxes.tpl'}>
            </div>
            <div class="form-group">
              <label for="mobile">Téléphone Mobile :</label>
              <input type="text" name="mobile" id="mobile" />
            </div>
            <div class="form-group">
              <label for="email">* Email :</label>
              <input type="text" name="email" id="email" />
            </div>
            <div class="form-group">
              <label for="newAccountImage">Image :</label>   
              <img id="imgPreview" src="#" alt="new image"/ style="display:none"/>
              <input type="file" name="newAccountImage" id="newAccountImage" />
            </div>
          </fieldset>
          <p>Les champs précédés d'un * sont obligatoires</p>
          <p>
            <input class="medium button" type="submit" value="Envoyer" />
          </p>


      </form>
    </p> 
    </div>
  </div>      
</div>
<{/block}> 