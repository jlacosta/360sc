<ul class="multi-check">
  <{if $smarty.session.adminMA }> 
    <li class="checkbox-inline">
      <input type="checkbox" name="permissions[]" value="adminMA" id="AAM"/>
      <label for="AAM"> Administration <acronym title="Application Mobile">MA</acronym>
      </label>
    </li>
  <{/if}>
  <{if $smarty.session.clientMA}>
    <li class="checkbox-inline">
      <input type="checkbox" name="permissions[]" value="clientMA" id="CAM"/> 
      <label for="CAM">Client <acronym title="Application Mobile">MA</acronym> 
      </label>
    </li>
  <{/if}>
  <{if $smarty.session.gestionTags}>
    <li class="checkbox-inline">
      <input type="checkbox" name="permissions[]" value="gestionTags" id="GMC"/> 
      <label for="GMC">Gestion des <acronym title="Modules Communiquants">MC</acronym> 
      </label>
    </li>
  <{/if}>
  <{if $smarty.session.searchTags}>
    <li class="checkbox-inline">
      <input type="checkbox" name="permissions[]" value="searchTags" id="RMC"/> 
      <label for="RMC">Recherche des <acronym title="Modules Communiquants">MC</acronym> 
      </label>
    </li>
  <{/if}>
  <{if $smarty.session.addTag}>
    <li class="checkbox-inline">
      <input type="checkbox" name="permissions[]" value="addTag" id="AMC"/> 
      <label for="AMC">Ajout de <acronym title="Module Communiquant">MC</acronym> 
      </label>
    </li>
  <{/if}>
  <{if $smarty.session.societePF}>
    <li class="checkbox-inline">
      <input type="checkbox" name="permissions[]" value="societePF" id="PF"/> 
      <label for="PF">Ajout de soci&eacute;t&eacute; 
      </label>
    </li>
  <{/if}>
</ul>
<ul class="multi-check">          
  <{if $smarty.session.editTags}>
    <li class="checkbox-inline">
      <input type="checkbox" name="permissions[]" value="editTags" id="EMC"/> 
      <label for="EMC"> Edition des <acronym title="Modules Communiquants">MC</acronym> </label>
    </li>
  <{/if}>
  <{if $smarty.session.addObject}>
    <li class="checkbox-inline">
      <input type="checkbox" name="permissions[]" value="addObject" id="AO"/> 
      <label for="AO"> Ajout d&apos;objet </label>
    </li>
  <{/if}>
  <{if $smarty.session.editObject}>
    <li class="checkbox-inline">
      <input type="checkbox" name="permissions[]" value="editObject" id="EO"/> 
      <label for="EO"> Edition d&apos;objet </label>
    </li>
  <{/if}>
  <{if $smarty.session.createUser}>
    <li class="checkbox-inline">
      <input type="checkbox" name="permissions[]" value="createUser" id="CU"/> 
      <label for="CU"> Création d&apos;utilisateur </label>
    </li>
  <{/if}>
  <{if $smarty.session.export}>
    <li class="checkbox-inline">
      <input type="checkbox" name="permissions[]" value="export" id="E"/> 
      <label for="E"> Exportation </label>
    </li>
  <{/if}>
  <{if $smarty.session.SURLE}>
    <li class="checkbox-inline">
      <input type="checkbox" name="permissions[]" value="SURLE" id="SURLE"/> 
      <label for="SURLE"> Voir les URLe </label>
    </li>
  <{/if}>
  <{if $smarty.session.type}> 
    <li class="checkbox-inline">
      <input type="checkbox" name="permissions[]" value="type" id="T"/> 
      <label for="T"> Ajout Type d'Objet </label>
    </li>
  <{/if}>
  </ul> 