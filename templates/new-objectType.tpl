<{extends file="showNotification.tpl"}> 
<{block name=jsZone append}>

<script type="text/javascript">

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#imgPreview').attr('src', e.target.result);
                $('#imgPreview').css('display', 'inline-block'); 
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

  $(document).ready(function(){        

    $('input#imgObjType').change(function(){
        readURL(this);
    }); 

  });

</script>
<{/block}>
<{block name=body append}>
<div class="row">
  <div class="columns large-12 small-12">
    <div class="panel">
      <p>
        Bienvenue dans l'interface d'administration de IZ.<br/>
        <!-- Form for encrypt an URLs in many tags -->
        <form action="<{$postPage}>" method="post">
          <div class="form-group">
            <label for="name">Nom du type</label>
            <input id="name" type="text" name="name" />
          </div>
          <div class="form-group">
            <label for="alpha">References externes du type d'objet</label>
            <input id="alpha" type="text" name="alpha" />
          </div>
          <div class="form-group">
            <label for="desc">Description du type</label>
            <input id="desc" type="text" name="desc" />
          </div>
          <div class="form-group">
            <label for="imgObjType">Image du type d'objet</label>
            <img id="imgPreview" src="#" alt="new image"/ style="display:none">
            <input id="imgObjType" type="file" name="imgObjType" />
          </div>
          <{if $godRight}>
          <div class="form-group">
            <input id="commonType" type="checkbox" name="commonType" />
            <label for="commonType">Type commun</label>             
          </div>
          <{/if}>
          <input class="medium button" type="submit" value="Envoyer" />
        </form>
      </p>
    </div>
  </div>      
</div>
<{/block}> 