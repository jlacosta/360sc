<{extends file="layout.tpl"}>

<{block name=jsZone}>
  <script src='<{$serverRoot}>/build/libs/jquery.js'></script>
  <script src='<{$serverRoot}>/build/libs/iscroll.js'></script>
  <script src='<{$serverRoot}>/build/libs/jquery.snippet.js'></script>
  <script src='<{$serverRoot}>/build/libs/gx.sidemenu.js'></script>
  <script src='<{$serverRoot}>/build/libs/modernizr.js'></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $("div.alert-box.success").fadeOut(2000);
    });
  </script>
<{/block}>

 <{block name=messages}>
 <{if isset($smarty.get.operation) && ($smarty.get.operation == 'success')}>
 <div data-alert class="alert-box success radius">
  Votre oprération a été bien effectuée.
  <a href="#" class="close">&times;</a>
</div>
<{/if}>

<{if isset($smarty.get.operation) && ($smarty.get.operation == 'failure')}>
<div data-alert class="alert-box alert round">
  Votre oprération a eu un problème.
  <a href="#" class="close">&times;</a>
</div>
<{/if}>
 <{/block}>