<!DOCTYPE html>
<html>
<head>
  <meta charset='UTF-8' />
  <meta name="description" content="Page d'administration de 360SmartMachine." />
  <meta name='viewport' content='width=device-width, initial-scale=1.0' />
  <meta property='og:title' content='360SmartMachine' />
  <title><{block name=title}>360SmartMachine<{/block}></title>
  <style type="text/css">
    /*
    * Jorge Luis
    *Changer Après*/
    nav.NavHeader .container h2{
      font-size: 62.5% !important;
    }
  </style>
  <script type="text/javascript">
    window.serverRoot =  '<{$serverRoot}>' ;
  </script>
  <{block name=linkZone}>
    <link rel='pingback' href='http://www.blocparc.fr/xmlrpc.php' />
    <link rel='stylesheet' href='<{$serverRoot}>/build/css/font-awesome.css'/>
    <link rel='stylesheet' href='<{$serverRoot}>/build/css/bootstrap.min.css' />
    <link rel='stylesheet' href='<{$serverRoot}>/build/css/foundation.css?0.0.0.0.1' />  
    <link rel='stylesheet' href='<{$serverRoot}>/build/css/style.css'/> 
    <link rel='shortcut icon' href='<{$serverRoot}>/img/favicon.png'>
    <link rel='stylesheet' href='<{$serverRoot}>/build/css/360sc_map.css'/>
    <link rel='stylesheet' href='<{$serverRoot}>/build/css/entypo.css'/>      
    <link rel='stylesheet' href='<{$serverRoot}>/build/css/gx-side-menu-light-override.css'/> 
    <link rel='stylesheet' href='<{$serverRoot}>/build/css/gx-sidemenu-light.css'/>
    <link rel='stylesheet' href='<{$serverRoot}>/build/css/jquery.snippet.css'/>
    <link rel='stylesheet' href='<{$serverRoot}>/build/css/changeDataTo360sc_map.css'/>
  <{/block}>
  <{block name=jsZone}>
  <script src='<{$serverRoot}>/build/libs/jquery.js'></script>
  <script src='<{$serverRoot}>/build/libs/iscroll.js'></script>
  <script src='<{$serverRoot}>/build/libs/jquery.snippet.js'></script>
  <script src='<{$serverRoot}>/build/libs/gx.sidemenu.js'></script>
  <script src='<{$serverRoot}>/build/libs/modernizr.js'></script>

  <{/block}>
  
 

  <{block name=head}><{/block}>
</head>
<body>
 


<{block name=body}>
<{include file='nav.tpl' serverRoot=$serverRoot}>
<{block name=blank}>
<!-- <div style="margin:40px">
  </div> -->
<{/block}>
<{block name=messages}>
<{/block}>
<div class="row">
  <div class="column large-12 small-12">
    <h1 class="textCenter"> <{$smarty.session.pageTitle}></h1>
  </div>
</div>

<{/block}>



<{block name=footer}>
<{include file='footer.tpl'}>
<{/block}>
</body>
</html>