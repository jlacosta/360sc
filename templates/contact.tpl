﻿<{extends file="showNotification.tpl"}> 

<{block name=jsZone append}>
  <script>
        $(document).ready(function(){ 
         
        });
        </script> 
<{/block}>

<{block name=body append}>
<div class="row">
  <div class="columns large-12 small-12">
    <div class="panel">
      <p>
        <h3>Formulaire de contact.</h3><br/>
        <form action="<{$postPage}>" method="post" >
        <span>Les champs marqués d'un * sont obligatoires</span><br/>
        <div class="form-group">
          <label for="MCNumber">Nombre de modules communicants souhaités* :</label>
          <input id="MCNumber" type="text" name="MCNumber" />
        </div>
        <div class="form-group">
          <label for="comment">Commentaires* :</label>
          <textarea id="comment" name="comment" ></textarea>
        </div>
        <input class="button" type="submit" value="Envoyer" name="submit"/>
      </form>
    </p>
  </div>
  </div>      
</div>
<{/block}> 