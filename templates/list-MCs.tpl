<{extends file="noNotification.tpl"}> 

<{block name=jsZone append}>
  <script>
        $(document).ready(function(){
          $("tr.MC-id").on('click', function(){
            var id = $(this).attr("id");  
            var url = "<{$serverRoot}>"+"/MC/" + id;
            $(location).attr('href',url);
          });
        });
        </script> 
<{/block}>

<{block name=body append}>
<div class="row">
  <div class="columns large-12 small-12">
    <div class="panel">
      <p>
        Voici les <{$paginate.total}> MC(s) dont vous disposez.<br/> 
        <div class="paginateNav" >
          <{paginate_prev id="MCs"}> 
          <{paginate_middle id="MCs"}> 
          <{paginate_next id="MCs"}>  
        </div>
        <div class="tableContainer">
          <table style="width:100%">
            <tr>
              <th>ID</th>
              <th>Nom</th>
              <th>Description</th>
              <th>URL de sortie</th>
              <th>Actif</th>  
            </tr>

            <{section name=key loop=$MCs}> 
            <tr id="<{$MCs[key].TagID}>" class="MC-id">
              <td><{$MCs[key].TagID}></td>
              <td><{$MCs[key].TagName}></td>
              <td><{$MCs[key].description}></td>
              <td><{$MCs[key].OriginalURL}></td>
              <td><{$MCs[key].isActive}></td> 
            </tr>

            <{/section}>
          </table>
        </div>
        <div class="paginateNav" >
          <{paginate_prev id="MCs"}> 
          <{paginate_middle id="MCs"}> 
          <{paginate_next id="MCs"}>  
        </div>
      </p>
    </div>
  </div>      
</div>
<{/block}> 