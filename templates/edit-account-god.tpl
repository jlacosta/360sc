<{extends file="showNotification.tpl"}> 
<{block name=jsZone append}>

<script type="text/javascript">

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#imgPreview').attr('src', e.target.result);
                $('#imgPreview').css('display', 'inline-block'); 
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

  $(document).ready(function(){        

    $('input#newAccountImage').change(function(){
        readURL(this);
    }); 

    $("form").submit(function(){
      if($("#confirm").val()!==$("#newPassword").val()){
        alert("Votre nouveau mot de passe et votre confirmation sont différents");
        alert($("#confirm").val()+" "+$("#newPassword").val()) ;
        return false;
      }else{
        return;
      }
    });


    var accounts; 
    $.getJSON("<{$serverRoot}>/php/DAF/getAccountsAjax.php",function(data){
      accounts = data; 
    });

    $("select#account").change(function(){ 
         
      var selectedAccount = $.grep(accounts,function(account){
        return account.CustomersID==$("select#account").val();
      })[0]; 
      $("select#societyName").val(selectedAccount['societe']);
      $("input#mobile").val(selectedAccount['mobile']);
      $("input#fixe").val(selectedAccount['fixe']);
      $("input#email").val(selectedAccount['email']);
      $("input#accountName").val(selectedAccount['Name']);
      $("img#accountImage").attr('src',"<{$serverRoot}>/"+selectedAccount['image']);


    });

  });

</script>
<{/block}>

<{block name=body append}>
<div class="row">
  <div class="columns large-12 small-12">
    <div class="panel">
      <p>
        
        <!-- Form for create an account -->
        <form action="" method="post" enctype="multipart/form-data">
          <fieldset>
            <legend>Identifiants</legend> 
            <div class="form-group">
              <label for="account">Nom de compte :</label>
              <select name="account" id="account">
                <{html_options options=$accounts selected=$accountId}>
              </select> 
              <input name="accountName" type="hidden" id="accountName" value="<{$account.Name}>"/>
            </div> 
            <div class="form-group">
              <label for="newPassword">Nouveau mot de passe :</label>
              <input type="password" name="newPassword" id="newPassword" />
            </div>
            <div class="form-group">
              <label for="confirm">Confirmer le nouveau mot de passe :</label>
              <input type="password" name="confirm" id="confirm" />
            </div>
            

          </fieldset>
          <fieldset>
            <legend>Contact</legend>
            <div class="form-group">
              <label>Nom d'entreprise :</label>
              <select name="society" id="societyName">
                <{html_options options=$societies selected=$account.societe}>
              </select> 
            </div> 
            <div class="form-group">
              <label for="mobile">Téléphone Mobile :</label>
              <input type="text" name="mobile" id="mobile" value="<{$account.mobile}>" />
            </div>
            <div class="form-group">
              <label for="fixed">Téléphone Fixe :</label>
              <input type="text" name="fixed" id="fixed" value="<{$account.fixe}>"/>
            </div>
            <div class="form-group">
              <label for="email">Email :</label>
              <input type="text" name="email" id="email" value="<{$account.email}>"/>
            </div>
            <div class="form-group">
              <label >Image :</label>  
               <img id="accountImage" src="<{$serverRoot}>/<{$account.image}>" onerror="this.error=null;this.width=160;this.src='<{$serverRoot}>/public/img_profil/profil_erreur.png'; "/> 
              <span id="spanPreview" style="white-space:pre-inline;display:none">   Remplace par  </span>
              <img id="imgPreview" src="#" alt="new image"/ style="display:none"/>
              <input type="file" name="newAccountImage" id="newAccountImage" />
            </div>
          </fieldset> 
          <p>
            <input class="medium button" type="submit" value="Modifier" />
          </p>


      </form>
    </p> 
    </div>
  </div>      
</div>
<{/block}> 