﻿<{extends file="noNotification.tpl"}> 

<{block name=jsZone append}>
  <script>
        $(document).ready(function(){ 
          $("select#owner").change(function(){
            $("input#societyName").val($("select#owner option:selected").text());
          });
        });
        </script> 
<{/block}>

<{block name=body append}>
<div class="row">
  <div class="columns large-12 small-12">
    <div class="panel">
      <p>
        <span>Récupérer les données de vos Modules Communicants au format '.xls'<br/></span>
            <form action="<{$postPage}>" method="post" enctype="multipart/form-data">
             
              <div class="form-group">
                <label for="owner">client à visualiser</label>
                <select id="owner" class="medium" name="owner" >
                  <{html_options options=$societies}> 
                </select>
              </div>  
              <input type="hidden" id="societyName" name="societyName" value="<{$societyName}>" />
              <input class="medium button" type="submit" value="Confirmer" />
            </form>
      </p>
    </div>
  </div>      
</div>
<{/block}> 