<{extends file="showNotification.tpl"}> 
<{block name=jsZone append}>
<script>
  var gMap;
  var gMarker;

  var Wrap = function (){       
    this.installed = false; 
    var activeMap = function() {            
      if ($('#firstModal #map').length == 0)
        $('#firstModal').prepend('<div style="width: 100%; height:450px;" id="map"></div>');

      this.wrapMap = function() { 
        $.__latlng = false;
        var lat = 43.503474;
        var lng =  6.485704;
        var google = new L.Google('ROAD');
        if (installed == false){
          gMap = this.map = L.map('map').setView([
            lat, 
            lng
            ], 
            11);
          installed = true;
          this.map.addLayer(google);
          gMarker = this.marker = L.marker([lat, lng], {draggable: true});
          $.__latlng = this.marker.getLatLng();
          this.marker.bindPopup(lat+', '+lng);
          this.marker.on('dragend', function(event){
            $.__latlng = this.getLatLng();
            this.setPopupContent(this.getLatLng().lat + ', ' + this.getLatLng().lng);
          }); 
          this.marker.addTo(this.map);
        }
      }
      wrapMap = wrapMap.bind(this);           
      setTimeout(wrapMap, 500);
    }
    return activeMap;
  }
  
  var activeMap = Wrap();

  var onValider = function (){
    if ($.__latlng){
      $("#position").val($.__latlng.lat+', '+$.__latlng.lng);
    }
    $('#firstModal').foundation('reveal', 'close');
  }

  var changerParCoord = function(){
    console.log(gMarker.getLatLng());
    var lng = parseFloat($("#modallongitud").val());
    var lat = parseFloat($("#modallatitud").val());
    gMarker.setLatLng([lat, lng]);

    $.__latlng = this.marker.getLatLng();
    
    gMap.panTo(gMarker.getLatLng());
    gMap.setZoom( gMap.getMaxZoom() );
  }

  var changerParAdress = function(){
    console.log(gMarker.getLatLng());
    var adresse = $("#modaladresse").val();
    var pays = $("#modalpays").val();
    var finalAdresse = adresse + ' ' + pays;

    var geocoder = new self.google.maps.Geocoder();
    geocoder.geocode( { 'address': finalAdresse}, function (results, status){
      if (status == self.google.maps.GeocoderStatus.OK) {
              console.log(results[0].geometry.location.lat());
              console.log(results[0].geometry.location.lng());

              var lat = parseFloat(results[0].geometry.location.lat());
              var lng = parseFloat(results[0].geometry.location.lng());

              gMarker.setLatLng([lat, lng]);

              $.__latlng = this.marker.getLatLng();

        gMap.panTo(gMarker.getLatLng());
        gMap.setZoom( gMap.getMaxZoom() );
              
          }
    }); 
  }

  </script> 
  <script>
  $(document).ready(function(){
    function readURL(input) {

      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
          $('#imgPreview').attr('src', e.target.result);
          $('#imgPreview').css('display', 'inline-block');
          $('#spanPreview').css('display', 'inline-block');
        }

        reader.readAsDataURL(input.files[0]);
      }
    }
    $('#imgObject').change(function(){ 
      readURL(this);
    });
    
    function initMCs(){
      $("#MCchosen").html("<span>MCs attachés :</span><br/>");
      $("#MCfree").html("<span>MCs libres :</span><br/>");
      $("input[name='newMCs[]']").remove();
      $("input[name='oldMCs[]']").remove();
      var societe = $("#owner").val();
      $.getJSON("<{$serverRoot}>/php/DAF/getTagsAjax.php",{object:<{$object.objetID}>,societe:societe},function(data){
        data.objectTags.forEach(function(tag){
          $("#MCs").prepend("<input type='hidden' value='"+tag.TagID+"' name='oldMCs[]'/>");
          $("#MCs").prepend("<input type='hidden' value='"+tag.TagID+"' name='newMCs[]'/>");
          $("#MCchosen").append("<span class='tagChosen'>"+tag.TagID+" : "+tag.TagName+"</span>");
        });
        data.societeFreeTags.forEach(function(tag){
          $("#MCfree").append("<span class='tagFree'>"+tag.TagID+" : "+tag.TagName+"</span>");
        }); 
      });

    }

    initMCs();
                    
    $("#MCchosen").on('click', 'span', function(){ 
      var tagId = $(this).html().split(" : ")[0];
      $(this).removeClass("tagChosen");
      $(this).addClass("tagFree"); 
      $("#MCfree").append($(this).clone());
      $(this).remove();
      $("input[type='hidden'][name='newMCs[]'][value='"+tagId+"']").remove();
    });
    $("#MCfree").on('click', 'span', function(){ 
      var tagId = $(this).html().split(" : ")[0];
      $(this).removeClass("tagFree");
      $(this).addClass("tagChosen"); 
      $("#MCchosen").append($(this).clone());
      $(this).remove();
      $("#MCs").prepend("<input type='hidden' value='"+tagId+"' name='newMCs[]'/>");
    });

    $("select#owner").change(function(){
      initMCs();
    });


  });
  
  </script>


  <script src='http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.js'></script>
  <script src='https://maps.googleapis.com/maps/api/js?sensor=false&KEY=AIzaSyAeaVmn26e5tMD1kYr68MiLIii9jG91rdQ'></script>
  <script src='<{$serverRoot}>/js/Google.js'></script> 

<{/block}>
<{block name=linkZone append}>
<link rel='stylesheet' href='http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.css' />
<{/block}>

<{block name=body append}>
<div class="row">


    <div id="firstModal" style="padding:0px !important;" class="reveal-modal" data-reveal aria-labelledby="firstModalTitle" aria-hidden="true" role="dialog">
    <div class="row">
      <div style="position: absolute; top:7rem; left:15px; width: 18rem; height:150px;">
        <form>
          <div class="row">
            <div class="small-12 large-12 columns">
              <div class="row">
                <div class="small-5 large-5 columns" style="padding: 0rem 0rem !important;">
                  <input id="modallatitud" type="text" value="0" placeholder="Latidude">
                </div>
                <div class="small-5 large-5 columns" style="padding: 0rem 0rem !important;">
                  <input id="modallongitud" type="text" value="0" placeholder="Longitud">
                </div>
                <div class="small-2 large-2 columns" style="padding: 0rem 0rem !important;">
                  <span onclick="changerParCoord()" class="button postfix" style="padding:0px 0px 0px 0px !important; background-color: #008CBA; color: white;">Voir</span>
                </div>
              </div>
            </div>
            <div class="small-12 large-12 columns">
              <div class="row">
                <div class="small-12 large-12 columns" style="padding: 0rem 0rem !important;">
                  <input type="text" id="modaladresse" placeholder="Adresse">
                </div>
                <div class="small-10 large-10 columns" style="padding: 0rem 0rem !important;">
                  <input type="text" id="modalpays" placeholder="Etat / Pays">
                </div>
                <div class="small-2 large-2 columns" style="padding: 0rem 0rem !important;">
                  <sapn href="#" onclick="changerParAdress()" class="button postfix" style="padding:0px 0px 0px 0px !important; background-color: #008CBA; color: white;">Voir</span>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      <input onclick="onValider()" style="margin-bottom:0px !important; width:100% !important;" class="button" type="button" value="Valider"/>
    </div>  
  </div>

  <div class="columns large-12 small-12">
    <div class="panel">
      <form action="<{$postPage}>" method="post" enctype="multipart/form-data">
        <div class="form-group">
          <label for="name">Nom de l'objet</label>
          <input name="name" value="<{$object.name}>"  type="text" />
        </div>
        <div class="form-group">
          <label for="parent">Type d'objet : </label>
          <select name="idType" id="idType">
            <{html_options options=$objectTypes selected = $object.idParent}>
          </select>
        </div>
        <div class="form-group">
          <label for="desc">Description de l'objet</label>
          <input id="desc" type="text" name="desc" value="<{$object.description}>"/>
        </div>
        <div class="form-group">
          <label for="alphaId">Alpha ID :</label>
          <{if $godRight }>
            <input id="alphaId" type="text" name="alphaId" value="<{$object.alphaID}>"/>
          <{else}>
            <input id="alphaId" type="text" name="alphaId" value="<{$object.alphaID}>" disabled/>
          <{/if}>
        </div>
        <div class="form-group">
          <label for="owner">Propiétaire</label>
          <select id='owner' name='owner' >
            <{html_options options=$societies selected=$object.societeID}>
          </select>
        </div>
        <{if $godRight}>
          <div class="form-group">
            <a target="_blank" href="<{$serverRoot}>/outils/changerUrlTags/">
              Changer URL des tags 
            </a>
            <div id="MCs" class="row">
              <div id="MCchosen" class=" columns large-6 small-6"><span>MCs attachés : </span><br/></div>
              <div id="MCfree" class=" columns large-6 small-6"><span>MCs libres: </span><br/>
              </div>
            </div>
          </div>
        <{/if}>
        
        <div class="form-group">
          <label for="active">Active :</label>
          <input id="active" type="text" name="active" value="<{$object.active}>"/>
        </div>
        <div class="form-group">
          <label for="dateActive">Date d'activation (format: yyyy-mm-dd) :</label>
          <input id="dateActive" type="text" name="dateActive" value="<{$object.dateActive}>"/>
        </div>
        <div class="form-group">
          <label for="appairage">Appairage :</label>
          <input id="appairage" type="text" name="appairage" value="<{$object.appairage}>"/>
        </div>
        <div class="form-group">
          <label for="dateAppairage">Date d'appairage (format: yyyy-mm-dd) :</label>
          <input id="dateAppairage" type="text" name="dateAppairage" value="<{$object.dateAppairage}>"/>
        </div>
        <div class="form-group">
          <label for="position">Position :</label>
          <input id="position" style="width:87.5% !important; display:inline !important;" type="text" name="latlng" value="<{$object.latlng}>"/>
          <input onclick="activeMap()" data-reveal-id="firstModal"
            style="height:2.3125rem !important; padding: 0px !important; width:95.7969px !important;"  
            type="button" class="button" value="Ajouter" /> 
        </div>
        <div class="form-group">
          <label for="descPosition">Description position :</label>
          <input id="descPosition" type="text" name="descPosition" value="<{$object.descriptionPos}>"/>
        </div>
        <div class="form-group">
          <label for="imgObject">Image de l'objet : </label>
          <{if $object.image!=null}>
            <img src="<{$serverRoot}>/<{$object.image}>" height="50" width="60">
            <span id="spanPreview" style="white-space:pre-inline;display:none">   
              Remplace par  
            </span>
          <{/if}>
          <img id="imgPreview" src="#" alt="new image" style="display:none" height="50" width="60" />
          <input name="imgObject" type="file" id="imgObject"/> 
        </div> 
        <div class="form-group">
          <label for="fpObjet">Fiche produit de l'objet : </label>
          <{if isset($object.ficheProduit) && $object.ficheProduit!=null }>
            <embed src="<{$serverRoot}>/<{$object.ficheProduit}>" >
          <{/if}>
          <input name="fpObjet" type="file" id="fpObjet"/>
        </div>
                    
          <input class="medium button" type="submit" value="Valider" />
          <a href="<{$serverRoot}>" class="button" style="text-decoration:none;float:right">Annuler</a>
      </form>
    </div>
  </div>

</div>
<{/block}>