<{extends file="showNotification.tpl"}> 

<{block name=jsZone append}>
  <script>
        $(document).ready(function(){
          $(".modalClose").click(function(){
            $("#removeMCModal").foundation('reveal','close');
          });
          $("tr.MC-id").on('click', function(){ 
            var modal = $("#removeMCModal");
            var MCId = $(this).attr('id');
            modal.find("form").attr('action',modal.find("form").attr('action')+MCId);
            modal.foundation('reveal','open');
          });
        });
        </script> 
<{/block}>

<{block name=body append}>

<div id="removeMCModal" class="reveal-modal" data-reveal aria-labelledby="removeMC" aria-hidden="true" role="dialog">
  <form action="<{$postPage}>" method="post">
    <div class="row">
      <div class="column large-12 small-12">
        Etes-vous sur de vouloir supprimer ce tag ?
      </div>
      <div class="columns large-3 small-3">
        <input type="submit" value="Oui"/>
      </div>
      <div class="column large-2 small-2">
        <input type="button" value="Non" class="modalClose"/>
      </div>
    </div>
  </form> 
  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>


<div class="row">
  <div class="columns large-12 small-12">
    <div class="panel">
      <p>
        Voici les <{$paginate.total}> MC(s) dont vous disposez.<br/> 
        <div class="paginateNav" >
          <{paginate_prev id="dMCs"}> 
          <{paginate_middle id="dMCs"}> 
          <{paginate_next id="dMCs"}>  
        </div>
        <div class="tableContainer">
          <table style="width:100%">
            <tr>
              <th>ID</th>
              <th>Nom</th>
              <th>Description</th>
              <th>URL de sortie</th>
              <th>Actif</th>  
            </tr>

            <{section name=key loop=$MCs}> 
            <tr id="<{$MCs[key].TagID}>" class="MC-id">
              <td><{$MCs[key].TagID}></td>
              <td><{$MCs[key].TagName}></td>
              <td><{$MCs[key].description}></td>
              <td><{$MCs[key].OriginalURL}></td>
              <td><{$MCs[key].isActive}></td> 
            </tr>

            <{/section}>
          </table>
        </div>
        <div class="paginateNav" >
          <{paginate_prev id="dMCs"}> 
          <{paginate_middle id="dMCs"}> 
          <{paginate_next id="dMCs"}>  
        </div>
      </p>
    </div>
  </div>      
</div>
<{/block}> 