<{extends file="showNotification.tpl"}> 
<{block name=jsZone append}>
<script>
  var gMap;
  var gMarker;

  var Wrap = function (){       
    this.installed = false; 
    var activeMap = function() {            
      if ($('#firstModal #map').length == 0)
        $('#firstModal').prepend('<div style="width: 100%; height:450px;" id="map"></div>');

      this.wrapMap = function() { 
        $.__latlng = false;
        var lat = 43.503474;
        var lng =  6.485704;
        var google = new L.Google('ROAD');
        if (installed == false){
          gMap = this.map = L.map('map').setView([
            lat, 
            lng
            ], 
            11);
          installed = true;
          this.map.addLayer(google);
          gMarker = this.marker = L.marker([lat, lng], {draggable: true});
          $.__latlng = this.marker.getLatLng();
          this.marker.bindPopup(lat+', '+lng);
          this.marker.on('dragend', function(event){
            $.__latlng = this.getLatLng();
            this.setPopupContent(this.getLatLng().lat + ', ' + this.getLatLng().lng);
          }); 
          this.marker.addTo(this.map);
        }
      }
      wrapMap = wrapMap.bind(this);           
      setTimeout(wrapMap, 500);
    }
    return activeMap;
  }
  
  var activeMap = Wrap();

  var onValider = function (){
    if ($.__latlng){
      $("#position").val($.__latlng.lat+', '+$.__latlng.lng);
    }
    $('#firstModal').foundation('reveal', 'close');
  }

  var changerParCoord = function(){
    console.log(gMarker.getLatLng());
    var lng = parseFloat($("#modallongitud").val());
    var lat = parseFloat($("#modallatitud").val());
    gMarker.setLatLng([lat, lng]);

    $.__latlng = this.marker.getLatLng();
    
    gMap.panTo(gMarker.getLatLng());
    gMap.setZoom( gMap.getMaxZoom() );
  }

  var changerParAdress = function(){
    console.log(gMarker.getLatLng());
    var adresse = $("#modaladresse").val();
    var pays = $("#modalpays").val();
    var finalAdresse = adresse + ' ' + pays;

    var geocoder = new self.google.maps.Geocoder();
    geocoder.geocode( { 'address': finalAdresse}, function (results, status){
      if (status == self.google.maps.GeocoderStatus.OK) {
              console.log(results[0].geometry.location.lat());
              console.log(results[0].geometry.location.lng());

              var lat = parseFloat(results[0].geometry.location.lat());
              var lng = parseFloat(results[0].geometry.location.lng());

              gMarker.setLatLng([lat, lng]);

              $.__latlng = this.marker.getLatLng();

        gMap.panTo(gMarker.getLatLng());
        gMap.setZoom( gMap.getMaxZoom() );
              
          }
    }); 
  }

  </script> 
  <script>
  $(document).ready(function(){
    function readURL(input) {

      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
          $('#imgPreview').attr('src', e.target.result);
          $('#imgPreview').css('display', 'inline-block');
          //$('#spanPreview').css('display', 'inline-block');
        }

        reader.readAsDataURL(input.files[0]);
      }
    }

    $('#imgObject').change(function(){ 
      readURL(this);
    });



  });
  </script>


  <script src='http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.js'></script>
  <script src='https://maps.googleapis.com/maps/api/js?sensor=false&KEY=AIzaSyAeaVmn26e5tMD1kYr68MiLIii9jG91rdQ'></script>
  <script src='<{$serverRoot}>/build/libs/Google.js'></script> 

<{/block}>
<{block name=linkZone append}>
<link rel='stylesheet' href='http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.css' />
<{/block}>

<{block name=body append}>
<div class="row">

    <div id="firstModal" style="padding:0px !important;" class="reveal-modal" data-reveal aria-labelledby="firstModalTitle" aria-hidden="true" role="dialog">
    <div class="row">
      <div style="position: absolute; top:7rem; left:15px; width: 18rem; height:150px;">
        <form>
          <div class="row">
            <div class="small-12 large-12 columns">
              <div class="row">
                <div class="small-5 large-5 columns" style="padding: 0rem 0rem !important;">
                  <input id="modallatitud" type="text" value="0" placeholder="Latidude">
                </div>
                <div class="small-5 large-5 columns" style="padding: 0rem 0rem !important;">
                  <input id="modallongitud" type="text" value="0" placeholder="Longitud">
                </div>
                <div class="small-2 large-2 columns" style="padding: 0rem 0rem !important;">
                  <a href="" onclick="changerParCoord()" class="button postfix" style="padding:0px 0px 0px 0px !important;">Voir</a>
                </div>
              </div>
            </div>
            <div class="small-12 large-12 columns">
              <div class="row">
                <div class="small-12 large-12 columns" style="padding: 0rem 0rem !important;">
                  <input type="text" id="modaladresse" placeholder="Adresse">
                </div>
                <div class="small-10 large-10 columns" style="padding: 0rem 0rem !important;">
                  <input type="text" id="modalpays" placeholder="Etat / Pays">
                </div>
                <div class="small-2 large-2 columns" style="padding: 0rem 0rem !important;">
                  <a href="#" onclick="changerParAdress()" class="button postfix" style="padding:0px 0px 0px 0px !important;">Voir</a>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      <input onclick="onValider()" style="margin-bottom:0px !important; width:100% !important;" class="button" type="button" value="Valider"/>
    </div>  
  </div>


  <div class="columns large-12 small-12">
    <div class="panel">
      <p>
        Bienvenue dans l'interface d'administration de IZ.<br/>
        <!-- Form for encrypt an URLs in many tags -->
        <form action="<{$postPage}>" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <label for="name">*Nom de l'objet</label>
            <input id="name" type="text" name="name" />
          </div>
          <div class="form-group">
            <label for="parent">*Type d'objet :</label>
            <select name="idType" id="idType">
              <{html_options options=$objectTypes}>
            </select>
          </div>
          <div class="form-group">
            <label for="date-streetSign">Date de maintenance Paneau (moins)</label>
            <input id="date-streetSign" type="number" value="0" name="date-streetSign" min="0">
          </div>
          <div class="form-group">
            <label for="alphaId">*Alpha ID :</label>
            <input id="alphaId" type="text" name="alphaId" />
          </div>
          <div class="form-group">
            <label for="lat">Position</label>
            <input id="position" style="width:87.5% !important; display:inline !important;" name="latlng" type="text" />
            <input onclick="activeMap()" data-reveal-id="firstModal"
            style="height:2.3125rem !important; padding: 0px !important; width:95.7969px !important;"  
            type="button" class="button" value="Ajouter" /> 
          </div>
          <div class="form-group">
            <label for="owner">*Propiétaire</label>
            <select id='owner' name='owner' >
              <{html_options options=$societies selected=$accountSociety}>
            </select>
          </div>
          <div class="form-group">
            <label for="imgObject">Image de l'objet :</label>
            <img id="imgPreview" src="#" alt="new image"/ style="display:none">
            <input type="file" name="imgObject" id="imgObject">
          </div>
          <div class="form-group">
            <label for="desc">Description de l'objet</label>
            <input id="desc" type="text" name="desc" />
          </div>
          <div class="form-group checkboxes_3">
            <{html_checkboxes name='appairageMCs' options=$freeMCs large=3}>
          </div>

          <input class="medium button" type="submit" value="Envoyer" />
        </form>
      </p>
    </div>
  </div>      
</div>
<{/block}> 