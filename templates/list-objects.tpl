<{extends file="noNotification.tpl"}> 

<{block name=jsZone append}>
  <script>
        $(document).ready(function(){
          $("tr.objet-id").on('click', function(){
            var id = $(this).attr("id");  
            var url = "<{$serverRoot}>"+"/object/" + id;
            $(location).attr('href',url);
          });
        });
        </script> 
<{/block}>

<{block name=body append}>
<div class="row">
  <div class="columns large-12 small-12">
    <div class="panel">
      <p>
        Voici les <{$paginate.total}> objet(s) dont vous disposez.<br/> 
        <div class="paginateNav" >
          <{paginate_prev id="objects"}> 
          <{paginate_middle id="objects"}> 
          <{paginate_next id="objects"}>
        </div>
        <div class="tableContainer">
          <table>
            <tr>
              <th>Nom</th>
              <th>Description</th>
              <th>Description Position</th>
              <th>Apparé</th>
              <th>Date Apparage</th>
              <th>Activé</th>
              <th>Date d'Activation</th>
              <th>Latitude</th>
              <th>Longitude</th>
            </tr>
            
            <{section name=key loop=$objects}> 
            <tr id="<{$objects[key].objetID}>" class="objet-id">
              <td><{$objects[key].name}></td>
              <td><{$objects[key].description}></td>
              <td><{$objects[key].descriptionPos}></td>
              <td><{$objects[key].appairage}></td>
              <td><{$objects[key].dateAppairage}></td>
              <td><{$objects[key].active}></td>
              <td><{$objects[key].dateActive}></td>
              <td><{$objects[key].objLat}></td>
              <td><{$objects[key].objLong}></td>
            </tr>

            <{/section}>
          </table>
        </div>
        <div class="paginateNav" >
          <{paginate_prev id="objects"}> 
          <{paginate_middle id="objects"}> 
          <{paginate_next id="objects"}>
        </div>
      </p>
    </div>
  </div>      
</div>
<{/block}> 