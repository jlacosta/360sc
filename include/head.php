<?php 
if(($_SESSION['page'] != "SansConnect") && ($_SESSION['connect'] === false)){
	$_SESSION['page'] = "connexion";
	$_SESSION['redirect'] = true;
	header('Location: '.get_link().'_connexion/');
	exit();
}
		
echo "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
<html>
	<head>
		<meta charset='UTF-8' />
		<title>360SmartMachine - ".$_SESSION['page']."</title>
		<meta name='description' content='Page d'administration de 360SmartMachine.' />
		<meta name='viewport' content='width=device-width, initial-scale=1.0' />

		<meta property='og:title' content=360SmartMachine' />

		<link rel='pingback' href='http://www.blocparc.fr/xmlrpc.php' />
		<!--[if lt IE 9]>
			<link rel='stylesheet' type='text/css' href='http://www.blocparc.fr/wp-content/themes/flatbox/ie.css' />
		<![endif]-->	
		<!--[if IE]> <link href='css/ie.css' type='text/css' rel='stylesheet'> <![endif]-->
		<!-- begin JS -->
		<!-- end JS -->
		<meta name='viewport' content='width=device-width, initial-scale=1.0' />
		<link rel='stylesheet' href='".get_link()."css/bootstrap.min.css' />
		<link rel='stylesheet' href='".get_link()."css/foundation.css?0.0.0.0.1' />
		<link rel='stylesheet' href='".get_link()."css/responsive-tables.css' />
		<link rel='shortcut icon' href='".get_link()."img/favicon.png'>
		<script src='".get_link()."js/vendor/modernizr.js'></script>
		<script type='text/javascript' src='".get_link()."include/data.json'></script>
		<style type='text/css'>

		/**/
		.has-form {
		  position: absolute;
		  top: 0;
		  left: 6rem;
		  min-width: 14rem; }
		  @media only screen and (max-width: 40em) {
		    .has-form {
		      min-width: 10rem;
		      top: 0.5rem; }
		      .has-form .button {
		        height: 1.85rem; } }
		/**/

			/* Set a size for our map container, the MabBox will take up 100% of this container */
			#map {
				width: 100%;
				height: 92.5% !important;
				position: absolute;
			}
			.contentPopup {
				text-align: center;						
			}
			.actions {
				margin:0 0 15px;
			}
			.actionPopup-L {
				float: left;
			}
			.actionPopup-R {
				float: right; 
			}
			.name{
				border-bottom: 10px !important;
				border-bottom-color: black;
			}
			.hidePopup{
				display: none;
			}
		</style>

 		

		<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js'></script>
		<script src='https://maps.googleapis.com/maps/api/js?sensor=false&KEY=AIzaSyAeaVmn26e5tMD1kYr68MiLIii9jG91rdQ'></script>
		
		<script type='text/javascript' src='".get_link()."js/tooltip.js'></script>

		<link rel='stylesheet' href='http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css' />
		<script src='//cdn.leafletjs.com/leaflet-0.7.3/leaflet.js'></script> 

		<!-- <script src='https://api.mapbox.com/mapbox.js/v2.2.2/mapbox.js'></script>
		<link href='https://api.mapbox.com/mapbox.js/v2.2.2/mapbox.css' rel='stylesheet' /> -->
		
    	<script src='".get_link()."js/Google.js'></script> 

    	<script type='text/javascript' src='".get_link()."js/foundation/responsive-tables.js'></script>

		<script type='text/javascript' src='".get_link()."js/PruneCluster.js'></script>
		<script type='text/javascript' src='".get_link()."js/fuzzyset.js'></script>
		<script type='text/javascript' src='".get_link()."js/popup.js'></script>
		<script type='text/javascript' src='".get_link()."js/underscore.js'></script>
		<script type='text/javascript' src='".get_link()."js/tab.js'></script>
		<script type='text/javascript' src='".get_link()."js/HTMLtemplates.js'></script>
		<script type='text/javascript' src='".get_link()."js/requestAjax.js'></script>
		<script type='text/javascript' src='".get_link()."js/filters.js'></script>
		<script type='text/javascript' src='".get_link()."js/alertes.js'></script>
		<script type='text/javascript' src='".get_link()."js/utilities.js'></script>
		<script type='text/javascript' src='".get_link()."js/carte.js'></script>

	</head>
	<body class=";
	echo isset($_SESSION['page'])?$_SESSION['page']:'connexion';
	echo ">";
	include "./include/nav.php";	
?>