<?php 
if(!($_SERVER['REQUEST_URI'] == "".get_link()."_connexion/" OR $_SERVER['REQUEST_URI'] == "".get_link()."_connexion")){
	if($_SESSION['connect'] === $_SESSION['redirect']){
		$_SESSION['redirect'] = true;
		header('Location: '.get_link().'_connexion/');
		exit();
	}
} else if (isset($_SESSION['client_id']) AND strlen($_SESSION['client_id']) != 0 AND $_SESSION['client_id'] != null) {
	header('Location: '.get_link().'');
}


echo "<!DOCTYPE html>
<html>
	<head>
		<meta charset='UTF-8' />
		<title>360SmartMachine - ".$_SESSION['page']."</title>
		<meta name='description' content='Page d'administration de 360SmartMachine.' />
		<meta name='viewport' content='width=device-width, initial-scale=1.0' />

		<meta property='og:title' content='360SmartMachine' />

		<link rel='pingback' href='http://www.blocparc.fr/xmlrpc.php' />
		<!--[if lt IE 9]>
			<link rel='stylesheet' type='text/css' href='http://www.blocparc.fr/wp-content/themes/flatbox/ie.css' />
		<![endif]-->	
		<!--[if IE]> <link href='".get_link()."css/ie.css' type='text/css' rel='stylesheet'> <![endif]-->
		<!-- begin JS -->
		<!-- end JS -->
		<meta name='viewport' content='width=device-width, initial-scale=1.0' />
		<link rel='stylesheet' href='".get_link()."css/bootstrap.min.css' />
		<link rel='stylesheet' href='".get_link()."css/foundation.css?0.0.0.0.1' />		
		<link rel='shortcut icon' href='".get_link()."img/favicon.png'>
		<script src='".get_link()."js/vendor/modernizr.js'></script>
		<script type='text/javascript'>
			var get_link = function(){
				return '".get_link()."';
			}
		</script>

		<script src='https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js'
		></script>
		
		<link rel='stylesheet' href='http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.css' />
		<script src='http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.js'></script>
		<script src='https://maps.googleapis.com/maps/api/js?sensor=false&KEY=AIzaSyAeaVmn26e5tMD1kYr68MiLIii9jG91rdQ'></script>
		<script src='".get_link()."js/Google.js'></script> 

	<body class=";
	echo isset($_SESSION['page'])?$_SESSION['page']:'connexion';
	echo">";
		include "./include/nav.php";

			echo"<section class='row main-section'>";
				if(isset($_SESSION['success'])){
					if($_SESSION['success']){
			?>
				<script>
				jQuery(document).ready(function(){
					goodalert();
				});</script>
			<?php
					} else {
			?>
				<script>jQuery(document).ready(function(){
					badalert();
				});</script>
			<?php
					}
					$_SESSION['success'] = false;
				}

		echo "<h1> Interface d'administration</h1>";
		?>