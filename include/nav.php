<?php 
echo"<div class='off-canvas-wrap' data-offcanvas>
		  	<div class='inner-wrap'>
				<nav class='tab-bar'>";
					if($_SESSION['page'] == "accueil") {
						echo "<section class='right-small'>
							<a class='right-off-canvas-toggle menu-icon' href='#'><span></span></a>
						</section>";
					}
				    echo "
				    <section class='middle tab-bar-section'>
				    	<h1> 
				    		360SmartMachine ALPHA
				    		<a id='showHideForm' href='#' >
				    			<i class='fi-magnifying-glass' style='margin-left: 15px; font-size: 15px;'></i>
				    		</a>	
				    	</h1>	
				    </section>";
				    if($_SESSION['page'] != "SansConnect") {
					    echo "<section class='left-small'>
						  <a class='left-off-canvas-toggle menu-icon' href='#'><span></span></a>
							";	
					echo "</section>
							</nav>";		
					}	
		if 	($_SESSION['page'] != "SansConnect"){
			echo "
			<aside class='left-off-canvas-menu'>
			 <ul class='off-canvas-list'>
				<li>
					<a class='left-off-canvas-toggle'><span aria-hidden='true' class='glyphicon glyphicon-chevron-left'></span> Menu</a>
				</li>
				<li><img  src='".get_link().$_SESSION['client_image']."'  style='width:100%' onerror=\"this.error=null ;this.src= '".get_link()."public/img_profil/profil_erreur.png'; \" /></li>
				<li>
           <a style='text-decoration:none'>
          <span aria-hidden='true' class='glyphicon glyphicon-chevron-left' style='color: transparent !important;'></span>  					 
					<span style='font-size:18px; font-style:italic'>Utilisateur</span>  <br/> 
					<span aria-hidden='true' class='glyphicon glyphicon-chevron-left' style='color: transparent !important;'></span> 
					<span id='nav-client-name' style='width: 100%;font-size:25px;color:#C4D600;'>".$_SESSION['client_name']."</span></a>
				</li> 
				<li><a href='".get_link()."'><span aria-hidden='true' class='glyphicon glyphicon-home'></span>Accueil</a></li>
				";
				echo"<li class='has-submenu'><a href='#'><span aria-hidden='true' class='glyphicon glyphicon-tags'></span> MC</a>
					<ul class='left-submenu'>
						<li class='back'>
							<a href='#'>Précédent</a>
						</li>
						<li>
							<label>MC</label>
						</li>";
						echo (isset($_SESSION['client_id']) AND $_SESSION['addTag'])?"<li><a href='".get_link()."adds/'><span aria-hidden='true' class='glyphicon glyphicon-plus'></span>Ajouter des MC</a></li>":"";
						echo (isset($_SESSION['client_id']) AND $_SESSION['searchTags'])?"<li><a href='".get_link()."search/'><span aria-hidden='true' class='glyphicon glyphicon-search'></span>Chercher un MC</a></li>":"";
						echo (isset($_SESSION['client_id']) AND $_SESSION['export'])?"<li><a href='".get_link()."export/'><span aria-hidden='true' class='glyphicon glyphicon-open'></span> Exporter les MC</a></li>":"";
						echo "<li><a href='".get_link()."tags/'><span aria-hidden='true' class='glyphicon glyphicon-tags'></span> Liste des MC</a></li>";
						echo (isset($_SESSION['client_id']) AND $_SESSION['editTags'])?"<li><a href='".get_link()."edit/'><span aria-hidden='true' class='glyphicon glyphicon-pencil'></span> Modifier un MC</a></li>":"";
						echo (isset($_SESSION['client_id']) AND $_SESSION['level']==4)?"<li><a href='".get_link()."give/'><span aria-hidden='true' class='glyphicon glyphicon-send'></span>Donner des MCs</a></li>":"";
						echo (isset($_SESSION['client_id']) AND $_SESSION['level']==4)?"<li><a href='".get_link()."delete/'><span aria-hidden='true' class='glyphicon glyphicon-remove'></span>Supprimer un MC</a></li>":"";
						echo (isset($_SESSION['client_id']) AND $_SESSION['SURLE'])?"<li><a href='".get_link()."urle/'><span aria-hidden='true' class='glyphicon glyphicon-link'></span>Liste des URLe</a></li>":"";
					echo "</ul>
				</li>
				<li class='has-submenu'><a href='#'><span aria-hidden='true' class='glyphicon glyphicon-list'></span> Objets</a>
					<ul class='left-submenu'>
						<li class='back'>
							<a href='#'>Précédent</a>
						</li>
						<li>
							<label>Objets</label>
						</li>";
						echo (isset($_SESSION['client_id']) AND $_SESSION['addObject'])?"<li><a href='".get_link()."objet/'><span aria-hidden='true' class='glyphicon glyphicon-scissors'></span> Créer un objet</a></li>":"";
						echo (isset($_SESSION['client_id']) AND $_SESSION['editObject'])?"<li><a href='".get_link()."edit-objet/'><span aria-hidden='true' class='glyphicon glyphicon-edit'></span> Modifier un objet</a></li>":"";
						echo (isset($_SESSION['client_id']) AND $_SESSION['type'])?"<li><a href='".get_link()."type/'><span aria-hidden='true' class='glyphicon glyphicon-plus'></span> Ajouter un type d'objet</a></li>":"";
						echo "<li><a href='".get_link()."objets/'><span aria-hidden='true' class='glyphicon glyphicon-list'></span> Liste des objets</a></li>";
					echo "</ul>
				</li>
				<li class='has-submenu'><a href='#'><span aria-hidden='true' class='glyphicon glyphicon-cog'></span> Compte Utilisateur</a>
					<ul class='left-submenu'>
						<li class='back'>
							<a href='#'>Précédent</a>
						</li>
						<li>
							<label>Compte Utilisateur</label>
						</li>";
						echo "<li><a href='".get_link()."gestion/'><span aria-hidden='true' class='glyphicon glyphicon-cog'></span> Gestion de compte</a></li>";
						echo (isset($_SESSION['client_id']) AND $_SESSION['createUser'])?"<li><a href='".get_link()."register/'><span aria-hidden='true' class='glyphicon glyphicon-user'></span> Ajouter un utilisateur</a></li>":"";
					echo "</ul>
				</li>";
				echo (isset($_SESSION['client_id']) AND $_SESSION['editObject'])?"<li><a href='".get_link()."societe/'><span aria-hidden='true' class='glyphicon glyphicon-briefcase'></span> Cr&eacute;er une entreprise</a></li>":"";
				echo "<li><a href='".get_link()."contact/'><span aria-hidden='true' class='glyphicon glyphicon-envelope'></span>Contact</a></li>";
				echo "<li><a href='".get_link()."outils/changerUrlTags/'>
							<span aria-hidden='true' class='glyphicon glyphicon-tag'></span>
							Changer l'URL d'un MC
					      </a>
					  </li>";
				echo (isset($_SESSION['client_id']) AND strlen($_SESSION['client_id']) != 0)?"<li><a href='".get_link()."deconnexion/'><span aria-hidden='true' class='glyphicon glyphicon-off'></span>Deconnexion</a></li>":"<li><a href='".get_link()."connexion/'><span aria-hidden='true' class='glyphicon glyphicon-user'></span>Connexion</a></li>";
				
				echo "
			  </ul>
			</aside>";
		}	
			if($_SESSION['page'] == "accueil") {
				echo "
<aside class='right-off-canvas-menu'>
<ul class='off-canvas-list'>
	<li>
		<a class='right-off-canvas-toggle'><span aria-hidden='true' class='glyphicon glyphicon-chevron-right'></span> Option de l'objet</a>
	</li>

	

	<li>
			<label>Nom
				<li>
					<a href='#' class='off-canvas-submenu-call'>Banc classique<span class='left'> [+] </span></a>
				</li>
				<ul class='off-canvas-submenu'>
					<li>
						<form method='post' action=''>
							Modifier : <input type='text' name='pseudo'/><input type='submit'  class='button' name='pseudo' />
						</form>
					</li>
				</ul>
			</label>
		</li>

		
		<li>
		<a href='#' class='off-canvas-submenu-call'> Info <span class='left'> + </span></a>
		</li>
		<ul class='off-canvas-submenu'>
		<li>
			<label>ID</label>
			<p>340924237</p>
		</li>

		<li>
			<label>Cordonnée</label>
			<p>42.30943098; 90.340923</p>
		</li>
	</ul>

	<li>
		<a href='#' class='off-canvas-submenu-call'>Option 2 <span class='left'> + </span></a>
	</li>

	<ul class='off-canvas-submenu'>
		<li><a href='#'>Sub menu 1</a></li>
		<li><a href='#'>Sub menu 2</a></li>
		<li><a href='#'>Sub menu 3</a></li>
	</ul>

	<li>
		<a href='#'>Option 3</a>
	</li>
	<li>
		<a href='#' class='off-canvas-submenu-call'>Option 4 <span class='left'> + </span></a></li>
		<ul class='off-canvas-submenu'>
			<li><a href='#'>Sub menu 1</a></li>
			<li><a href='#'>Sub menu 2</a></li>
			<li><a href='#'>Sub menu 3</a></li>
		</ul>
		<li><a href='#'>Option 5</a></li>
		<li><a href='#'>Option 6</a></li>

		<li class='has-submenu'>
			<a href='#'>Filters</a>
			<ul id='filters' class='right-submenu'>
				<li class='back'>
					<a href='#'>Back</a>
				</li>
				<li>
					<label>ID</label>
					<input id='filterById' class='' type='text'></input>
				</li>
				<li>
					<label>Name</label>
					<input id='filterByName' class='' type='text'></input>
				</li>
				<li>
					<label>Adresse</label>
					<input id='filterByAdresse' class='' type='text'></input>
				</li>
				<li>
					<input id='rechercher' class='button' name='pseudo' value='Rechercher' type='button'></input>
				</li>	
				<li class='has-submenu'>
					<a href='#'>Link 2 w/ submenu</a>
					<ul class='right-submenu'>
						<li class='back'><a href='#'>Back</a></li>
						<li><label>Level 2</label></li>
						<li><a href='#'>test</a></li>
					</ul>
				</li>
				<li>
					<a href='#'>test</a>
				</li>
			</ul>
		</li>
		<li class='has-submenu'>
			<a href='#'>Alertes</a>
			<ul id='alerteAdded' class='right-submenu'>
				<li class='back'>
					<a href='#'>Back</a>
				</li>		
				<li class='has-submenu'>
					<a href='#'>Historie des alertes</a>
					<ul id='historieAlerte' class='right-submenu'>
					<li class='back'>
						<a href='#'>Back</a>
					</li>					
			</ul>
		</li>	
			</ul>
		</li>
		<li class='has-submenu'>
			<a href='#'>Markers</a>
			<ul id='listMarker' class='right-submenu'>
				<li class='back'>
					<a href='#'>Back</a>
				</li>
					
			</ul>
		</li>	
	</li>	
</ul>
</aside>
		<script>
		
			self.google = google;
		</script>	
";
			}
			?>