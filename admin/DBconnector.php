<?php

require_once('DBsetting.php');

$pdo = new PDO('mysql:host='. $DB_HOST.';port='. $DB_PORT.';dbname='. $DB_NAME,  
          $DB_USER,  
          $DB_PASS,
          array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',PDO::ATTR_PERSISTENT => true) // Paramètres de transaction avec la base de données.
      );  

if( $pdo == null ) {
    echo 'Erreur lors de la connexion à la base de données. Merci de vérifier les informations d\'identification...';
    exit(0);
  }

?>