<?php

  
  /**
  * To check the authentication state
  */
  function checkLogin(){
    if(!($_SERVER['REQUEST_URI'] == "".get_link()."_connexion/" OR $_SERVER['REQUEST_URI'] == "".get_link()."_connexion")){
          if(!(isset($_SESSION['client_id']) && strlen($_SESSION['client_id']) != 0 && $_SESSION['client_id'] != null)){
            header('Location: '.get_link().'_connexion/');
          }
        } 
  }

function checkPassword($id,$pass){
  $pdo =connection_db();
  $sql = "SELECT COUNT(*) FROM customers WHERE CustomersID = :id AND password = :pass";
  $target = $pdo->prepare($sql);
  $target->bindValue(":id",$id,PDO::PARAM_INT);
  $target->bindValue(":pass",md5($pass),PDO::PARAM_STR);
  $target->execute();
  $nb = $target->fetchColumn(); 
  return $nb>0?true:false;
}



/*
  Get company's image
  */
  function get_societe_image($idSociete){
    $bdd = connection_db();
    $query=$bdd->prepare("SELECT  image FROM societe WHERE ID = $idSociete");
    $query->execute();
    $data=$query->fetch();
    
    if($data){
      if($data['image']!=null){
        return $data['image'];
      }else{
        $query=$bdd->prepare("SELECT  societe FROM societe_soussociete WHERE soussociete = $idSociete");
        $query->execute();
        $data=$query->fetch();
        return get_societe_image($data['societe']);
      }
    }else{
      return null;
    }        

  }


/*
  Get the account image or its company's image
  */
  function get_customer_image($idClient){
    $bdd = connection_db();
    $query=$bdd->prepare("SELECT  image,societe FROM customers WHERE CustomersID = $idClient");
    $query->execute();
    $data=$query->fetch();
    
    if($data){
      if($data['image']!=null){
        return $data['image'];
      }else{
        return get_societe_image($data['societe']);
      }
    }else{
      return null;
    }        

  }

    /**
  * function to know if the visitor is conected
  */
  function get_connect(){
    return $_SESSION['connect'];
  }



  /**
  * init function
  */
  function get_client_key() {
    $bdd=connection_db();
    $query=$bdd->prepare('SELECT c.CustomersKey
    FROM 360sc_iz.customers AS c WHERE c.CustomersID = :id');
    $query->bindValue(':id',1, PDO::PARAM_STR);
    $query->execute();
    $data = $query->fetch();
    $query->closeCursor();
    return $data['CustomersKey'];
  };

  function get_client_domain() {
    $bdd=connection_db();
    $query=$bdd->prepare('SELECT c.customersDomain
    FROM 360sc_iz.customers AS c WHERE c.CustomersID = :id');
    $query->bindValue(':id',$_SESSION['societe'], PDO::PARAM_STR);
    $query->execute();
    $data = $query->fetch();
    $query->closeCursor();
    return $data['customersDomain'];
  };

  function get_societe_domain($societe) {
    $bdd=connection_db();
    $query=$bdd->prepare('SELECT c.customersDomain
    FROM 360sc_iz.customers AS c WHERE c.CustomersID = :id');
    $query->bindValue(':id',$societe, PDO::PARAM_STR);
    $query->execute();
    $data = $query->fetch();
    $query->closeCursor();
    return $data['customersDomain'];
  };
  
  /**
  * To know the domain of client
  */
  function get_domain_key($domain) {
    $bdd=connection_db();
    $query=$bdd->prepare('SELECT c.domain
    FROM 360sc_iz.customers AS c WHERE c.customersDomain = :id');
    $query->bindValue(':id',$domain, PDO::PARAM_STR);
    $query->execute();
    $result = $query->fetch();
    $query->closeCursor();
    return $result;
  };
  

  
  function societes($societe){
    $bdd = connection_db();
    $sql = 'SELECT * FROM societe_soussociete AS ssc WHERE ssc.societe = :societe';
    $target = $bdd->prepare($sql);
    $target->bindValue(':societe', $societe);
    $target->execute();
    $resultIntern = array();
    while ($row = $target->fetch(PDO::FETCH_ASSOC)) {
      array_push($resultIntern, $row['soussociete']);
    }
    return $resultIntern;
  }

  function getWrapSocietes($societe){
    $resultTemp = array();
    $resultParcial =  array();
    $resultGeneral = array();
    
    $resultTemp = societes($societe);
    
    while(count($resultTemp) > 0){      
      array_push($resultGeneral, $resultTemp[0]);

      $resultParcial = societes($resultTemp[0]);
      
      array_shift($resultTemp);
  
      $c = count($resultParcial);
      for ($i=0; $i < $c; $i++) { 
        array_push($resultTemp, $resultParcial[$i]);
      }
    }
    return $resultGeneral;
  };

  function getAscendanceSocietes($societe){
    $bdd = connection_db();
    $sql = "SELECT * FROM societe_soussociete WHERE soussociete = :societe";
    $query = $bdd->prepare($sql); 
    $query->bindValue(":societe",$societe);
    $query->execute();
    $societes = array();
     
    if($ascendance = $query->fetch()){// il existe au plus une ascendance
      $societes = getAscendanceSocietes($ascendance['societe']);
      array_push($societes,$ascendance['societe']);
      return $societes;
      
      $query->closeCursor(); 
    }else{
      return array();
    }

  }

  function saveObjetsSession($objet){
    $_SESSION['objets'] = array();
    array_push($_SESSION['objets'], $objet['objetID']);
  }

  function selectObjet($societe, &$resultObjets, &$sessionObjetID){
    $bdd = connection_db();

      $sql = 'SELECT o.*,so.*,s.name AS societeName FROM objet AS o INNER JOIN societe_objet AS so INNER JOIN societe AS s
      WHERE 
      o.objetID=so.objetID and so.societeID = s.ID and so.societeID = :societeID';
      //$sql = "SELECT * FROM (CALL selectObjects(".$societe."))";
    $target = $bdd->prepare($sql);
    $target->bindValue(':societeID', $societe);
    $target->execute();
    while ($row = $target->fetch()) {
      if($row['idParent']!=null){
        $sql = 'SELECT * FROM typeobjet WHERE idType = :idParent';
        $target2 = $bdd->prepare($sql);
        $target2->bindValue(':idParent',$row['idParent']);
        $target2->execute();
        if($type=$target2->fetch(PDO::FETCH_ASSOC)){
          $row['nameType'] = $type['nameType'];
          $row['typeImage'] = $type['image'];
        } 
      }else{
        $row['nameType'] = 'Type non défini';
      }

      array_push($resultObjets, $row);
      array_push($sessionObjetID, $row['objetID']);
    }
  }

  function selectPosObjet($societe, &$resultObjets, &$sessionObjetID){
    $bdd = connection_db();

      $sql = 'SELECT o.objLong, o.objLat, o.name, o.objetID, o.idParent FROM objet AS o INNER JOIN societe_objet AS so INNER JOIN societe AS s
      WHERE 
      o.objetID=so.objetID and so.societeID = s.ID and so.societeID = :societeID';
      //$sql = "SELECT * FROM (CALL selectObjects(".$societe."))";
    $target = $bdd->prepare($sql);
    $target->bindValue(':societeID', $societe);
    $target->execute();
    while ($row = $target->fetch()) {
      if($row['idParent']!=null){
        $sql = 'SELECT * FROM typeobjet WHERE idType = :idParent';
        $target2 = $bdd->prepare($sql);
        $target2->bindValue(':idParent',$row['idParent']);
        $target2->execute();
        if($type=$target2->fetch(PDO::FETCH_ASSOC)){
          $row['nameType'] = $type['nameType'];
          $row['typeImage'] = $type['image'];
        } 
      }else{
        $row['nameType'] = 'Type non défini';
      }

      array_push($resultObjets, $row);
      array_push($sessionObjetID, $row['objetID']);
    }
  }


  function selectTag($societe, &$resultTags){
    $bdd = connection_db();
    $sql = 'SELECT st.customersID AS societeID, t.*  FROM tags AS t, societe_tags AS st, 360sc_iz.yourls AS y WHERE t.YourlsID = y.YourlsID 
          AND  y.YourlsID = st.yourlsID AND st.customersID = :societeID';
    $target = $bdd->prepare($sql);
    $target->bindValue(':societeID', $societe);
    $target->execute();
    while ($row = $target->fetch(PDO::FETCH_ASSOC)) { 
      // besoin d'index dans l'ordre pour faciliter le tri
      $resultTags[$row['TagID']] = $row; 
    }


  }


  // vérifier si la societe peut voir l'objet
  function check_object_societe($objectId,$societeId){
    $societes = getWrapSocietes($societeId);
    $societes[] = $societeId;
    $match = false;
    foreach ($societes as $key => $societe) {
      $pdo = connection_db();
      $query=$pdo->prepare('SELECT * FROM societe_objet WHERE societeID = :societeId AND objetID = :objectId');
      $query->bindValue(':societeId',$societe);
      $query->bindValue(':objectId',$objectId);
      $query->execute();
      if($query->fetch()) $match=true;
    }
    return $match;
  }


?>