var filters = (function (){

	var resetDefault = function (){
		var trouve = false;
		for (var i = self.markers.length - 1; i >= 0; i--) {
			if (self.markers[i].filtered){
				trouve = true;
				self.markers[i].filtered = false;
			}
		};
		if (trouve)
			self.leafletView.ProcessView();
	}
	var trouverById = function (id){
		for (var i = self.markers.length - 1; i >= 0; i--) {
			if (self.markers[i].features.id == id ){
				return self.markers[i];
			}
		};
		return false;
	}
	var trouverByName = function (name) {
		console.log(name);
		var listName = [];
		for (var i = self.markers.length - 1; i >= 0; i--) {
			listName.push(self.markers[i].features.data.name);
		};
		var fuzzy = FuzzySet(listName);
		name = fuzzy.get(name);
			
		console.log(name);	
		var listMarkers = [];
		for (var i = self.markers.length - 1; i >= 0; i--) {
			for (var j = 0; j < name.length; ++j)
				if (self.markers[i].features.data.name == name[j][1]){
					listMarkers.push(self.markers[i]);
				}
		};
		if (listMarkers.length)
			return listMarkers;
		return false;
	}
	var trouverByAdresse = function (adresse) {
		var listAdresse = [];
		for (var i = self.markers.length - 1; i >= 0; i--) {
			listAdresse.push(self.markers[i].features.data.adresse);
		};
		var fuzzy = FuzzySet(listAdresse);
		adresse = fuzzy.get(adresse);
		for (var i = self.markers.length - 1; i >= 0; i--) {
			if (self.markers[i].features.data.adresse == adresse){
				return self.markers[i];
			}
		};
		return false;
	}

	return {
		trouverById: trouverById,
		trouverByName: trouverByName,
		trouverByAdresse: trouverByAdresse,
		resetDefault: resetDefault
	}	
})();