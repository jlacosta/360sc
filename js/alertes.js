alerte = (function(){
	var data = [];
	var markerFormData = [];
	var historique = [];

	updateHistorique = function(marker){
		if (marker.features.historique == false){
			marker.featurespdata.historique = true;
			historique.push(marker);
		}		
	}
	
	searchMarkerByData = function (){
		for (var i = 0; i < self.markers.length; ++i)
			for (var j = 0; j < data.length; ++j)
				if (self.markers[i].features.id == data[j]['objetID'])
					markerFormData.push(self.markers[i]);

	}

	isActive = function (id) {}

	activerAlerte = function (id){

	}

	desactiverAlerte = function (id){
		
	}

	setData = function(pdata){
		/*Attention, if fault utiliser l'outil (set) pour savoir
		si il y a des elements differents entre pdata et data.
		Pour ne pas metrre a jour la page tous 3 seconds*/
		data = pdata;
		searchMarkerByData();
		updateMarkerAlerte();
	}

	getData = function(){
		return data;
	}

	updateMarkerAlerte = function(){
		/*Ajouter un icon defferent lors qu'il a  une alerte*/
		for (var i = 0; i < markerFormData.length; ++i) {
			if (!markerFormData[i].features.historique)
				markerFormData[i].features.historique = false;
			historique.push(markerFormData[i]);		

			//Category
			markerFormData[i].category = 2;
			//Icon
			markerFormData[i].data.icon = icons.red;

			//Ajouter les alertes au bar droite 
			$('#alerteAdded').append(get.notificationAlerte(markerFormData[i]));
			//Lier le event click au la fn centreMarker
			var str = '#'+markerFormData[i].features.data.objetID;
			$(str).click(function (){
				var lat = parseFloat($(this).attr('lat'));
				var lng = parseFloat($(this).attr('lng'));
				centreMarkerByLatLng(lat, lng);
			});		
				
		}	

		self.leafletView.RedrawIcons();
		self.leafletView.ProcessView();

		return true;
	}

	var run = function (){
		requestAjax.getJson('carte/alertes/', function ( data, textStatus, jqXHR ){
        	alerte.setData(data);
    	});
	}

	return {
		run: run, 
		setData: setData,
		getData: getData,
	}

})();

