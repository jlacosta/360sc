popup = (function (){

	var showPropiete = function (c){
		if ($('.off-canvas-wrap').hasClass('offcanvas-overlap-left') && !$('#listMarker').hasClass('offcanvas-overlap-left'))
			$('.off-canvas-wrap').removeClass('offcanvas-overlap-left');

		/*Ouvrir et fermer le droit side-bar*/
		$('.off-canvas-wrap').toggleClass('offcanvas-overlap-left');
		$('#listMarker').toggleClass('offcanvas-overlap-left');
		/*Afficher les propietes d'un objet*/
		var id = String($(c).attr('name')).split('-')[1];
		var str = '#propiete'+id;
		$(str).toggleClass('offcanvas-overlap-left');
	}

	var addClassEtat = function (id){
		if (self.ByIdMarkers[id].data.etat == 'popupDeplacer'){
			if (!$('#popupContentNormal').hasClass('hidePopup'))
				$('#popupContentNormal').addClass('hidePopup');
			if ($('#popupContentDeplacement').hasClass('hidePopup'))
				$('#popupContentDeplacement').removeClass('hidePopup');
		}
		else if (self.ByIdMarkers[id].data.etat != 'popupDeplacer'){
			if ($('#popupContentNormal').hasClass('hidePopup'))
				$('#popupContentNormal').removeClass('hidePopup');
			if (!$('#popupContentDeplacement').hasClass('hidePopup'))
				$('#popupContentDeplacement').addClass('hidePopup');
		}
	}

	var deplacer = function (c){
		var id = String($(c).attr('name')).split('-')[1];
		
		self.ByIdMarkers[id].data.etat = 'popupDeplacer';
		addClassEtat(id);

		self.ByIdMarkers[id].leafletMarker.dragging.enable();

		self.ByIdMarkers[id].data.draggable = true;
		if (!self.ByIdMarkers[id].data.dragging){
			self.ByIdMarkers[id].data.dragging = false;
			/*Savoir quelle est la premiere fois où je fais ça*/
			self.ByIdMarkers[ id ].data.initPos = _.clone(self.ByIdMarkers[ id ].position);
			self.ByIdMarkers[ id ].data.initAdresse = _.clone(self.ByIdMarkers[ id ].data.features.adresse);
		}
		addClassEtat(id);
		self.leafletView.ProcessView();
	}

	var check = function (c){
		var id = String($(c).attr('name')).split('-')[1];
		self.ByIdMarkers[id].data.etat = 'popupNormal';
		self.ByIdMarkers[id].data.draggable = false;
		self.ByIdMarkers[id].leafletMarker.dragging.disable();
		addClassEtat(id);
		/*requestAjax.post('carte/changer/LatLng/', {
			id: id,
			lat: self.ByIdMarkers[id].position.lat,
			lng: self.ByIdMarkers[id].position.lng
		});*/
	}

	var cancel = function (c){
		var id = String($(c).attr('name')).split('-')[1];
		self.ByIdMarkers[id].data.etat = 'popupNormal';
		self.ByIdMarkers[id].data.draggable = false;
		self.ByIdMarkers[id].leafletMarker.closePopup();
		self.ByIdMarkers[id].leafletMarker.dragging.disable();
		//Envoyer la nouvel adresse au serveur
	}

	var run = function (){
		for (var i=0; i<self.markers.length; ++i){
			self.markers[i].data.popup = get.popupNormal(self.markers[i].features.data);
			self.markers[i].data.popupOptions = {
				maxWidth: '300',
				className: 'popupsm',
				closeButton: false
			}
		}
		self.leafletView.ProcessView();
	}	

	return {
		run: run,
		deplacer: deplacer,
		cancel: cancel,
		check: check,
		addClassEtat,
		showPropiete: showPropiete
	}

})();