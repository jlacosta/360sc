requestAjax = (function(){
	var success = function (){
		if ( console && console.log ) {
            console.log( "Todo fue bien" );
        }
	}
	var fail = function( jqXHR, textStatus, errorThrown ) {
        if ( console && console.log ) {
            console.log( "Algo ha fallado: " +  textStatus );
        }
	} 

	var getJson = function(url, successfn, failfn){

		success = successfn || success; 
		fail = failfn || fail; 

		$.getJSON(url)
		    .done(success)
		    .fail(fail);
	}
	var get = function(){}
	var post = function(url, data, successfn, failfn){

		success = successfn || success; 
		fail = failfn || fail; 

		$.post(url, data)
		    .done(success)
		    .fail(fail);	
	}
	var when = function(){}
	return {
		post: post,
		get: get,
		getJson: getJson,
		when: when
	}
})();
