iconSM = L.Icon.Default.extend({});
icons = {
  'bleu': new iconSM({
    iconUrl: 'img/picto-flat-bleu-1.png', 
    iconSize: [31, 41],
    shadowUrl: 'img/ombre.png',
    shadowSize:   [42, 32],
    shadowAnchor: [4, 31],
    popupAnchor:  [-3, -41],
  }),
  'red': new iconSM({
    iconUrl: 'img/picto-flat-red-1.png', 
    iconSize: [31, 41],
    shadowUrl: 'img/ombre.png',
    shadowSize:   [42, 32],
    shadowAnchor: [4, 31],
    popupAnchor:  [-3, -41]
  }), 
  'bleuAlert': new iconSM({
    iconUrl: 'img/picto-flat-bleu-1.png', 
    iconSize: [31, 41],
    shadowUrl: 'img/ombre.png',
    shadowSize:   [42, 32],
    shadowAnchor: [4, 31],
    popupAnchor:  [-3, -41],
    className: 'bleuAlert'
  }) 
}

updatePropMark = function(who, data){
    if (!who.features)
        who.features = {};
    who.features.id = data['objetID'];
    who.features.data = data;
    who.data.features = {};
    who.data.features = data;
    return who;
}

centreMarkerByMarker = function (marker){
  var latlng =  L.latLng(marker.position);
  self.map.setZoom(self.map.getMaxZoom());
  self.map.panTo(latlng);
}

centreMarkerByLatLng = function (lat, lng){
  self.map.setZoom(self.map.getMaxZoom());
  self.map.panTo(L.latLng(lat, lng));
}

configMarker = function(marker, data, i){
    /*Set le lat et long*/
    var ll = marker.position;
    ll.lat = parseFloat(data['objLat']);
    ll.lng = parseFloat(data['objLong']);
    marker = updatePropMark(marker, data);
    marker.category = 1;
    marker.data.icon = icons.bleu;
    marker.data.forceIconRedraw = true;
    return marker;
}

fnFoundationSubMenu = function(s){
    var icon = $(s).parent().next(".off-canvas-submenu").is(':visible') ? '+' : '-';
    $(s).parent().next(".off-canvas-submenu").slideToggle('fast');
    $(s).find("span").text(icon);
}