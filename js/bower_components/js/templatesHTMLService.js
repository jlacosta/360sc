(function () {
	angular.module('Demo.templateService', ['Demo.countActionsService']);
	angular.module('Demo.templateService')
	.service('popupAlerte', function(){
		_templateAlerte = _.template(
			'<style>.labelTitle{font-size:1.1rem;}</style>'+
			'<div style="width: 270px;" class="isOpenPopup">' +
				'<div class="row">' +
					'<div class="small-12 large-12 columns">'+
						'<label class="medium-text-left labelTitle" style="margin:4px 0"> <b>ALERTE</b> </label>' +
					'</div>' +
				'</div>' +	
				'<div class="row" >' +
						'<div class="small-12 large-12 columns" style="padding:0">'+
							'<label class="medium-text-center" style="background-color:#c1d101;font-weight:700;color:white" > <%= t.nameType %> </label>' +
						'</div>' +
						'<div class="small-12 large-12 columns" style="padding:0">'+
							'<label class="medium-text-center" style="background-color:#c1d101;color:white"> <%= t.name %> </label>' +
						'</div>' +
				'</div>'+
				'<div class="row">' +
					'<div class="small-12 large-12 columns"  style="padding:0">'+
						'<img src="<%= t.image %>" alt="Pas d\'image" width="210" height="auto" style="width:100%" onerror="this.error=null;this.src='+"'<%= t.typeImage %>'"+'">' +
					'</div>' +
				'</div>' + 
				'<div class="row">' +
					'<div class="small-12 large-12 columns">'+
						'<% if(t.messageAlerte){ %>' + 
						'<i class="fi-alert" style="color: red; margin:0 0 0 calc(50% - 50px); font-size: 50px;"></i>'  +
							'<span class="glyphicon glyphicon-envelope" style="font-size:42px;margin-left:15px;position:relative;top: 3px;"></span>'+ 
						'<% } else{ %> <i class="fi-alert" style="color: red; margin:0 0 0 calc(50% - 25px); font-size: 50px;"></i> <%} %>' +
					'</div>' +
				'</div>' +
				'<div class="row">' + 
					'<div class="small-12 large-12 columns">' +
						'<label class="medium-text-left labelTitle"> <b> <%= t.Name %> le: </b></label>' +
					'</div>' +
				'</div>' +
				'<div class="row">' + 
					'<div  class="small-12 large-12 columns">' +
						'<label class="medium-text-center" style="background-color:#dadbda;font-size:1.5rem "> <%= t.createdTime.slice(0, 10) %> à <%= t.createdTime.slice(11, 19) %> </label>' +
					'</div>' +
				'</div>' +
				'<div class="row">' + 
					'<div  class="small-12 large-12 columns" style="margin-top:5px">' +
						'<label class="medium-text-left"> <%= t.messageAlerte %> </label>' +
					'</div>' +
				'</div>' +
				'<div class="row" style="padding-top: 5px;">' +
					'<div class="small-12 large-12 columns">'+ 
						'<div class="switch round">' +
					 		'<input id="inputSwitch-<%= t.actionID %>" type="checkbox" <% if(t.prisEnCompte == 1) { %> checked <% } %> onclick="prisEnCompteAlerte_(this)" >' +
					 		'<label class="labelGreen" for="inputSwitch-<%= t.actionID %>" style="margin:0 0 0 calc(50% - 2rem); background: #f93C32;"></label>' +
						'</div>' + 
					'</div>' +
				'</div>' +
			'</div>'			
		);
		this.template = function (t){
			return _templateAlerte({t:t});
		}
	})
	.service('popupMessEval', function(countActionsMarker){
		_templateMessage = _.template(
			'<style>.labelTitle{font-size:1.1rem;}</style>'+
			'<div style="width: 270px;"  class="isOpenPopup">' +
				'<div class="row">' +
					'<div class="small-12 large-12 columns">'+
						'<label class="medium-text-left labelTitle"  style="margin:4px 0"> <b> MESSAGE </b> </label>' +
					'</div>' +
				'</div>' +	
				'<div class="row" >' +
						'<div class="small-12 large-12 columns" style="padding:0">'+
							'<label class="medium-text-center" style="background-color:#c1d101;font-weight:700;color:white" > <%= t.nameType %> </label>' +
						'</div>' +
						'<div class="small-12 large-12 columns" style="padding:0">'+
							'<label class="medium-text-center" style="background-color:#c1d101;color:white"> <%= t.name %> </label>' +
						'</div>' +
					'</div>'+
				'<div class="row">' +
					'<div class="small-12 large-12 columns" style="padding:0">'+
						'<img src="<%= t.image %>" alt="Pas d\'image" width="210" height="auto" style="width:100%" onerror="this.error=null;this.src='+"'<%= t.typeImage %>'"+'">' +
					'</div>' +
				'</div>' +
				'<div class="row">' +
					'<div class="small-12 large-12 columns">'+
						'<% if(t.evaluation == 3) {%>' + 
							'<img style="width:50px; height:50px;  margin:0 0 0 calc(50% - 50px);"  src="img/smile-happy.svg">'+
						'<% } else if(t.evaluation == 2){ %>' + 
							'<img style="width:50px; height:50px;  margin:0 0 0 calc(50% - 50px);"  src="img/smile-mid.svg">'+
						'<% } else if(t.evaluation == 1){ %>' + 
							'<img style="width:50px; height:50px;  margin:0 0 0 calc(50% - 50px);"  src="img/smile-sad.svg">'+
						'<% } %>' +	
							'<span class="glyphicon glyphicon-envelope" style="font-size:42px;margin-left:15px;position:relative;top: 18px;"></span>'+ 
					'</div>' +
				'</div>' +
				'<div class="row" style="margin-top:0.7rem">' + 
					'<div class="small-12 large-12 columns">' +
						'<label class="medium-text-left labelTitle"> <b> <%= t.Name %> le: </b></label>' +
					'</div>' +
				'</div>' +
				'<div class="row">' + 
					'<div  class="small-12 large-12 columns">' +
						'<label class="medium-text-center" style="background-color:#dadbda;font-size:1.5rem "> <%= t.createdTime.slice(0, 10) %> à <%= t.createdTime.slice(11, 19) %> </label>' +
					'</div>' +
				'</div>' +
				'<div class="row">' + 
					'<div  class="small-12 large-12 columns" style="margin-top:5px">' +
						'<label class="medium-text-left"> <%= t.messageEvaluation %> </label>' +
					'</div>' +
				'</div>' +
				'<div class="row" style="padding-top: 5px;">' +
					'<div class="small-12 large-12 columns">'+ 
						'<div class="switch round">' +
					 		'<input id="inputSwitch-<%= t.actionID %>" type="checkbox" <% if(t.prisEnCompte == 1) { %> checked <% } %> onclick="prisEnCompteEvaluation_(this)" >' +
					 		'<label class="labelGreen" for="inputSwitch-<%= t.actionID %>" style="margin:0 0 0 calc(50% - 2rem); background: #f93C32;"></label>' +
						'</div>' + 
					'</div>' +
				'</div>' +
			'</div>'			
		);
		_templateEvaluation = _.template(
			'<style>.labelTitle{font-size:1.1rem;}</style>'+
			'<div style="width: 270px;"  class="isOpenPopup popupEvaluation">' +
				'<div class="row">' +
					'<div class="small-12 large-12 columns">'+
						'<label class="medium-text-left labelTitle" style="margin:4px 0"><b> AVIS</b> </label>' +
					'</div>' +
				'</div>' +
				'<div class="row" >' +
						'<div class="small-12 large-12 columns sansPadding">'+
							'<label class="medium-text-center" style="background-color:#c1d101;color:white" > <%= t.nameType %> </label>' +
						'</div>' +
						'<div class="small-12 large-12 columns sansPadding">'+
							'<label class="medium-text-center" style="background-color:#c1d101;color:white"> <%= t.name %> </label>' +
						'</div>' +
					'</div>'+
				'<div class="row">' +
					'<div class="small-12 large-12 columns sansPadding">'+
						'<img src="<%= t.image %>" alt="Pas d\'image" width="210" height="auto" style="width:100%" onerror="this.error=null;this.src='+"'<%= t.typeImage %>'"+'">' +
					'</div>' +
				'</div>' +
				'<div style="margin-bottom: 0.7rem !important; margin-top:0.7rem !important;" class="row">' +
					'<div class="small-12 large-12 columns">'+
						'<% if(t.evaluation == 3) {%>' + 
							'<img style="width:80px; height:80px; display:table-cell; align:center;margin:0 0 0 calc(50% - 40px);"  src="img/smile-happy.svg">'+
						'<% } else if(t.evaluation == 2){ %>' + 
							'<img style="width:80px; height:80px; display:table-cell; align:center;margin:0 0 0 calc(50% - 40px);"  src="img/smile-mid.svg">'+
						'<% } else if(t.evaluation == 1){ %>' + 
							'<img style="width:80px; height:80px; display:table-cell; align:center;margin:0 0 0 calc(50% - 40px);"  src="img/smile-sad.svg">'+
						'<% } %>' +	
					'</div>' +
				'</div>' +
				'<div class="row" style="padding-left:8px !important; padding-right:5px !important;">' +
					'<div style="text-align: left; padding-left:0px !important;" class="small-12 large-12 columns">' +
						'<label class="medium-text-left labelTitle"> <%= t.Name %> le: </label>' +
					'</div>' +
					'<divclass="small-12 large-12 columns" >' +
						'<label class="medium-text-center"  style="background-color:#dbdbdb;font-size:1.5rem " > <%= t.createdTime.slice(0, 10) %> à <%= t.createdTime.slice(11, 19) %> </label>' +
					'</div>' +
				'</div>' +
				'<div class="row">' +
					'<div class="small-12 large-12 columns">'+
						'<div class="switch round">' +
					 		'<input id="inputSwitch-<%= t.actionID %>" type="checkbox" <% if(t.prisEnCompte == 1) { %> checked <% } %> onclick="prisEnCompteEvaluation1_(this)" >' +
					 		'<label class="labelGreen" for="inputSwitch-<%= t.actionID %>" style="margin:0 0 0 calc(50% - 2rem);background: #f93C32;"></label>' +
						'</div>' + 
					'</div>' +
				'</div>' +			
			'</div>'
		);
		
		this.templateMessage = function (t){
			var xDataEval = countActionsMarker.evaluationData(t.objetID);
			return _templateMessage({t:t});
		}
		this.templateEvaluation = function (t){
			var xDataEval = countActionsMarker.evaluationData(t.objetID);
			t['evalData'] = xDataEval;
			console.log(t.prisEnCompte);
			return _templateEvaluation({t:t});
		}
	})
	.service('popupInfo', ['countActionsMarker', 
		function(countActionsMarker){
			_templateInfo = _.template(
			'<style>.labelTitle{font-size:1.1rem;}</style>'+
				'<div class="rowPopup isOpenPopup" style="width: 270px;">' +
					'<div class="row"  style="padding:3px 0" >' +
						'<div class="small-6 large-6 columns">'+
							'<label class="medium-text-left labelTitle"><b>INFO</b></label>' +
						'</div>' +
						'<div class="small-6 large-6 columns">' +
							'<a href="#" onclick="ouvrirOffPrpieteMarker(<%= t.objetID %>, \'alerte\')">' +
								'<div class="iconPopup">' +
									'<span aria-hidden="true" class="glyphicon glyphicon-bell bell" style="font-size: 18px; top:1px; color:rgb(255, 97, 85);">' +
	                            	'</span>' +
								'</div>' +
							'</a>' +
							'<a href="#" onclick="ouvrirOffPrpieteMarker(<%= t.objetID %>, \'evaluation\')">' +
								'<div class="iconPopup">' +
	                            	'<img style="width:20px; height:20px;"  src="img/smile-happy.svg" alt="">' +
								'</div>' +
							'</a>' +
						'</div>' +
					'</div>' +
					'<div class="row ">' +
						'<div class="small-12 large-12 columns sansPadding">'+
							'<label class="medium-text-center" style="background-color:#c1d101;color:white;" > <%= t.nameType %> </label>' +
						'</div>' +
						'<div class="small-12 large-12 columns sansPadding">'+
							'<label class="medium-text-center" style="background-color:#c1d101;color:white;"> <%= t.name %> </label>' +
						'</div>' +
					'</div>'+
					'<div class="row ">' +
						'<div class="small-12 large-12 columns sansPadding">'+
							'<img src="<%= t.image %>" alt="Pas d\'image" width="210" height="auto" style="width:100%" onerror="this.error=null;this.src='+"'<%= t.typeImage %>'"+'">' +
						'</div>' +
					'</div>' + 
					'<div class="row">' +
						'<div  class="small-12 large-12 columns">'+
							'<label class="medium-text-left labelTitle" style=""> <b> Localisation: </b> </label>' +
						'</div>' +
						'<div style="background-color:#dbdbdb;background-clip:content-box" class="small-12 large-12 columns">'+
							'<label style="padding:5px" class="" > <%= t.adresse %> </label>' +
						'</div>' +
					'</div>' +
					'<div class="row">' +
						'<div  class="small-12 large-12 columns">'+
							'<label class="medium-text-left labelTitle" style=""> <b> Mise en service le: </b> </label>' +
						'</div>' +
						'<div  class="small-12 large-12 columns">'+
							'<label class="medium-text-center" style="background-color:#c1d101;font-size:1.5rem"> 19/10/2015 </label>' +
						'</div>' +
					'</div>' +
					'<div class="row">' +
						'<div  class="small-12 large-12 columns">'+
							'<label class="medium-text-left labelTitle"> <b> Prochaine événement: </b> </label>' +
						'</div>' +
						'<div  class="small-12 large-12 columns">'+
							'<label class="medium-text-center" style="background-color:#dbdbdb;font-size:1.5rem"> 19/03/2016 </label>' +
						'</div>' +
					'</div>' +
					'<div class="row" style="padding:10px">' +
						'<div class="small-12 large-12 columns sansPadding" style="text-align:center">'+
							'<a href="#" style="color:black" onclick="ouvrirOffPrpieteMarker(<%= t.objetID %>, \'info\')">' +
				    			'<i class="fi-plus fi-pr_" style="font-size: 25px;">Informations</i>' +
				    		'</a>' +	
						'</div>' +
					'</div>' + 
				'</div>'
			);
			this.template = function (t){
				var countAlerte = countActionsMarker.countAlerte(t.objetID);
				var countMessage = countActionsMarker.countMessage(t.objetID);
				t['countAlerte'] = countAlerte;
				t['countMessage'] = countMessage;

				//console.log(countAlerte, countMessage);
				return _templateInfo({t:t});
			}
		}
	]);
})();