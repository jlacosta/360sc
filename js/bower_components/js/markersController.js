'use strict';

(function (){
	angular.module('Demo.markersController', []);
	angular.module('Demo.markersController')
	.controller('markerController', ['$rootScope', '$scope',
		'$timeout', 'leafletData', 'popupAlerte', 'popupMessEval',
		'popupInfo', 'countActionsMarker',
		function ($rootScope, $scope, $timeout, leafletData,
		 popupAlerte, popupMessEval, popupInfo, countActionsMarker){
			$scope.markerFocusID = false;
			
			angular.extend($rootScope, {
	            center: {
	                lat: 43.5333,
	                lng: 6.4667,
	                zoom: 9
	            },
	            markers: {},
	            controls: {
                    scale: true
                },
	            defaultIcon: {},
	            layers: {
	                baselayers: {
	                    googleRoadmap: {
	                        name: 'Google Streets',
	                        layerType: 'ROADMAP',
	                        type: 'google'
	                    },
	                    googleTerrain: {
	                        name: 'Google Terrain',
	                        layerType: 'TERRAIN',
	                        type: 'google'
	                    },
	                    googleHybrid: {
	                        name: 'Google Hybrid',
	                        layerType: 'HYBRID',
	                        type: 'google'
	                    }
	                },
	                overlays: {
	                    general: {
	                        name: "General",
	                        type: "markercluster",
	                        visible: true,
	                        layerOptions: {
	                            showCoverageOnHover: false,
	                            disableClusteringAtZoom: 18, 
	                            iconCreateFunction: function (cluster) {
	                                var childCount = cluster.getChildCount();
	                                var child = cluster.getAllChildMarkers();
               						
               						var icon = 'icon-360-'
               						var ilya = false;
               						for (var i = childCount - 1; i >= 0; i--) {
               							if ($scope.etatMarker(child[i].options.info.objetID)){
	                                		ilya = true;
	                                		break;
               							}
               						};
	                                if(ilya) icon += 'ilyaDedans';
	                                return new L.DivIcon({ 
	                                	html: ' <span> \
	                                				<span> \
	                                					<b>' + cluster.getChildCount() + '</b> \
	                                				</span> \
	                                				<span></span> \
	                                			</span>' ,
	                                	className: 'mycluster ' + icon
	                                });                           
	                            }
	                        }
	                    }
	                }
	            },
	            events: {
	            	markers: {
	            		enable: ['dragend', 'click']
	            	}
	            }
	        });

			/*Definir etat d'un marker*/
			$scope.etatMarker = function (objetID){
				if (countActionsMarker.countAlerte(objetID) > 0)
					return true;
				if (countActionsMarker.countMessage(objetID) > 0)
					return true;
				if (countActionsMarker.countEvaluation(objetID) > 0)
					return true;

				return false;
			}
			/*Montrer les markers chaque fois qu'il y en a nouveaux*/
			$scope.$on('EVENT_TOUS_MARKERS', function (event, data){
				console.log('tous markers');
				for (var i=0; i<data.length; ++i){
					$scope.markers[data[i].objetID] = {
						name: data[i].name,
						objetID: data[i].objetID,
						info: data[i],
						layer: 'general',
                   		lat: parseFloat(data[i].objLat),
                    	lng: parseFloat(data[i].objLong),
                    	message: 'vide',
                    	popupOptions: {
                    		maxWidth: 271
                    	},
                    	icon: {
		                    type: 'extraMarker',
		                    markerColor: 'red',
		                    prefix: 'fa',
		                    shape: 'circle',
		                    icon: '',
		                    shadowAnchor: [0, 14],
                    		popupAnchor:  [-150, 24]
		                },
		                focus: false
					}
				}
			});

			/*$timeout(function(){
				console.log($scope.j);
				$scope.markers[33] = $scope.j;

			}, 3000);*/
			$scope.$on('EVENT_MAP', function (event, p, action){

				if (typeof p === 'object'){
					var uniqueList = _.uniq(p);
					var len = uniqueList.length;
					for (var i=0; i<len; ++i){
						/*Faire la mise a jour seulement qu'objets qui son en regroupement*/
						$scope.updateCluster(uniqueList[i]);
						$scope.changeIconMarker(uniqueList[i]);
					}
				}
				else if (typeof p === 'number'){
					console.log(p);
					$scope.changeIconMarker(p);
				}
			});

			$scope.updateCluster = function (id){
				console.log(id);
				$scope.markers[id].lat += 0.1;
				$timeout(function(){
					$scope.markers[id].lat -= 0.1;
				}, 200);
			}

			$scope.changeIconMarker = function(id){
				console.log(id);
				var prisOuPas = countActionsMarker.totalActionPasPrisEnCompte(id);
				console.log(prisOuPas);
				if (prisOuPas > 0) $scope.markers[id].icon.markerColor = 'orange-dark';
				else $scope.markers[id].icon.markerColor = 'red';
				
			}

			$scope.$on('EVENT_CENTRE_MARKER', function (event, id){
				$scope.centrerMap(id);
			});

			/*Centre la carte a partir d'un ID d'objet*/
			$scope.centrerMap = function(id){	
				console.log('Centre Map');			
				leafletData.getMap().then(function(map) {
					$scope.center = {
						lat: $scope.markers[id].lat,
						lng: $scope.markers[id].lng,
						zoom: map.getMaxZoom()
					};
					//console.log(map.getMaxZoom());
            	});
			}
			$scope.centrerMapCoord = function(lat, lng){	
				console.log('Centre Map Coord');	
				leafletData.getMap().then(function(map) {
					map.panTo([lat, lng]);
					map.setZoom( map.getMaxZoom() );


					console.log(map.getMaxZoom());
					console.log(lat, lng);
            	});
			}

			/*Ouvrir l'off-canvas etat du parc*/
			$scope.ovrirOffEtatDuParc = function (){
				//Ouvrir l'off-canvas etat du parc
				/*if ($('.off-canvas-wrap').hasClass('offcanvas-overlap-left') && !$('#etatParc').hasClass('offcanvas-overlap-left'))
					$('.off-canvas-wrap').removeClass('offcanvas-overlap-left');*/
				$('.off-canvas-wrap').removeClass('offcanvas-overlap-left');
				$('#etatParc').removeClass('offcanvas-overlap-left');
				//Ouvrir et fermer le droit side-bar
				$('.off-canvas-wrap').toggleClass('offcanvas-overlap-left');
				$('#etatParc').toggleClass('offcanvas-overlap-left');
				/*if (!($('.off-canvas-wrap').hasClass('offcanvas-overlap-left') && 
					$('#etatParc').hasClass('offcanvas-overlap-left'))){
					$('.off-canvas-wrap').toggleClass('offcanvas-overlap-left');
					$('#etatParc').toggleClass('offcanvas-overlap-left');
				}*/
			}

			/*Ouvrir l'off-canvas marker*/
			$scope.ouvrirOffPrpieteMarker = function(id){
				$('.off-canvas-wrap').removeClass('offcanvas-overlap-left');
				$('#markerList').removeClass('offcanvas-overlap-left');
				var str = '#markerShowProperties-'+id;
				$(str).removeClass('offcanvas-overlap-left');

				$('.off-canvas-wrap').toggleClass('offcanvas-overlap-left');
				$('#markerList').toggleClass('offcanvas-overlap-left');
				$(str).toggleClass('offcanvas-overlap-left');
			}


			/*Montre les alerts arrivant au system*/
			$scope.$on('EVENT_ALERTES', function (event, data){	
				
				//console.log('event alerts');
				console.log(data.objetID);
				if ($scope.markers[data.objetID].focus)
					$scope.markers[data.objetID].focus = false;			
				$timeout(function (){
					//Centre la carte
					$scope.centrerMap(data.objetID);
					//Ouvrir le popup
					$scope.markers[data.objetID].focus = true;
					//Changer info du popup
					data['image'] = $scope.dataMarkers[data.objetID].image;
					data['nameType'] = $scope.dataMarkers[data.objetID].nameType;
					data['name'] = $scope.dataMarkers[data.objetID].name;
					$scope.markers[data.objetID].message = popupAlerte.template(data);
					//Changer l'icon
					$scope.markers[data.objetID].icon.markerColor = 'orange-dark';
					//$scope.ovrirOffEtatDuParc();
					//$scope.updateCluster(data.objetID);
				}, 100);
			});

			/*$scope.$on('EVENT_OUVRIR_ALERTES', function (event, data){
				//console.log('event alerts');
				if ($scope.markers[data.objetID].focus)
					$scope.markers[data.objetID].focus = false;		
				$timeout(function (){
					//Ouvrir le popup
					$scope.markers[data.objetID].focus = true;
					//Changer info du popup
					$scope.markers[data.objetID].message = popupAlerte.template(data);
					//Changer l'icon
					//$scope.markers[data.objetID].icon.markerColor = 'orange-dark';
					//Centre la carte
					$scope.centrerMap(data.objetID);
					//$scope.ovrirOffEtatDuParc();
					//$scope.updateCluster(data.objetID);
				}, 100);
			});*/

			/*Montre les messages arrivant au system*/
			$scope.$on('EVENT_EVALUATIONS', function (event, data){
				//console.log('event messages');
				console.log(data.objetID);
				if ($scope.markers[data.objetID].focus)
					$scope.markers[data.objetID].focus = false;			
				$timeout(function (){
					//Centre la carte
					$scope.centrerMap(data.objetID);
					//Ouvrir le popup
					$scope.markers[data.objetID].focus = true;
					//Changer info du popup
					data['image'] = $scope.dataMarkers[data.objetID].image;
					data['nameType'] = $scope.dataMarkers[data.objetID].nameType;
					data['name'] = $scope.dataMarkers[data.objetID].name;
					data['typeImage'] = $scope.dataMarkers[data.objetID].typeImage;
					console.log(data['typeImage']);
					console.log(data['Name']);
					$scope.markers[data.objetID].message = 
					(data._type === 'message') ? popupMessEval.templateMessage(data) : popupMessEval.templateEvaluation(data);
					//Changer l'icon
					//$scope.markers[data.objetID].icon.markerColor = 'orange-dark';
					//$scope.ovrirOffEtatDuParc();
					//$scope.updateCluster(data.objetID);
				}, 100);
			});
			/*Actualiser le popup a son etat du click*/
			$scope.$on('leafletDirectiveMarker.click', function(event, args){
				console.log('event click');
				if (args.model.hasOwnProperty('objetID'))
					$scope.markers[args.model.objetID].message = popupInfo.template($scope.dataMarkers[args.model.objetID]);
				//$scope.ouvrirOffPrpieteMarker(args.model.objetID);
			});	 

			//Recherche une adresse
			$scope.rechercheAdresse = function (){
				var geocoder = new self.google.maps.Geocoder();
			    var adresse =  $('#mapCAdresse form input').val();
			    	
			    var geo = function(results, status) {
			        /* Si l'adresse a pu être géolocalisée */
			        if (status == self.google.maps.GeocoderStatus.OK) {
			           	console.log(results[0].geometry.location.lat());
			            console.log(results[0].geometry.location.lng());

			            var lat = parseFloat(results[0].geometry.location.lat());
			            var lng = parseFloat(results[0].geometry.location.lng());
			            
			            $scope.centrerMapCoord(lat, lng);

			            if (!$scope.markers.hasOwnProperty('rechercheAdresse'))
				            $scope.markers['rechercheAdresse'] = {
								name: 'adresse',
		                   		lat: lat,
		                    	lng: lng,
		                    	message: adresse
							}
						else{
							$scope.markers['rechercheAdresse'].lat =  lat;
							$scope.markers['rechercheAdresse'].lng =  lng;
							$scope.markers['rechercheAdresse'].message = adresse;
						}
			        }
			        else
			        	 $('#mapCAdresse form input').val('Vous pouvez saisir une bonne adresse');
    			}
    			geo = geo.bind($scope);
			    geocoder.geocode( { 'address': adresse}, geo); 
				
				
			}

		}
	]);	
})();