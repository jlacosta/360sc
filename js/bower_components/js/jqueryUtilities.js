window.systemActionIdAngular = {
    evaluation: -1,
    alerte: -1,
    evaluation1: -1
};

window.systemTabs = {
    opened: ''
};

var ouvrirOffPrpieteMarker = function (id, quoi){
    console.log(quoi);
    fermerTousRightMenu();
    $('.off-canvas-wrap').removeClass('offcanvas-overlap-left');
    $('#markerList').removeClass('offcanvas-overlap-left');
    var str = '#markerShowProperties-'+id;
    $(str).removeClass('offcanvas-overlap-left');

    $('.off-canvas-wrap').toggleClass('offcanvas-overlap-left');
    $('#markerList').toggleClass('offcanvas-overlap-left');
    $(str).toggleClass('offcanvas-overlap-left');

    var str = "a[id='a_tab-"+id+"-"+quoi+"']"; 
    console.log(str);
    //console.log(str);
    $(str).trigger('click');
}

var ovrirOffEtatDuParc = function(){
    fermerTousRightMenu();
    $('.off-canvas-wrap').removeClass('offcanvas-overlap-left');
    $('#etatParc').removeClass('offcanvas-overlap-left');
    //Ouvrir et fermer le droit side-bar
    $('.off-canvas-wrap').toggleClass('offcanvas-overlap-left');
    $('#etatParc').toggleClass('offcanvas-overlap-left');
}

var prisEnCompteAlerte = function(actionID){
    window.systemActionIdAngular.alerte = actionID;
    console.log(actionID);
}

var prisEnCompteEvaluation = function(actionID){
    window.systemActionIdAngular.evaluation = actionID;
}

var prisEnCompteEvaluation1 = function(actionID){
    window.systemActionIdAngular.evaluation1 = actionID;
}

var prisEnCompteEvaluation_ = function(self){
    var actionId = $(self).attr('id').split('-')[1];
    prisEnCompteEvaluation(actionId);
}

var prisEnCompteEvaluation1_ = function(self){
    var actionId = $(self).attr('id').split('-')[1];
    prisEnCompteEvaluation1(actionId);
    console.log('evaluation');
}

var prisEnCompteAlerte_ = function(self){
    var actionId = $(self).attr('id').split('-')[1];
    prisEnCompteAlerte(actionId);   
}


var  fermerTousRightMenu = function(){
    $('.right-submenu ').each(function(index){
        if ($(this).hasClass('offcanvas-overlap-left'))
            $(this).removeClass('offcanvas-overlap-left');
    });
    $('.off-canvas-wrap').removeClass('offcanvas-overlap-left');
}

var appendBackAllElement = function(){
    $(".right-submenu .back").each(function(index){
        console.log(index);
        $(this).after("<li class='backAll'><a href='#' onclick='fermerTousRightMenu()'> \
        <span aria-hidden='true' class='glyphicon glyphicon-remove'></span></a></li>");
    });
}

var openCloseTabs = function(self){
    
    /*
    section#tab-?-?
    a_tab-?-?
    */

    var idElementClicked = $(self).attr('id').split('_')[1];
    var idMarker = idElementClicked.split('-')[1];
    var tabMarker = idElementClicked.split('-')[2];

    var sectionOpenedInfo = 'section#tab-'+idMarker+'-info';
    var sectionOpenedMessage = 'section#tab-'+idMarker+'-message';
    var sectionOpenedAerte = 'section#tab-'+idMarker+'-alerte';
    var sectionOpenedHistorique = 'section#tab-'+idMarker+'-historique';
    var sectionOpenedEvaluation = 'section#tab-'+idMarker+'-evaluation';
    var sectionOpenedMaintenance= 'section#tab-'+idMarker+'-maintanance';
    var parent, str;


    if ( $(sectionOpenedInfo).hasClass('active') ){
        $(sectionOpenedInfo).removeClass('active');
        str = '#a_tab-'+idMarker+'-info';
    }
    else if ( $(sectionOpenedMessage).hasClass('active') ){
        $(sectionOpenedMessage).removeClass('active');
        str = '#a_tab-'+idMarker+'-message';
    }
    else if ( $(sectionOpenedAerte).hasClass('active') ){
        $(sectionOpenedAerte).removeClass('active');
        str = '#a_tab-'+idMarker+'-alerte';
    }
    else if ( $(sectionOpenedHistorique).hasClass('active') ){
        $(sectionOpenedHistorique).removeClass('active');
        str = '#a_tab-'+idMarker+'-historique';
    }
    else if ( $(sectionOpenedEvaluation).hasClass('active') ){
        $(sectionOpenedEvaluation).removeClass('active');
        str = '#a_tab-'+idMarker+'-evaluation';
    }
    else if ( $(sectionOpenedMaintenance).hasClass('active') ){
        $(sectionOpenedMaintenance).removeClass('active');
        str = '#a_tab-'+idMarker+'-maintanance';
    }
    
    $(str).parent().removeClass('active');

    var sectionToOpen = 'section#'+idElementClicked;
    $(sectionToOpen).addClass('active');
    $(self).parent().addClass('active');
}

 /*Barre de recherche*/
$('#mapCAdresse').css({
    'position': 'absolute',
    'width': '0px',
    'left': '0px',
    'height': '0px',
    'display': 'none'
});


