angular.module('Demo.factory', 
['btford.socket-io'])
.factory('msocketIO', ['socketFactory', function(socketFactory){
	var myIoSocket = io.connect('ws://82.97.21.144:9000');

	mySocket = socketFactory({
		ioSocket: myIoSocket
	});

	return mySocket;
}]);