'use strict';

(function (window){
    angular.module('Demo', [
        'leaflet-directive',
        'Demo.factory',
        'Demo.filters',
        'Demo.templateService',
        'Demo.infoShowController',
        'Demo.markersController'
    ]);

    angular.module('Demo')
    .run(function ($rootScope, $http, $interval, $timeout, leafletData, msocketIO){

        /*Order By */
        $rootScope.predicate = 'createdTime';
        $rootScope.reverse = true;


        $rootScope.dataMarkers = {};
        $rootScope.actions = {};
        $rootScope.action = {};
        $rootScope.systemActions = {};

        $rootScope.changeImgSrc = function(id, src){
            var imgId = '#imgInfo-'+id;    
            $(imgId).attr('src', '/'+$rootScope.dataMarkers[id].image);
        }

        $rootScope.updateClusterIcon = function (id){

        }

        $rootScope.enregistrerMarkers = function (DataMarkers){
           for (var i = 0; i < DataMarkers.length; ++i){
                $rootScope.enregistrerMarker(DataMarkers[i]);
            }
        }
        $rootScope.enregistrerMarker = function (DataMarker){
            $rootScope.dataMarkers[DataMarker.objetID] = DataMarker;
        }
        $rootScope.actionfn = function (actions, data){
            var objetID = data.objetID;
            var actionsLen = actions.length;
            for (var i=0; i<actionsLen; ++i){
                var action = actions[i];
                if (!$rootScope.dataMarkers.hasOwnProperty(objetID)) return;

                if (!$rootScope.action.hasOwnProperty(action))
                    $rootScope.action[action] = {};
                if (!$rootScope.dataMarkers[objetID].hasOwnProperty(action))
                    $rootScope.dataMarkers[objetID][action] = [];  

                if (!$rootScope.action[action].hasOwnProperty(data.actionID)){
                    $rootScope.dataMarkers[objetID][action].push(data.actionID);
                }
                $rootScope.action[action][data.actionID] = data;
                $rootScope.systemActions[data.actionID] = data;
            }    
        }

        $interval(function(){
            if (window.systemActionIdAngular.alerte != -1){
                $rootScope.changePrisEnCompte(window.systemActionIdAngular.alerte, 'alerte');
                window.systemActionIdAngular.alerte = -1;
            }
            else if (window.systemActionIdAngular.evaluation != -1){
                $rootScope.changePrisEnCompte(window.systemActionIdAngular.evaluation, 'message');
                window.systemActionIdAngular.evaluation = -1;   
            }
            else if (window.systemActionIdAngular.evaluation1 != -1){
                $rootScope.changePrisEnCompte(window.systemActionIdAngular.evaluation1, 'evaluation');
                window.systemActionIdAngular.evaluation1 = -1;
            }
        }, 1000);


        /**/
        $rootScope.prisEnCompteEvaluation = function (e){
            console.log(e);
        }
        /*ChangePriseEnCompte*/
        $rootScope.changePrisEnCompte = function (actionID, action){

            console.log(actionID + ' ' + action);

            var inputPopup = '.leaflet-popup-content-wrapper #inputSwitch-' + actionID;
            var input = $(inputPopup).length;
            if (parseInt($rootScope.action[action][actionID].prisEnCompte) == 1){
                $rootScope.action[action][actionID].prisEnCompte = 0;
                $rootScope.action[action][actionID].prisEnCompteStr = '';
                $(inputPopup).prop('checked', 0);
            }
            else{
                $rootScope.action[action][actionID].prisEnCompte = 1;
                $rootScope.action[action][actionID].prisEnCompteStr = 'checked';
                $(inputPopup).prop('checked', 1);
            } 

            $rootScope.$broadcast('EVENT_MAP', parseInt($rootScope.action[action][actionID].objetID), 'null');
        }
        /**/
        

        /*****************
        ***HTTP Demande***
        ******************/

        /*Faire suivre les markeurs au MarkerController*/
        $http.get('demo/carte/markers')
        .then(function (response){
            //console.log(response.data);
            $rootScope.enregistrerMarkers(response.data);
            //console.log(response.data);
            $rootScope.$broadcast('EVENT_TOUS_MARKERS', response.data);

            $http.get('demo/carte/alertes')
            .then(function (response){
                //$rootScope.$broadcast('EVENT_TOUTES_ALERTES', response.data);
                var dataLen = response.data.length;
                var actions = [];
                var ids = [];
                var actionsValue = [];
                for (var i = 0; i < dataLen; ++i){     
                    var actions = ['alerte'];
                    if (response.data[i].messageAlerte != null){
                        actions.push('message');
                    }
                    response.data[i]._type = 'alerte';
                    response.data[i].prisEnCompte = parseInt(response.data[i].prisEnCompte);
                    if (response.data[i].prisEnCompte == 0)
                        response.data[i].prisEnCompteStr = '';
                    else
                        response.data[i].prisEnCompteStr = 'checked';
                    $rootScope.actionfn(actions, response.data[i]);
                    ids.push(response.data[i].objetID);
                    actionsValue.push(response.data[i]._type);
                }
                $rootScope.$broadcast('EVENT_MAP', ids, actionsValue);
            });
            $http.get('demo/carte/evalsMsg')
            .then(function (response){
                //$rootScope.$broadcast('EVENT_TOUTES_ALERTES', response.data);
                var dataLen = response.data.length;
                var actions = [];
                var ids = [];
                var actionsValue = [];
                for (var i = 0; i < dataLen; ++i){     
                    var actions = ['evaluation'];
                    if (response.data[i].messageEvaluation != null){
                        actions.push('message');
                    }
                    //console.log(response.data[i].prisEnCompte);
                    response.data[i]._type = response.data[i].messageEvaluation ? 'message' : 'evaluation';
                    $rootScope.actionfn(actions, response.data[i]);
                    ids.push(response.data[i].objetID);
                    actionsValue.push(response.data[i]._type);
                }
                $rootScope.$broadcast('EVENT_MAP', ids,  actionsValue);
            });
        });

        /*Faire suivre les alertes au MarkerController*/
        /*Verifier que l'objet qui a faire emis l'alerte et dans la carte*/
        /*$rootScope.qui = 'alerte';
        $interval(function(){
            if ($rootScope.qui == 'alerte'){
                $http.get('demo/carte/dernierAlerte')
                .then(function(response){
                    //console.log('Quantité d\'Alertes -> ' + response.data.length);
                    if (response.data.length != 0){    
                        var actions = ['alerte'];
                        if (response.data[0].messageAlerte != null)
                            actions.push('message');
                        $rootScope.actionfn(actions, response.data[0]);
                        response.data[0]._type = 'alerte';
                        response.data[0].prisEnCompte = parseInt(response.data[0].prisEnCompte);
                        $rootScope.$broadcast('EVENT_ALERTES', response.data[0]);
                    }
                });
                $rootScope.qui = 'message';
            }
            else{
                $http.get('demo/carte/dernierEvalsMsg')
                .then(function (response){
                    //console.log('Quantité d\'EvalsMsg -> ' + response.data.length);
                    if (response.data.length != 0){
                        var actions = ['evaluation'];
                        if (response.data[0].messageEvaluation != null)
                            actions.push('message');
                        $rootScope.actionfn(actions, response.data[0]);
                        response.data[0].prisEnCompte = parseInt(response.data[0].prisEnCompte); 
                        response.data[0]._type = response.data[0].messageEvaluation ? 'message' : 'evaluation';
                        $rootScope.$broadcast('EVENT_EVALUATIONS', response.data[0]);
                    }
                });
                $rootScope.qui = 'alerte';
            }    
        }, 1000);*/
        $rootScope.proxyActionsId = function (id){
            console.log(id);
            if ($rootScope.dataMarkers.hasOwnProperty(id)){
                //$rootScope.$apply();
                console.log(true);   
                return true;
            } 
            console.log(false);
            return false;
        }

        /*Socket.io - ws://82.97.21.144:9000*/
        msocketIO.on('alerte', function(data){
            //console.log(typeof data.message);
            //data = JSON.parse(data.message)[0];           
            data = data.message[0];
            console.log(data);
            if (data.hasOwnProperty('objetID') && $rootScope.proxyActionsId(data.objetID)){    
                var actions = ['alerte'];
                if (data.messageAlerte != null)
                    actions.push('message');
                $rootScope.actionfn(actions, data);
                data._type = 'alerte';
                data.prisEnCompte = parseInt(data.prisEnCompte);
                $rootScope.$broadcast('EVENT_ALERTES', data);
            }
        });
        msocketIO.on('evaluation', function(data){
            //data = JSON.parse(data.message)[0];
            data = data.message[0];
            console.log(data);
            if (data.hasOwnProperty('objetID') && $rootScope.proxyActionsId(data.objetID)){
                var actions = ['evaluation'];
                if (data.messageEvaluation != null)
                    actions.push('message');
                $rootScope.actionfn(actions,data);
                data.prisEnCompte = parseInt(data.prisEnCompte); 
                data._type = data.messageEvaluation ? 'message' : 'evaluation';
                $rootScope.$broadcast('EVENT_EVALUATIONS', data);
            }
        });

        /*******************
        ****Fn Generals*****
        *******************/
        /*Ouvrir l'off-canvas etat du parc*/
        $rootScope.ovrirOffEtatDuParc = function (){
            //Ouvrir l'off-canvas etat du parc
            /*if ($('.off-canvas-wrap').hasClass('offcanvas-overlap-left') && !$('#etatParc').hasClass('offcanvas-overlap-left'))
                $('.off-canvas-wrap').removeClass('offcanvas-overlap-left');*/
            $('.off-canvas-wrap').removeClass('offcanvas-overlap-left');
            $('#etatParc').removeClass('offcanvas-overlap-left');
            //Ouvrir et fermer le droit side-bar
            $('.off-canvas-wrap').toggleClass('offcanvas-overlap-left');
            $('#etatParc').toggleClass('offcanvas-overlap-left');
            /*if (!($('.off-canvas-wrap').hasClass('offcanvas-overlap-left') && 
                $('#etatParc').hasClass('offcanvas-overlap-left'))){
                $('.off-canvas-wrap').toggleClass('offcanvas-overlap-left');
                $('#etatParc').toggleClass('offcanvas-overlap-left');
            }*/
        }

        /*Ouvrir l'off-canvas marker*/
        $rootScope.ouvrirOffPrpieteMarker = function(id){
            console.log(id);
            $('.off-canvas-wrap').removeClass('offcanvas-overlap-left');
            $('#markerList').removeClass('offcanvas-overlap-left');
            var str = '#markerShowProperties-'+id;
            $(str).removeClass('offcanvas-overlap-left');

            $('.off-canvas-wrap').toggleClass('offcanvas-overlap-left');
            $('#markerList').toggleClass('offcanvas-overlap-left');
            $(str).toggleClass('offcanvas-overlap-left');
        }

        $rootScope.centreMap = function (actionID, type){
            
            console.log(type +' '+ actionID);

            var event_ = false;
            if (type == 'alerte')
                event_ = 'EVENT_ALERTES';
            else if (type == 'evaluation' || type == 'message')
                event_ = 'EVENT_EVALUATIONS';

            $rootScope.$broadcast(event_, $rootScope.action[type][actionID.toString()]);
        }

        $rootScope.centreMapMerker = function (id){
            $rootScope.$broadcast('EVENT_CENTRE_MARKER', id);
        }




    });
})(window);