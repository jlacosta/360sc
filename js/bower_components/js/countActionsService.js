(function (){
	angular.module('Demo.countActionsService', []);
	angular.module('Demo.countActionsService')
	.service('countActionsMarker', ['$rootScope', function ($rootScope){

		this.countAlerte = function (id){
			var q = 0;
			if ($rootScope.dataMarkers[id].hasOwnProperty('alerte')){
				var len = $rootScope.dataMarkers[id].alerte.length;
				alt = $rootScope.dataMarkers[id].alerte;
				for (var i=0; i<len; ++i){
					if (parseInt($rootScope.action.alerte[ alt[i] ].prisEnCompte) == 0)
						++q;
					//console.log($rootScope.action.alerte[ alt[i] ].prisEnCompte);
				}
			}
			return q;
		}
		this.countMessage = function (id){
			var q = 0;
			if ($rootScope.dataMarkers[id].hasOwnProperty('message')){
				var len = $rootScope.dataMarkers[id].message.length;
				/**/
				alt = $rootScope.dataMarkers[id].message;
				for (var i=0; i<len; ++i){
					//console.log($rootScope.action.message[ alt[i] ]);
					if (parseInt($rootScope.action.message[ alt[i] ].prisEnCompte) == 0)
						++q;
				}
			}
			return q;
		}

		
		this.countEvaluation = function (id){
			var q = 0;
			if ($rootScope.dataMarkers[id].hasOwnProperty('evaluation')){
				var len = $rootScope.dataMarkers[id].evaluation.length;
				/**/
				alt = $rootScope.dataMarkers[id].evaluation;
				for (var i=0; i<len; ++i){
					//console.log($rootScope.action.message[ alt[i] ]);
					if (parseInt($rootScope.action.evaluation[ alt[i] ].prisEnCompte) == 0)
						++q;
				}
			}
			return q;
		}

		this.totalActionPasPrisEnCompte = function(id){
			return this.countEvaluation(id) + this.countMessage(id) + this.countAlerte(id);
		}

		/*total*/
		this.evaluationData = function(id){
			var eval = {'total':0};
			var moyen = 0;
			if ($rootScope.dataMarkers[id].hasOwnProperty('evaluation')){
				var len = $rootScope.dataMarkers[id].evaluation.length;
				alt =  $rootScope.dataMarkers[id].evaluation;
				for (var i = len - 1; i >= 0; i--) {
					var niveau = $rootScope.action.evaluation[ alt[i] ].evaluation;
					if (!eval.hasOwnProperty(niveau)) {
						eval[niveau] = 0;
					}
					eval[niveau] += 1;
					eval['total']++;
				};
			}
			for (var i = Object.keys(eval).length - 1; i >= 0; i--) {
				moyen += eval[ Object.keys(eval)[i] ] * (i+1);
			}
			moyen = Math.round(moyen);
			if (moyen == 1) eval['img'] = 'smile-sad.svg';
			else if (moyen == 2) eval['img'] = 'smile-mid.svg';
			else if (moyen == 3) eval['img'] = 'smile-happy.svg';
			else eval['img'] = 'smile-happy.svg';
			return eval;
		}	
		this.countTotalAlerte = function (id){
			if ($rootScope.dataMarkers[id].hasOwnProperty('alerte'))
				return $rootScope.dataMarkers[id].alerte.length; 
			return 0.
		}
		this.countTotalMessage = function (id){
			if ($rootScope.dataMarkers[id].hasOwnProperty('message'))
				return $rootScope.dataMarkers[id].message.length; 
			return 0;
		}
		
	}])
	.service('countActionsSystemService', ['$rootScope', 'countActionsMarker', 
		function($rootScope, countActionsMarker){
			console.log('countActionsSystemService');
			this.countAlerte = function (){
				var actionAlerteLen = 0
				if ($rootScope.action.hasOwnProperty('alerte'))
					actionAlerteLen = Object.keys($rootScope.action['alerte']).length;
				return actionAlerteLen;
			}
			this.countMessage = function (){
				var actionMessageLen = 0;
				if ($rootScope.action.hasOwnProperty('message'))
					actionMessageLen = Object.keys($rootScope.action['message']).length;
				return actionMessageLen;		
			}
			this.countEvaluation = function (){
				var actionEvaluationLen = 0;
				if ($rootScope.action.hasOwnProperty('evaluation'))
					actionEvaluationLen = Object.keys($rootScope.action['evaluation']).length;
				return actionEvaluationLen;		
			}
			this.countAlerteAPrise = function (){
				var keysMarker = Object.keys($rootScope.dataMarkers);
				var len = keysMarker.length;
				var aPrise = 0;
				for (var i=0; i<len; ++i){
					aPrise += countActionsMarker.countAlerte(keysMarker[i]);
				}
				return aPrise;
			} 
			this.countMessageAPrise = function (){
				var keysMarker = Object.keys($rootScope.dataMarkers);
				var len = keysMarker.length;
				var aPrise = 0;
				for (var i=0; i<len; ++i){
					aPrise += countActionsMarker.countMessage(keysMarker[i]);
				}
				return aPrise;
			}
			this.countEvaluationAPrise = function (){
				var keysMarker = Object.keys($rootScope.dataMarkers);
				var len = keysMarker.length;
				var aPrise = 0;
				for (var i=0; i<len; ++i){
					aPrise += countActionsMarker.countEvaluation(keysMarker[i]);
				}
				return aPrise;
			}
			this.countEvaluationDataAPrise = function (){
				var keysMarker = Object.keys($rootScope.dataMarkers);
				var len = keysMarker.length;
				var dataEvaluation = {};
				var evaluationDataXMarker = {};

				for (var i = 0; i < len; ++i){
					var dataEvalMarker = {};
					dataEvalMarker = countActionsMarker.evaluationData(keysMarker[i]);
					//aki se puede guardar en una variable los valores;
					evaluationDataXMarker[keysMarker[i]] = dataEvalMarker;
					var evalKeys = Object.keys(dataEvalMarker);
					var lenn = evalKeys.length;
					for (var j = lenn - 1; j >= 0; j--) {
						if (!dataEvaluation.hasOwnProperty( evalKeys[j] )){
							dataEvaluation[ evalKeys[j] ] = 0;
						}
						if (!dataEvaluation.hasOwnProperty('total')) dataEvaluation['total'] = 0;
						dataEvaluation[ evalKeys[j] ]+= dataEvalMarker[ evalKeys[j] ];
						/*dataEvaluation['total'] += dataEvalMarker[ evalKeys[j] ];*/
					};
				}
				var result = {
					'general': dataEvaluation,
					'xMarker': evaluationDataXMarker
				}
				return result;
			}
			this.evaluationDataXMarker = function(){
				return JSON.parse(JSON.stringify(evaluationDataXMarker));
			}
		}
	]);
})();