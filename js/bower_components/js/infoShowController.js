angular.module('Demo.infoShowController', ['Demo.countActionsService']);
angular.module('Demo.infoShowController')
.controller('etatDuParkController', ['$rootScope', '$scope', 'countActionsSystemService',
function ($rootScope, $scope, countActionsSystemService){
	/*Hay que optimizar esto
	No se puede buscar que ha cambiado siempre, es mas efectivo 
	poner estas variables en el ScopeRoot y cuando cambie algo cambiarlas a ella
	Verificar esto, que pienso*/
	$scope.alertes = 0;
	$scope.messages = 0;
	$scope.evaluation = 0;
	$scope.evaluationData = {};
	$scope.evaluationDataXMarker = {};
	$scope.messageAPriseEncompte = 0;
	$scope.alerteAPriseEncompte = 0;
	$scope.evaluationAPriseEncompte = 0;
	$scope.couleurClocheVerte = {'color': "#95c11f"};
	$scope.couleurClocheGris = {'color': "#c8c6c7"};
	$scope.couleurClocheRouge = {'color': "#ff6155"};

	$scope.image = 'smile-happy.svg';
	$scope.evaluer = function (){
		var evalKeys = Object.keys($scope.evaluationData);
		var len = evalKeys.length;
		var total = 0;
		var moyen = 0;
		for (var i = len - 1; i >= 0; i--) {
			moyen = $scope.evaluationData[ evalKeys[i] ] * (i+1);
			total += $scope.evaluationData[ evalKeys[i] ] 
		};
		moyen = Math.round(moyen/total);
		if (moyen == 1) $scope.image = 'smile-sad.svg';
		if (moyen == 2) $scope.image = 'smile-mid.svg';
		if (moyen == 3) $scope.image = 'smile-happy.svg';
	}
	$scope.$watch(countActionsSystemService.countAlerte, function (newValue, oldValue){
		$scope.alertes = newValue;	
	});
	$scope.$watch(countActionsSystemService.countMessage, function (newValue, oldValue){
		$scope.messages = newValue;
	});
	$scope.$watch(countActionsSystemService.countEvaluation, function (newValue, oldValue){
		$scope.evaluation = newValue;
		var result = countActionsSystemService.countEvaluationDataAPrise();
		$scope.evaluationData = result['general'];
		$scope.evaluationDataXMarker = result['xMarker'];
		$scope.evaluer();
	});
	$scope.$watch(countActionsSystemService.countMessageAPrise, function (newValue, oldValue){
		$scope.messageAPriseEncompte = newValue;
	});
	$scope.$watch(countActionsSystemService.countAlerteAPrise, function (newValue, oldValue){
		$scope.alerteAPriseEncompte = newValue;
	});
	$scope.$watch(countActionsSystemService.countEvaluationAPrise, function (newValue, oldValue){
		$scope.evaluationAPriseEncompte = newValue; //Pour etat du Park, c'est un #
	});
	 /*Afficher la barre de recherche*/
	$scope.glyphiconSearchAdresse = function(){

		delete $rootScope.markers['rechercheAdresse'];
	    console.log('Icon');

	    $('#mapCAdresse').css({
	        'position': 'absolute',
	        'width': '350px',
	        'left': 'calc(50% - 175px)',
	        'height': '45px',
	        'display': '',
	        'margin-top': '10px'
	    });

	    $('#mapCAdresse form').toggle('slow', function(){
	        if ($('#mapCAdresse').attr('d') === 'o'){
	            $('#mapCAdresse').css({
	                'position': 'absolute',
	                'width': '0px',
	                'left': '0px',
	                'height': '0px',
	                'display': 'none'
	            });
	            $('#mapCAdresse').attr('d', 'd'); /*pour savoir si est la fn va montrer ou cacher*/
	        }
	        else
	            $('#mapCAdresse').attr('d', 'o'); /*pour savoir si est la fn va montrer ou cacher*/
	                       
	            /*if (!self.MarkerCherched) self.MarkerCherched = [];
	            if (self.MarkerCherched && self.MarkerCherched.length){
	                for (var i = 0; i < self.MarkerCherched.length; ++i) {
	                    self.map.removeLayer(self.MarkerCherched[i]);
	                };
	            }
	            self.MarkerCherched = [];*/
	    });
	}
}])
.controller('InfoTabController', ['$rootScope', '$scope', 'countActionsMarker', 'countActionsSystemService',
function($rootScope, $scope, countActionsMarker, countActionsSystemService){
	console.log('InfoTabController');
	$scope.data = {};
	/*$scope.listMessage = [];
	$scope.listAlerte = [];*/
	/*$scope.$watch(countActionsSystemService.countMessage, function(newValue, oldValue){
		var keysMarker = Object.keys($rootScope.dataMarkers);
		if ($rootScope.action.hasOwnProperty('message')){
			$scope.listMessage = [];
        	for (var i=0; i< Object.keys($rootScope.action.message).length; ++i)
            	$scope.listMessage.push($rootScope.action.message[ Object.keys($rootScope.action.message)[i] ]);
			//console.log($scope.listMessage);
		}
		if ($rootScope.action.hasOwnProperty('alerte')){
			$scope.listAlerte = [];
        	for (var i=0; i< Object.keys($rootScope.action.alerte).length; ++i)
            	$scope.listAlerte.push($rootScope.action.alerte[ Object.keys($rootScope.action.alerte)[i] ]);
		}
	});*/
	$scope.$watch(countActionsSystemService.countMessageAPrise, function(newValue, oldValue){
		var keysMarker = Object.keys($rootScope.dataMarkers);
		var len = keysMarker.length;
		for (var i=0; i<len; ++i){
			if (!$scope.data.hasOwnProperty(keysMarker[i]))
				$scope.data[ keysMarker[i] ] = {
						  //[Total, APrendEnCompte]	
				 	message: {message:0, totalMessage:0}, 
				 	alerte: {alerte:0, totalAlerte:0},
					evaluation: {}
				}
			$scope.data[ keysMarker[i] ].message.message = countActionsMarker.countTotalMessage(keysMarker[i]);
			$scope.data[ keysMarker[i] ].message.totalMessage =	countActionsMarker.countMessage(keysMarker[i]);
		}
	});
	$scope.$watch(countActionsSystemService.countAlerteAPrise, function(newValue, oldValue){
		var keysMarker = Object.keys($rootScope.dataMarkers);
		var len = keysMarker.length;
		for (var i=0; i<len; ++i){
			if (!$scope.data.hasOwnProperty(keysMarker[i]))
				$scope.data[ keysMarker[i] ] = {
						  //[Total, APrendEnCompte]	
				 	message: {message:0, totalMessage:0}, 
				 	alerte: {alerte:0, totalAlerte:0},
					evaluation: {}
				}
			$scope.data[ keysMarker[i] ].alerte.alerte = countActionsMarker.countTotalAlerte(keysMarker[i]);
			$scope.data[ keysMarker[i] ].alerte.totalAlerte =	countActionsMarker.countAlerte(keysMarker[i]);
		}
	});

	/*Maintenance*/
	$scope.mAns = 2015;
    $scope.mMois = 9;
    $scope.mJour = 17;
	$scope.fAns = 1;
    $scope.fMois = 2;
    $scope.fJour = 3;
    $scope.pDate = '';
    $scope.dd = function(){
        $scope.newDate = new Date('2015/09/17');
        if (typeof $scope.fJour != 'number') $scope.fJour = 0;
        if (typeof $scope.fMois != 'number') $scope.fMois = 0;
        if (typeof $scope.fAns != 'number') $scope.fAns = 0;

        $scope.quant = parseInt($scope.fAns*365) + parseInt($scope.fMois*31) + parseInt($scope.fJour);
        if ($scope.quant > 0)
        	$scope.newDate.setDate( $scope.newDate.getDate() + $scope.quant );
	    
	    $scope.pDate = $scope.newDate.toDateString();
        /*console.log($scope.pDate, $scope.quant,  $scope.fJour);
        console.log( $scope.fAns, $scope.fMois, $scope.fJour);*/
    }
    $scope.dd();
}]);