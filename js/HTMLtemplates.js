/*
marker.properties.id -> data.data.objetID
 */
get = (function (){
	var sectionAlerte = _.template(
		'<li attrId="<%= data.objetID %>">' +
			'<a id="<%= data.objetID %>" name="<%= data.objetID %>" lat="<%= data.objLat %>" lng="<%= data.objLong %>" href="#">' +
				'<%= data.name %>' +
			'</a>' +
		'</li>'
	);

	var notificationAlerte = function (marker){
		return sectionAlerte({data: marker.features.data});
	}

	/*Historique*/
	var sectionHistoriqueAlerte = _.template(
		'<li attrId="<%= data.objetID %>">' +
			'<label class="left" style="word-wrap: break-word; width:80%;"><%= d.name %></label>' +
			'<a href="#" class="right" onclick="alerte.activer(this)" name="alerte-<%= d.objetID %>">' +
				'<i class="fi-power right" style="margin-left: 7px; font-size: 20px;"></i>' +
			'</a>' +
		'</li>'
	);

	var historiqueAlerte = function (marker){
		return sectionHistoriqueAlerte({d: marker.features.data})
	}
	/*End Historique*/

	//Section filtre lorsque il y a plus option a afficher
	var sectionSelectPartieFiltre = _.template(
		'<select class="selectFilter"><%= p %></select>'
	);
	var sectionOptionPartieFiltre = _.template(
		'<option id="<%= d.objetID %>"value="<%= d.name %>"><%= d.name %>, <%= d.objetID %></option>'
	);
	var plusOptions = function (markers){
		var r='<option>Il y en a plus d\'un</option>';
		for (var i=0; i<markers.length; ++i){
			d = markers[i].features.data;
			r+=sectionOptionPartieFiltre({d:d});
		}	

		return sectionSelectPartieFiltre({p:r});
	}

	var sectionmarkers = _.template(
		'<li id="<%= d.objetID %>" class="has-submenu">' +
			'<a href="#" name="<%= d.objetID %>">' +
				'<%= d.name %>' + 
			'</a>' +
			'<ul id="propiete<%=d.objetID%>" class="right-submenu">' +
				'<li class="back">' +
					'<a href=#>Back</a>' +
				'</li>' +
				'<%= sectionPropietes %>' +
			'</ul>' +
		'</li>'		
	);

	var sectionPropietes = _.template(
		'<li>' +
			'<label> Name' + 
				'<li> <a href="#"><%= d.name %> </a> </li>' +
			'</label>' +
		'</li>' +
		'<li>' +
			'<label> ID' + 
				'<li> <a href="#"><%= d.objetID %> </a> </li>' +
			'</label>' +
		'</li>' +
		'<li>' +
			'<label> AlphaID' + 
				'<li> <a href="#"> <%= d.alphaID %> </a> </li>' +
			'</label>' +
		'</li>' +
		'<li>' +
			'<label> Adresse' + 
					'<li>' +	
						'<a href="#" onclick="fnFoundationSubMenu(this)" class="off-canvas-submenu-call">Changer Adresse <span class="left"> [+] </span></a>' +
					'</li>' +	
					'<ul class="off-canvas-submenu" style="display:none;"> ' +
						'<li> <form>' + 
								'Modifier : <input type="text"></input> <input type="submit"  class="button" name="pseudo"></input>' +
						'</form> </li>' + 
					'</ul>' +
			'</label>' +	
		'</li>' +
		'<li>' +
			'<label> Latitud' + 
				'<li> <a href="#"><%= d.objLat %> </a> </li>' +
			'</label>' +
		'</li>' +
		'<li>' +
			'<label>Longitud' + 
				'<li> <a href="#"><%= d.objLong %> </a> </li>' +
			'</label>' +
		'</li>' +
		'<li>' +
			'<label>Description' + 
				'<li> <a href="#"><%= d.description %> </a> </li>' +
			'</label>' +
		'</li>' +
		'<li>' +
			'<label>Appairage' + 
				'<li> <a href="#"><%= d.appairage %> </a> </li>' +
			'</label>' +
		'</li>' +
		'<li>' +
			'<label>Date Appairage' + 
				'<li> <a href="#"><%= d.dateAppairage %> </a> </li>' +
			'</label>' +
		'</li>' +
		'<li>' +
			'<label>Respo Appairage' + 
				'<li> <a href="#"><%= d.respoAppairage %> </a> </li>' +
			'</label>' +
		'</li>'
	);

	var popupContent = _.template(
		'<div id="popupContentNormal" style="width:150px;">'+
			'<div class="row" style="border-bottom-style: solid; border-bottom-color: #333333;">' +
				'<label class="left" style="word-wrap: break-word; width:100px;"><%= d.name %></label>' +
				'<a href="#" onclick="popup.showPropiete(this)" id="popupConfig" name="popup-<%= d.objetID %>">' +
					'<i class="fi-widget right" style="margin-left: 7px; font-size: 20px;"></i>' +
				'</a>' +
				'<a href="#" id="popupDeplacer" onclick="popup.deplacer(this)" name="popup-<%= d.objetID %>">' +	
					'<i class="fi-arrows-out right" style="font-size: 20px;"></i>' +
				'</a>' +	
				'<div class="small-8 large-8 columns"></div>' +
			'</div>' +
			'<br>' +
			'<table style="width:100%;">' +
			'<tdbody>' +
				'<tr>' +
					'<th>AlphaID</th>' +
				'</tr>' +
				'<tr>' +
					'<td><%= d.alphaID %></td>' +
				'</tr>' +
				'<tr>' +
					'<th>Type</th>' +
				'</tr>' +
				'<tr>' +
					'<td><%= d.description %></td>' +
				'</tr>' +
			'</tdbody>' + 
			'</table>' +	
		'</div>' +
		'<div id="popupContentDeplacement" class="hidePopup" style="width:150px;">'+
			'<div class="row" style="border-bottom-style: solid; border-bottom-color: #333333;">' +
				'<label class="left" style="word-wrap: break-word; width:100px;"><%= d.name %></label>' +
				'<a href="#" onclick="popup.cancel(this)" id="popupCancel" name="popup-<%= d.objetID %>">' +
					'<i class="fi-x right" style="margin-left: 7px; font-size: 20px;"></i>' +
				'</a>' +
				'<a href="#" id="popupCheck" onclick="popup.check(this)" name="popup-<%= d.objetID %>">' +	
					'<i class="fi-check right" style="font-size: 20px;"></i>' +
				'</a>' +	
				'<div class="small-8 large-8 columns"></div>' +
			'</div>' +
			'<br>' +
			'<table style="width:100%;">' +
			'<tdbody>' +
				'<tr>' +
					'<th>Adresse</th>' +
				'</tr>' +
				'<tr>' +
					'<td><%= d.adresse %></td>' +
				'</tr>' +
				'<tr>' +
					'<th>Latitude</th>' +
				'</tr>' +
				'<tr>' +
					'<td><%= d.objLat %></td>' +
				'</tr>' +
				'<tr>' +
					'<th>Longitud</th>' +
				'</tr>' +
				'<tr>' +
					'<td><%= d.objLong %></td>' +
				'</tr>' +
			'</tdbody>' + 
			'</table>' +	
		'</div>'
	);

	var marker = function (marker){
		data = marker.features.data;
		return sectionmarkers({d: data, sectionPropietes: sectionPropietes({d: data})})
	}

	var popupNormal = function (data){
		return popupContent({d: data});
	}

	return {
		historiqueAlerte: historiqueAlerte,
		marker: marker,
		popupNormal: popupNormal,
		notificationAlerte: notificationAlerte,
		plusOptions: plusOptions
	}

})();