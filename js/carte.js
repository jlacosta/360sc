self = this;
self.ByIdMarkers = {};
self.markers = [];
$(document).ready(function () {

    //L.mapbox.accessToken = 'pk.eyJ1IjoiamxhY29zdGEiLCJhIjoiMzg1MTkxNzM3ZmI2NzJkM2E5NzcyOTMwMWQ1MjU2NGEifQ.dYPfXlspQrxUmOiRP4bdFQ';
    //self.map = L.mapbox.map('map', null).setView([43.5333,  6.4667], 9);
    self.map = new L.Map('map', {center: new L.LatLng(43.5333,  6.4667), zoom: 9});
    /*var BingKey = "AlTq6EVcon6TNnja0zdhtpZOIqvNHsMLJaSdItdwSlX7sNYKhfdBotjP8clyPdDd";
    var layers = {
      StreetsBing: new L.BingLayer(BingKey, {type: 'Road'}) ,
      SatelliteBing: new L.BingLayer(BingKey, {type: 'Road'})
    };
    self.map.addLayer(layers.StreetsBing);*/

    var google = new L.Google('ROAD');
    self.map.addLayer(google);

    //L.control.layers(layers).addTo(map);
    L.control.scale().addTo(map);

    /*Barre de recherche*/
    $('#mapCAdresse').css({
        'position': 'absolute',
        'width': '0px',
        'left': '0px',
        'height': '0px',
        'display': 'none'
    });

    /*Afficher la barre de recherche*/
    $('#showHideForm i').click(function(){
        $('#mapCAdresse').css({
            'position': 'absolute',
            'width': '350px',
            'left': 'calc(50% - 175px)',
            'height': '45px',
            'display': ''
        });

        $('#mapCAdresse form').toggle('slow', function(){
            if ($('#mapCAdresse').attr('d') === 'o'){
                $('#mapCAdresse').css({
                    'position': 'absolute',
                    'width': '0px',
                    'left': '0px',
                    'height': '0px',
                    'display': 'none'
                });
                $('#mapCAdresse').attr('d', 'd'); /*pour savoir si est la fn va montrer ou cacher*/
            }
            else
                $('#mapCAdresse').attr('d', 'o'); /*pour savoir si est la fn va montrer ou cacher*/
                       
            if (!self.MarkerCherched) self.MarkerCherched = [];
            if (self.MarkerCherched && self.MarkerCherched.length){
                for (var i = 0; i < self.MarkerCherched.length; ++i) {
                    self.map.removeLayer(self.MarkerCherched[i]);
                };
            }
            self.MarkerCherched = [];
        });
    });
    /*Chercher le coord a partir d'une adresse*/
    $('#mapCAdresse form a').click(function(){
        var geocoder = new self.google.maps.Geocoder();
        var adresse =  $('#mapCAdresse form input').val();
        geocoder.geocode( { 'address': adresse}, function(results, status) {
            /* Si l'adresse a pu être géolocalisée */
            if (status == self.google.maps.GeocoderStatus.OK) {
                console.log(results[0].geometry.location);
                var mapboxLatlng = L.latLng(results[0].geometry.location.lat(),  results[0].geometry.location.lng());
                self.map.panTo(mapboxLatlng);
                var marker = new L.marker(mapboxLatlng);
                self.MarkerCherched.push(marker);
                self.map.addLayer(marker);
                centreMarkerByLatLng(marker.getLatLng());
            }
        }); 
    });
    /*End barre de recherche*/

    self.leafletView = new PruneClusterForLeaflet();

    self.leafletView.PrepareLeafletMarker = function (marker, data, category) {
        if (!marker.features) 
            marker.features = {};
        /*changer le car peut etre qu'il y ait une variable features definie*/
        marker.features = data.features;
        self.ByIdMarkers[marker.features.objetID].leafletMarker = marker;
        if (data.icon) {
            if (typeof data.icon === 'function') {
                marker.setIcon(data.icon(data, category));
            }
            else {
                marker.setIcon(data.icon);
            }
        }
        if (data.popup) {
            var content = typeof data.popup === 'function' ? data.popup(data, category) : data.popup;
            if (marker.getPopup()) {
                marker.setPopupContent(content, data.popupOptions);
            }
            else {
                marker.bindPopup(content, data.popupOptions);
            }
        }
        marker.on('click', function(e){
            this.openPopup();
            popup.addClassEtat(this.features.objetID);
        });        
    }

    self.leafletView.BuildLeafletClusterIcon = function(cluster) {
        var e = new L.Icon.MarkerCluster();

        e.stats = cluster.stats;
        e.population = cluster.population;
        return e;
    };

    var colors = ['#ff4b00', '#bac900', '#EC1813', '#55BCBE', '#D2204C', '#FF0000', '#ada59a', '#3e647e'],
        pi2 = Math.PI * 2;

    L.Icon.MarkerCluster = L.Icon.extend({
        options: {
            iconSize: new L.Point(48, 54),
            className: 'prunecluster leaflet-markercluster-icon'
        },

        createIcon: function () {
            // based on L.Icon.Canvas from shramov/leaflet-plugins (BSD licence)
            var e = document.createElement('canvas');
            this._setIconStyles(e, 'icon');
            var s = this.options.iconSize;
            e.width = s.x;
            e.height = s.y;
            this.draw(e.getContext('2d'), s.x, s.y);
            return e;
        },

        createShadow: function () {
            return null;
        },

        draw: function(canvas, width, height) {

            var lol = 0;

            var start = 0;
            for (var i = 0, l = colors.length; i < l; ++i) {

                var size = this.stats[i] / this.population;


                if (size > 0) {
                    size = 0.9;
                    canvas.beginPath();

                    var angle = Math.PI/4*i;
                    var posx = Math.cos(angle) *18, posy = Math.sin(angle) *18;


                    var xa = 0, xb = 1, ya = 4, yb = 8;

                    // var r = ya + (size - xa) * ((yb - ya) / (xb - xa));
                    var r = ya + size * (yb - ya);


                    //canvas.moveTo(posx, posy);
                    ex = 27+posx;
                    ey = 27+posy;
                    canvas.arc(ex, ey, r, 0, pi2);
                    canvas.fillStyle = colors[i];
                    canvas.fill();
                    canvas.closePath();

                    canvas.fillStyle = 'black';
                    canvas.textAlign = 'center';
                    canvas.textBaseline = 'middle';
                    canvas.font = 'bold 12px sans-serif';

                    canvas.fillText((this.stats[i]>5)?'5+':this.stats[i], ex, ey, 48);
                }

            }

            canvas.beginPath();
            canvas.fillStyle = 'white';
            canvas.arc(24, 24, 16, 0, Math.PI*2);
            canvas.fill();
            canvas.closePath();

            canvas.fillStyle = 'black';
            canvas.textAlign = 'center';
            canvas.textBaseline = 'middle';
            canvas.font = 'bold 12px sans-serif';

            canvas.fillText(this.population, 24, 24, 48);
        }
    });

    /**/
    var upCarte = function (){
        /*Succes Ajax, Adresse=carte/markers*/
        var successAjaxGetMarkers = function( data, textStatus, jqXHR ) { 
            for (var i=0; i<data.length; ++i){
                var marker = new PruneCluster.Marker();
                marker = configMarker(marker, data[i], i);
                /*Ajouter a la list de marker -> side-bar rigth*/
                $('#listMarker').append(get.marker(marker));
                self.markers.push(marker);
                self.ByIdMarkers[marker.features.id] = marker;
                self.leafletView.RegisterMarker(marker);
            }
            
            self.leafletView.ProcessView();
            /*Ajouter les alertes */
            alerte.run();
            /*Ajouter les popups*/
            popup.run();
            
            return true;
        }
        successAjaxGetMarkers = successAjaxGetMarkers.bind(self);
        requestAjax.getJson('carte/markers/', successAjaxGetMarkers);
    }


    upCarte();

    self.map.addLayer(leafletView);

    /*Config fn onclick sur bouton id=rechercher*/
    $('#rechercher').click(function(){
        if ($('#filterById').val()){
            var marker = filters.trouverById( $('#filterById').val() );
            if (marker)
                centreMarkerByMarker(marker);
        }
        else if ($('#filterByName').val()){
            var markers = filters.trouverByName( $('#filterByName').val() );
            if (markers)
                if (markers.length > 1)
                    $('#filterByName').parent().append(get.plusOptions(markers));
                else
                    centreMarkerByMarker(markers[0]);    
        }
        else if ($('#filterByAdresse').val())
            var marker = filters.trouverByAdress( $('#filterByAdresse').val() );
    });

});