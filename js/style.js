var badalert = function(){
	$("#echec").fadeIn(1500);
};

var goodalert = function(){
	$("#success").fadeIn(1500);
};

$('#success .close').on('click', function () {
	$("#success").fadeOut(1500);
});

$('#echec .close').on('click', function () {
	$("#echec").fadeOut(1500);
});

$("tr.tag-id").on('click', function(){
	var url1 = get_link();
	var id = $(this).attr("id");
	var url = ""+url1+"edit/" + id;
	$(location).attr('href',url);
});

$("tr.objet-id").on('click', function(){
	var url1 = get_link();
	var id = $(this).attr("id");
	var url = ""+url1+"objet/" + id;
	$(location).attr('href',url);
});

$("tr.del-t").on('click', function(){
	var url1 = get_link();
	var id = $(this).attr("id");
	var url = ""+url1+"delete-tag/" + id;
	$(location).attr('href',url);
});

var remove = function (arr, what) {
    var found = arr.indexOf(what);

    while (found !== -1) {
        arr.splice(found, 1);
        found = arr.indexOf(what);
    }
}

var selected = [];
$("tr.tag-select").on('click', function(){
	var id = $(this).attr("id");
	
	if($(this).hasClass("selected")){
		$(this).removeClass("selected");
		remove(selected, id);
		$(this).children("td:first-child").children("input").attr("checked",false);
	} else {
		selected.push(id);
		$(this).addClass("selected");
		$(this).children("td:first-child").children("input").attr("checked",true);
	}
	
});

$("#checkall").on('click', function(){
	$("tr.tag-select").each(function(){
		$(this).click();
	});
});