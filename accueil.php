<?php 
/** 
* Home Page
* @author: ROUMEGUE Jérémy - 360sc
* We include the specific header for home page.
*/
include "./include/head.php";
?>
		<div class="panel" id="map"></div>
		<div class="row" id="mapCAdresse">
			<form style="height:90%; width:100%; display:none;">		    
          		<input style="margin-bottom: 0px; width:60% !important; float:left; height: 100% !important; display:inline !important;" type="text" placeholder="Chercher une Adresse">
         		<a style="margin-bottom: 0px; width: 40% !important; float:left; height: 100% !important; text-align: center;" href="#" class="button postfix">
         			Chercher
         		</a>
		    </form>
		</div>
	</div>
	  
	<?php
	/**
	* Check if was a previous operation.
	*/
		if(isset($_SESSION['success'])){
			if($_SESSION['success']){
			/**
			* If operation and succes
			*/
	?>
		<script>
		$(document).ready(function(){
			goodalert();
		});</script>
	<?php
			} else {
			/**
			* If operation and fail.
			*/
	?>
		<script>$(document).ready(function(){
			badalert();
		});</script>
	<?php
			}
			/**
			* Reset success
			*/
			$_SESSION['success'] = null;
		}
	?>
<?php
/**
* We include the specific footer for home page.
*/
include "./include/footer.php";
?>