var clientMap = {
	map: null,
	data: null,
	maxZoomCluester: 18,
	maxZoomPanToEvent: 19,
	maxZoomPanToInfoWindow: 19,
	historyNotactiveObjects: null,
	generalinfoWindow: null,
	boundMapCenter:  new google.maps.LatLngBounds(),
	dependency: {
		objects: false,
		events: false
	},
	markers: [],
	positionMarkers: {},
	markerCluster: null,
	imageIcons: {
	},

	clusterStyles: [
		{
			//textColor: 'white',
			url: './build/img/marker-cluster/m1.png',
			height: 53,
			width: 53
		},
		{
			//textColor: 'white',
			url: './build/img/marker-cluster/m2.png',
			height: 56,
			width: 56
		},
		{
			//textColor: 'white',
			url: './build/img/marker-cluster/m3.png',
			height: 66,
			width: 66
		}
	],

	optionsMarkerCluster: {
		styles: null,
		maxZoom: null
	},

	States: States,

	initMap: function(){
		this.map = new google.maps.Map(document.getElementById('map'), {
		    center: {lat: 43.5333, lng: 6.4667},
		    zoom: 6
		});
		this.generalinfoWindow = new google.maps.InfoWindow({maxWidth: '250'});
		//utils.ZoomControl(this.map);
		google.maps.event.addListener(this.generalinfoWindow, 'domready', function() {
			utils.resizeInfoWindow();
			console.log('domready initMap');
		});
		// event to close the infoWindow with a click on the map
		google.maps.event.addListener(this.map, 'click', function() {
			clientMap.generalinfoWindow.close();
		});
	},

	newMarkers: function(data){
		utils.addData(data);
		this.dependency.objects = true;
		//if (this.dependency.events)
			this.addToMap();
	},

	addToMap: function(){
		//console.time('addToMap');
		var keys = utils.getKeysFrom(this.data);
		var lenKeys = keys.length;
		var marker = null;

		this.nowDate = ReactBrigde.momentClass();
		console.log(this.nowDate.format('D-M-Y'));
		this.filterStreetSignDefault = new ReactBrigde.filters.streetSign({
			initialDate: this.nowDate,
  			finalDate: ReactBrigde.momentClass().add(12, 'month')
		});

		for (var datai = 0; datai < lenKeys; ++datai){
			var info = this.data[ keys[datai] ];

			if (isNaN(info.objLat) || !parseInt(Number(info.objLat)) ||
				isNaN(info.objLong) || !parseInt(Number(info.objLong)) ){
				delete this.data[ keys[datai] ];
				continue;
			}

			/*A change dans la nouvelle version*/
			if (info.urgence == null || info.urgence == 'undefined')
				info.urgence = false;
			/*End - A change*/

			/*Street Sign*/
			if(typeof info.dateStreetSign !== "undefined" &&
		   	!isNaN(info.dateStreetSign) &&
		   	parseInt(Number(info.dateStreetSign)) == info.dateStreetSign
		   	&& info.dateStreetSign > 0){
				marker = this.createMarkerByStatus(info);
			}
			else
				marker = this.createMarker(info);

			this.calculateMapCentre(marker);

			/*Que pour cet version*/
			if (info.idParent < 41 || info.idParent > 43)
				marker = this.setIcon(marker, info.urgence);
			
			var fn = function(marker, info) {
		        if (this.generalinfoWindow !== null)
					this.generalinfoWindow.close();

				var React =  ReactBrigde.reactClass.React;
				var ReactDOMServer =  ReactBrigde.reactClass.ReactDOMServer;
				var ReactDOM =  ReactBrigde.reactClass.ReactDOM;
				var MarkerInfoWindow = ReactBrigde.reactComponent.MarkerInfoWindow;
				
				content = "<div id='react-popup'> </div>";
				this.map.panTo(new google.maps.LatLng( marker.getPosition().lat(), marker.getPosition().lng() ));
				this.map.setZoom(this.maxZoomPanToEvent);
				this.generalinfoWindow.setContent(content);
				this.generalinfoWindow.open(this.map, marker);
				ReactDOM.render(React.createElement(MarkerInfoWindow, {'info': info}), 
						document.getElementById('react-popup'));
		    };

			google.maps.event.addListener(marker, 'click', fn.bind(this, marker, info));

			this.markers.push(marker);
			this.positionMarkers[ keys[datai] ] = this.markers.length - 1;
		}
		this.createClusterMarkers();

		this.map.setCenter(this.boundMapCenter.getCenter());
		this.map.fitBounds(this.boundMapCenter);
		
		/*I think that when the MapUtils emit the event, there is a conflit with the update component 
		of React, with FilterStreetComponent.
			.The setTimeout is for waits that the render of component FilterStreetComponent 
			finish the render
			.This setTimeout is for update the inital values of the component MaintenanceComponent
		*/
		setTimeout(function(){
			utils.MapUtils.emit(utils.MapUtils.callbackNames.streetSign, valueExt.initialStreetSignValues);
		}, 1500);
	},

	calculateMapCentre: function(marker){
		this.boundMapCenter.extend( new google.maps.LatLng
			(marker.getPosition().lat(), marker.getPosition().lng()) );
	},

	createMarker: function(info){
		var objLat = parseFloat(info.objLat);
		var objLong = parseFloat(info.objLong);
		var LatLng = new google.maps.LatLng(objLat, objLong);

		return new google.maps.Marker({
		    position: LatLng,
		    title: info.name,
		    icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
		    objectID: info.objetID
		});
	},

	createMarkerByStatus: function(info){
		var objLat = parseFloat(info.objLat);
		var objLong = parseFloat(info.objLong);
		var LatLng = new google.maps.LatLng(objLat, objLong);

		var status = this.filterStreetSignDefault.check(info);
		var groupIcon = null;

		if(valueExt.initialStreetSignValues.hasOwnProperty(status.state))
			valueExt.initialStreetSignValues[status.state]++
		else
			valueExt.initialStreetSignValues[status.state] = 1;

		if (info.idParent == '41') groupIcon = 'SIGNAL-STOP';
		if (info.idParent == '42') groupIcon = 'SIGNAL-DANGER';
		if (info.idParent == '43') groupIcon = 'SIGNAL-SENS-INTERDIR';

		return new google.maps.Marker({
	    	position: LatLng,
		    title: info.name,
		    icon: './build/img/markers-icon/' + groupIcon + '-' + status.icon + '.png',
		    objectID: info.objetID,
		    dateStreetSign: info.dateStreetSign,
		    state: status.state,
		    groupIcon: groupIcon,
		    dateActive: info.dateActive,
		    streetSignUrgence: (status.valueState & 6) ? true : false
		});
	},

	setIcon: function(marker, urgence){
		if (urgence != false) { // active == true
			//console.log(marker.title);
			marker.setIcon('./build/img/markers-icon/markers_default-a.png');
			return marker
		}
		else{ // active == false
			marker.setIcon('./build/img/markers-icon/markers_default-c.png');
			return marker;		
		}
	},

	setIconByStatus: function(marker, status){
		var icon = status.icon;
		var groupIcon = marker.groupIcon;
		marker.setIcon('./build/img/markers-icon/' + groupIcon + '-' + icon + '.png' );
		return marker;
	},

	setStyles: function(options){
	},

	createClusterMarkers: function(){
		// Init styles clusters
		this.optionsMarkerCluster.maxZoom = 18;
		this.optionsMarkerCluster.styles = this.clusterStyles;

		console.log(this.optionsMarkerCluster);
		this.markerCluster = new MarkerClusterer(this.map, 
			this.markers, 
			this.optionsMarkerCluster
		);
		
		this.markerCluster.setCalculator(this.newCalculator);
		console.log(this.markerCluster.getStyles());
	},

	newCalculator: function(markers, numStyles) {
		var index = 0;
		var count = markers.length;
	
		var group_active = false;
		for(var m=0; m<markers.length; ++m){
			if (clientMap.data[ markers[m].objectID ].urgence == 1 || clientMap.data[ markers[m].objectID ].urgence == true){
				group_active = true;
				break;
			}
			/*Fake*/
			if (markers[m].streetSignUrgence){
				group_active = true;
				break;
			}
		}

		if (group_active) index = 3;
		else index = 1;

		return {
			text: count,
			index: index
		};
	},

	newEventStream: function (evnt){
		/*Amelioration: 
		1- Suprimer le marker de markerCluster tant que l'urgence change*/

		var pos = clientMap.positionMarkers[evnt.objetID];
		var m = clientMap.markers[pos];

		clientMap.markerCluster.removeMarker(m);

		clientMap.markers[pos] = null;

		utils.processNewEventStream(evnt);

		evnt;

		var newM = clientMap.createMarker(this.data[evnt.objetID]);
		newM = clientMap.setIcon(m, clientMap.data[evnt.objetID].urgence);

		clientMap.markers.push(newM);
		clientMap.positionMarkers[evnt.objetID] = clientMap.markers.length - 1;

		clientMap.markerCluster.addMarker(newM);

		clientMap.defaultProcess(newM, evnt);

		clientMap.addedProcess(newM, evnt);
	},

	newEventsHistory: function(notactive){
		this.dependency.events = true;
		utils.addNotactive(notactive);
		if (this.dependency.objects)
			this.addToMap();
	},

	defaultProcess: function(){
	},

	addedProcess: function(marker, evnt){
		if (evnt.hasOwnProperty('infoData'))  //Suprimer aprés - Fake
			if (evnt.infoData.prisEnCompte) {
				this.closeInfoWindowBy(evnt);	
				return;
			}

		if (this.generalinfoWindow !== null)
			this.generalinfoWindow.close();

		this.map.setZoom(this.maxZoomPanToEvent);

		setTimeout(function(){
			this.map.panTo(new google.maps.LatLng( marker.getPosition().lat(), marker.getPosition().lng() ));
		}.bind(this), 100);

		setTimeout(function(){
			var React =  ReactBrigde.reactClass.React;
			var ReactDOMServer =  ReactBrigde.reactClass.ReactDOMServer;
			var ReactDOM =  ReactBrigde.reactClass.ReactDOM;
			var AlertInfoWindowComponent = ReactBrigde.reactComponent.AlertInfoWindowComponent;
			var EvalMsgInfoWindowComponent = ReactBrigde.reactComponent.EvalMsgInfoWindowComponent;
			var EvalInfoWindowComponent = ReactBrigde.reactComponent.EvalInfoWindowComponent;
			
			var adresse = this.data[ evnt.objetID ].adresse;
			var objetImage = this.data[ evnt.objetID ].image;
			evnt.infoData['adresse'] = adresse;
			evnt.infoData['objetImage'] = objetImage;

			content = "<div id='react-popup'> </div>";
			this.generalinfoWindow.setContent(content);		
			this.generalinfoWindow.open(this.map, marker);

			if (evnt.typ == 'alert'){
				ReactDOM.render(React.createElement(AlertInfoWindowComponent, {'info': evnt.infoData}), 
					document.getElementById('react-popup'));
			}else if (evnt.typ == 'evalmsg'){
				ReactDOM.render(React.createElement(EvalMsgInfoWindowComponent, {'info': evnt.infoData}), 
					document.getElementById('react-popup'));
			}else{
				ReactDOM.render(React.createElement(EvalInfoWindowComponent, {'info': evnt.infoData}), 
					document.getElementById('react-popup'));
			}

			
			utils.resizeInfoWindow();
		}.bind(this), 700);
	},

	toCenter: function(options){
	},

	openInfoWindow: function(options){
		var t = this;
		if (clientMap.generalinfoWindow !== null)
			clientMap.generalinfoWindow.close();

		clientMap.map.setZoom(this.maxZoomPanToInfoWindow);

		var marker = options.marker;
		var latlng = new google.maps.LatLng(
			marker.getPosition().lat(),
			marker.getPosition().lng()
		);

		var fn = function(latlng){
			return function(){
				clientMap.map.panTo(latlng);
			}.bind(t)
		};

		setTimeout(fn(latlng), 100);

		setTimeout(function(){
			clientMap.generalinfoWindow =  new google.maps.InfoWindow({maxWidth: '250'}); 
			google.maps.event.addListener(this.generalinfoWindow, 'domready', function() {
				utils.resizeInfoWindow();
				console.log('domready openInfoWindow');
			});


			var React =  ReactBrigde.reactClass.React;
			var ReactDOM =  ReactBrigde.reactClass.ReactDOM;

			
			content = "<div id='react-popup'> </div>"
			clientMap.generalinfoWindow.setContent(content);
			clientMap.generalinfoWindow.open(clientMap.map, marker);
			ReactDOM.render(React.createElement(options.component, {'info': options.infoData}), 
					document.getElementById('react-popup'));		
		}.bind(this), 700);		
	},

	closeInfoWindowBy: function(evnt){
		var className = '.';
		switch(evnt.typ){
			case 'alert':
				className += 'AlertInfoWindowComponent';
				break
			default:
				className += 'EvalMsgInfoWindowComponent';
			break;	
		}
		if ($(className).length)
			clientMap.generalinfoWindow.close();
	},

	markerChangeStatus: function(marker, status){
		/*console.log(marker.objectID + '-' + status.state);*/

		if (status.state == 'street-sign-default' ||
			status.state == 'street-sign-notif' ||
			status.state == 'street-sign-alert' || 
			status.state == 'hide'){
			this.States.states[status.state](marker, status);
			return {
				state: status.state,
				value: 1
			}
		}
		return false;
	}
}

var utils = {
	getKeysFrom: function(data){
		return Object.keys(data);
	},

	addData: function(data){
		clientMap.data = data;
	},

	addNotactive: function(data){
		clientMap.historyNotactiveObjects = data;
	},

	getValueByDefault: function(from, key, byDefault){
		if (!from.hasOwnProperty(key)) return byDefault;
		return from[key];
	},

	processNewEventStream: function(evnt){
		var info = clientMap.data[evnt.objetID];

		/*if (evnt.prisEnCompte == 0 || evnt.prisEnCompte == false){ // si c'est pas prisEnCompte
			info.niveauUrgence++;
			info.urgence = true;
		}
		else{
			info.niveauUrgence--;
			if (!info.niveauUrgence)
				info.urgence = false;
		}*/

		/*clientMap.data[evnt.objetID] = info;*/
	},

	openInfoWindow: function(component, objectID, infoEvent){
		var objInfo = clientMap.data[objectID];
		var LatLng = new google.maps.LatLng(objInfo.objLat, objInfo.objLong);
		var pos = clientMap.positionMarkers[objectID];
		var marker = clientMap.markers[pos];
		var options = {
			component: component,
			marker: marker,
			LatLng: LatLng,
			infoData: infoEvent 
		}
		clientMap.openInfoWindow(options);
	},

	resizeInfoWindow: function(){
		var parent = $('.gm-style-iw').parent();
		var wrapPopup = $(parent).find('div:first');
		var transparentBackgroud = $(wrapPopup).find('div:nth-child(2)');
		var whiteBackgroud = $(wrapPopup).find('div:nth-child(4)');
		var myPopup = $('.gm-style-iw');

		if(!$(whiteBackgroud).hasClass('gm-white-layer'))
			$(whiteBackgroud).addClass('gm-white-layer');
		if (!$(transparentBackgroud).hasClass('gm-transparent-layer'))
			$(transparentBackgroud).addClass('gm-transparent-layer');
		if(!$(parent).hasClass('gm-parent-layer'))
			$(parent).addClass('gm-parent-layer');
	},

	ZoomControl: function(map) {
		var zoomControlDiv = document.createElement('div');
		controlDiv = zoomControlDiv;
		// Set CSS for the control border.
		var controlUI = document.createElement('div');
		controlUI.style.top = '9.5px';
		controlUI.style.left = '-5px';
		controlUI.style.position = 'absolute';

		controlUI.style.backgroundColor = '#fff';
		controlUI.style.border = '2px solid #fff';
		controlUI.style.borderRadius = '3px';
		controlUI.style.borderRadius = '3px';
		controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
		controlUI.style.cursor = 'pointer';
		controlUI.style.marginBottom = '0px';
		controlUI.style.textAlign = 'center';
		controlUI.title = 'reset_center';
		controlDiv.appendChild(controlUI);

		// Set CSS for the control interior.
		var controlText = document.createElement('div');
		controlText.style.color = 'rgb(25,25,25)';
		controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
		controlText.style.fontSize = '11px';
		controlText.style.lineHeight = '27px';
		controlText.style.paddingLeft = '5px';
		controlText.style.paddingRight = '5px';
		controlText.innerHTML = '****';
		controlUI.appendChild(controlText);

		controlUI.addEventListener('click', function() {
		   this.map.setCenter(this.boundMapCenter.getCenter());
		   this.map.fitBounds(this.boundMapCenter);
		}.bind(clientMap));

		map.controls[google.maps.ControlPosition.TOP_LEFT].push(zoomControlDiv);
	},

	filter: function(type, filter){
		// Do this action async (to do)
		if (type == 'StreetSign'){
			var list = {};
			var markers = clientMap.markers;
			var data = clientMap.data;
			var len = markers.length;
			var status = null;
			var marker = null;
			var r = {};
			while(len--){
				marker = markers[ len ];
				if (!marker) continue;
				status = filter.check( data[ marker.objectID ] );
				r = clientMap.markerChangeStatus(marker, status);
				if (!r) continue;
				if (list.hasOwnProperty(r.state)) list[ r.state ]++;
				else list[ r.state ] = 1;
			}
			utils.MapUtils.emit(utils.MapUtils.callbackNames.streetSign, list);
			console.log('*******************');
			console.log('*******************');
		}
	}

}

valueExt = {
	initialStreetSignValues: {

	}
}