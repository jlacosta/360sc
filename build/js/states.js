var StatesCls = function(){

	this._undefined = function(marker, status){ console.log('undefined'); return false; },

	this._uncheck = function(marker, status){ console.log('uncheck'); return false; },

	this._hide = function(marker, status){ 
		console.log('hide');

		console.log(marker.objectID + 'hide');

		var pos = clientMap.positionMarkers[marker.objectID];
		var m = clientMap.markers[pos];
		var objectID = marker.objectID;

		clientMap.markerCluster.removeMarker(m);
		clientMap.markers[pos] = null;
		
		var newM = clientMap.createMarker(clientMap.data[objectID]);
		newM = clientMap.setIconByStatus(marker, status);

		clientMap.markers.push(newM);
		clientMap.positionMarkers[objectID] = clientMap.markers.length - 1;

		return false; 
	},

	/*Create a function for wrap the process*/
	this._streetSignDefault = function(marker, status){
		console.log(marker.objectID + '::' + marker.dateActive + '::' + marker.dateStreetSign + ' default');

		var pos = clientMap.positionMarkers[marker.objectID];
		var m = clientMap.markers[pos];
		var objectID = marker.objectID;

		clientMap.markerCluster.removeMarker(m);
		clientMap.markers[pos] = null;

		var newM = clientMap.createMarker(clientMap.data[objectID]);
		newM = clientMap.setIconByStatus(marker, status);
		newM.streetSignUrgence = false;

		clientMap.markers.push(newM);
		clientMap.positionMarkers[objectID] = clientMap.markers.length - 1;

		clientMap.markerCluster.addMarker(newM);
	},

	this._streetSignNotif = function(marker, status){ 
		console.log(marker.objectID + '::' + marker.dateActive + '::' + marker.dateStreetSign + ' notif');

		var pos = clientMap.positionMarkers[marker.objectID];
		var m = clientMap.markers[pos];
		var objectID = marker.objectID;

		clientMap.markerCluster.removeMarker(m);
		clientMap.markers[pos] = null;

		var newM = clientMap.createMarker(clientMap.data[objectID]);
		newM = clientMap.setIconByStatus(marker, status);
		newM.streetSignUrgence = true;

		clientMap.markers.push(newM);
		clientMap.positionMarkers[objectID] = clientMap.markers.length - 1;

		clientMap.markerCluster.addMarker(newM); 
	},

	this._streetSignAlert = function(marker, status){
		console.log(marker.objectID + '::' + marker.dateActive + '::' + marker.dateStreetSign + ' alert');

		var pos = clientMap.positionMarkers[marker.objectID];
		var m = clientMap.markers[pos];
		var objectID = marker.objectID;

		clientMap.markerCluster.removeMarker(m);
		clientMap.markers[pos] = null;

		var newM = clientMap.createMarker(clientMap.data[objectID]);
		newM = clientMap.setIconByStatus(marker, status);
		newM.streetSignUrgence = true;

		clientMap.markers.push(newM);
		clientMap.positionMarkers[objectID] = clientMap.markers.length - 1;

		clientMap.markerCluster.addMarker(newM);
	},

	this.states = {
		'undefined': this._undefined,
		'uncheck': this._uncheck,
		'hide': this._hide,
		'street-sign-default': this._streetSignDefault,
		'street-sign-notif': this._streetSignNotif,        /*Reserved 3, 5..7 states*/
		'street-sign-alert': this._streetSignAlert,
	}

	return this;
}

var States = new StatesCls();