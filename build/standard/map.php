<?php 

	if(($_SESSION['page'] != "SansConnect") && ($_SESSION['connect'] === false)){
		$_SESSION['page'] = "connexion";
		$_SESSION['redirect'] = true;
		header('Location: '.get_link().'connexion/');
		exit();
	}

?>	

<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
<html ng-app="application">
<head>
	<meta charset='UTF-8' />

	<?php 
		echo "<title>360SmartMachine - ".$_SESSION['page']."</title>";
	?>

	<meta name="description" content="" />
	<meta name="author"			content="" />
	<meta name="publisher"		content="Rolland Mellet" />
	<meta name="keywords"		content="" />
	<meta name='viewport' content='width=device-width, initial-scale=1.0' />
	<meta property="og:title" content="360SmartMachine" />
	<meta name='viewport' content='width=device-width, initial-scale=1.0' />
	
	<?php
		echo "<link rel='shortcut icon' type='image/png' href='".get_link()."build/img/favicon.png'>";
	?>
	<link rel='pingback' href='http://www.blocparc.fr/xmlrpc.php' />

	<?php 
		echo "
			<link rel='stylesheet' href='".get_link()."build/css/bootstrap.min.css' />
			<link rel='stylesheet' href='".get_link()."build/css/font-awesome.css'/>
			
			<link rel='stylesheet' href='".get_link()."build/css/entypo.css'/>
			<link rel='stylesheet' type='text/css' href='".get_link()."build/css/flaticon.css'>
			
			<link rel='stylesheet' href='".get_link()."build/css/gx-side-menu-light-override.css'/>	
			<link rel='stylesheet' href='".get_link()."build/css/gx-sidemenu-light.css'/>
			<link rel='stylesheet' href='".get_link()."build/css/jquery.snippet.css'/>

			<link rel='stylesheet' href='".get_link()."build/css/360sc_map.css'/>
			<link rel='stylesheet' href='".get_link()."build/css/changeDataTo360sc_map.css'/>

			<link rel='stylesheet' href='".get_link()."build/css/bootstrap-switch.css'/>
			<link rel='stylesheet' href='".get_link()."build/css/jquery.webui-popover.css'/>
	
			<link rel='stylesheet' href='".get_link()."build/css/bootstrap-slider.css'/>
		";
	?>

	<!-- <script src='https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> -->

	<script src='https://cdn.socket.io/socket.io-1.2.0.js'></script>
	<!-- <script src='https://maps.googleapis.com/maps/api/js?sensor=false&KEY=AIzaSyAeaVmn26e5tMD1kYr68MiLIii9jG91rdQ'></script> -->
	<script src="https://maps.googleapis.com/maps/api/js?sensor=false&KEY=AIzaSyAeaVmn26e5tMD1kYr68MiLIii9jG91rdQ"></script>
	<!-- libs Map -->	
	<?php
		echo "<script src='".get_link()."build/libs/markerclusterer.js'></script>"
	?>

	<?php 
		echo "

			<script src='".get_link()."build/libs/jquery.js'></script>

			<script src='".get_link()."build/libs/modernizr.js'></script>

			<script src='".get_link()."build/libs/iscroll.js'></script>
			<script src='".get_link()."build/libs/gx.sidemenu.js'></script>
			<script src='".get_link()."build/libs/jquery.snippet.js'></script>
			<script src='".get_link()."build/libs/bootstrap.min.js'></script>
			<script src='".get_link()."build/libs/bootstrap-tab.js'></script>
			<script src='".get_link()."build/libs/bootstrap-collapse.js'></script>
			<script src='".get_link()."build/libs/bootstrap-transition.js'></script>
			<script src='".get_link()."build/libs/bootstrap-switch.js'></script>
			<script src='".get_link()."build/libs/jquery.webui-popover.js'></script>
			<script src='".get_link()."build/libs/bootstrap-slider.js'></script>
		";
	?>
</head>
<body>
			<!-- 
			<a href="javascript:" class="gx-menu gx-trigger-left entypo list"></a>
			<a href="javascript:" class="gx-menu gx-trigger-right entypo list"></a> 
			-->

	<?php
		echo "
			<script>
				window.serverRoot =  '".SERVERROOT."';
			</script>
		";
	?>
	<span id="react-application"></span>

	<div class="opt-slider-date. vertical left hidden">
		<in<!-- put
			type="text"
			class="span2" 
			value="" 
			data-slider-min="10" 
			data-slider-max="1000" 
			data-slider-step="5" 
			data-slider-value="[250,450]"
			data-slider-orientation="vertical"
		/> -->
	</div>

	<div id="opt-slider-date" class="opt-slider-date horizontal bottom hidden">
		<!-- <input id="ext1"
			type="text"
			class="span2"
			value="" 
			data-slider-min="10" 
			data-slider-max="1000" 
			data-slider-step="5" 
			data-slider-value="[250, 450]"
		/> -->
	</div>

	<!-- Module Map -->	
	<?php
		echo "
			<script src='".get_link()."build/js/clientMap.js'></script>
		";
	?>

	<!-- Module App ReactJs -->
	<?php 
		echo "<script src='".get_link()."build/bundle.js'></script>"
	?>

</body>
</html>