<?php

/************************************************************************************************************
************************  TABLE ROUTE  Functions*********************************************
************************************************************************************************************/


  /******Carte****************
  *******By Jorge Luis*******/

  /**********************
  **Montrer des Markers**
  ***********************/

  $router->get('/carte/markers/', function (){
    if($_SESSION['connect'] === false){
      $_SESSION['page'] = "connexion";
      $_SESSION['redirect'] = true;
      header('Location: '.get_link().'connexion/');
      exit();
    }
    $_SESSION['page'] = "markers";

      $bdd = connection_db();
      $sql = 'SELECT DISTINCT o.objetID, o.name, o.appairage, o.dateAppairage, o.respoAppairage, o.description, o.active, o.objLat, o.objLong FROM objet AS o, societe_objet AS so WHERE o.objetID=so.objetID';
      $sql1 = 'SELECT DISTINCT o.dateActive, o.respoActive, o.adresse, o.descriptionPos, o.alphaID, o.comment FROM objet AS o, societe_objet AS so WHERE o.objetID=so.objetID';
      $target = $bdd->prepare($sql);
      $target1 = $bdd->prepare($sql1);
    $target->execute();
    $target1->execute();
    $tarjetArray = array();
    $countArr = 0;
    $countArr1 = 0;
      while ($row = $target->fetch()) {
      $tarjetArray[ $countArr++ ] = $row;
    }
    while ($row1 = $target1->fetch()) {
      foreach ($row1 as $key => $value) {
        $tarjetArray[$countArr1][$key] = $value;
      }
      $countArr1++;
    }
    $target->closeCursor();
    $target1->closeCursor();

    header('Content-type: application/json; charset=utf-8');
      echo json_encode($tarjetArray);
  });

  $router->post('/carte/changer/LatLng/', function (){
    if($_SESSION['connect'] === false){
      $_SESSION['page'] = "connexion";
      $_SESSION['redirect'] = true;
      //header('Location: '.get_link().'connexion/');
      exit();
    }
    $_SESSION['page'] = "markers";

    $bdd = connection_db();
    $sql = 'UPDATE  360sc_cms.objet SET  objLat =  :lat, objLong = :lng WHERE  objet.objetID = :id';
    $target = $bdd->prepare($sql);
    $target->bindValue(':id',$_POST['id'], PDO::PARAM_INT);
    $target->bindValue(':lat',$_POST['lat'], PDO::PARAM_STR);
    $target->bindValue(':lng',$_POST['lng'], PDO::PARAM_STR);
    $target->execute();

    header('Content-type: application/json; charset=utf-8');
    echo json_encode('ça marche');
  });

  $router->get('/carte/alertes/',function(){
    if($_SESSION['connect'] === false){
      $_SESSION['page'] = "connexion";
      $_SESSION['redirect'] = true;
      header('Location: '.get_link().'connexion/');
      exit();
    }
    $_SESSION['page'] = "markers";
    
    $bdd = connection_db();
    $sql = 'SELECT goo.objetID FROM groupeobjet_objet AS goo WHERE goo.groupObjet IN (SELECT idGroup FROM groupeobjet AS go WHERE go.nameGroup = :name);';
    $target = $bdd->prepare($sql);
    $target->bindValue(':name', 'alerte', PDO::PARAM_STR);
    $target->execute();
    $targetArray = array();
    while($row = $target->fetch()){
      array_push($targetArray, $row);
    }

    header('Content-type: application/json; charset=utf-8');
      echo json_encode($targetArray);
  });



  /*$router->get('/demo/curl/', function (){
    $result = array();  
    
    header('Content-type: application/json; charset=utf-8');
    echo json_encode([
      getWrapSocietes($_SESSION['societe']), 
      $_SESSION['societe'], 
      $_SESSION['objets']
    ]);
    exit();
  });*/

  /*Migration de base de donnée => /dev*/
  $router->get('/dev/_sso/', function(){
    if($_SESSION['connect'] === false){
      $_SESSION['page'] = "connexion";
      $_SESSION['redirect'] = true;
      exit();
    }
    $bdd = connection_db();
    $sql = 'SELECT * FROM societe_objet';
    $target = $bdd->prepare($sql);
    $target->execute();
    $sql_1 = 'CALL addRefSocietesoussocieteObjet(:societeID, :objetID)';
    while($row = $target->fetch()){
      $target_1 = $bdd->prepare($sql_1);
      $target_1->bindValue(':societeID', $row['societeID']);
      $target_1->bindValue(':objetID', $row['objetID']);
      $target_1->execute();
    }
    var_dump($target_1->errorInfo());
    exit();
  });

  $router->get('/dev/_al/', function(){
    /*if($_SESSION['connect'] === false){
      $_SESSION['page'] = "connexion";
      $_SESSION['redirect'] = true;
      exit();
    }*/
    $bdd = connection_db();
    $SelectAllEvent = 'SELECT * FROM alerte';
    $IsOnTableObjEvnt = 'SELECT COUNT(objetID) as c FROM objet_event WHERE objetID = :objetID';
    $UpdateTotal = 'UPDATE objet_event SET totalAlerts = totalAlerts + 1 WHERE objetID = :objetID';

    $target = $bdd->prepare($SelectAllEvent);
    $target->execute();

    while($row = $target->fetch()){
      $target1 = $bdd->prepare($IsOnTableObjEvnt);
      $target1->bindValue(':objetID', $row['objetID']);
      $target1->execute();
      $row1 = $target1->fetch();
      /*Si l'event est dans la table objet_event, 
      dont on augement la valeur du total-? (totalAlerts)*/
      if ($row1['c'] > 0){
        $target2 = $bdd->prepare($UpdateTotal);
        $target2->bindValue(':objetID', $row['objetID']);
        $target2->execute();

        /*Si l'event est pas prisEnCompte mais il est dans la bd, dont on 
        change:
          urgence = 1
          niveauUrgence = niveauUrgence + 1
          alert = alert + 1*/
        if ($row['prisEnCompte'] == 0){
          $sql = 'UPDATE objet_event SET niveauUrgence = niveauUrgence + 1, 
          urgence = 1, alerts = alerts + 1 WHERE objetID = :objetID';
          $target2 = $bdd->prepare($sql);
          $target2->bindValue(':objetID', $row['objetID']);
          $target2->execute();
        }
      }
      /*Si l'event est pas dans la table objet_event, dont on change:
        insert l'event
        */
      else{
        /*Insert event, qui est pas PrisEnCompte*/
        if (intval($row['prisEnCompte']) == 0){
          $sql = 'INSERT INTO objet_event(objetID, urgence, niveauUrgence, totalAlerts, alerts)
           VALUES (:objetID, 1, 1, 1, 1)';
          $target2 = $bdd->prepare($sql);
          $target2->bindValue(':objetID', $row['objetID']);
          $target2->execute();
        }
        /*Insert event, qui est PrisEnCompte*/
        else{
           $sql = 'INSERT INTO objet_event(objetID, urgence, niveauUrgence, totalAlerts, alerts)
           VALUES (:objetID, 0, 0, 1, 0)';
          $target2 = $bdd->prepare($sql);
          $target2->bindValue(':objetID', $row['objetID']);
          $target2->execute();
        }
      }
    }


    var_dump($target1->errorInfo());
    var_dump($target2->errorInfo());
    exit();
  });
  
  $router->get('/dev/_alCounter/', function(){
    /*if($_SESSION['connect'] === false){
      $_SESSION['page'] = "connexion";
      $_SESSION['redirect'] = true;
      exit();
    }*/
    $bdd = connection_db();
    $SelectAllEvent = 'SELECT * FROM alerte'; 
    $target = $bdd->prepare($SelectAllEvent);
    $target->execute();

    $result = array();
    while($row = $target->fetch()){
      if ($row['objetID'] == 125) echo $row['prisEnCompte'];
      echo "\n";
      if (array_key_exists($row['objetID'], $result))
        $result[ $row['objetID'] ] += 1;
      else
        $result[ $row['objetID'] ] = 1;
    }

    var_dump($result);
    exit();
  });

  $router->get('/dev/_evalMsg/', function(){
    /*if($_SESSION['connect'] === false){
      $_SESSION['page'] = "connexion";
      $_SESSION['redirect'] = true;
      exit();
    }*/
    $bdd = connection_db();
    $SelectAllEvent = 'SELECT * FROM evaluation WHERE messageEvaluation IS NOT NULL';

    $IsOnTableObjEvnt = 'SELECT COUNT(objetID) as c FROM objet_event WHERE objetID = :objetID';
    $UpdateTotal = 'UPDATE objet_event SET totalEvalmsg = totalEvalmsg + 1 WHERE objetID = :objetID';

    $target = $bdd->prepare($SelectAllEvent);
    $target->execute();

    while($row = $target->fetch()){
      $target1 = $bdd->prepare($IsOnTableObjEvnt);
      $target1->bindValue(':objetID', $row['objetID']);
      $target1->execute();
      $row1 = $target1->fetch();
      /*Si l'event est dans la table objet_event, 
      dont on augement la valeur du total-? (totalAlerts)*/
      if ($row1['c'] > 0){
        $target2 = $bdd->prepare($UpdateTotal);
        $target2->bindValue(':objetID', $row['objetID']);
        $target2->execute();

        /*Si l'event est pas prisEnCompte mais il est dans la bd, dont on 
        change:
          urgence = 1
          niveauUrgence = niveauUrgence + 1
          alert = alert + 1*/
        if ($row['prisEnCompte'] == 0){
          $sql = 'UPDATE objet_event SET niveauUrgence = niveauUrgence + 1, 
          urgence = 1, evalmsg = evalmsg + 1 WHERE objetID = :objetID';
          $target2 = $bdd->prepare($sql);
          $target2->bindValue(':objetID', $row['objetID']);
          $target2->execute();
        }
      }
      /*Si l'event est pas dans la table objet_event, dont on change:
        insert l'event
        */
      else{
        /*Insert event, qui est pas PrisEnCompte*/
        if (intval($row['prisEnCompte']) == 0){
          $sql = 'INSERT INTO objet_event(objetID, urgence, niveauUrgence, totalEvalmsg, evalmsg)
           VALUES (:objetID, 1, 1, 1, 1)';
          $target2 = $bdd->prepare($sql);
          $target2->bindValue(':objetID', $row['objetID']);
          $target2->execute();
        }
        /*Insert event, qui est PrisEnCompte*/
        else{
           $sql = 'INSERT INTO objet_event(objetID, urgence, niveauUrgence, totalEvalmsg, evalmsg)
           VALUES (:objetID, 0, 0, 1, 0)';
          $target2 = $bdd->prepare($sql);
          $target2->bindValue(':objetID', $row['objetID']);
          $target2->execute();
        }
      }
    }


    var_dump($target1->errorInfo());
    var_dump($target2->errorInfo());
    exit();
  });
  
  $router->get('/dev/_evalMsgCounter/', function(){
    /*if($_SESSION['connect'] === false){
      $_SESSION['page'] = "connexion";
      $_SESSION['redirect'] = true;
      exit();
    }*/
    $bdd = connection_db();
    $SelectAllEvent = 'SELECT * FROM evaluation WHERE messageEvaluation IS NOT NULL'; 
    $target = $bdd->prepare($SelectAllEvent);
    $target->execute();

    $result = array();
    while($row = $target->fetch()){
      echo $row['messageEvaluation'];
      echo "<br>";
      if (array_key_exists($row['objetID'], $result))
        $result[ $row['objetID'] ] += 1;
      else
        $result[ $row['objetID'] ] = 1;
    }

    var_dump($result);
    exit();
  });

  $router->get('/dev/_eval/', function(){
    /*if($_SESSION['connect'] === false){
      $_SESSION['page'] = "connexion";
      $_SESSION['redirect'] = true;
      exit();
    }*/
    $bdd = connection_db();
    $SelectAllEvent = "SELECT * FROM evaluation WHERE messageEvaluation is null or messageEvaluation ='' ";

    $IsOnTableObjEvnt = 'SELECT COUNT(objetID) as c FROM objet_event WHERE objetID = :objetID';
    $UpdateTotal = 'UPDATE objet_event SET totalEvalmsg = totalEvalmsg + 1 WHERE objetID = :objetID';

    $target = $bdd->prepare($SelectAllEvent);
    $target->execute();

    while($row = $target->fetch()){
      $target1 = $bdd->prepare($IsOnTableObjEvnt);
      $target1->bindValue(':objetID', $row['objetID']);
      $target1->execute();
      $row1 = $target1->fetch();
      /*Si l'event est dans la table objet_event, 
      dont on augement la valeur du total-? (totalAlerts)*/
      if ($row1['c'] > 0){
        $target2 = $bdd->prepare($UpdateTotal);
        $target2->bindValue(':objetID', $row['objetID']);
        $target2->execute();

        /*Si l'event est pas prisEnCompte mais il est dans la bd, dont on 
        change:
          urgence = 1
          niveauUrgence = niveauUrgence + 1
          alert = alert + 1*/
        if ($row['prisEnCompte'] == 0){
          $sql = 'UPDATE objet_event SET niveauUrgence = niveauUrgence + 1, 
          urgence = 1, evalmsg = evalmsg + 1 WHERE objetID = :objetID';
          $target2 = $bdd->prepare($sql);
          $target2->bindValue(':objetID', $row['objetID']);
          $target2->execute();
        }
      }
      /*Si l'event est pas dans la table objet_event, dont on change:
        insert l'event
        */
      else{
        /*Insert event, qui est pas PrisEnCompte*/
        if (intval($row['prisEnCompte']) == 0){
          $sql = 'INSERT INTO objet_event(objetID, urgence, niveauUrgence, totalEvalmsg, evalmsg)
           VALUES (:objetID, 1, 1, 1, 1)';
          $target2 = $bdd->prepare($sql);
          $target2->bindValue(':objetID', $row['objetID']);
          $target2->execute();
        }
        /*Insert event, qui est PrisEnCompte*/
        else{
           $sql = 'INSERT INTO objet_event(objetID, urgence, niveauUrgence, totalEvalmsg, evalmsg)
           VALUES (:objetID, 0, 0, 1, 0)';
          $target2 = $bdd->prepare($sql);
          $target2->bindValue(':objetID', $row['objetID']);
          $target2->execute();
        }
      }
    }


    var_dump($target1->errorInfo());
    var_dump($target2->errorInfo());
    exit();
  });

  $router->get('/dev/_evalCounter/', function(){
    /*if($_SESSION['connect'] === false){
      $_SESSION['page'] = "connexion";
      $_SESSION['redirect'] = true;
      exit();
    }*/
    $bdd = connection_db();
    $SelectAllEvent = "SELECT * FROM evaluation WHERE messageEvaluation is null or messageEvaluation ='' "; 
    $target = $bdd->prepare($SelectAllEvent);
    $target->execute();

    $result = array();
    while($row = $target->fetch()){
      echo $row['messageEvaluation'];
      echo "<br>";
      if (array_key_exists($row['objetID'], $result))
        $result[ $row['objetID'] ] += 1;
      else
        $result[ $row['objetID'] ] = 1;
    }
    var_dump($result);
    exit();
  });

  $router->get('/demo/carte/markers/', function (){
    if($_SESSION['connect'] === false){
      $_SESSION['page'] = "connexion";
      $_SESSION['redirect'] = true;
      header('Location: '.get_link().'connexion/');
      exit();
    }
    $_SESSION['page'] = "markers";

    $societes = array();
    $resultObjets = array();
    $sessionObjetID = array();
    $societes = getWrapSocietes($_SESSION['societe']);
    array_push($societes, $_SESSION['societe']);
    for ($i=0; $i < count($societes); $i++) { 
      selectObjet($societes[$i], $resultObjets, $sessionObjetID);
    }
   
    header('Content-type: application/json; charset=utf-8');
    $_SESSION['objets'] = $sessionObjetID;
      echo json_encode($resultObjets);
  });

  $router->get('/markers/pos/', function (){
    if($_SESSION['connect'] === false){
      $_SESSION['page'] = "connexion";
      $_SESSION['redirect'] = true;
      exit();
    }
    $_SESSION['page'] = "Initialize Objects";

    $societeID = $_SESSION['societe'];

    $bdd = connection_db();
    $sql = 'SELECT obj.*, oe.urgence, oe.niveauUrgence, oe.totalAlerts, 
    oe.totalEvalmsg, oe.alerts, oe.evalmsg, oe.eval, oe.totalEval FROM societesoussociete_objet AS sso 
    INNER JOIN objet AS obj
    LEFT JOIN objet_event AS oe ON sso.objetID = oe.objetID
    WHERE sso.objetID = obj.objetID and sso.societeID = :societeID'; 

    $target = $bdd->prepare($sql);
    $target->bindValue(':societeID', $societeID);
    $target->execute();
    $targetArray = array();
    $_SESSION["objets"] = array();
    while ($row = $target->fetch()) {
      $targetArray[] = $row;
      $_SESSION["objets"][] = $row['objetID'];
    }

    header('Content-type: application/json; charset=utf-8');
      echo json_encode($targetArray);
  });

  $router->post('/history/pagination/', function(){
    if($_SESSION['connect'] === false){
      $_SESSION['page'] = "connexion";
      $_SESSION['redirect'] = true;
      exit();
    }

    $diff = 30;
    $target = null;
    $target_ = null;
    $targetArray = [];
    $lastObjectCreatedTime = intval($_POST['end']); /*0;*/
    $objetID = $_POST['objectId'];
    if ($lastObjectCreatedTime == 0){
      $sql = "SELECT * FROM `historique_action` WHERE ActionTypeID = 1 ORDER BY `HeureAction` DESC";
      $bdd = connection_db();
      $target = $bdd->prepare($sql);
      $target->execute();
      $sqlAlert = "SELECT a.*, c.Name as customerName FROM alerte as a INNER JOIN customers as c ON a.creatorID = c.CustomersID WHERE objetID = $objetID AND actionID = ";
      $sqlEval = "SELECT e.*, c.Name as customerName FROM evaluation as e INNER JOIN customers as c ON e.creatorID = c.CustomersID WHERE objetID = $objetID AND actionID = ";
      while ($row = $target->fetch()) {
        if ($row['TableActionID'] == 2){
          //$targetArray[] = $row['ActionID'];
          $sql_ = $sqlAlert.$row['ActionID'];
          $target_ = $bdd->prepare($sql_);
          //$target_->bindValue(':actionID', $row['ActionID']);
          $target_->execute();
          while ($row_ = $target_->fetch()) {
            $row_['type'] = 'alert';
            $targetArray[] = $row_;
          }
        }
        else{
          $sql_ = $sqlEval.$row['ActionID'];
          $target_ = $bdd->prepare($sql_);
          //$target_->bindValue(':actionID', $row['ActionID']);
          $target_->execute();
          while ($row_ = $target_->fetch()) {
            $row_['type'] = 'evalmsg';
            if ($row_['messageEvaluation'] == null || 
                    $row_['messageEvaluation'] == '' ||
                    !$row_['messageEvaluation'])
                    $row_['type'] = 'eval';
            $targetArray[] = $row_;
          }
        }

        if (count($targetArray) == $diff) break;
      }
    }
    else{
      $sql = "SELECT * FROM `historique_action` WHERE HeureAction > `$lastObjectCreatedTime` ORDER BY `HeureAction` DESC";
      $bdd = connection_db();
      $target = $bdd->prepare($sql);
      $target->execute();
      $sqlAlert = "SELECT a.*, c.Name as customerName FROM alerte as a INNER JOIN customers as c ON a.creatorID = c.CustomersID WHERE objetID = $objetID AND actionID = ";
      $sqlEval = "SELECT e.*, c.Name as customerName FROM evaluation as e INNER JOIN customers as c ON e.creatorID = c.CustomersID WHERE objetID = $objetID AND actionID = ";
      while ($row = $target->fetch()) {
        if ($row['TableActionID'] == 2){
          //$targetArray[] = $row['ActionID'];
          $sql_ = $sqlAlert.$row['ActionID'];
          $target_ = $bdd->prepare($sql_);
          //$target_->bindValue(':actionID', $row['ActionID']);
          $target_->execute();
          while ($row_ = $target_->fetch()) {
            $row_['type'] = 'alert';
            $targetArray[] = $row_;
          }
        }
        else{
          $sql_ = $sqlEval.$row['ActionID'];
          $target_ = $bdd->prepare($sql_);
          //$target_->bindValue(':actionID', $row['ActionID']);
          $target_->execute();
          while ($row_ = $target_->fetch()) {
            $row_['type'] = 'evalmsg';
            if ($row_['messageEvaluation'] == null || 
                    $row_['messageEvaluation'] == '' ||
                    !$row_['messageEvaluation'])
                    $row_['type'] = 'eval';
            $targetArray[] = $row_;
          }
        }

        if (count($targetArray) == $diff) break;
      }
    }
    
    header('Content-type: application/json; charset=utf-8');
    echo json_encode($targetArray);
    exit();
  });

  $router->get('/history/pagination/', function(){ 
    $diff = 30;
    $target = null;
    $target_ = null;
    $targetArray = [];
    $lastObjectCreatedTime = intval($_POST['end']);
    $objetID = $_POST['objectId'];
    if ($lastObjectCreatedTime == 0){
      $sql = "SELECT * FROM `historique_action` ORDER BY `HeureAction` DESC";
      $bdd = connection_db();
      $target = $bdd->prepare($sql);
      $target->execute();
      $sqlAlert = "SELECT a.*, c.Name as customerName FROM alerte as a INNER JOIN customers as c ON a.creatorID = c.CustomersID WHERE objetID = $objetID AND actionID = ";
      $sqlEval = "SELECT e.*, c.Name as customerName FROM evaluation as e INNER JOIN customers as c ON e.creatorID = c.CustomersID WHERE objetID = $objetID AND actionID = ";
      while ($row = $target->fetch()) {
        if ($row['TableActionID'] == 2){
          //$targetArray[] = $row['ActionID'];
          $sql_ = $sqlAlert.$row['ActionID'];
          $target_ = $bdd->prepare($sql_);
          //$target_->bindValue(':actionID', $row['ActionID']);
          $target_->execute();
          while ($row_ = $target_->fetch()) {
            $row_['type'] = 'alert';
            $targetArray[] = $row_;
          }
        }
        else{
          $sql_ = $sqlEval.$row['ActionID'];
          $target_ = $bdd->prepare($sql_);
          //$target_->bindValue(':actionID', $row['ActionID']);
          $target_->execute();
          while ($row_ = $target_->fetch()) {
            $row_['type'] = 'evalmsg';
            if ($row_['messageEvaluation'] == null || 
                    $row_['messageEvaluation'] == '' ||
                    !$row_['messageEvaluation'])
                    $row_['type'] = 'eval';
            $targetArray[] = $row_;
          }
        }

        if (count($targetArray) == $diff) break;
      }
    }
    else{
      $sql = "SELECT * FROM `historique_action` WHERE HeureAction > `$lastObjectCreatedTime` ORDER BY `HeureAction` DESC";
      $bdd = connection_db();
      $target = $bdd->prepare($sql);
      $target->execute();
      $sqlAlert = "SELECT a.*, c.Name as customerName FROM alerte as a INNER JOIN customers as c ON a.creatorID = c.CustomersID WHERE objetID = $objetID AND actionID = ";
      $sqlEval = "SELECT e.*, c.Name as customerName FROM evaluation as e INNER JOIN customers as c ON e.creatorID = c.CustomersID WHERE objetID = $objetID AND actionID = ";
      while ($row = $target->fetch()) {
        if ($row['TableActionID'] == 2){
          //$targetArray[] = $row['ActionID'];
          $sql_ = $sqlAlert.$row['ActionID'];
          $target_ = $bdd->prepare($sql_);
          //$target_->bindValue(':actionID', $row['ActionID']);
          $target_->execute();
          while ($row_ = $target_->fetch()) {
            $row_['type'] = 'alert';
            $targetArray[] = $row_;
          }
        }
        else{
          $sql_ = $sqlEval.$row['ActionID'];
          $target_ = $bdd->prepare($sql_);
          //$target_->bindValue(':actionID', $row['ActionID']);
          $target_->execute();
          while ($row_ = $target_->fetch()) {
            $row_['type'] = 'evalmsg';
            if ($row_['messageEvaluation'] == null || 
                    $row_['messageEvaluation'] == '' ||
                    !$row_['messageEvaluation'])
                    $row_['type'] = 'eval';
            $targetArray[] = $row_;
          }
        }

        if (count($targetArray) == $diff) break;
      }
    }
    
    header('Content-type: application/json; charset=utf-8');
    echo json_encode($targetArray);
    exit();
  });

  $router->post('/alert/pagination/', function(){
    if($_SESSION['connect'] === false){
      $_SESSION['page'] = "connexion";
      $_SESSION['redirect'] = true;
      exit();
    }

    $diff = 45;
    $init = intval($_POST['end']);
    if (!empty($_POST['objetID']) && isset($_POST['objetID'])) $objetID = $_POST['objetID'];
    else $objetID = null;
    
    $sql = '';
    $bdd = connection_db();
    $target = null;

    if ($init == 0) {
      if ($objetID == null)
        $sql = "SELECT a.*, c.Name as customerName FROM alerte as a INNER JOIN customers as c ON a.creatorID = c.CustomersID WHERE prisEnCompte = 0 ORDER BY `createdTime` DESC LIMIT $diff";
      else
        $sql = "SELECT a.*, c.Name as customerName FROM alerte as a INNER JOIN customers as c ON a.creatorID = c.CustomersID WHERE prisEnCompte = 0 AND a.objetID = $objetID ORDER BY `createdTime` DESC LIMIT $diff";    
      $target = $bdd->prepare($sql);
    }
    else{
      $init = $_POST['end'];
      if ($objetID == null)
        $sql = "SELECT a.*, c.Name as customerName FROM alerte as a INNER JOIN customers as c ON a.creatorID = c.CustomersID WHERE prisEnCompte = 0 AND createdTime < '$init' ORDER BY `createdTime` DESC LIMIT $diff";
      else
        $sql = "SELECT a.*, c.Name as customerName FROM alerte as a INNER JOIN customers as c ON a.creatorID = c.CustomersID WHERE prisEnCompte = 0 AND createdTime < '$init' AND a.objetID = $objetID ORDER BY `createdTime` DESC LIMIT $diff";
      $target = $bdd->prepare($sql);
    }

    $target->execute();
    $targetArray = array();
    while ($row = $target->fetch()) {
      $targetArray[] = $row;
    }
    header('Content-type: application/json; charset=utf-8');
    //echo "$target";
    echo json_encode($targetArray);
    //echo var_dump($target->errorInfo());

    exit();
  });

  $router->post('/evalmsg/pagination/', function(){
    if($_SESSION['connect'] === false){
      $_SESSION['page'] = "connexion";
      $_SESSION['redirect'] = true;
      exit();
    }

    $diff = 45;
    $init = intval($_POST['end']);
    if (!empty($_POST['objetID']) && isset($_POST['objetID'])) $objetID = $_POST['objetID'];
    else $objetID = null;
    
    $sql = '';
    $bdd = connection_db();
    $target = null;

    if ($init == 0) {
      if ($objetID == null)
        $sql = "SELECT a.*, c.Name as customerName FROM evaluation as a INNER JOIN customers as c ON a.creatorID = c.CustomersID 
        WHERE prisEnCompte = 0 AND messageEvaluation <> '' ORDER BY `createdTime` DESC LIMIT $diff";
      else
        $sql = "SELECT a.*, c.Name as customerName FROM evaluation as a INNER JOIN customers as c ON a.creatorID = c.CustomersID 
        WHERE prisEnCompte = 0 AND objetID = $objetID AND messageEvaluation <> '' ORDER BY `createdTime` DESC LIMIT $diff";  
      $target = $bdd->prepare($sql);
    }
    else{
      $init = $_POST['end'];
      if ($objetID == null)
        $sql = "SELECT a.*, c.Name as customerName FROM evaluation as a INNER JOIN customers as c ON a.creatorID = c.CustomersID
        WHERE prisEnCompte = 0 AND createdTime < '$init' AND messageEvaluation <> '' ORDER BY `createdTime` DESC LIMIT $diff";
      else
        $sql = "SELECT a.*, c.Name as customerName FROM evaluation as a INNER JOIN customers as c ON a.creatorID = c.CustomersID
      WHERE prisEnCompte = 0 AND objetID = $objetID AND createdTime < '$init' AND messageEvaluation <> '' ORDER BY `createdTime` DESC LIMIT $diff";  
      $target = $bdd->prepare($sql);
    }

    $target->execute();
    $targetArray = array();
    while ($row = $target->fetch()) {
      $targetArray[] = $row;
    }
    header('Content-type: application/json; charset=utf-8');
    //echo "$target";
    echo json_encode($targetArray);
    //echo var_dump($target->errorInfo());

    exit();
  });

  $router->post('/eval/pagination/', function(){
    if($_SESSION['connect'] === false){
      $_SESSION['page'] = "connexion";
      $_SESSION['redirect'] = true;
      exit();
    }

    $diff = 45;
    $init = intval($_POST['end']);
    if (!empty($_POST['objetID']) && isset($_POST['objetID'])) $objetID = $_POST['objetID'];
    else $objetID = null;
    $sql = '';
    $bdd = connection_db();
    $target = null;

    if ($init == 0) {
      if ($objetID == null)
        $sql = "SELECT a.*, c.Name as customerName FROM evaluation as a INNER JOIN customers as c ON a.creatorID = c.CustomersID 
        WHERE prisEnCompte = 0 AND a.messageEvaluation is Null ORDER BY createdTime DESC LIMIT $diff";
      else
        $sql = "SELECT a.*, c.Name as customerName FROM evaluation as a INNER JOIN customers as c ON a.creatorID = c.CustomersID 
        WHERE prisEnCompte = 0 AND objetID = $objetID AND a.messageEvaluation is Null ORDER BY createdTime DESC LIMIT $diff";          
      $target = $bdd->prepare($sql);
    }
    else{
      $init = $_POST['end'];
      if ($objetID == null)
        $sql = "SELECT a.*, c.Name as customerName FROM evaluation as a INNER JOIN customers as c ON a.creatorID = c.CustomersID
        WHERE prisEnCompte = 0 AND createdTime < '$init' AND a.messageEvaluation is Null ORDER BY createdTime DESC LIMIT $diff";
      else
        $sql = "SELECT a.*, c.Name as customerName FROM evaluation as a INNER JOIN customers as c ON a.creatorID = c.CustomersID
        WHERE prisEnCompte = 0 AND objetID = $objetID AND createdTime < '$init' AND a.messageEvaluation is Null ORDER BY createdTime DESC LIMIT $diff";  
      $target = $bdd->prepare($sql);
    }

    $target->execute();
    $targetArray = array();
    while ($row = $target->fetch()) {
      $targetArray[] = $row;
    }
    header('Content-type: application/json; charset=utf-8');
    //echo "$target";
    echo json_encode($targetArray);
    //echo var_dump($target->errorInfo());

    exit();
  });

  $router->post('/eventChanged/alert', function(){
    if($_SESSION['connect'] === false){
      $_SESSION['page'] = "connexion";
      $_SESSION['redirect'] = true;
      exit();
    }
    $_SESSION['page'] = "Event Changed";
    $prisEnCompte = $_POST['event']['prisEnCompte'];
    $objetID = $_POST['event']['objetID'];
    $actionID = $_POST['event']['actionID'];

    $bdd = connection_db();
    $sql = "UPDATE alerte SET prisEnCompte = $prisEnCompte WHERE objetID = $objetID AND actionID = $actionID";
    
    $target = $bdd->prepare($sql);
    $target->execute();

    header('Content-type: application/json; charset=utf-8');
      echo json_encode(['good']);
  });

  $router->post('/eventChanged/evalmsg', function(){
    if($_SESSION['connect'] === false){
      $_SESSION['page'] = "connexion";
      $_SESSION['redirect'] = true;
      exit();
    }
    $_SESSION['page'] = "Event Changed";
    $prisEnCompte = $_POST['event']['prisEnCompte'];
    $objetID = $_POST['event']['objetID'];
    $actionID = $_POST['event']['actionID'];

    $bdd = connection_db();
    $sql = "UPDATE evaluation SET prisEnCompte = $prisEnCompte WHERE objetID = $objetID AND actionID = $actionID";
    
    $target = $bdd->prepare($sql);
    $target->execute();

    header('Content-type: application/json; charset=utf-8');
      echo json_encode(['good']);
  });

  $router->post('/eventChanged/eval', function(){
    if($_SESSION['connect'] === false){
      $_SESSION['page'] = "connexion";
      $_SESSION['redirect'] = true;
      exit();
    }
    $_SESSION['page'] = "Event Changed";
    $prisEnCompte = $_POST['event']['prisEnCompte'];
    $objetID = $_POST['event']['objetID'];
    $actionID = $_POST['event']['actionID'];

    $bdd = connection_db();
    $sql = "UPDATE evaluation SET prisEnCompte = $prisEnCompte WHERE objetID = $objetID AND actionID = $actionID";
    
    $target = $bdd->prepare($sql);
    $target->execute();

    header('Content-type: application/json; charset=utf-8');
      echo json_encode(['good']);
  });

  $router->post('/information/object/tab/', function(){
    if($_SESSION['connect'] === false){
      $_SESSION['page'] = "connexion";
      $_SESSION['redirect'] = true;
      exit();
    }
    $_SESSION['page'] = "Search information object tab";
    $objectId = $_POST["objectId"];
    $parentId = $_POST["parentId"];
    
    $bdd = connection_db();

    $sql = "SELECT s.name as societeName, t.nameType, $objectId as objetID FROM societe_objet as so 
    INNER JOIN societe as s INNER JOIN typeobjet as t ON so.societeID = s.ID AND t.idType = $parentId
    WHERE so.objetID = $objectId";
    $target = $bdd->prepare($sql);
    $target->execute();

    header('Content-type: application/json; charset=utf-8');
      echo json_encode($target->fetchAll());
  });

  $router->get('/demo/carte/alertes/', function(){
    if($_SESSION['connect'] === false){
      $_SESSION['page'] = "connexion";
      $_SESSION['redirect'] = true;
      //header('Location: '.get_link().'connexion/');
      exit();
    }
    $_SESSION['page'] = "markers";
    
    $bdd = connection_db();
    $objets = $_SESSION['objets'];
    $countObjets = count($objets);
    $targetArray = array();
    $sql = 'SELECT * FROM alerte as a INNER JOIN customers as c WHERE 
    a.creatorID = c.CustomersID and a.ObjetID = :objetID';
    $sql_update = 'UPDATE  alerte SET  envoye =  1 
      WHERE alerte.objetID = :objetID';
    for ($i=0; $i < $countObjets; $i++) { 
      $target = $bdd->prepare($sql);
      $target->bindValue(':objetID', $objets[$i]);
      $target->execute();
      while($row = $target->fetch()){
        array_push($targetArray, $row);
      }
      $target_update = $bdd->prepare($sql_update);
      $target_update->bindValue(':objetID', $objets[$i]);
      $target_update->execute();
    }   

    header('Content-type: application/json; charset=utf-8');
      echo json_encode($targetArray);
  });

  $router->get('/demo/carte/dernierAlerte/', function(){
      if($_SESSION['connect'] === false){
      $_SESSION['page'] = "connexion";
      $_SESSION['redirect'] = true;
      //header('Location: '.get_link().'connexion/');
      exit();
    }
    $_SESSION['page'] = "markers";
    

    $bdd = connection_db();
    $objets = $_SESSION['objets'];
    $countObjets = count($objets);
    $targetArray = array();
    $sql = 'SELECT * FROM alerte as a INNER JOIN customers as c 
    WHERE a.creatorID = c.CustomersID and a.ObjetID = :objetID and a.envoye = 0';
    $sql_update = 'UPDATE  alerte SET  envoye = 1 
      WHERE alerte.objetID = :objetID';
    for ($i=0; $i < $countObjets; $i++) { 
      $target = $bdd->prepare($sql);
      $target->bindValue(':objetID', $objets[$i]);
      $target->execute();
      while($row = $target->fetch()){
        array_push($targetArray, $row);
      }
      $target_update = $bdd->prepare($sql_update);
      $target_update->bindValue(':objetID', $objets[$i]);
      $target_update->execute();
    }   

    header('Content-type: application/json; charset=utf-8');
    $count = count($targetArray);


    if ($count > 0)
      shell_exec('redis-cli publish all '.json_encode($targetArray[0]));
    
      echo json_encode($targetArray);
  });

  $router->get('/demo/carte/evalsMsg/', function(){
      if($_SESSION['connect'] === false){
      $_SESSION['page'] = "connexion";
      $_SESSION['redirect'] = true;
      //header('Location: '.get_link().'connexion/');
      exit();
    }
    $_SESSION['page'] = "markers";
    
    $bdd = connection_db();
    $objets = $_SESSION['objets'];
    $countObjets = count($objets);
    $targetArray = array();
    $sql = 'SELECT * FROM evaluation as e INNER JOIN customers as c 
    WHERE e.creatorID = c.CustomersID and e.ObjetID = :objetID';
    $sql_update = 'UPDATE  evaluation SET  envoye =  1 
      WHERE evaluation.objetID = :objetID';
    for ($i=0; $i < $countObjets; $i++) { 
      $target = $bdd->prepare($sql);
      $target->bindValue(':objetID', $objets[$i]);
      $target->execute();
      while($row = $target->fetch()){
        array_push($targetArray, $row);
      }
      $target_update = $bdd->prepare($sql_update);
      $target_update->bindValue(':objetID', $objets[$i]);
      $target_update->execute();
    }     

    header('Content-type: application/json; charset=utf-8');
      echo json_encode($targetArray);
  });

  $router->get('/demo/carte/dernierEvalsMsg/', function(){
    if($_SESSION['connect'] === false){
      $_SESSION['page'] = "connexion";
      $_SESSION['redirect'] = true;
      //header('Location: '.get_link().'connexion/');
      exit();
    }
    $_SESSION['page'] = "markers";
    
    $bdd = connection_db();
    $objets = $_SESSION['objets'];
    $countObjets = count($objets);
    $targetArray = array();
    $sql = 'SELECT * FROM evaluation as e INNER JOIN customers as c 
    WHERE e.creatorID = c.CustomersID and e.ObjetID = :objetID and e.envoye = 0';
    $sql_update = 'UPDATE  evaluation SET  envoye =  1 
      WHERE evaluation.objetID = :objetID and evaluation.envoye = 0';
    for ($i=0; $i < $countObjets; $i++) { 
      $target = $bdd->prepare($sql);
      $target->bindValue(':objetID', $objets[$i]);
      $target->execute();
      while($row = $target->fetch()){
        array_push($targetArray, $row);
      }
      $target_update = $bdd->prepare($sql_update);
      $target_update->bindValue(':objetID', $objets[$i]);
      $target_update->execute();
    }   

    header('Content-type: application/json; charset=utf-8');
      echo json_encode($targetArray);
  });
  
  /*Des Outils*/
  $router->get('/outils/changerUrlTags/', function(){
    $_SESSION['page'] = "outilsChangerUrlTags";
    require "./php/ChangerTagWebApp.php";
  });

  $router->post('/outils/changerUrlTags/', function(){
    $_SESSION['page'] = "";
    require "./php/ChangerTagWebApp.php";
  });

  $router->post('/outils/textPromotion/(\w+)', function ($text){
    if($_SESSION['connect'] === false){
      $_SESSION['page'] = "connexion";
      $_SESSION['redirect'] = true;
      header('Location: '.get_link().'connexion/');
      exit();
    }
    $_SESSION['page'] = "promotion";
    require 'textPromotion.php';
    exit();
  });
  $router->get('/outils/textPromotion/', function (){
    if($_SESSION['connect'] === false){
      $_SESSION['page'] = "connexion";
      $_SESSION['redirect'] = true;
      header('Location: '.get_link().'connexion/');
      exit();
    }
    $_SESSION['page'] = "";
    require 'textPromotion.php';
    exit();
  });
/** 
  * Début
  * Definition d'autorisation aux gestion des objets
  * @author: ACOSTA Jorge Luis - 360sc
*/
  $router->get('/autorisation/url/', function(){
    if($_SESSION['connect'] === false){
      $_SESSION['page'] = "connexion";
      $_SESSION['redirect'] = true;
      //header('Location: '.get_link().'connexion/');
      exit();
    }

    $_SESSION['page'] = "autorisation";
    
    $bdd = connection_db();

        $query = $bdd->prepare('SELECT s.email, s.societe, s.CustomersID, s.Name, s.level, s.login, d.adminMA, d.clientMA, d.type, 
    d.gestionTags, d.searchTags, d.addTag, d.editTags, d.addObject, d.editObject, d.createUser, d.export, d.societePF, d.SURLE
        FROM customers AS s, droit_client AS d WHERE s.login = :login AND d.idClient = s.CustomersID');
        
        $query->bindValue(':login', $_SESSION['login'], PDO::PARAM_STR);
        $query->execute();
        
        $data = $query->fetch();

        $data['get_link'] = get_link();

        header('Content-type: application/json; charset=utf-8');
      echo json_encode($data);
  });
/** 
  * Fin
  * Definition d'autorisation aux gestion des objets
  * @author: ACOSTA Jorge Luis - 360sc
*/
/** 
  * Début
  * Definition recuperation des infos client
  * @author: ACOSTA Jorge Luis - 360sc
*/  
  $router->get('/client/info/', function(){
    if($_SESSION['connect'] === false){
      $_SESSION['page'] = "connexion";
      $_SESSION['redirect'] = true;
      //header('Location: '.get_link().'connexion/');
      exit();
    }
    $_SESSION['page'] = "client_infos";
    
    $data = array("client_image" => $_SESSION['client_image'], 
      "client_name" => $_SESSION['client_name']);

    header('Content-type: application/json; charset=utf-8');
        echo json_encode($data);
        exit();
  });



  /**
  * Suprimer aprés
  */

  $router->get('(\w)+/client/info/', function(){
    if($_SESSION['connect'] === false){
      $_SESSION['page'] = "connexion";
      $_SESSION['redirect'] = true;
      //header('Location: '.get_link().'connexion/');
      exit();
    }
    $_SESSION['page'] = "client_infos";
    
    $data = array("client_image" => $_SESSION['client_image'], 
      "client_name" => $_SESSION['client_name']);

    header('Content-type: application/json; charset=utf-8');
        echo json_encode($data);
        exit();
  });

  $router->get('(\w)+/autorisation/url/', function(){
    if($_SESSION['connect'] === false){
      $_SESSION['page'] = "connexion";
      $_SESSION['redirect'] = true;
      //header('Location: '.get_link().'connexion/');
      exit();
    }

    $_SESSION['page'] = "autorisation";
    
    $bdd = connection_db();

        $query = $bdd->prepare('SELECT s.email, s.societe, s.password, s.CustomersID, s.Name, s.level, s.login, d.adminMA, d.clientMA, d.type, 
    d.gestionTags, d.searchTags, d.addTag, d.editTags, d.addObject, d.editObject, d.createUser, d.export, d.societePF, d.SURLE
        FROM customers AS s, droit_client AS d WHERE s.login = :login AND d.idClient = s.CustomersID');
        
        $query->bindValue(':login', $_SESSION['login'], PDO::PARAM_STR);
        $query->execute();
        
        $data = $query->fetch();

        $data['get_link'] = get_link();

        header('Content-type: application/json; charset=utf-8');
      echo json_encode($data);
  });


  /** 
  * Fin
  * Definition recuperation des infos client
  * @author: ACOSTA Jorge Luis - 360sc
*/


?>