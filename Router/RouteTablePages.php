﻿<?php
/************************************************************************************************************
************************  TABLE ROUTE  PAGES*********************************************
************************************************************************************************************/


  /***************************
  ******** Main page *********
  ***************************/
  $router->get('/', function () {
    /**
    * We open the database for the SQL request.
    */
    $bdd = connection_db();

    /**
    * We display home page
    */

    $_SESSION['page'] = "accueil";

    if($_SESSION['connect'] === false){
      $_SESSION['page'] = "connexion";
      $_SESSION['redirect'] = true;
      header('Location: '.get_link().'_connexion/');
      exit();
    }

    header('Location: '.get_link().'map');

    exit();
  });

  $router->get('/map/', function () {
    /**
    * We display home page
    */

    if($_SESSION['connect'] === false){
      $_SESSION['page'] = "connexion";
      $_SESSION['redirect'] = true;
      header('Location: '.get_link().'_connexion/');
      exit();
    }

    $_SESSION['page'] = "react_map";
    include "./build/map.php";

    exit();
  });

  /***************************
  ******** MPB DEMO 0.0 *********
  ***************************/
  $router->get('/mpbDemo/', function () {
    /**
    * We open the database for the SQL request.
    */
    $bdd = connection_db();

    /**
    * We display home page
    */

    $_SESSION['page'] = "accueil";
    include "./php/360ScDemo.php";

    exit();
  });

  $router->get('/m360sc/', function () {
    /**
    * We open the database for the SQL request.
    */
    $bdd = connection_db();

    /**
    * We display home page
    */

    $_SESSION['page'] = "m360sc";
    include "./php/m360sc.php";

    exit();
  });





/***************************
******* Create MCs page *******
***************************/
$router->match('GET|POST','/addMCs/', function () {

  checkLogin();
  $_SESSION['page'] = "New MCs";
  $_SESSION['pageTitle'] = "Ajouter de nouveaux MCs";
  include "./php/create-MCs.php";
});

/***************************
******* edit MC page *******
***************************/
$router->match('GET|POST','/MC/(\d+)', function ($MCId) {

  checkLogin();
  $_SESSION['page'] = "Edit MC";
  $_SESSION['pageTitle'] = "Gestion de MC";
  include "./php/update-MC.php";
});


/***************************
******* remove MC page *******
***************************/
$router->match('GET','/delete-MC/', function () {

  checkLogin();
  $_SESSION['page'] = "Remove MC";
  $_SESSION['pageTitle'] = "Supression de MC";
  include "./php/delete-MC.php";
});

$router->match('POST','/delete-MC/(\d+)', function ($MCId) {

  checkLogin();
  $_SESSION['page'] = "Remove MC";
  $_SESSION['pageTitle'] = "Supression de MC";
  include "./php/delete-MC.php";
});

/***************************
******* list MCs page *******
***************************/
$router->match('GET|POST','/MCs/', function () {

  checkLogin();
  $_SESSION['page'] = "List MCs";
  $_SESSION['pageTitle'] = "Liste des MCs";
  include "./php/read-MCs.php";
});

/***************************
******* export MCs page *******
***************************/
$router->match('GET|POST','/export-MCs/', function () {

  checkLogin();
  $_SESSION['page'] = "Export MCs";
  $_SESSION['pageTitle'] = "Export des MCs";
  include "./php/export-MCs.php";
});

/***************************
******* distribute MCs page *******
***************************/
$router->match('GET|POST','/redistribute/', function () {

  checkLogin();
  $_SESSION['page'] = "Distribute MCs";
  $_SESSION['pageTitle'] = "Attribuer des MCs";
  include "./php/give-MCs.php";
});


/***************************
******* list urles page *******
***************************/
$router->match('GET|POST','/urles/', function () {

  checkLogin();
  $_SESSION['page'] = "List URLEs";
  $_SESSION['pageTitle'] = "Liste des urls encryptés";
  include "./php/read-urles.php";
});

/***************************
******* Create Object Type *******
***************************/
$router->match('GET|POST','/type/', function () {

  checkLogin();
  $_SESSION['page'] = "New Object Type";
  $_SESSION['pageTitle'] = "Ajouter un nouveau type d'objet";
  include "./php/create-objectType.php";
});

/***************************
******* Create Objet *******
***************************/

$router->match('GET|POST','/object/',function(){

  checkLogin();
  $_SESSION['page'] = "New Object";
  $_SESSION['pageTitle'] = "Ajouter un nouvel objet";
  include "./php/create-object.php";
});

/***************************
******* list objects page *******
***************************/
$router->match('GET|POST','/objects/', function () {

  checkLogin();
  $_SESSION['page'] = "List Objects";
  $_SESSION['pageTitle'] = "Liste des objets";
  include "./php/read-objects.php";
});

/***************************
******* Edit Objet *******
***************************/

$router->match('GET|POST','/object/(\w+)',function($objectId){

  checkLogin();
  $_SESSION['page'] = "Edit Object";
  $_SESSION['pageTitle'] = "Gestion d'objet";
  include "./php/update-object.php";
});

/***************************
***** Inscription page *****
***************************/
$router->match('GET|POST','/account/', function () {

  checkLogin();
  $_SESSION['page'] = "Register";
  $_SESSION['pageTitle'] = "Créer un compte";
  require "./php/create-account.php";
});

/***************************
***** gestion account page *****
***************************/
$router->match('GET|POST','/account/(\d+)', function ($accountId) {

  checkLogin();
  $_SESSION['page'] = "gestion-account";
  $_SESSION['pageTitle'] = "Gestion du compte";
  require "./php/update-account.php";
});

/***************************
***** Create  company *****
***************************/
$router->match('GET|POST','/society/', function () {

  checkLogin();
  $_SESSION['page'] = "New Society";
  $_SESSION['pageTitle'] = "Créer une entreprise";
  require "./php/create-society.php";
});


/***************************
  ******* Contact Page *******
  ***************************/

$router->match('GET|POST','/contact/',function(){
  checkLogin();
  $_SESSION['page'] = "Contact";
  $_SESSION['pageTitle'] = "Demande de MCs";
  require "./php/contact.php";

});

  /***************************
  ****** WEBAPP FingWaMCInfo**
  ******By Jorge Luis*********
  ***************************/
  $router->get('/webApp/FingWa_MCInfo/(\w+)', function($key){
    $_SESSION['page'] = "SansConnect";
    include "./php/FingWa_MCInfo.php";    
  });

  /***************************
  ***** Multi encryption *****
  ***************************/
  $router->post('/encrypt/multi/', function () {
    /**
    * We open the database for the SQL request.
    */

    include "./php/encrypt.php";
  });

  /***************************
  ***** Multi encryption *****
  ***************************/
  $router->post('/encrypt/mobile/', function () {
    /**
    * We open the database for the SQL request.
    */

    include "./php/encrypt.php";
  });
  
?>