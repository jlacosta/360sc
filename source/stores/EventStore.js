var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
EventEmitter.prototype.setMaxListeners(Infinity);

var ObjectActionCreator = require('../actions/ObjectActionCreator');
var EventActionCreator = require('../actions/EventActionCreator');

var ObjectStore = require('./ObjectStore');

var WebApiUtils = require('../utils/WebApiUtils');

var EventStore = assign({}, EventEmitter.prototype, {

    addNewEventListenner: function(callback){
        this.on('new_event', callback);
    },

    removeNewEventListenner: function(callback){
        this.removeListener('new_event', callback);
    },

    addNewEventMessageListenner: function(callback){
        this.on('new_message_event', callback);
    },

    removeNewEventMessageListenner: function(callback){
        this.removeListener('new_message_event', callback);
    },

    addNewEventStreamListenner: function (callback) {
        this.on('new_event_stream', callback);
    },

    removeNewEventStreamListenner: function (callback) {
        this.removeListener('new_event_stream', callback);
    },

    addNewEventHistoryListenner: function (callback) {
        this.on('event_history', callback);
    },

    removeNewEventHistoryListenner: function (callback) {
        this.removeListener('event_history', callback);
    },

    addEventListenner: function (eventName, callback){
        this.on(eventName, callback);
    },

    removeEventListenner: function (eventName, callback){
        this.removeListener(eventName, callback);
    },

    getEventsType: function(type){
        if (DataStore.eventCollection.hasOwnProperty(type))
            return DataStore.eventCollection[type];
        return [];
    },

    getEventTypeId: function(type, id){
        var eventType = this.getEventsType(type);
        var len = eventType.length;
        for (var i = 0; i < len; ++i){
            if (eventType[i].actionID == id) return eventType[i];
        }
        return null;
    },

    getEventsObject: function(typ, id){
        var events = this.getEventsType(typ);
        var eventsTyped = [];
        for (var i = events.length - 1; i >= 0; i--) {
            if (events[i].objetID == id) eventsTyped.push(events[i]);
        };
        return eventsTyped;
    }
});

var DataStore = {
    eventCollection: {},

    eventPosCollection: {}
}

var CollectionActivity = {
    addCollectionFromStream: function(data){
        var typ = data.typ;
        data = data.data;
        if (!Array.isArray(data)){
            data = [data];
        }
        if (!DataStore.eventCollection.hasOwnProperty(typ)){
            DataStore.eventCollection[typ] = [];
        }
        console.log(data);
        if (ObjectStore.isValidId(data[0].objetID))
            return this.proccesEventStreamOrChange(data, typ);
        return { newElm: false }
    },

    addCollectionFromHistory: function(data){
        var typ = data.typ;
        data = data.data;
        if (!Array.isArray(data)){
            data = [data];
        }
        if (!DataStore.eventCollection.hasOwnProperty(typ)){
            DataStore.eventCollection[typ] = [];
        }
        return this.proccesEventFromHistory(data, typ);
    },

    proccesEventFromHistory: function(data, typ){
        var lenData = data.length;
        console.log(data);
        for (var i = 0; i < lenData; ++i){
            /*Sauvergarder les event*/
            if (ObjectStore.isValidId(data[i].objetID))
                DataStore.eventCollection[typ].push(data[i]);
            else
                return false;      
        }
        return true;
    },
     /*Ce fonction peut etre lancé par un event qui arrive ou bien si l'utilisateur
        change une propieté d'un event*/
    proccesEventStreamOrChange: function(data, typ){
        /*Fake - Probleme de format DATE*/
        /*data[0].createdTime.substr(0, 10) + data[0].createdTime.substr(11, 19)*/
        var d = new Date(data[0].createdTime);
        data[0].createdTime = d.toJSON().substr(0, 10) + ' ' + d.toLocaleTimeString();
        /*console.log('proccesEventStreamOrChange'); */      
        
        /*Fake*/
        if (typ === 'alert' && data.length === 1 && 
            data[0].messageAlerte !== null){
            if (!data[0].hasOwnProperty('innerOpertation') ||
                data[0].innerOpertation === false) {
                var listEvent = DataStore.eventCollection[typ];

                var r_evnt = null;    
                for (var i = listEvent.length - 1; i >= 0; i--) {
                    if (listEvent[i].actionID == data[0].actionID){
                        r_evnt = listEvent[i]; break;
                    }
                };

                var evnt = {
                    objetID: data[0].objetID,
                    typ: 'alert',
                    infoData: Object.assign({}, data[0])
                }
                EventActionCreator.fakeEvent('fake_event', evnt);
                
                if (r_evnt != null){
                    /*console.log(r_evnt);*/
                    r_evnt.messageAlerte = data[0].messageAlerte;
                    ManageEmmitter.emit(typ + '-' + data[0].actionID);
                    return {newElm: false};
                }
            }
        } /*End Fake*/

        var lenData = data.length;
        for (var i = 0; i < lenData; ++i){
            /*Sauvergarder les event*/ 
            /*Traiter les events '(data)' et changer les props de EventStore*/
            var changeControl = this.changePropEvent(data[i], typ);
            /*transformer a un object '{}' facile a lire pour ObjectStore et MapBrige*/
            var d = this.eventToDataObject(data[i], typ);

            var innerOpertation = false;
            if (data[i].hasOwnProperty('innerOpertation') && data[i].innerOpertation)
                innerOpertation = true;

            d['innerOpertation'] = innerOpertation;

            if (changeControl.newElm || changeControl.changedElem){
                //console.log('changedEvent');
                if (changeControl.changedElem){
                    //console.log('changedElem');
                    ManageEmmitter.emit(typ + '-' + data[i].actionID);
                    ManageEmmitter.emit('changed_' + typ);
                }
                data[i].innerOpertation = false;
                ObjectActionCreator.changedObjectProps(d);
                if (innerOpertation){
                    WebApiUtils.eventChangedToServer(d);
                } /*ExitToNet*/
            }
            /*
                1- Il faut filer l'objet 'd' au serveur Nodejs pour envoyer ce change aux autres clients qui peuvent 
                voir l'event
                2- Comme ça on peut avoir un système qui distribue les events.
            */
        }
        return changeControl;
    },

    eventToDataObject: function(data, typ){
        data = {
            typ: typ,
            objetID: data.objetID,
            actionID: data.actionID,
            infoData: data,
            prisEnCompte: data.prisEnCompte,

        }
        return data;
    },

    changePropEvent: function(data, typ){
        var newElm = true;
        var changedElem = false;
        /*
        -Changer les props des events.
        -Verifier si l'event ou les event sont pas egals à ceux qui sont déjà traité
        -si le event existe et il est different a celui qui est traité il faul emit l'event 'typeEvent-actionID'
        */
        var events = DataStore.eventCollection[typ];
        var lenEvent = events.length;
        for (var i = lenEvent - 1; i >= 0; i--) {
            if (events[i].actionID != data.actionID) continue;
            //console.log('changePropEvent: ' + data.prisEnCompte);
            if (events[i].prisEnCompte != data.prisEnCompte){
                changedElem = true;
                newElm = false;
                events[i].prisEnCompte = data.prisEnCompte; /*Change la propieté prisEnCompe d'un event
                changé par l'utilisateur ou d'un autre client*/
                /*console.log(events.length);*/
                events.splice(i, 1); /*Suprime l'event*/
                /*console.log(events.length);*/
                /*console.log(DataStore.eventCollection[typ].length);*/
                return {
                    newElm: newElm,
                    changedElem: changedElem
                };
            }else if (events[i].prisEnCompte == data.prisEnCompte){
                newElm = false;
                return {
                    newElm: newElm,
                    changedElem: changedElem
                };
            };
        };

        DataStore.eventCollection[typ].push(data);

        return {
            newElm: newElm,
            changedElem: changedElem
        };
    },

    clasifyEvent: function(evnt){}
}

var ManageEmmitter = {
    emitNewEventHistory: function(){ EventStore.emit('event_history'); 
    },

    emitNewEvent: function(){
        /*console.log('EventStore-new_event');*/
        EventStore.emit('new_event');
    },

    emit: function(eventName){ EventStore.emit(eventName); }
};

var ManageAction = {
    receive_events_stream_history: function(data){
        if (CollectionActivity.addCollectionFromHistory(data)){
            ManageEmmitter.emitNewEvent();
            ManageEmmitter.emitNewEventHistory();
        }
    },
    
    receive_events_stream: function(data){
        /*console.log('receive_events_stream-EventStore');*/
        /*console.log(data);*/
        var changeControl = CollectionActivity.addCollectionFromStream(data);
        if (changeControl.newElm)
            ManageEmmitter.emitNewEvent();
    },

    receive_event_change: function(data){

    },

    receive_propagated_events: function(data){
        data.propagated = true;
        /*ManageAction.receive_events_stream(data);*/
    }
}

var handleManageAction = function(payload){
    var action = payload.action;
    if (ManageAction.hasOwnProperty(action.type))
        ManageAction[action.type](action.data);
}

EventStore.dispatchToken = AppDispatcher.register(handleManageAction);

module.exports = EventStore;