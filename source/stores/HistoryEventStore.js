var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var ObjectActionCreator = require('../actions/ObjectActionCreator');

var WebApiUtils = require('../utils/WebApiUtils');

var HistoryEventStore = assign({}, EventEmitter.prototype, {

    addNewEventListenner: function(callback){
        this.on('new_event', callback);
    },

    removeNewEventListenner: function(callback){
        this.removeListener('new_event', callback);
    },

    getEventTypeId: function(type, id){
        var eventType = DataStore.eventByTypeCollection[type];
        var len = eventType.length;
        for (var i = 0; i < len; ++i){
            if (eventType[i].actionID == id) return eventType[i];
        }
        return null;
    },

    getEventsByObjectId: function(id){
        return DataStore.eventCollection[id];
    }

   /* getEventsObject: function(typ, id){
        var events = this.getEventsType(typ);
        var eventsTyped = [];
        for (var i = events.length - 1; i >= 0; i--) {
            if (events[i].objetID == id) eventsTyped.push(events[i]);
        };
        return eventsTyped;
    }*/
});

var DataStore = {
    eventCollection: {},

    eventByTypeCollection: {}
}

var CollectionActivity = {
    addCollectionFromHistory: function(data){
        /*console.log('from history');*/
        /*console.log(data);*/
        if (!Array.isArray(data)){
            data = [data];
        }

        return this.proccesEventFromHistory(data);
    },

    proccesEventFromHistory: function(data){
        var lenData = data.length;
        for (var i = 0; i < lenData; i++) {
            if (!DataStore.eventCollection.hasOwnProperty(data[i].objetID))
                DataStore.eventCollection[data[i].objetID] = [];
            if (!DataStore.eventByTypeCollection.hasOwnProperty(data[i].type))
                DataStore.eventByTypeCollection[data[i].type] = [];

            DataStore.eventCollection      [ data[i].objetID ].push(data[i]);
            DataStore.eventByTypeCollection[ data[i].type ].push(data[i]);
        };
    }
}

var ManageEmmitter = {
    emitNewEvent: function(){
        HistoryEventStore.emit('new_event');
    }
};

var ManageAction = {
    receive_events_history: function(data){
        /*console.log(data);*/
        CollectionActivity.addCollectionFromHistory(data);
        ManageEmmitter.emitNewEvent();
    }
}

var handleManageAction = function(payload){
    var action = payload.action;
    if (ManageAction.hasOwnProperty(action.type))
        ManageAction[action.type](action.data);
}

HistoryEventStore.dispatchToken = AppDispatcher.register(handleManageAction);

module.exports = HistoryEventStore;