var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var WebAPIUtils = require('../utils/WebAPIUtils');

EventEmitter.prototype.setMaxListeners(Infinity);

var ObjectStore = assign({}, EventEmitter.prototype, {

	addNewObjectsListener: function(callback){
		this.on('new_objects', callback);
	},

	removeNewAlertListener: function(callback){
		this.removeListener('new_objects', callback);
	},

	addChangeObjectListenner: function(callback){
		this.on('change_object', callback);
	},

	removeChangeObjectListenner: function(callback){
		this.removeListener('change_object', callback);
	},

	addInitObjectListenner: function(callback){
		this.on('init_object', callback);
	},

	removeInitObjectListenner: function(callback){
		this.removeListener('init_object', callback);
	},

	addListenner: function(eventName, callback){
		this.on(eventName, callback);
	},

	deleteListener: function(eventName, callback){
		this.removeListener(eventName, callback);
	},

	getObject: function(id){ return DataStore.objectCollection[id]; },

	getObjects: function(){ return DataStore.objectCollection; },

	getNiveauUrgence: function(){ return DataStore.niveauUrgence; },

	getLastObjectId: function(){ return DataStore.lastChangedObjects[ DataStore.lastChangedObjects.length - 1 ]; },

	isValidId: function(id){ return  DataStore.objectCollection.hasOwnProperty(id); }
});

var DataStore = {
	objectCollection: {},

	niveauUrgence: 0,

	lastChangedObjects:[]
}

var CollectionActivity = {
	addObject: function(object){
		if (isNaN(object.objLat) || !parseInt(Number(object.objLat)) ||
			isNaN(object.objLong) || !parseInt(Number(object.objLong)) ){
			return;
		}
		object = this.refillObject(object);
		DataStore.objectCollection[ object['objetID'] ] = object;
		if (object.urgence) DataStore.niveauUrgence += parseInt(object.niveauUrgence);
	},

	refillObject: function(object){
		var keys = ['alerts', 'evalmsg', 'eval', 'totalAlerts', 'totalEvalmsg', 'totalEval'];
		for (var i = keys.length - 1; i >= 0; i--) {
			if (!object.hasOwnProperty(keys[i]) ||
				object[ keys[i] ] == null)
				object[ keys[i] ] = 0;
		};
		if (object.hasOwnProperty('image') && 
			object.image == '' || object.image == null )
			object.image = 'public/img_objet/Pasd-ImagesDisponible.jpg';
		return object;
	},

	addObjects: function(objects){
		var len = objects.length;

		for(var i=0; i<len; ++i){
			this.addObject(objects[i]);
		}
		ManageEmmitter.emitInitObject();
	},

	initCollection: function(data){
		if (Array.isArray(data)){
			this.addObjects(data);
			return;
		}
		this.addObject(data);
	},

	setUrgence: function(data){
		if (!Array.isArray(data)){
			data = [data];
		}
		this.setObjectsUrgence(data);
	},

	setObjectUrgence: function(data){
		/*console.log(data);*/
		
		if (!data.prisEnCompte){
			DataStore.objectCollection[ data.objetID ].niveauUrgence++;
			DataStore.objectCollection[ data.objetID ].urgence = true;

			DataStore.niveauUrgence++;

			/*console.log(DataStore.objectCollection[ data.objetID ]);*/
			if (data.typ == 'alert'){
				DataStore.objectCollection[ data.objetID ].alerts++;
				if (!data.innerOpertation)
					DataStore.objectCollection[ data.objetID ].totalAlerts++;
			}else if (data.typ == 'evalmsg'){
				DataStore.objectCollection[ data.objetID ].evalmsg++;
				if (!data.innerOpertation)
					DataStore.objectCollection[ data.objetID ].totalEvalmsg++;
			}else{
				DataStore.objectCollection[ data.objetID ].eval++;
				if (!data.innerOpertation)
					DataStore.objectCollection[ data.objetID ].totalEval++;
			}

			/*console.log(DataStore.objectCollection[ data.objetID ]);*/
		}
		else{
			DataStore.objectCollection[ data.objetID ].niveauUrgence--;
			if (!DataStore.objectCollection[ data.objetID ].niveauUrgence)
				DataStore.objectCollection[ data.objetID ].urgence = false;

			DataStore.niveauUrgence--;

			if (data.typ == 'alert'){
				DataStore.objectCollection[ data.objetID ].alerts--;
				if (!data.innerOpertation)
					DataStore.objectCollection[ data.objetID ].totalAlerts++;
			}else if (data.typ == 'evalmsg'){
				DataStore.objectCollection[ data.objetID ].evalmsg--;
				if (!data.innerOpertation)
					DataStore.objectCollection[ data.objetID ].totalEvalmsg++;
			}else{
				DataStore.objectCollection[ data.objetID ].eval--;
				if (!data.innerOpertation)
					DataStore.objectCollection[ data.objetID ].totalEval++;
			}
		}
	},

	setObjectsUrgence: function(data){
		var last = [];

		var len = data.length;
		for(var i=0; i<len; ++i){
			this.setObjectUrgence(data[i]);
			last.push(data[i].objetID);
		}

		DataStore.lastChangedObjects.push(last);
	},

	manageObjectPropagation: function(data){
	},

	addNewProperties: function(data){
		if (typeof data === typeof undefined){
			return false;
		}
		var keysObject = Object.keys(data);
		var lenKeysObject = keysObject.length;
		
		while(lenKeysObject--){
			if (parseInt(keysObject[lenKeysObject]) === parseInt(0)) continue;
			if (keysObject[lenKeysObject] == "objetID") continue;

			DataStore.objectCollection[data.objetID][ keysObject[lenKeysObject] ] = data[ keysObject[lenKeysObject] ];
		}
		return true;
	}
}

var ManageEmmitter = {
	emitNewsObject: function(){ ObjectStore.emit('new_objects'); },

	emitChangeObject: function(){ ObjectStore.emit('change_object'); },

	emitInitObject: function(){ ObjectStore.emit('init_object'); },

	emit: function(eventName){ ObjectStore.emit(eventName); }
};

var ManageAction = {
	receive_objects: function(data){
 		CollectionActivity.initCollection(data);
 		ManageEmmitter.emitNewsObject();
 		/*console.log('receive objects');
 		console.log(data);*/
 		WebAPIUtils.initializeEvents();
 	},

 	changed_object_props: function(data){
 		/*Mettre a jours les objets*/
 		CollectionActivity.setUrgence(data);
 		ManageEmmitter.emitChangeObject();
 	},

 	receive_new_property: function(data){
 		console.log('receive_new_property - ObjectStore');
 		var executed = CollectionActivity.addNewProperties(data);	
 		if (executed){
 			var eventName = 'newPropertyObject-' + data.objetID;
 			ManageEmmitter.emit(eventName);
 		}
 	}
}

var handleManageAction = function(payload){
	var action = payload.action;
	if (ManageAction.hasOwnProperty(action.type))
		ManageAction[action.type](action.data);
}

ObjectStore.dispatchToken = AppDispatcher.register(handleManageAction);

module.exports = ObjectStore;