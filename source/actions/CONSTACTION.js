var ACTIONS = {
	MAP: {
		RECEIVEINFOMAP: 'receive_info_map'
	},

	OBJECT: {

	},

	EVENT: {
		RECEIVEEVENTSSTREAM: 'receive_events_stream',
		RECEIVEEVENTHISTORY: 'receive_events_history',
		RECEIVEEVENTHISTORYMAP: 'receive_events_history_map',
		CHANGESTATUSLU: 'change_event_status_lu'
	}
}