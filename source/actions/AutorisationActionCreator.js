var AppDispatcher = require('../dispatcher/AppDispatcher');

function autorisation(_event) {
	var action = {
		type: 'receive_autorisation',
		data: _event
	};

	AppDispatcher.handleStoreInnerAction(action);
}

module.exports = {
	autorisation: autorisation
}