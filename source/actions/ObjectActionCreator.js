var AppDispatcher = require('../dispatcher/AppDispatcher');

function receiveObjects(eventData){
	var action = {
		type: 'receive_objects',
		data: eventData
	};
	console.timeEnd('getObjects');
	AppDispatcher.handleStoreInnerAction(action);
}

function changedObjectProps(eventData){
	var action = {
		type: 'changed_object_props',
		data: eventData
	}

	AppDispatcher.handleStoreInnerAction(action);
}

function receiveNewProperty(eventData){
	var action = {
		type: 'receive_new_property',
		data: eventData
	}
	AppDispatcher.handleStoreInnerAction(action);
}

module.exports = {
	receiveObjects: receiveObjects,
	changedObjectProps: changedObjectProps,
	receiveNewProperty: receiveNewProperty
};