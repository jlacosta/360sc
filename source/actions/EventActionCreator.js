var AppDispatcher = require('../dispatcher/AppDispatcher');

function receiveEventsStream(_event) {
	var action = {
		type: 'receive_events_stream',
		data: _event
	};

	AppDispatcher.handleStoreInnerAction(action);
}

function receiveEventsHistory(_events){
	console.log(_events);
	var action = {
		type: 'receive_events_stream_history',
		data: _events
	};
	AppDispatcher.handleStoreInnerAction(action);
}

function changeEventStatus(type, _event) {
	var action = {
		type: type,
		data: _event
	};

	AppDispatcher.handleStoreInnerAction(action);
}

function fakeEvent(type, _event) {
	var action = {
		type: type,
		data: _event
	};
	console.log('fake');
	AppDispatcher.handleStoreInnerAction(action);
}

function receivePropagatedEventsStream(_event){
	_event['propagated'] = true;
	var action = {
		type: 'receive_events_stream',
		data: _event
	};

	AppDispatcher.handleStoreInnerAction(action);
}

function receiveEventsFromHistory(_event){
	var action = {
		type: 'receive_events_history',
		data: _event
	}
	AppDispatcher.handleStoreInnerAction(action);
}

module.exports = {
	receiveEventsStream: receiveEventsStream,
	receiveEventsHistory: receiveEventsHistory,
	receiveEventsFromHistory: receiveEventsFromHistory,
	receivePropagatedEventsStream: receivePropagatedEventsStream,
	fakeEvent: fakeEvent
};