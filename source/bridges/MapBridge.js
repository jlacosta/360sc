var AppDispatcher = require('../dispatcher/AppDispatcher');

var MapUtils = require('../utils/MapUtils');

var MapBridge = {
	init: function(){
		this.dispatchToken = AppDispatcher.register(handleManageAction);
	}
}

var ManageAction = {

 	changed_object_props: function(data){
 		MapUtils.onNewEventsStream(data);
 	},

 	fake_event: function(data){
 		MapUtils.onNewEventsStream(data);
 	}
}

var handleManageAction = function(payload){
	var action = payload.action;
	if (ManageAction.hasOwnProperty(action.type))
		ManageAction[action.type](action.data);
}

module.exports = MapBridge;