var React = require('react');
var ReactDOM = require('react-dom');
var ReactDOMServer = require('react-dom/server');

var Administration = require('./components/Administration.react');

ReactDOM.render(<Administration />, document.getElementById('react-Administration-header'));