var React = require('react');
var ReactDOM = require('react-dom');
var ReactDOMServer = require('react-dom/server');

var Application = require('./components/Application.react');
var WebAPIUtils = require('./utils/WebAPIUtils');
var ContentInfoWindowMapComponent = require('./components/ContentInfoWindowMapComponent.react');
var MarkerInfoWindow = require('./components/MarkerInfoWindow.react');
var AlertInfoWindowComponent = require('./components/AlertInfoWindowComponent.react');
var EvalMsgInfoWindowComponent = require('./components/EvalMsgInfoWindowComponent.react');
var EvalInfoWindowComponent = require('./components/EvalInfoWindowComponent.react');
//test//
var ObjectStore = require('./stores/ObjectStore');
var EventStore = require('./stores/EventStore');
var HistoryEventStore = require('./stores/HistoryEventStore');
//test//

var streetSign = require('./utils/filters/StreetSign');

//initialize after Application mounted//
WebAPIUtils.initializeObjects();
WebAPIUtils.initializeStreaming();


window.ReactBrigde = {
	reactClass: { React: React, ReactDOM: ReactDOM, ReactDOMServer: ReactDOMServer },
	reactComponent: {
		ContentInfoWindowMapComponent: ContentInfoWindowMapComponent,
		MarkerInfoWindow: MarkerInfoWindow,
		AlertInfoWindowComponent: AlertInfoWindowComponent,
		EvalMsgInfoWindowComponent: EvalMsgInfoWindowComponent,
		EvalInfoWindowComponent: EvalInfoWindowComponent
	},
	momentClass: require('moment'),
	filters: {
		streetSign: streetSign
	}
}

ReactDOM.render(<Application />, document.getElementById('react-application'));