var React = require('react');
var ReactDOM = require('react-dom');
var ReactAddons = require('react-addons');

var LIComponent = require('./LIComponent.react');
var AComponent = require('./AComponent.react');
var BackLIButtonComponent = require('./BackLIButtonComponent.react');
var ULSimpleComponent = require('./ULSimpleComponent.react');
var LiListComponent = require('./ULComplexComponent.react');
var LiListEventComponent = require('./ULComplexComponentAlert.react');
var LiListObjectComponent = require('./LiListObjectComponent.react');
var AAlertComponent = require('./AAlertComponent.react');
var AMessageComponent = require('./AMessageComponent.react');

var ULComponent = {};

ULComponent.simple = ULSimpleComponent;
ULComponent.li = LIComponent;
ULComponent.a = AComponent;
ULComponent.backButton = BackLIButtonComponent;
ULComponent.LiListEvent = LiListEventComponent;
ULComponent.LiListObject = LiListObjectComponent;
ULComponent.aAlert = AAlertComponent;
ULComponent.aMessage = AMessageComponent;

module.exports = ULComponent;