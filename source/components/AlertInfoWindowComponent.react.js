var React = require('react');
var ReactDOM = require('react-dom');

var EventStore = require('../stores/EventStore');
var HistoryEventStore = require('../stores/HistoryEventStore');
var ObjectStore = require('../stores/ObjectStore');
var EventActionCreator = require('../actions/EventActionCreator');
var MapUtils = require('../utils/MapUtils');

var _ = require('underscore');

var WebAPIUtils = require('../utils/WebAPIUtils');

var AlertInfoWindowComponent = React.createClass({
	displayName: 'AlertInfoWindowComponent',

	getInitialState: function(){
		return {
			loadedInformation: false,
			info: {}
		}
	},

	componentDidMount: function(){
		var objectID = this.props.info.objetID;
		var infoObject = AlertInfoWindowComponent.prototype.ObjectStore.getObject(objectID);
		AlertInfoWindowComponent.prototype.WebAPIUtils.searchInfoObjectToTab(objectID, infoObject.idParent);
		AlertInfoWindowComponent.prototype.ObjectStore.addListenner('newPropertyObject-' + this.props.info.objetID, this.onNewProperty);
	},

	onNewProperty: function(){
		console.log('onNewProperty - AlertInfoWindowComponent');
		AlertInfoWindowComponent.prototype.ObjectStore.deleteListener('newPropertyObject-' + this.props.info.objetID, this.onNewProperty);
		this.state.info = AlertInfoWindowComponent.prototype.ObjectStore.getObject( this.props.info.objetID );
		this.setState({loadedInformation: true});
	},

	handleClickCheck: function(event, state, actionID){
		/*
		* Si le state est 'false' on suprime l'event, 
		* du coup il faut fermer le popup
		*/
	
		var r_evnt = AlertInfoWindowComponent.prototype.EventStore.getEventTypeId('alert', actionID);
		
		if (r_evnt != null){ /*la prisEnCompte d'event est false*/
			var evnt = _.clone(r_evnt);
			evnt.prisEnCompte = state;
			evnt.innerOpertation = true;

			var data = {
				typ: 'alert',
				data: [evnt] 
			}
			//console.log('handleClickCheckbox: ' + evnt.actionID + ';; ' + r_evnt.prisEnCompte + '!= ' + evnt.prisEnCompte + ';' + state);
			AlertInfoWindowComponent.prototype.EventActionCreator.receiveEventsStream(data);
		}
		else{
			var r_evnt = AlertInfoWindowComponent.prototype.HistoryEventStore.getEventTypeId('alert', actionID);
			var evnt = _.clone(r_evnt);
			evnt.prisEnCompte = false;
			evnt.innerOpertation = true;

			var data = {
				typ: 'alert',
				data: [evnt] 
			}
			//console.log('handleClickCheckbox: ' + evnt.actionID + ';; ' + r_evnt.prisEnCompte + '!= ' + evnt.prisEnCompte + ';' + state);
			AlertInfoWindowComponent.prototype.EventActionCreator.receiveEventsStream(data);
		}		
	},

	render: function(){
		return (
			<div className="AlertInfoWindowComponent">
				<div></div>
				<div className="title">
					<span className="info">alerte</span>
					<a href="#" onClick={this.handleClickCheck.bind(this, {}, true, this.props.info.actionID)}>
						<span className="flaticon-check" aria-hidden="true"></span>
					</a>	
				</div>
				<div className="subTitle">
					<div>{this.state.info.nameType}</div>
					<div>{this.props.info.objetName}</div>
				</div>
				<div className="img">
					<img className="center-block" src={'./' + this.props.info.objetImage} />
				</div>
				<div className="body">
					<div>{this.props.info.customerName}</div>
					<div>{this.props.info.createdTime}</div>
					<div>{this.props.info.messageAlerte}</div>
					<div>Localisation</div>
					<div>{this.props.info.adresse}</div>
				</div>
			</div>
		);
	}
});

AlertInfoWindowComponent.prototype.EventStore = EventStore;
AlertInfoWindowComponent.prototype.HistoryEventStore = HistoryEventStore;
AlertInfoWindowComponent.prototype.EventActionCreator = EventActionCreator;
AlertInfoWindowComponent.prototype.WebAPIUtils = WebAPIUtils;
AlertInfoWindowComponent.prototype.ObjectStore = ObjectStore;

module.exports = AlertInfoWindowComponent;