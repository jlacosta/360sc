var React = require('react');
var ReactDOM = require('react-dom');

LIComponent = React.createClass({
	displayName: 'LIComponent',

	componentWillMount: function(){
	},

	componentWillUpdate: function(){
	},

	renderChildren: function(){
		var self = this;
		return React.Children.map(this.props.children, function(child){
			return React.cloneElement(child, {
				info: self.props.info
			});
		});
	},

	render: function(){
		if (this.props.keyName){
			return(
				<li className={this.props.PropClassName}>
					{this.renderChildren()}
				</li>
			);
		}
		else{
			return(
				<li className={this.props.PropClassName}>
					<div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						{this.props.children}
					</div>	
				</li>
			);	
		}
	}
});

module.exports = LIComponent;