var React = require('react');
var ReactDOM = require('react-dom');
var moment = require('moment');

FilterStreetSingComponent = React.createClass({
	displayName: 'FilterStreetSingComponent',

	getInitialState: function(){
		return {
			checkboxState: {
				1: true, 2: true, 4: true
			},
			diffInput: 0,
			chekboxValue: 7,
			Slider: null
		}
	},

	getDafaultProps: function(){
		return {
			show: this.props.show || false,
			initialDate: this.props.initialDate || 0,
			finalValableDate: this.props.finalValableDate || 0,
			min: this.props.min || 0,
			max: this.props.max || 1,
			step: 1,
			value: this.props.value || [0, 1],
			Slider: null,
			leftDate: null,
			rightDate: null,
			list: {},
		}
	},

	componentDidMount: function(){
		//this.setSlider();
	},

	componentDidUpdate: function(){
		this.setSlider();
	},

	formatter: function(values) {
		/*console.log(value);*/
		var values = this.setMinMaxDate(values)
    	return values[0].format('D-M-Y') + ' - ' + values[1].format('D-M-Y');
  	},

  	setSlider: function(){
  		var self = this;
  		if (this.state.Slider != null) return;
  		
  		var slider = Slider = $('.opt-date-range-filter input#ext1').slider({
          	formatter: this.formatter
        });

        $('.opt-date-range-filter input#ext1').on('slideStop', function(values){
    	   self.onClickFilter(); 	
        });
        self.setState({
    		Slider: slider
    	});
  	},

  	setMinMaxDate: function(values){
  		var leftDate = moment(this.props.initialDate).add(values[0], 'month');
		var rightDate = moment(this.props.initialDate).add(values[1], 'month');
		return [leftDate, rightDate];
  	},

  	onClickFilter: function(){
  		console.log('onClickFilter');
  		this.onFilter();
  	},

  	onFilter: function(){
  		var values = $('.opt-date-range-filter input#ext1').slider('getValue');
  		values = this.setMinMaxDate(values);
  		this.props.activeFilter({
  			initialDate: values[0],
  			finalDate: values[1],
  		}, this.state.chekboxValue, this.state.diffInput);
  	},

  	onClickBtn: function(stateBtn){
  		var self = this;
  		this.state.checkboxState[stateBtn] = !this.state.checkboxState[stateBtn];
  		if (this.state.checkboxState[stateBtn]){
  			this.state.chekboxValue |= stateBtn;
  		}
  		else
  			this.state.chekboxValue ^= stateBtn;
  		
  		console.log(this.state.chekboxValue);

  		var sel = '#f' + stateBtn + ' span.glyphicon' ; 	
  		$(sel).toggleClass('glyphicon-check glyphicon-unchecked');
  		this.onClickFilter();
  	},

  	onChangeInputNumber: function(event){
  		this.state.diffInput = event.target.value;
  	},

	render: function(){
		if (this.props.show){
			return (
				<div className="streetSign">
					<li className="">
						<a href="javascript:">
							<span className="icon entypo">
								
							</span>
							<span className="text span80pecent">
								{ 'Date' }
								<div className="opt-date-range-filter">
									<input id="ext1" 
									type="text" 
									className="span2" 
									value="" 
									data-slider-min={this.props.min}
									data-slider-max={this.props.max} 
									data-slider-step={this.props.step} 
									data-slider-value={JSON.stringify(this.props.value)} />
								</div>
								{/*<div className="filters">
									<button id="f1" onClick={this.onClickBtn.bind(this, 1)} className="btn btn-default btn-sm">Production</button>
									<button id="f2" onClick={this.onClickBtn.bind(this, 2)} className="btn btn-default btn-sm">Prévenir</button>
									<button id="f4" onClick={this.onClickBtn.bind(this, 4)} className="btn btn-default btn-sm">Prévoir</button>
								</div>*/}
							</span>
						</a>
					</li>
					<li className="">
						<a href="#" id="f1" onClick={this.onClickBtn.bind(this, 1)}>
							<span className="glyphicon glyphicon-check" aria-hidden="true">
								
							</span>
							<span className="text">
								Panneaux en production
								<span className="badge">
									{ (this.props.list['street-sign-default'] || 0) }
								</span>
							</span>
						</a>
					</li>
					<li className="">
						<a href="#" id="f2" onClick={this.onClickBtn.bind(this, 2)}>
							<span className="glyphicon glyphicon-check" aria-hidden="true">
								
							</span>
							<span className="text">
								Remplacement à prévoir
								<span className="badge">
									{ (this.props.list['street-sign-notif'] || 0) }
								</span>
							</span>
						</a>
					</li>
					<li className="">
						<a href="#" id="f4" onClick={this.onClickBtn.bind(this, 4)}>
							<span className="glyphicon glyphicon-check" aria-hidden="true">
								
							</span>
							<span className="text">
								Opération à planifier
								<span className="badge">
									{ (this.props.list['street-sign-alert'] || 0) }
								</span>
							</span>
						</a>
					</li>
					<li className="divider"></li>
					<li className="">
						<a href="javascript:">
							<span className="icon entypo"> </span>
							<span className="text span80pecent">
								{ 'Nombre de mois avant operation de remplacement' }
								<input className="diff" type="number" defaultValue="7" min="0" 
								onChange={this.onChangeInputNumber}/>
							</span>
						</a>
					</li>
				</div>
			);
		}	
		return <label value=""/>
	}
});

module.exports = FilterStreetSingComponent;