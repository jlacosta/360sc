var React = require('react');
var ReactDOM = require('react-dom');
var ReactAddons = require('react-addons');
var _ = require('underscore');

var LIComponent = require('./LIComponent.react');
var AComponent = require('./AComponent.react');
var BackLIButtonComponent = require('./BackLIButtonComponent.react');

var LiListComponent = React.createClass({
	displayName: 'LiListComponent',

	componentWillUpdate: function(){
	},

	getDefaultProps: function (){
		return {
			PropClassName: '',
			groupInfo: {}
		}
	},

	getGroupInfoKeys: function(){
		return Object.keys(this.props.groupInfo);
	},

	renderChildrenGroupInfo: function(){
		var children = [];
		var groupInfo = this.props.groupInfo;
		var groupInfoKeys = this.getGroupInfoKeys();
		var groupInfoKeysLen = groupInfoKeys.length;

		for (var i = groupInfoKeysLen - 1; i >= 0; i--) {
			React.Children.forEach(this.props.children, function(child){
				children.push(React.cloneElement(child, {
					keyName: groupInfoKeys[i],
					key: groupInfoKeys[i],
					info: groupInfo[ groupInfoKeys[i] ]
				}));
			});
		}
		
		return children;
	},

	render: function(){
		return (
			<div id="LiListComponent"> 
				{this.renderChildrenGroupInfo()}
			</div>
		);
	}
});

module.exports = LiListComponent;