var React = require('react');
var ReactDOM = require('react-dom');

var ULComponent = require('./ULComponent.react');
var StateParkEvaluationInfoComponent = require('./StateParkEvaluationInfoComponent.react');

var EventStore = require('../stores/EventStore');
var ObjectStore = require('../stores/ObjectStore');
var EventActionCreator = require('../actions/EventActionCreator');
var WebApiUtils = require('../utils/WebApiUtils');

var TabsInformationEvaluationListComponent = React.createClass({
	displayName: 'TabsInformationEvaluationListComponent',

	getInitialState: function(){
		var events = EventStore.getEventsObject('eval', this.props.objectId);
		var object = ObjectStore.getObject(this.props.objectId);
		if (!Array.isArray(events)) events = [];
		return {
			events: events,
			lenEvents: events.length,
			info: object,
			lastEvent: null,
			updateCounter: false
		}
	},

	componentDidUpdate: function(){
		this.triggerResizeWindow();
	},

	componentDidMount: function(){
		EventStore.addNewEventListenner(this.onChangeEventCollection);
		EventStore.addEventListenner('changed_eval', this.onChangeEventCollection);
		ObjectStore.addChangeObjectListenner(this.onChangeObjectCollection);
	},

	shouldComponentUpdate: function(nextProps, nextState){
		if (this.props.active == false && nextProps.active == true) return true;
		if (nextState.events.length != this.state.lenEvents)
			return true;
		if (nextState.updateCounter) {
			nextState.updateCounter = false;
			return true;
		}
		return false;
	},

	onChangeEventCollection: function(){
		var events = EventStore.getEventsObject('eval', this.props.objectId);
		this.setState({
			events: events,
			lenEvents: events.length 
		});
	},

	onChangeObjectCollection: function(){
		/*console.log('onChangeObjectCollection - TabsInformationAlertListComponent');*/
		var object = ObjectStore.getObject(this.props.objectId);
		this.setState({
			info: object,
			updateCounter: true
		});
	},

	defaultValue: function(value){
		if (value != undefined &&
			value != null)
			return value;
		return 0;
	},

	moreEvent: function(){
		var date = 0;
		if (this.state.lastEvent != null){
			date = this.state.lastEvent.createdTime;
		}
		var option = {
			fn: EventActionCreator.receiveEventsHistory,
			typ: 'eval',
			date: date,
			objectID: this.props.objectId
		}
		WebApiUtils.getMoreEventsByObject(option);
	},

	triggerResizeWindow: function(){
		$(window).trigger('resize');
	},

	render: function(){
		return (
			<div role="tabpanel" className="tab-pane messages TabsInformationEvaluationListComponent" id={"evaluations" + this.props.objectId}>
		    	<div className="a-div-head">
		    		<div className="row">
		    			<div className="col-sm-4 col-md-4 col-lg-4">
		    				<div className="title-vide">Au Total</div>
		    				<span className="glyph-icon flaticon-smiley flaticon-yellGreen" aria-hidden="true"></span>
		    			</div>
		    			<div className="col-sm-4 col-md-4 col-lg-4 total">
		    				<div className="title-total">Au Total</div>
		    				<div className="circle-outside">
		    					<div className="circle-inside">
		    						<div className="text-total">{this.defaultValue(this.state.info.totalEval)}</div>
		    					</div>
		    				</div>
		    			</div>
		    			<div className="col-sm-4 col-md-4 col-lg-4 nosee">
		    				<div className="title-total">Non Lu</div>
		    				<div className="circle-outside">
		    					<div className="circle-inside">
		    						<div className="text-nosee">{this.defaultValue(this.state.info.eval)}</div>
		    					</div>
		    				</div>
		    			</div>
		    		</div>	
		    	</div>
		    	<div className="a-div-body">
		    		<ul className="event-message StateParkMessageListComponent" id="StateParkEvaluationList">
		    			<li className="btnMoreEvent">
							<a href="#" onClick={this.moreEvent}>
								<h2>...</h2>
							</a>
						</li>
						<li className="divider"></li>
		    			<ULComponent.LiListEvent groupInfo = {this.state.events} filtre = {this.props.filtre}>
		    				<StateParkEvaluationInfoComponent refId={this.props.refId} href = {'javascript:'} iconClassName = {'icon entypo user'}>						
							</StateParkEvaluationInfoComponent>
		    			</ULComponent.LiListEvent>
		    		</ul>
		    	</div>
		    </div>
		);
	}	
});

module.exports = TabsInformationEvaluationListComponent;