var React = require('react');
var ReactDOM = require('react-dom');
var ReactAddons = require('react-addons');
var _ = require('underscore');

var LIComponent = require('./LIComponent.react');
var AComponent = require('./AComponent.react');
var BackLIButtonComponent = require('./BackLIButtonComponent.react');

var LiListComponentEvent = React.createClass({
	displayName: 'LiListComponentEvent',

	/*
	Definir typeProp pour la fn 'filtre'
	*/

	componentWillUpdate: function(){
		//console.log('componentWillUpdate');
	},

	componentDidUpdate: function(){
		//console.log('componentDidUpdate')
	},

	getDefaultProps: function (){
		return {
			PropClassName: '',
			groupInfo: [],
			filtre: function(evnt){return true;}
		}
	},

	renderChildren: function(){
		var children = [];
		var groupInfo = this.props.groupInfo;
		var groupInfoKeysLen = groupInfo.length;

		for (var i = 0; i < groupInfoKeysLen; i++) {
			if (this.props.filtre(groupInfo[i]))
				React.Children.forEach(this.props.children, function(child){
					children.push(React.cloneElement(child, {
						keyName: 'alert' + groupInfo[i].actionID, /*Pour le faire general 'alert' devienne le type d'event*/
						key: 'alert' + groupInfo[i].actionID,
						info: groupInfo[i]
					}));
				});
		}
		
		return children;
	},

	render: function(){
		//console.log('render');
		/*Changer le li.back a premier position de l'ul*/
		return (
			<div id="id-LiListComponentEvent">
				<div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					{this.renderChildren()}
				</div>	
			</div>
		);
	}
});

module.exports = LiListComponentEvent;