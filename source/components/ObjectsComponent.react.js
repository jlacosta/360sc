var React = require('react');
var ReactDOM = require('react-dom');

var ULComponent = require('./ULComponent.react');

var ObjectsComponent = React.createClass({
	displayName: 'ObjectsComponent', 

	getDefaultProps: function (){
		return {
			autorisation: [],
			keyAutorisation : ['addObject', 'editObject', 'type'],
			buttons: {
				'Créer un objet': { posKey : 0, value : 1, url: '/object', icon:'glyphicon glyphicon-scissors' },
				'Modifier un objet': { posKey : 1, value : 1, url: '/objects', icon:'glyphicon glyphicon-edit' },
				"Ajouter un type d'objet": { posKey : 2, value : 1, url: '/type', icon:'glyphicon glyphicon-plus' },
				'Liste des objets': { posKey : -1, value : -1, url: '/objects', icon:'glyphicon glyphicon-list' }
			}
		}
	},

	componentWillReceiveProps: function(nextProps){
		
	},

	renderChildren: function(){
		var children = [], 
			child = null; 
		var buttons = this.props.buttons, 
			keyButtons = Object.keys(buttons);
			lenKeyButtons = keyButtons.length;
		var keyAutorisation = this.props.keyAutorisation,
			dataAutorisation = this.props.autorisation;
		var item = null;

		if (dataAutorisation.length == 0) return children;

		for (var i = 0; i < lenKeyButtons; i++) {
			item = buttons[ keyButtons[i] ];
			if (item == null) continue;
			child = null;
			if (item.value == -1)
				child = (
					<li className = {'objects-' + keyButtons[i]} key={keyButtons[i]}>
						{/*<a href = {window.serverRoot + item.url} target="_blank"> 
													<span className={item.icon}></span>
													<span className="text"> {keyButtons[i]} </span>
												</a>*/}
						<a href="#" onClick={this.props._link.bind(this, window.serverRoot + item.url)}>
							<span className={item.icon}></span>
							<span className="text"> {keyButtons[i]} </span>
						</a>
					</li>
				);
			else if (dataAutorisation[ keyAutorisation[ item.posKey ] ] == item.value){
				child = (
					<li className = {'objects-' + keyButtons[i]} key={keyButtons[i]}>
						{/*<a href = {window.serverRoot + item.url} target="_blank">
													<span className={item.icon}></span>
													<span className="text"> {keyButtons[i]} </span>
												</a>*/}
						<a href="#" onClick={this.props._link.bind(this, window.serverRoot + item.url)}>
							<span className={item.icon}></span>
							<span className="text"> {keyButtons[i]} </span>
						</a>
					</li>
				);
			}
			if (child === null) continue;
			children.push(child);
		};

		return children;
	},

	render: function(){
		return (
			<ul className="simple">
				{this.renderChildren()}
			</ul>
		);
	}
});

module.exports = ObjectsComponent;