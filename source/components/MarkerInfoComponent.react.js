var React = require('react');
var ReactDOM = require('react-dom');
var ReactDOMServer = require('react-dom/server');

var TabsInformationComponent = require('./TabsInformationComponent.react');
var MarkerInfoWindow = require('./MarkerInfoWindow.react');

var GxMenu = require('../utils/GxMenu');
var MapUtils = require('../utils/MapUtils');

var MarkerInfoComponent = React.createClass({
	displayName: 'MarkerInfoComponent',

	getInitialState: function(){
		return  {
			show: false
		}
	},

	componentDidMount: function(){
		if (this.props.element === 0) {
			GxMenu.renderGxMenuRight();
			GxMenu.show('#gx-sidemenu-right');
		}

		$(ReactDOM.findDOMNode(this)).click(this.setShowState.bind(this, true));
	},

	componentDidUpdate: function(){
		var elemUl = ReactDOM.findDOMNode(this.refs.MarkerInfoComponentUlsimple);
		$(elemUl).find('.nameRefMarker').appendTo($(elemUl));
		$(elemUl).find('.TabsInformationComponent').appendTo($(elemUl));
		$(elemUl).find('.back:first span:nth-child(2)').text('Liste d\'objects');
		$(window).trigger('resize');
	},

	setShowState: function(value){
		this.setState({ show: value });
	},

	handleCenterMarker: function(objetID, infoObject, source){
		console.log(objetID);
		var objetID = objetID;
		var infoObject = infoObject;
		var source = source;
		MapUtils.openInfoWindow(MarkerInfoWindow, objetID, infoObject);
	},

	render: function(){
		var infoObject = this.props.info;
		var objetID = infoObject.objetID;
		if (this.state.show == true)
			return (
				<li className="MarkerInfoComponent" id = {'MarkerInfoComponent' + this.props.info.objetID}>
					<a href="#" onClick = {this.setShowState.bind(this, true)} ref={'MarkerInfoComponent' + this.props.info.objetID}>
						<span className="icon entypo attach parentAttach">
							<span className="icon entypo tag childAttach"></span>
						</span>
						<span className="text">{this.props.info.name}</span>
					</a>
					<ul ref="MarkerInfoComponentUlsimple" className='simple'>
						<div className="nameRefMarker">
							<span className="text">{this.props.info.name}</span>
							<a href="#" onClick = {this.handleCenterMarker.bind(this, objetID, infoObject, 'marker')}>
								<span className="icon entypo location" />
							</a>
						</div>

						<TabsInformationComponent info = {this.props.info} />
					</ul>
				</li>
			);
		else
			return (
				<li className="MarkerInfoComponent" id = {'MarkerInfoComponent' + this.props.info.objetID}>
					<a href="javascript:" onClick = {this.setShowState.bind(this, true)}>
						<span className="icon entypo attach parentAttach">
							<span className="icon entypo tag childAttach"></span>
						</span>
						<span className="text">{this.props.info.name}</span>
					</a>
					<ul ref="MarkerInfoComponentUlsimple" className='simple'>
					</ul>
				</li>
			);
	}
});

module.exports = MarkerInfoComponent;