var React = require('react');
var ReactDOM = require('react-dom');

var ULComponent = require('./ULComponent.react');
var StateParkComponent = require('./StateParkComponent.react');
var MarkersComponent = require('./MarkersComponent.react');
var MaintenanceComponent = require('./MaintenanceComponent.react');
var GxMenu = require('../utils/GxMenu');

var divStyle = {
	'zIndex': 9999
}

var OffCanvasRight = React.createClass({
	displayName: 'OffCanvasRight',

	propTypes:  {
		id: React.PropTypes.string.isRequired
	},

	componentDidMount: function(){
		/*GxMenu.renderGxMenuRight();
		GxMenu.show(this.props.id);*/
		GxMenu.hide('#'+this.props.id);
	/*	console.log('componentDidMount - OffCanvasRight');*/
	},

	componentDidUpdate: function(){
		/*Remove all listenner*/
	},

	shouldComponentUpdate: function(nextProps, nextState){
		return true;
	},

	render: function(){
		return (
			<div id={this.props.id} style= {divStyle}>
				<div className="gx-sidemenu-inner" id="gx-sidemenu-inner-2">
					<div className="scroll">
						<ULComponent.simple PropClassName={'gx-menu'}>
							<li className="back">
								<a href="javascript:" className="gx-trigger-right">
									<span className="icon entypo chevron-left"></span>
									<span className="text">Précedent</span>
								</a>
							</li>
							<li className="divider" />
							<StateParkComponent />
							<MarkersComponent  />
							<MaintenanceComponent />
						</ULComponent.simple>
					</div>
				</div>
				<div id="gx-sidemenu-login">
					
				</div>
			</div>	
		);
	}
});

module.exports = OffCanvasRight;