var React = require('react');
var ReactDOM = require('react-dom');

var ObjectStore = require('../stores/ObjectStore');
var WebAPIUtils = require('../utils/WebAPIUtils');

var TabsInformationAlertListComponent = require('./TabsInformationAlertListComponent.react');
var TabsInformationMessageListComponent = require('./TabsInformationMessageListComponent.react');
var TabsInformationHistoryListComponent = require('./TabsInformationHistoryListComponent.react');
var TabsInformationEvaluationListComponent = require('./TabsInformationEvaluationListComponent.react');

var style = {
	img: {
		'height': '250px',
		'maxHeight': '300px',
		'paddingTop': "2px"
	},
	separationTop: {
		height: '55px !important'
	}
}

TabsInformationComponent = React.createClass({
	displayName: 'TabsInformationComponent',

	getInitialState: function(){
		return {
			loadedInformation: false
		}
	},

	componentWillMount: function(){
	},

	componentDidMount: function(){
		/*console.log('componentDidMount - TabsInformationComponent');*/
		if (!this.props.info.hasOwnProperty('societeName')){
			WebAPIUtils.searchInfoObjectToTab(this.props.info.objetID, this.props.info.idParent);
			ObjectStore.addListenner('newPropertyObject-' + this.props.info.objetID, this.onNewProperty);
		}

		var elem = ReactDOM.findDOMNode(this.refs['TabsInformationComponent-' + this.props.info.objetID]);
		var parent = $(elem).parent();
		var a = $(parent).find('a.back');

		var fn = function(){
			console.log(this.props.info.objetID);
			/*var node = ReactDOM.findDOMNode(this);
			React.unmountComponentAtNode(node);
			$(node).remove();*/
		}

		$(a).click(fn.bind(this));
	},

	componentWillUnmount: function(){
		
	},

	onNewProperty: function(){
		console.log('onNewProperty - TabsInformationComponent');
		ObjectStore.deleteListener('newPropertyObject-' + this.props.info.objetID, this.onNewProperty);
		this.props.info = ObjectStore.getObject( this.props.info.objetID );
		this.setState({loadedInformation: true});
	},

	defaultValue: function(value){
		if (typeof value !== typeof undefined &&
			typeof value !== typeof null)
			return value;
		return 0;
	},

	triggerResizeWindow: function(){
		$(window).trigger('resize');
	},

	openInfo: function(){
		
	},

	render: function(){

		return (
			<div ref = {'TabsInformationComponent-' + this.props.info.objetID} className="TabsInformationComponent">
				<div>
				  <div className="clearfix" />
				  <ul className="nav nav-tabs nav-justified optionsFilters" role="tablist">
				    <li role="presentation" className="active"><a onClick={this.triggerResizeWindow} href={"#generalInformation" + this.props.info.objetID} aria-controls={"generalInformation" + this.props.info.objetID} role="tab" data-toggle="tab">Info</a></li>
				    <li role="presentation"><a onClick={this.triggerResizeWindow} href={"#alerts" + this.props.info.objetID} aria-controls={"alerts" + this.props.info.objetID} role="tab" data-toggle="tab">Alertes</a></li>
				    <li role="presentation"><a onClick={this.triggerResizeWindow} href={"#messages" + this.props.info.objetID} aria-controls={"messages" + this.props.info.objetID} role="tab" data-toggle="tab">Messages</a></li>
				    <li role="presentation"><a onClick={this.triggerResizeWindow} href={"#evaluations" + this.props.info.objetID} aria-controls={"evaluations" + this.props.info.objetID} role="tab" data-toggle="tab">Evaluations</a></li>
				    <li role="presentation"><a onClick={this.triggerResizeWindow} href={"#history" + this.props.info.objetID} aria-controls={"history" + this.props.info.objetID} role="tab" data-toggle="tab">Historique</a></li>
				    {/*<li role="presentation"><a onClick={this.triggerResizeWindow} href={"#maintenance" + this.props.info.objetID} aria-controls={"maintenance" + this.props.info.objetID} role="tab" data-toggle="tab">Maintenance</a></li>*/}
				  </ul>
				  <div className="tab-content">
				    <div role="tabpanel" className="tab-pane active generalInformation" id={"generalInformation" + this.props.info.objetID}>
				    	<div className="gi-span-image">
				    		<img className="img-rounded center-block" src={'./' + this.props.info.image} alt="L'image d'objet"
				    		style = {style.img} />
				    	</div>
				    	<div className="gi-span-info">
				    		<div className="title"> Propriétaire </div>
				    		<div className="sub-title"> {this.props.info.societeName} </div>
				    		<div className="title"> Type </div>
				    		<div className="sub-title"> {this.props.info.nameType} </div>
				    		<div className="title"> Référence </div>
				    		<div className="sub-title"> {this.props.info.alphaID} </div>
				    		<div className="title"> Description </div>
				    		<div className="sub-title"> {this.props.info.description} </div>
				    		<div className="title"> Date de Mise en Service </div>
				    		<div className="sub-title"> {this.props.info.dateActive} </div>
				    		<div className="title"> Localisation </div>
				    		<div className="sub-title Localisation"> {this.props.info.adresse} </div>
				    		<div className="title"> Latitude </div>
				    		<div className="sub-title"> {this.props.info.objLat} </div>
				    		<div className="title"> Longitude </div>
				    		<div className="sub-title"> {this.props.info.objLong} </div>
				    	</div>
				    	<div className="pdfInfo">
				    		<a href={window.serverRoot + '/' + this.props.info.ficheProduit} target='_blank'>
				    			Fiche Produit
				    		</a>
				    	</div>
				    </div>

			    	<TabsInformationAlertListComponent refId='tic' objectId={this.props.info.objetID}/>

				    <TabsInformationMessageListComponent refId='tic' objectId={this.props.info.objetID}/>

					<TabsInformationEvaluationListComponent refId='tic' objectId={this.props.info.objetID}/>				    

				    <TabsInformationHistoryListComponent refId='tic' objectId={this.props.info.objetID}/>
				   
				  </div>
				</div>
			</div>	
		);
	}
});

module.exports = TabsInformationComponent;