var React = require('react');
var ReactDOM = require('react-dom');
var ReactAddons = require('react-addons');
var _ = require('underscore');

var LIComponent = require('./LIComponent.react');
var AComponent = require('./AComponent.react');
var BackLIButtonComponent = require('./BackLIButtonComponent.react');

var LiListObjectComponent = React.createClass({
	displayName: 'LiListObjectComponent',

	componentWillUpdate: function(){
		//console.log('componentWillUpdate');
	},

	componentDidUpdate: function(){
		//console.log('componentDidUpdate')
	},

	getDefaultProps: function (){
		return {
			groupInfo: []
		}
	},

	renderChildren: function(){
		var children = [];
		var groupInfo = this.props.groupInfo;
		var keyGroupInfo = Object.keys(groupInfo);
		var groupInfoKeysLen = keyGroupInfo.length;

		for (var i = groupInfoKeysLen - 1; i >= 0; i--) {
			React.Children.forEach(this.props.children, function(child){
				children.push(React.cloneElement(child, {
					keyName: 'object' + groupInfo[ keyGroupInfo[i] ].objetID, /*Pour le faire general 'alert' devienne le type d'event*/
					key: 'object' + groupInfo[ keyGroupInfo[i] ].objetID,
					info: groupInfo[ keyGroupInfo[i] ]
				}));
			});
		}
		
		return children;
	},

	render: function(){
		return (
			<ul className='simple'>
				{this.renderChildren()}
			</ul>			
		);
	}
});

module.exports = LiListObjectComponent;