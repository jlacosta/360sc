var React = require('react');
var ReactDOM = require('react-dom');
var classNames = require('classnames');

var ReactScrollableList = require('../dist/react-scrollable-list');

var PopoverComponent = React.createClass({
	displayName: 'PopoverComponent',

	componentDidMount: function(){
	},

	render: function(){

		var listItems = [];
		for (var i = 0; i < 10000; i++) {
			listItems.push({ id: i, content: i});
		}

		return (
			<div className="notification-component-rendered">
			    <ReactScrollableList listItems = {listItems} 
			    	heightOfItem = {30} maxItemsToRender = {20} />
			</div>
		);
	}
});

module.exports = PopoverComponent;