var React = require('react');
var ReactDOM = require('react-dom');
var NavHeaderAdministration = require('./NavHeaderAdministration.react');
var ScrollerAdministration = require('./ScrollerAdministration.react');

var Administration = React.createClass({

    render: function () {
        return (
        	<span id='mainReact'> 
	            <NavHeaderAdministration />
	            <ScrollerAdministration />
	        </span>    
        );
    }
});

module.exports = Administration;