var React = require('react');
var ReactDOM = require('react-dom');

var ContentInfoWindowMapComponent = React.createClass({
	displayName: 'ContentInfoWindowMapComponent',

	render: function(){
		if (this.props.source == 'event')
			return(
				<div className="ContentInfoWindowMapComponent">
					<div> {'ObjectID:' + this.props.info.objetID} </div>
					<div> {'ActionID:' + this.props.info.actionID} </div>
					<div> {'Type Event:' + this.props.typ} </div>
				</div>	
			);
		else if (this.props.source == 'marker')
			return(
				<div className="ContentInfoWindowMapComponent">
					<div> {'ObjectID:' + this.props.info.objetID} </div>
					<div> {'Name:' + this.props.info.name} </div>
				</div>
			);
	}
});
module.exports = ContentInfoWindowMapComponent;