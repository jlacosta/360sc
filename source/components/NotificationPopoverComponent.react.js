var React = require('react');
var ReactDOM = require('react-dom');
var classNames = require('classnames');

var ObjectStore = require('../stores/ObjectStore');

var PopoverComponent = require('./PopoverComponent.react');

var MapUtils = require('../utils/MapUtils');

var NotificationPopoverComponent = React.createClass({
	displayName: 'NotificationPopoverComponent',

	componentDidMount: function(){
		ObjectStore.addChangeObjectListenner(this.onChangeEvent);
		ObjectStore.addInitObjectListenner(this.onInitEvent);
		$(ReactDOM.findDOMNode(this)).webuiPopover({
			url: "#notification-popover-content",
			placement: 'bottom',
			trigger: 'hover',
			width:'254px',
			height: 'auto',
			padding: false,
			cache: true,
			onShow: function(){
				if (!$('.notification-component-rendered').length)
					ReactDOM.render(<PopoverComponent />, 
						$('.notification-react-content')[0]);
			},
			onHide: function(){

			}
		});
	},

	onChangeEvent: function(){
		this.setState({
			notSeen: ObjectStore.getNiveauUrgence()
		});
	},

	onInitEvent: function(){
		this.setState({
			notSeen: ObjectStore.getNiveauUrgence()
		});
	},

	getInitialState: function(){
		return {
			notSeen: 0
		}
	},

	onChangeStatistics: function(){
		this.setState({
			notSeen: 0
		});
	},

	shouldComponentUpdate: function(nextProps, nextState){
		return (nextProps.notSeen != this.state.notSeen);
	},

	openStatePark: function(){
		$.when( MapUtils.closeGxMenu() ).then(function(v){
			if (!$('#gx-sidemenu-right').hasClass('opened')){
				$('.gx-menu.gx-trigger-right.entypo.list').trigger('click');
				$('.StateParkComponent a:first').trigger('click');
			}
		}.bind(this));
	},

	render: function(){
		var bellStyle = classNames({
			'glyphicon glyphicon-bell bell': true,
			'n-icon-bell': true
		});

		return (
			<a className='notificationComponent' href="#" onClick={this.openStatePark}>
				<span>
					<span aria-hidden='true' className={bellStyle}></span>
					<span className='text-notificationComponent'>{ this.state.notSeen }</span>
				</span>
				<div id="notification-popover-content" className="notification-react-content" />
			</a>
			
		);
	}
});

module.exports = NotificationPopoverComponent;