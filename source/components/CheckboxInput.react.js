var React = require('react');
var ReactDOM = require('react-dom');

var CheckboxInput = React.createClass({
  getInitialState: function () {
    return {
        checked: this.props.checked || true
     };
  },

  render: function () {
    return (
        <label>
            <input type="checkbox"
              name={this.props.name}
              defaultChecked={this.state.checked}
              onClick={this.props.handleClick}
              value={this.props.value} />
              {this.props.label}
      </label>
    );
  },

  handleClick: function(e) {
    this.setState({checked: e.target.checked});
    this.props.parentHandle();
  }
});

module.exports = CheckboxInput;