var React = require('react');
var ReactDOM = require('react-dom');

var NotificationComponent = require('./NotificationComponent.react');

var style = {
	img: {
		'height': '5em !important',
	}
}

var NavHeaderAdministration = React.createClass({
	displayName: 'NavHeaderAdministration',

	render: function(){
		return (
			<nav id="360-navbar" className="navbar navbar-default navbar-fixed-top NavHeader">
				<a href="javascript:" className="gx-menu gx-trigger-left entypo list"></a>
				<div className="container">
					<h2>
						<img id="logo" src={window.serverRoot + "/build/img/head-logo.svg"} style={style.img} />
					</h2>
				</div>
				<div className="clearfix"></div>
			</nav>
		);
	}
});

module.exports = NavHeaderAdministration;