var React = require('react');
var ReactDOM = require('react-dom');

var StateParkAlertInfoComponent = require('./StateParkAlertInfoComponent.react');
var StateParkEvaluationInfoComponent = require('./StateParkEvaluationInfoComponent.react');
var AMessageComponent = require('./AMessageComponent.react');

var EventActionCreator = require('../actions/EventActionCreator');
var HistoryEventStore = require('../stores/HistoryEventStore');

var WebAPIUtils = require('../utils/WebAPIUtils');

var TabsInformationHistoryListComponent = React.createClass({
	displayName: 'TabsInformationHistoryListComponent',

	getInitialState: function(){
		return{
			events: [],
			lastEvent: null
		}
	},

	componentDidMount: function(){
		HistoryEventStore.addNewEventListenner(this.onInitEvents);
		WebAPIUtils.getHistory(EventActionCreator.receiveEventsFromHistory, {objectId: this.props.objectId, end: 0});
	},

	componentDidUpdate: function(){
		$(window).trigger('resize');
	},

	renderChildren: function(){
		var events = this.state.events;
		var lenEvents = events.length;
		var children = [];
		var componentToRender = null;
		for (var i = lenEvents - 1; i >= 0; i--) {
			if (events[i]['type'] == 'alert') componentToRender = StateParkAlertInfoComponent;
			else if (events[i]['type'] == 'evalmsg') componentToRender = AMessageComponent;
			else if (events[i]['type'] == 'eval') componentToRender = StateParkEvaluationInfoComponent;
			child = React.createElement(componentToRender, {
				refId: 'tihlc',
				key: 'tihlc' + events[i].objetID + '-' + events[i]['type'] + '-' + events[i].actionID + '-' + i,
				info: events[i]
			});
			children.push(child);
		};
		return children;
	},

	onInitEvents: function(){
		var events = HistoryEventStore.getEventsByObjectId(this.props.objectId);
		if (events == null) return;
		var len = events.length;
		var lastEvent = events[len - 1];
		this.setState({
			events: events,
			lastEvent: lastEvent
		});
	},

	moreEvent: function(events){
		var date = 0;
		if (this.state.lastEvent != null){
			var date = this.state.lastEvent.createdTime;
		}
		WebAPIUtils.getHistory(EventActionCreator.receiveEventsFromHistory, {objectId: this.props.objectId, end: date});
	},

	showFiltre_0: function(evnt){
		if (evnt.prisEnCompte == 0 || evnt.prisEnCompte == false) return true;
		return false;
	},


	render: function(){
		return (
			<div role="tabpanel" className="tab-pane history" id={"history" + this.props.objectId}>
				<div className="a-div-body">
					<ul className="event-message StateParkMessageListComponent TabsInformationHistoryListComponent" id="StateParkMessageList">
						{this.renderChildren()}
						<li className="btnMoreEvent">
							<a href="#" onClick={this.moreEvent}>
								<h2>...</h2>
							</a>
						</li>
					</ul>
				</div>
			</div>
		);
	}
});

module.exports = TabsInformationHistoryListComponent;