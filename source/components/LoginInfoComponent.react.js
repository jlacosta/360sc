var React = require('react');
var ReactDOM = require('react-dom');

var WebAPIUtils = require('../utils/WebAPIUtils');

var LoginInfoComponent = React.createClass({
	displayName: 'LoginInfoComponent',

	getInitialState: function(){
		return {
			client_info: null
		}
	},

	componentDidMount: function(){
		WebAPIUtils.getInfoClient(this.onReceiveInfoClient);
	},

	onReceiveInfoClient: function(data){
		/*console.log(data);*/
		this.setState({
			client_info: data
		});
	},

	render: function(){
		if (this.state.client_info == null)
			return (
				<div id="gx-sidemenu-login">
				</div>
			);
		else
			return (
				<div id="gx-sidemenu-login">
					<div className="row">
						<div className="col-sm-12 col-md-12 col-lg-12">
							<div> {this.state.client_info.client_name} </div>
						</div>
						<div className="col-sm-12 col-md-12 col-lg-12">
							<img id="client_image" src = {window.serverRoot + '/' + this.state.client_info.client_image} />
						</div>
					</div>
				</div>
			);
	}
});

module.exports = LoginInfoComponent;