var React = require('react');
var ReactDOM = require('react-dom');
var NavHeader = require('./NavHeader.react');
var Map = require('./Map.react');
var Scroller = require('./Scroller.react');

var Application = React.createClass({

    render: function () {
        return (
        	<span id='mainReact'> 
	            <NavHeader />
	            <Map />
	            <Scroller />
	        </span>    
        );
    }
});

module.exports = Application;