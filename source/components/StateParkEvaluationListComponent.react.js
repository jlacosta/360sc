var React = require('react');
var ReactDOM = require('react-dom');

var ULComponent = require('./ULComponent.react');
var StateParkEvaluationInfoComponent = require('./StateParkEvaluationInfoComponent.react');

var EventStore = require('../stores/EventStore');
var EventActionCreator = require('../actions/EventActionCreator');
var WebApiUtils = require('../utils/WebApiUtils');

var StateParkEvaluationListComponent = React.createClass({
	displayName: 'StateParkEvaluationListComponent',

	getInitialState: function(){
		var events = EventStore.getEventsType('eval');
		if (!Array.isArray(events)) events = [];
		return {
			events: events,
			lenEvents: events.length,
			lastEvent: null
		}
	},

	componentDidMount: function(){
		EventStore.addNewEventListenner(this.onChangeEventCollection);
		EventStore.addEventListenner('changed_eval', this.onChangeEventCollection);
		$(ReactDOM.findDOMNode(this.refs['linkStateParkEvaluationList'])).click(function(){
			this.setNameBackBtn();
		}.bind(this));
	},

	componentDidUpdate: function(){
		this.triggerResizeWindow();
	},

	shouldComponentUpdate: function(nextProps, nextState){
		if (this.props.active == false && nextProps.active == true) return true;
		if (nextState.events.length != this.state.lenEvents)
			return true;
		return false;
	},

	onChangeEventCollection: function(){
		var events = EventStore.getEventsType('eval');
		this.setState({
			events: events,
			lenEvents: events.length,
			lastEvent: events[ events.length - 1 ] 
		});
	},

	setNameBackBtn: function(){
		$('.StateParkMessageListComponentUL').find('.back:first span:nth-child(2)').text('evaluation');
	},

	moreEvent: function(){
		if (this.state.lastEvent == null) return;
		var option = {
			fn: EventActionCreator.receiveEventsHistory,
			typ: 'eval',
			date: this.state.lastEvent.createdTime
		}
		WebApiUtils.getMoreEvents(option);
	},

	triggerResizeWindow: function(){
		$(window).trigger('resize');
	},

	render: function(){
		if (this.props.active)
			return (
				<li className="event-message StateParkMessageListComponent" id="StateParkEvaluationList">
					<a href="javascript:" ref="linkStateParkEvaluationList">
						<span className="glyph-icon flaticon-smiley"></span>
						<span className="text">Evaluation
							<span className="badge" style = {this.props.pstyle.badge}>{ this.state.events.length } </span>
						</span>
					</a>
					<ul className="simple StateParkMessageListComponentUL">
						<div className = "separationTop" />
						<li className="btnMoreEvent">
							<a href="#" onClick={this.moreEvent}>
								<h2>...</h2>
							</a>
						</li>
						<li className="divider"></li>
						<ULComponent.LiListEvent groupInfo = {this.state.events} filtre = {this.props.filtre}>
							<StateParkEvaluationInfoComponent refId='spelc' href = {'javascript:'} iconClassName = {'icon entypo user'}>						
							</StateParkEvaluationInfoComponent>
						</ULComponent.LiListEvent>
					</ul>
				</li>
			);
		else
			return (
				<li className="event-message StateParkMessageListComponent" id="StateParkEvaluationList">
					<a href="javascript:" ref="linkStateParkEvaluationList">
						<span className="icon entypo users"></span>
						<span className="text">{ this.state.events.length } Messages</span>
					</a>
					<ul className="simple StateParkMessageListComponentUL">
					</ul>
				</li>
			);
	}
});

module.exports = StateParkEvaluationListComponent;