var React = require('react');
var ReactDOM = require('react-dom');

var NotificationComponent = require('./NotificationComponent.react');
var NotificationPopoverComponent = require('./NotificationPopoverComponent.react');

var style = {
	img: {
		'height': '2.8125rem',
		'margin-right': '5px'
	}
}

var NavHeader = React.createClass({
	displayName: 'NavHeader',

	componentDidMount: function(){
		
	},

	render: function(){
		return (
			<nav id="360-navbar" className="navbar navbar-default navbar-fixed-top NavHeader">
				<a href="javascript:" className="gx-menu gx-trigger-left entypo list"></a>
				<a href="javascript:" className="gx-menu gx-trigger-right entypo list"></a>
				<div className="container">
					<h2>
						<img id="logo" src="./build/img/head-logo.svg" />
						<NotificationComponent />
					</h2>
				</div>

				<div className="clearfix"></div>
			</nav>
		);
	}
});

module.exports = NavHeader;