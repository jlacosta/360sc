var React = require('react');
var ReactDOM = require('react-dom');

var ULComponent = require('./ULComponent.react');

var MCsComponent = React.createClass({
	displayName: 'MCsComponent', 

	getDefaultProps: function (){
		return {
			autorisation: [],
			keyAutorisation : ['addTag', 'searchTags', 'export', 'editTags', 'level', 'SURLE'],
			buttons: {
				'Ajouter des MCs': { posKey : 0, value : 1, url: '/addMCs', icon:'icon entypo plus' },
				'Exporter les MCs': { posKey : 2, value : 1, url: '/export-MCs', icon:'glyphicon glyphicon-export' },
				'Liste des MCs': { posKey : -1, value : -1, url: '/MCs', icon:'glyphicon glyphicon-tags' },
				'Modifier un MC': { posKey : 3, value : 1, url: '/MCs', icon:'glyphicon glyphicon-pencil' },
				'Donner des MCs': { posKey : 4, value : 4, url: '/redistribute', icon:'glyphicon glyphicon-send' },
				'Supprimer un MC': { posKey: 4, value: 4, url: '/delete-MC', icon:'glyphicon glyphicon-remove' },
				'Liste des URLes': { posKey: 5, value: 1, url: '/urles', icon:'glyphicon glyphicon-link' }
			}
		}
	},

	componentWillReceiveProps: function(nextProps){

	},

	renderChildren: function(self){
		var children = [], 
			child = null; 
		var buttons = this.props.buttons, 
			keyButtons = Object.keys(buttons);
			lenKeyButtons = keyButtons.length;
		var keyAutorisation = this.props.keyAutorisation,
			dataAutorisation = this.props.autorisation;
		var item = null;

		if (dataAutorisation.length == 0) return children;

		for (var i = 0; i < lenKeyButtons; i++) {
			item = buttons[ keyButtons[i] ];
			if (item == null) continue;
			child = null;
			if (item.value == -1){
				child = (
					<li className = {'mcs-' + keyButtons[i]} key={keyButtons[i]}>
						{/*<a href = {window.serverRoot + item.url} target="_blank">
													<span className={item.icon}></span>
													<span className="text"> {keyButtons[i]} </span>
												</a>*/}
						<a href="#" onClick={this.props._link.bind(self, window.serverRoot + item.url)}>
							<span className={item.icon}></span>
							<span className="text"> {keyButtons[i]} </span>
						</a>
					</li>
				);
			}
			else if (dataAutorisation[ keyAutorisation[ item.posKey ] ] == item.value){
				child = (
					<li className = {'mcs-' + keyButtons[i]} key={keyButtons[i]}>
						{/*<a href = {window.serverRoot + item.url} target="_blank">
													<span className={item.icon}></span>
													<span className="text"> {keyButtons[i]} </span>
												</a>*/}
						<a href="#" onClick={this.props._link.bind(self, window.serverRoot + item.url)}>
							<span className={item.icon}></span>
							<span className="text"> {keyButtons[i]} </span>
						</a>
					</li>
				);
			}
			if (child === null) continue;
			children.push(child);
		};

		return children;
	},

	render: function(){
		return (
			<ul className="simple">
				{this.renderChildren(this)}
			</ul>
		);
	}
});

module.exports = MCsComponent;