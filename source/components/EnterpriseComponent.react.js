var React = require('react');
var ReactDOM = require('react-dom');

var EnterpriseComponent = React.createClass({
	displayName: 'EnterpriseComponent', 

	getDefaultProps: function (){
		return {
			autorisation: [],
			keyAutorisation : ['editObject'],
			buttons: {
				'Créer une entreprise': { posKey : 0, value : 1, url: '/society', icon:'icon entypo plus' }
			}
		}
	},

	componentWillReceiveProps: function(nextProps){
		
	},

	renderChildren: function(){
		var children = [], 
			child = null; 
		var buttons = this.props.buttons, 
			keyButtons = Object.keys(buttons);
			lenKeyButtons = keyButtons.length;
		var keyAutorisation = this.props.keyAutorisation,
			dataAutorisation = this.props.autorisation;
		var item = null;

		if (dataAutorisation.length == 0) return children;

		for (var i = 0; i < lenKeyButtons; i++) {
			item = buttons[ keyButtons[i] ];
			if (item == null) continue;
			child = null;
			if (item.value == -1)
				child = (
					<li className = {'compte-' + keyButtons[i]} key={keyButtons[i]}>
						{/*<a href = {window.serverRoot + item.url} target="_blank">
													<span className={item.icon}></span>
													<span className="text"> {keyButtons[i]} </span>
												</a>*/}
						<a href="#" onClick={this.props._link.bind(this, window.serverRoot + item.url)}>
							<span className={item.icon}></span>
							<span className="text"> {keyButtons[i]} </span>
						</a>
					</li>
				);
			else if (dataAutorisation[ keyAutorisation[ item.posKey ] ] == item.value){
				child = (
					<li className = {'compte-' + keyButtons[i]} key={keyButtons[i]}>
						{/*<a href = {window.serverRoot + item.url} target="_blank">
													<span className={item.icon}></span>
													<span className="text"> {keyButtons[i]} </span>
												</a>*/}
						<a href="#" onClick={this.props._link.bind(this, window.serverRoot + item.url)}>
							<span className={item.icon}></span>
							<span className="text"> {keyButtons[i]} </span>
						</a>
					</li>
				);
			}
			if (child === null) continue;
			children.push(child);
		};

		return children;
	},

	render: function(){
		return (
			<ul className="simple">
				{this.renderChildren()}
			</ul>
		);
	}
});

module.exports = EnterpriseComponent;