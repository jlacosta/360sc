var React = require('react');
var ReactDOM = require('react-dom');
var ULComponent = require('./ULComponent.react.js');

var EventStore = require('../stores/EventStore');
var EventActionCreator = require('../actions/EventActionCreator');

var StateParkAlertInfoComponent = React.createClass({
	displayName: 'StateParkAlertInfoComponent',

	getDefaultProps: function (){
		return {
			info: null,
			aClassName: 'simple',
			iconClassName: 'icon entypo picture'
		}
	},

	componentDidMount: function(){
		/*console.log(this.props.info.actionID);*/

		var myIdComponent = 'alert-' + this.props.info.actionID;
		//console.log(myIdComponent);

		EventStore.addEventListenner(myIdComponent, this.onChangeProp);

		/*Initialize Bootstrap Switch.*/
		var selector = "[name="+ 'alert-' + this.props.info.actionID + "]";
		$(selector).bootstrapSwitch();

		/*Event switchChange*/
		selector = "#ialert-" + this.props.info.actionID;
		var switchChange = function(event, state) {
			this.handleClickCheckbox(event, state, this.props.info.actionID);
		}.bind(this);
		$(selector).on('switchChange.bootstrapSwitch', switchChange);
	}, 

	componentDidUpdate: function(){	
	},

	onChangeProp: function(){
		//console.log('onChangeProp');

		var myIdComponent = 'alert-' + this.props.info.actionID;
		/*var isChecked = EventStore.getEventTypeId('alert', this.props.info.actionID).prisEnCompte;
		if (isChecked) isChecked = true;
		else isChecked = false;*/
		var isChecked = true;

		var selector = "input[name="+ 'alert-' + this.props.info.actionID + "]";

		$(selector).bootstrapSwitch('state', isChecked);
	},

	handleClickCheckbox: function(event, state, actionID){
		/*console.log(actionID);	*/

		var r_evnt = EventStore.getEventTypeId('alert', actionID);
		var evnt = Object.assign({}, r_evnt);
		evnt.prisEnCompte = state;
		evnt.innerOpertation = true;
		//console.log(state);

		var data = {
			typ: 'alert',
			data: [evnt] 
		}
		//console.log('handleClickCheckbox: ' + evnt.actionID + ';; ' + r_evnt.prisEnCompte + '!= ' + evnt.prisEnCompte + ';' + state);
		EventActionCreator.receiveEventsStream(data);
	},

	render: function(){
		if (this.props.info === null)
			return (
				<a href={this.props.href} className={this.props.aClassName}>
					<span className={this.props.iconClassName}></span>
					<span className="text">{this.props.children}</span>
				</a>	
			);

		//console.log(this.props.info.actionID + ';;' + this.props.info.prisEnCompte);

		if (this.props.info.prisEnCompte == 1){
			//console.log('checked');
			return (
				<a href={this.props.href} className={this.props.aClassName}>
					<span className={this.props.iconClassName}></span>
					<span className="text">
						{this.props.info.objetName}
						<input type="checkbox" name={"alert-"+this.props.info.actionID} 
						id={"ialert-"+this.props.info.actionID} defaultChecked />
					</span>
				</a>
			);
		}
		else{
			var d = new Date(this.props.info.createdTime);
			return (
				<div className="StateParkAlertInfoComponent">
					<div className="alertInfo-title">
						<span className="alertInfo-objectName"> {this.props.info.objetName} </span>
						<span className="alertInfo-date"> {d.toLocaleString()} </span>
						<div className="clear"></div>
					</div>
					<div className="alertInfo-subTitle">
						<span className="icon entypo mail"></span>
						<span className="icon entypo switch"></span>
						<span className="icon entypo share"></span>
						<span className="icon entypo eye"></span>
						<div className="clear"></div>
					</div>
					<div className="divider"></div>
				</div>
			);
		}
	}
});

module.exports = StateParkAlertInfoComponent;