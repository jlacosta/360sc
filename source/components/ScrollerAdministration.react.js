var React = require('react');
var ReactDOM = require('react-dom');
var OffCanvas = require('./OffCanvas.react.js');


var ScrollerAdministration = React.createClass({

	render: function(){
		return (
			<div id="scroller">
				<div id="wrapper">
					<div>
						<nav className="sidebars">
							{/*side menu left*/}
							<OffCanvas id = {'gx-sidemenu-left'} />
						</nav>
					</div>
				</div>
			</div>	
		);
	}
});

module.exports = ScrollerAdministration;