var React = require('react');
var ReactDOM = require('react-dom');

var ULComponent = require('./ULComponent.react');
var StateParkAlertListComponent = require('./StateParkAlertListComponent.react');
var StateParkMessageListComponent = require('./StateParkMessageListComponent.react');
var StateParkEvaluationListComponent = require('./StateParkEvaluationListComponent.react'); 

var style = {
	badge: {
		'marginLeft': '7px' 
	}
}

var StateParkComponent = React.createClass({
	displayName: 'StateParkComponent',

	componentDidMount: function(){
		var self = this;
		$(ReactDOM.findDOMNode(this.refs['linkStatePark'])).click(function(){
			this.setShowState(true);
			this.setNameBackBtn();
		}.bind(this));
	},

	getInitialState: function(){
		return {
			show: false
		}
	},

	showFiltre_0: function(evnt){
		if (evnt.prisEnCompte == 0 || evnt.prisEnCompte == false) return true;
		return false;
	},

	setShowState: function(value){
		console.log('setShowState');
		this.setState({
			show: value
		});
	},

	setNameBackBtn: function(){
		$('.StateParkComponentUL').find('.back:first span:nth-child(2)').text('Notifications');
	},

	render: function(){
		return (
			<li className="StateParkComponent" id="StatePark">
				<a href="#" ref="linkStatePark">
					<span className="icon entypo bell"></span>
					<span className="text">Notifications</span>
				</a>
				<ul className="simple StateParkComponentUL">
					<StateParkAlertListComponent filtre = {this.showFiltre_0}  active = {this.state.show} 
					pstyle = {style} />
					<StateParkMessageListComponent filtre = {this.showFiltre_0} active = {this.state.show} 
					pstyle = {style} />
					<StateParkEvaluationListComponent filtre = {this.showFiltre_0} active = {this.state.show} 
					pstyle = {style} />
				</ul>
			</li>
		);
	}
});

module.exports = StateParkComponent;