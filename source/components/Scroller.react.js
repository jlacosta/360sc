var React = require('react');
var ReactDOM = require('react-dom');
var OffCanvas = require('./OffCanvas.react.js');


var Scroller = React.createClass({

	render: function(){
		return (
			<div id="scroller">
				<div id="wrapper">
					<div>
						<nav className="sidebars">
							{/*side menu right*/}
							<OffCanvas id = {'gx-sidemenu-right'} />
							{/*side menu left*/}
							<OffCanvas id = {'gx-sidemenu-left'} />
						</nav>
					</div>
				</div>
			</div>	
		);
	}
});

module.exports = Scroller;