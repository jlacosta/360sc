var React = require('react');
var ReactDOM = require('react-dom');

var GxMenu = require('../utils/GxMenu');

/*var ObjectComponent = require('./ObjectComponent.react');*/

var MCsComponent = require('./MCsComponent.react');
var ObjectsComponent = require('./ObjectsComponent.react');
var CompteUtilisateurComponent = require('./CompteUtilisateurComponent.react');
var EnterpriseComponent = require('./EnterpriseComponent.react');
var ULComponent = require('./ULComponent.react');
var LoginInfoComponent = require('./LoginInfoComponent.react');

var WebApiUtils = require('../utils/WebApiUtils');

var divStyle = {
	'zIndex': 9998
}

var OffCanvasLeft = React.createClass({
	displayName: 'OffCanvasLeft',

	getDefaultProps: function(){
		return {
			autorisation: []
		}
	},

	getInitialState: function(){
		return {
			autorisation: []
		}
	},

	componentDidMount: function(){
		GxMenu.show(this.props.id);
		GxMenu.renderGxMenuLeft();

		WebApiUtils.autorisation( this.onAutorisation );
	},

	componentDidUpdate: function(){
	},

	shouldComponentUpdate: function(nextProps, nextState){
		return true;
	},

	onAutorisation: function(data){
		this.setState({
			autorisation: data
		});
	},

	_link: function(){
		return function(url){
			$('.inner-page iframe').attr('src', url);

			$('#myModal').on('hidden.bs.modal', function (e) {
				if($('#myModal').hasClass('hideToOpen')){
					$('#myModal').modal('show');
					$('#myModal').removeClass('hideToOpen');
				}
				$('.inner-page iframe').attr('src', "");
			});

			if($('body').hasClass('modal-open')) {
				$('#myModal').modal('hide');
				$('#myModal').addClass('hideToOpen');
			}
			if (!$('#myModal').hasClass('hideToOpen'))
				$('#myModal').modal('show');
		}
	},

	render: function(){
		return (
			<div id={this.props.id} style= {divStyle}>
				<div className="gx-sidemenu-inner" id="gx-sidemenu-inner-1">
					<div className="scroll">
						<ULComponent.simple PropClassName={'gx-menu'}>
							<li className="back">
								<a href="javascript:" className="gx-trigger-left">
									<span className="icon entypo chevron-left"></span>
									<span className="text">Précedent</span>
								</a>
							</li>
							<li className="divider" />
							<li className="accueil">
								<a href={window.serverRoot + "/"}>
									<span className="icon entypo home"></span>
									<span className="text">Accueil</span>
								</a>
							</li>
							<li className="mcs">
								<a href="javascript:">
									<span className="icon entypo tag parentTag">
										<span className="icon entypo tag childTag"></span>
									</span>
									<span className="text">MCs</span>
								</a>
								<MCsComponent autorisation={this.state.autorisation}
								_link={this._link()}/>
							</li>
							<li className="objects">
								<a href="javascript:">
									<span className="icon entypo attach parentAttach">
										<span className="icon entypo tag childAttach"></span>
									</span>
									<span className="text">Objets</span>
								</a>
								<ObjectsComponent autorisation={this.state.autorisation}
								_link={this._link()}/>
							</li>
							<li className="compte">
								<a href="javascript:">
									<span className="icon entypo users"></span>
									<span className="text">Compte Utilisateur</span>
								</a>
								<CompteUtilisateurComponent autorisation={this.state.autorisation}
								_link={this._link()}/>
							</li>
							<li className="enterprise">
								<a href="javascript:">
									<span className="icon entypo briefcase"></span>
									<span className="text">Enterprise</span>
								</a>
								<EnterpriseComponent autorisation={this.state.autorisation}
								_link={this._link()}/>
							</li>
							{/*<li className="statistics">
															<a href={window.serverRoot + "/statistics"}>
																<span className="icon entypo pie-chart"></span>
																<span className="text">Statistics</span>
															</a>
														</li>*/}
							<li className="contact">
								{/*<a href={window.serverRoot + "/contact"} target="_blank">
																	<span className="icon entypo mail"></span>
																	<span className="text">Contact</span>
																</a>*/}
								<a href="#" onClick={this._link().bind(self, window.serverRoot + "/contact")}>
									<span className="icon entypo mail"></span>
									<span className="text">Contact</span>
								</a>
							</li>
							<li className="deconnexion">
								<a href={window.serverRoot + "/deconnexion"}>
									<span className="icon entypo logout"></span>
									<span className="text">Déconnexion</span>
								</a>
							</li>
						</ULComponent.simple>
					</div>
				</div>
				<LoginInfoComponent />
			</div>
		);
	}
});

module.exports = OffCanvasLeft;