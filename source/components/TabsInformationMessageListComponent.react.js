var React = require('react');
var ReactDOM = require('react-dom');

var ULComponent = require('./ULComponent.react');

var EventStore = require('../stores/EventStore');
var ObjectStore = require('../stores/ObjectStore');
var EventActionCreator = require('../actions/EventActionCreator');
var WebApiUtils = require('../utils/WebApiUtils');

var TabsInformationMessageListComponent = React.createClass({
	displayName: 'TabsInformationMessageListComponent',

	getInitialState: function(){
		var events = EventStore.getEventsObject('evalmsg', this.props.objectId);
		var object = ObjectStore.getObject(this.props.objectId);
		if (!Array.isArray(events)) events = [];
		return {
			events: events,
			lenEvents: events.length,
			lastEvent: null,
			info: object,
			updateCounter: false
		}
	},

	componentDidUpdate: function(){
		this.triggerResizeWindow();
	},

	componentDidMount: function(){
		EventStore.addNewEventListenner(this.onChangeEventCollection);
		EventStore.addEventListenner('changed_evalmsg', this.onChangeEventCollection);
		ObjectStore.addChangeObjectListenner(this.onChangeObjectCollection);
	},

	shouldComponentUpdate: function(nextProps, nextState){
		if (this.props.active == false && nextProps.active == true) return true;
		if (nextState.events.length != this.state.lenEvents)
			return true;
		if (nextState.updateCounter) {
			nextState.updateCounter = false;
			return true;
		}
		return false;
	},

	onChangeEventCollection: function(){
		var events = EventStore.getEventsObject('evalmsg', this.props.objectId);
		this.setState({
			events: events,
			lenEvents: events.length 
		});
	},

	onChangeObjectCollection: function(){
		/*console.log('onChangeObjectCollection - TabsInformationAlertListComponent');*/
		var object = ObjectStore.getObject(this.props.objectId);
		/*console.log(object);*/
		this.setState({
			info: object,
			updateCounter: true
		});
	},

	defaultValue: function(value){
		if (typeof value !== typeof undefined &&
			typeof value !== typeof null)
			return value;
		return 0;
	},

	moreEvent: function(){
		var date = 0;
		if (this.state.lastEvent != null){
			date = this.state.lastEvent.createdTime;
		}
		var option = {
			fn: EventActionCreator.receiveEventsHistory,
			typ: 'evalmsg',
			date: date,
			objectID: this.props.objectId
		}
		WebApiUtils.getMoreEventsByObject(option);
	},

	triggerResizeWindow: function(){
		$(window).trigger('resize');
	},

	render: function(){
		return (
			<div role="tabpanel" className="tab-pane messages" id={"messages" + this.props.objectId}>
		    	<div className="a-div-head">
		    		<div className="row">
		    			<div className="col-sm-4 col-md-4 col-lg-4">
		    				<div className="title-vide">Au Total</div>
		    				<span className="glyph-icon flaticon-web-1 flaticon-green" aria-hidden="true"></span>
		    			</div>
		    			<div className="col-sm-4 col-md-4 col-lg-4 total">
		    				<div className="title-total">Au Total</div>
		    				<div className="circle-outside">
		    					<div className="circle-inside">
		    						<div className="text-total">{this.defaultValue(this.state.info.totalEvalmsg)}</div>
		    					</div>
		    				</div>
		    			</div>
		    			<div className="col-sm-4 col-md-4 col-lg-4 nosee">
		    				<div className="title-total">Non Lu</div>
		    				<div className="circle-outside">
		    					<div className="circle-inside">
		    						<div className="text-nosee">{this.defaultValue(this.state.info.evalmsg)}</div>
		    					</div>
		    				</div>
		    			</div>
		    		</div>	
		    	</div>
		    	<div className="a-div-body">
		    		<ul className="event-message StateParkMessageListComponent" id="StateParkMessageList">
		    			<li className="btnMoreEvent">
							<a href="#" onClick={this.moreEvent}>
								<h2>...</h2>
							</a>
						</li>
						<li className="divider"></li>
		    			<ULComponent.LiListEvent groupInfo = {this.state.events} filtre = {this.props.filtre}>
		    				<ULComponent.aMessage refId='tic' href = {'javascript:'} iconClassName = {'icon entypo user'}>						
		    				</ULComponent.aMessage>
		    			</ULComponent.LiListEvent>
		    		</ul>
		    	</div>
		    </div>
		);
	}	
});

module.exports = TabsInformationMessageListComponent;