var React = require('react');
var ReactDOM = require('react-dom');

var EventStore = require('../stores/EventStore');
var HistoryEventStore = require('../stores/HistoryEventStore');
var ObjectStore = require('../stores/ObjectStore');
var WebAPIUtils = require('../utils/WebAPIUtils');
var EventActionCreator = require('../actions/EventActionCreator');

var _ = require('underscore');

var EvalMsgInfoWindowComponent = React.createClass({
	displayName: 'EvalInfoWindowComponent',

	getInitialState: function(){
		return {
			loadedInformation: false,
			info: {}
		}
	},

	componentDidMount: function(){
		if (!this.props.info.hasOwnProperty('societeName')){
			var objectID = this.props.info.objetID;
			var infoObject = EvalMsgInfoWindowComponent.prototype.ObjectStore.getObject(objectID);
			EvalMsgInfoWindowComponent.prototype.WebAPIUtils.searchInfoObjectToTab(objectID, infoObject.idParent);
			EvalMsgInfoWindowComponent.prototype.ObjectStore.addListenner('newPropertyObject-' + this.props.info.objetID, this.onNewProperty);
		}
	},

	onNewProperty: function(){
		console.log('onNewProperty - EvalMsgInfoWindowComponent');
		EvalMsgInfoWindowComponent.prototype.ObjectStore.deleteListener('newPropertyObject-' + this.props.info.objetID, this.onNewProperty);
		this.state.info = EvalMsgInfoWindowComponent.prototype.ObjectStore.getObject( this.props.info.objetID );
		this.setState({loadedInformation: true});
	},

	handleClickCheck: function(event, state, actionID){
		/*
		* Si le state est 'false' on suprime l'event, 
		* du coup il faut fermer le popup
		*/
	
		var r_evnt = EvalMsgInfoWindowComponent.prototype.EventStore.getEventTypeId('eval', actionID);
		
		if (r_evnt != null){ /*la prisEnCompte d'event est false*/
			var evnt = _.clone(r_evnt);
			evnt.prisEnCompte = state;
			evnt.innerOpertation = true;

			var data = {
				typ: 'eval',
				data: [evnt] 
			}
			//console.log('handleClickCheckbox: ' + evnt.actionID + ';; ' + r_evnt.prisEnCompte + '!= ' + evnt.prisEnCompte + ';' + state);
			EvalMsgInfoWindowComponent.prototype.EventActionCreator.receiveEventsStream(data);
		}
		else{
			var r_evnt = EvalMsgInfoWindowComponent.prototype.HistoryEventStore.getEventTypeId('eval', actionID);
			var evnt = _.clone(r_evnt);
			evnt.prisEnCompte = false;
			evnt.innerOpertation = true;

			var data = {
				typ: 'eval',
				data: [evnt] 
			}
			//console.log('handleClickCheckbox: ' + evnt.actionID + ';; ' + r_evnt.prisEnCompte + '!= ' + evnt.prisEnCompte + ';' + state);
			EvalMsgInfoWindowComponent.prototype.EventActionCreator.receiveEventsStream(data);
		}
	},

	render: function(){
		var evalImage = null
		console.log(this.props.info.evaluation);
		if (this.props.info.hasOwnProperty('evaluationImage') && 
			this.props.info.evaluationImage != null)
			evalImage = this.props.info.evaluationImage;
		else
			switch(this.props.info.evaluation){
				case 2:
					evalImage = 'smile-mid.svg';
				break;
				case 1:
					evalImage = 'smile-sad.svg';
				break;
				default:
					evalImage = 'smile-happy.svg';
				break;
			}
		console.log(evalImage);
		return (
			<div className="EvalMsgInfoWindowComponent">
				<div></div>
				<div className="title">
					<span className="info">evaluation</span>
					<a href="#" onClick={this.handleClickCheck.bind(this, {}, true, this.props.info.actionID)}>
						<span className="flaticon-check" aria-hidden="true"></span>
					</a>
				</div>
				<div className="subTitle">
					<div>{this.state.info.nameType}</div>
					<div>{this.props.info.objetName}</div>
				</div>
				<div className="img">
					<img className="center-block" src={'./' + this.props.info.objetImage} />
				</div>
				<div className="body">
					<div>{this.props.info.customerName}</div>
					<div>{this.props.info.createdTime}</div>
					<div className="img">
						<img src={'./build/img/' + evalImage} />
					</div>
					<div>Localisation</div>
					<div>{this.props.info.adresse}</div>
				</div>
			</div>
		);
	}
});

EvalMsgInfoWindowComponent.prototype.ObjectStore = ObjectStore;
EvalMsgInfoWindowComponent.prototype.EventStore = EventStore;
EvalMsgInfoWindowComponent.prototype.HistoryEventStore = HistoryEventStore;
EvalMsgInfoWindowComponent.prototype.WebAPIUtils = WebAPIUtils;
EvalMsgInfoWindowComponent.prototype.EventActionCreator = EventActionCreator;
module.exports = EvalMsgInfoWindowComponent;