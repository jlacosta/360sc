var React = require('react');
var ReactDOM = require('react-dom');

var LIComponent = require('./LIComponent.react');
var AComponent = require('./AComponent.react');

var BackLIButtonComponent = React.createClass({

	getDefaultProps: function(){
		return {
			aClassName: 'gx-trigger-left',
			iconClassName: 'icon entypo chevron-left'
		}
	},

	render: function(){
		return (
			<LIComponent className='back'>
				<AComponent href='javascript:' aClassName='gx-trigger-left' 
				iconClassName='icon entypo chevron-left'>
					Back
				</AComponent>	
			</LIComponent>
		);
	}
});

module.exports = BackLIButtonComponent;