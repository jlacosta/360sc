var React = require('react');
var ReactDOM = require('react-dom');

var ObjectStore = require('../stores/ObjectStore');
var EventStore = require('../stores/EventStore');

var MapUtils = require('../utils/MapUtils');

var MapBridge = require('../bridges/MapBridge');

var Map = React.createClass({

	componentDidMount: function(){
		MapUtils.fixMapAndInit();

		MapBridge.init();

		ObjectStore.addNewObjectsListener(this.onNewObjects);
	},

	onNewObjects: function(){
		MapUtils.onNewObjectToMap(ObjectStore.getObjects());
	},

	render: function(){
		return (
			<div id='map'></div>
		);
	}
});

module.exports = Map;