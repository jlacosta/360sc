var React = require('react');
var ReactDOM = require('react-dom');
var ReactDOMServer = require('react-dom/server');
var classNames = require('classnames');


var ULComponent = require('./ULComponent.react.js');
var AlertInfoWindowComponent = require('./AlertInfoWindowComponent.react');

var EventStore = require('../stores/EventStore');
var ObjectStore = require('../stores/ObjectStore');
var EventActionCreator = require('../actions/EventActionCreator');

var MapUtils = require('../utils/MapUtils');

var StateParkAlertInfoComponent = React.createClass({
	displayName: 'StateParkAlertInfoComponent',

	getDefaultProps: function (){
		return {
			info: null,
			aClassName: 'simple',
			href: '#',
			iconClassName: 'icon entypo picture'
		}
	},

	componentDidMount: function(){
		/*console.log(this.props.info.actionID);*/

		var myIdComponent = 'alert-' + this.props.info.actionID;
		//console.log(myIdComponent);

		EventStore.addEventListenner(myIdComponent, this.onChangeProp);

		/*Initialize Bootstrap Switch.*/
		var selector = "[name="+ 'alert-' + this.props.info.actionID + "]";
		$(selector).bootstrapSwitch();

		/*Event switchChange*/
		selector = "#ialert-" + this.props.info.actionID;
		var switchChange = function(event, state) {
			this.handleClickCheck(event, state, this.props.info.actionID);
		}.bind(this);
		$(selector).on('switchChange.bootstrapSwitch', switchChange);
	}, 

	componentDidUpdate: function(){	
	},

	onChangeProp: function(){
		//console.log('onChangeProp');

		var myIdComponent = 'alert-' + this.props.info.actionID;
		/*var isChecked = EventStore.getEventTypeId('alert', this.props.info.actionID).prisEnCompte;
		if (isChecked) isChecked = true;
		else isChecked = false;*/
		var isChecked = true;

		var selector = "input[name="+ 'alert-' + this.props.info.actionID + "]";

		$(selector).bootstrapSwitch('state', isChecked);
	},

	handleClickCheck: function(event, state, actionID){
		var r_evnt = EventStore.getEventTypeId('alert', actionID);
		var evnt = Object.assign({}, r_evnt);
		evnt.prisEnCompte = state;
		evnt.innerOpertation = true;

		var data = {
			typ: 'alert',
			data: [evnt] 
		}
		//console.log('handleClickCheckbox: ' + evnt.actionID + ';; ' + r_evnt.prisEnCompte + '!= ' + evnt.prisEnCompte + ';' + state);
		EventActionCreator.receiveEventsStream(data);
	},

	handleCenterEvent: function(objetID, infoEvent, typ, source){
		var objetID = objetID;
		var infoEvent = infoEvent;
		infoEvent.objetImage = ObjectStore.getObject(objetID).image;
		infoEvent.adresse = ObjectStore.getObject(objetID).adresse;

		MapUtils.openInfoWindow(AlertInfoWindowComponent, objetID, infoEvent);
	},

	haveMessage: function(){
		if (typeof this.props.info.messageAlerte !== typeof null &&
			this.props.info.messageAlerte !== '')
			return false;
		return true;
	},

	openInfoObject: function(){
		$.when( MapUtils.closeGxMenu() ).then(function(v){
			if (!$('#gx-sidemenu-right').hasClass('opened')){
				$('.gx-menu.gx-trigger-right.entypo.list').trigger('click');
				$('.MarkersComponent a:first').trigger('click');
				setTimeout(function(){
					var id = '#MarkerInfoComponent' + this.props.info.objetID + ' a:first';
					$(id).trigger('click');
					var href = "#generalInformation" + this.props.info.objetID;
					var selector = '.TabsInformationComponent a[href="' +  href + '"]';
					$(selector).tab('show');
				}.bind(this), 250);
			}
		}.bind(this));
	},

	render: function(){
		if (this.props.info === null)
			return (
				<a href={this.props.href} className={this.props.aClassName}>
					<span className={this.props.iconClassName}></span>
					<span className="text">{this.props.children}</span>
				</a>	
			);

		//console.log(this.props.info.actionID + ';;' + this.props.info.prisEnCompte);

		var id = this.props.refId+'-'+ this.props.info.actionID;
		var objectID = this.props.info.objetID;
		var infoEvent = this.props.info;
		var typ = 'alert';
		var cn_IconMessage = classNames({
			'glyph-icon flaticon-web-1': true,
			'hidden': this.haveMessage()
		});
		return (
			<div className="panel panel-default">
				<div className="panel-heading" role="tab" id="headingOne">
					<h4 className="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href = {"#"+id} aria-expanded="true" aria-controls = {id}>
							{this.props.info.objetName}
							<span className="date">
								{this.props.info.createdTime}
							</span>
							<div className="icons">
								<span className={cn_IconMessage} aria-hidden="true"></span>
								<span className="glyph-icon flaticon-shapes" aria-hidden="true"></span>
								<div className="clearfix" />	
							</div>
						</a>
					</h4>
				</div>
				<div id = {id} className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
					<div className="panel-body">
						<span className="body-title">
							{this.props.info.customerName}
						</span>
						<a href="#" onClick={this.openInfoObject}>
							<span className="flaticon flaticon-circle-2" aria-hidden="true"></span>
						</a>
						<span className="flaticon flaticon-connection" aria-hidden="true"></span>
						<a href="#" onClick={this.handleCenterEvent.bind(this, objectID, infoEvent, typ, 'event')}>	
							<span className="flaticon flaticon-gps" aria-hidden="true"></span>
						</a>	
						<a href="#" onClick={this.handleClickCheck.bind(this, {}, true, this.props.info.actionID)}>
							<span className="flaticon flaticon-check" aria-hidden="true"></span>
						</a>	
						<div className="clearfix" />
						<div className="body-subTitle">
							{this.props.info.messageAlerte}
						</div>
					</div>
				</div>				
			</div>
		);
	}
});

module.exports = StateParkAlertInfoComponent;