var React = require('react');
var ReactDOM = require('react-dom');

var ULComponent = require('./ULComponent.react');
var MarkerInfoComponent = require('./MarkerInfoComponent.react');

var ObjectStore = require('../stores/ObjectStore');

var MarkersComponent = React.createClass({
	displayName: 'MarkersComponent',

	getInitialState: function(){
		return {
			objects: ObjectStore.getObjects()
		}
	},

	componentWillMount: function(){
	},

	componentDidMount: function(){
		ObjectStore.addNewObjectsListener(this.onChangeObjectCollection);
		$('.MarkersComponent').find('a:first').click(this.setNameBackBtn);
	},

	componentDidUpdate: function(){
	},

	onChangeObjectCollection: function(){

		this.setState({
			objects: ObjectStore.getObjects()
		});
	},

	renderMarkerInfoComponent: function(){
		var objects = this.state.objects;
		var keysObject = Object.keys(objects);
		var lenKeysObject = keysObject.length; 
		var children = [];

		for (var i = 0; i < lenKeysObject; i++) {
			child = React.createElement(MarkerInfoComponent, {
				info: objects[ keysObject[i] ],
				key: 'MarkerInfoComponent' + keysObject[i],
				element: i
			});
			children.push(child);
		};
		return children;
	},

	setNameBackBtn: function(){
		$('.MarkersComponentUL').find('.back span:nth-child(2)').text('objects');
	},

	render: function(){
		return (
			<li className="MarkersComponent" id="Markers">
				<a href="javascript:">
					<span className="icon entypo attach parentAttach">
						<span className="icon entypo tag childAttach"></span>
					</span>
					<span className="text">Objects</span>
				</a>
				<ul className="simple MarkersComponentUL">
					{this.renderMarkerInfoComponent()}
				</ul>
			</li>
		);
	}
});

module.exports = MarkersComponent;