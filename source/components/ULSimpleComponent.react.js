
var React = require('react');
var ReactDOM = require('react-dom');
var ReactAddons = require('react-addons');

var ULSimpleComponent = React.createClass({
	displayName: 'ULSimpleComponent',

	render: function(){
		return (
			<ul className={this.props.PropClassName}>
				{this.props.children}
			</ul>
		);
	}
});


module.exports = ULSimpleComponent;