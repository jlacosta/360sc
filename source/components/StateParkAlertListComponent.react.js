var React = require('react');
var ReactDOM = require('react-dom');

var ULComponent = require('./ULComponent.react');
var StateParkAlertInfoComponent = require('./StateParkAlertInfoComponent.react');

var EventStore = require('../stores/EventStore');
var ObjectStore = require('../stores/ObjectStore');
var EventActionCreator = require('../actions/EventActionCreator');
var WebApiUtils = require('../utils/WebApiUtils');

var StateParkAlertListComponent = React.createClass({
	displayName: 'StateParkAlertListComponent',

	getInitialState: function(){
		var events = EventStore.getEventsType('alert');
		if (!Array.isArray(events)) events = [];
		return {
			events: events,
			lenEvents: events.length,
			lastEvent: null
		}
	},

	componentDidMount: function(){
		EventStore.addNewEventListenner(this.onChangeEventCollection);
		EventStore.addEventListenner('changed_alert', this.onChangeEventCollection);
		$(ReactDOM.findDOMNode(this.refs['linkStateParkAlertList'])).click(function(){
			this.setNameBackBtn();
		}.bind(this));
	},

	componentDidUpdate: function(){
		this.triggerResizeWindow();
	},

	shouldComponentUpdate: function(nextProps, nextState){
		if (this.props.active == false && nextProps.active == true) return true;
		if (nextState.events.length != this.state.lenEvents)
			return true;
		return false;
	},
	
	onChangeEventCollection: function(){
		var events = EventStore.getEventsType('alert');
		this.setState({
			events: events,
			lenEvents: events.length,
			lastEvent: events[ events.length - 1 ]
		});
	},

	setNameBackBtn: function(){
		$('.StateParkAlertListComponentUL').find('.back:first span:nth-child(2)').text('alerte');
	},

	moreEvent: function(){
		if (this.state.lastEvent == null) return;
		var option = {
			fn: EventActionCreator.receiveEventsHistory,
			typ: 'alert',
			date: this.state.lastEvent.createdTime
		}
		WebApiUtils.getMoreEvents(option);
	},

	triggerResizeWindow: function(){
		$(window).trigger('resize');
	},

	render: function(){
		if (this.props.active)
			return (
				<li className="event-alert StateParkAlertListComponent" id="StateParkAlertList">
					<a href="javascript:" ref="linkStateParkAlertList">
						<span className="glyph-icon flaticon-shapes"></span>
						<span className="text">Alertes 
							<span className="badge" style = {this.props.pstyle.badge}>{ this.state.events.length } </span>
						</span>
					</a>
					<ul className="simple StateParkAlertListComponentUL">
						<div className = "separationTop" />
						<li className="btnMoreEvent">
							<a href="#" onClick={this.moreEvent}>
								<h2>...</h2>
							</a>
						</li>
						<li className="divider"></li>
						<ULComponent.LiListEvent groupInfo={this.state.events} filtre = {this.props.filtre}>
							<StateParkAlertInfoComponent refId='spalc' href={'javascript:'} iconClassName={'icon entypo user'}>						
							</StateParkAlertInfoComponent>
						</ULComponent.LiListEvent>
					</ul>
				</li>
			);
		else
			return (
				<li className="event-alert StateParkAlertListComponent" id="StateParkAlertList">
					<a href="javascript:" ref="linkStateParkAlertList">
						<span className="icon entypo users"></span>
						<span className="text">{ this.state.events.length } Alertes</span>
					</a>
					<ul className="simple StateParkAlertListComponentUL">
					</ul>
				</li>
			);
	}
});

module.exports = StateParkAlertListComponent;