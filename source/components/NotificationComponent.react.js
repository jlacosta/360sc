var React = require('react');
var ReactDOM = require('react-dom');
var classNames = require('classnames');

var ObjectStore = require('../stores/ObjectStore');

var MapUtils = require('../utils/MapUtils');

var NotificationComponent = React.createClass({

	componentDidMount: function(){
		ObjectStore.addChangeObjectListenner(this.onChangeEvent);
		ObjectStore.addInitObjectListenner(this.onInitEvent);
	},

	onChangeEvent: function(){
		this.setState({
			notSeen: ObjectStore.getNiveauUrgence()
		});
	},

	onInitEvent: function(){
		this.setState({
			notSeen: ObjectStore.getNiveauUrgence()
		});
	},

	getInitialState: function(){
		return {
			notSeen: 0
		}
	},

	onChangeStatistics: function(){
		this.setState({
			notSeen: 0
		});
	},

	shouldComponentUpdate: function(nextProps, nextState){
		return (nextProps.notSeen != this.state.notSeen);
	},

	openStatePark: function(){
		$.when( MapUtils.closeGxMenu() ).then(function(v){
			if (!$('#gx-sidemenu-right').hasClass('opened')){
				$('.gx-menu.gx-trigger-right.entypo.list').trigger('click');
				$('.StateParkComponent a:first').trigger('click');
			}
		}.bind(this));
	},

	render: function(){
		var bellStyle = classNames({
			'glyphicon glyphicon-bell bell': true,
			'n-icon-bell': true
		});

		return (
			<a className='notificationComponent' href="#" onClick={this.openStatePark}>
				<span>
					<span aria-hidden='true' className={bellStyle}></span>
					<span className='text-notificationComponent'>{ this.state.notSeen }</span>
				</span>
			</a>
		);
	}
});

module.exports = NotificationComponent;