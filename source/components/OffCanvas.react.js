var React = require('react');
var ReactDOM = require('react-dom');
var OffCanvasLeft = require('./OffCanvasLeft.react.js');
var OffCanvasRight = require('./OffCanvasRight.react.js');

var OffCanvas = React.createClass({
	propTypes:  {
		id: React.PropTypes.string.isRequired
	},

	getDefaultProps: function(){
		return {
			id: '',
			objects: {}
		}
	},
	
	render: function(){
		switch(this.props.id){
		case 'gx-sidemenu-right':
			return <OffCanvasRight id={'gx-sidemenu-right'} />;
		case 'gx-sidemenu-left':
			return <OffCanvasLeft id={'gx-sidemenu'} />;
		default:
			return;	
		}
	}
});

module.exports = OffCanvas;