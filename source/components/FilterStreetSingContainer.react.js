var React = require('react');
var ReactDOM = require('react-dom');
var FilterStreetSingComponent = require('./FilterStreetSingComponent.react');
var ObjectStore = require('../stores/ObjectStore');
var _ = require('underscore');
var moment = require('moment');
var StretSignFilter = require('../utils/filters/StreetSign');
var MapUtils = require('../utils/MapUtils');

FilterStreetSingContainer = React.createClass({
	displayName: 'FilterStreetSingContainer',

	getDefaultProps: function(){
		return {
			delayMinMax: 840 //12 months * 70 years 
		}
	},

	getInitialState: function(){
		return {
			initialDate: moment().month(), //now
			finalValableDate: moment().month(), //now
			min: 0,
			max: 1,
			step: 1,
			value: [0, 1],
			show: false,
			list: {}
		}
	},

	componentDidMount: function(){
		ObjectStore.addNewObjectsListener(this.onNewData);
		MapUtils.utils.addNewFilterListener(MapUtils.utils.callbackNames.streetSign, 
			this.onNewDataFilterStreetSign
		);
	},

	onNewData: function(){
		var objects = ObjectStore.getObjects();
		var keys = Object.keys(objects);
		var len = keys.length;
		var show = false;
		initialDate = 0;
		min = moment();
		max = moment().add(10, 'month');
		maxDateStreetSign = 0;
		step = 1;
		value = [0, 10];

		len--;
		/*minDate = moment().add(840, 'month');*/
		minDate = null;
		/*maxDate = moment().subtract(840, 'month');*/
		maxDate = null;
		maxFinalDate = null;
		len++;

		while(len--){
			if (objects[ keys[len] ].hasOwnProperty('dateStreetSign') && 
				objects[ keys[len] ].dateStreetSign > 0){
				show = true;
				tempDate = moment(objects[ keys[len] ].dateActive);

				if (!minDate) minDate = tempDate;
				else minDate = moment.min(minDate, tempDate);/*Date active*/
				
				if (!maxDate) maxDate = tempDate;
				else maxDate = moment.max(maxDate, tempDate); /*maxDate active*/

				/*console.log('.' + tempDate.format('D-M-Y'));*/
				tmpDateMonth =  tempDate.add(objects[ keys[len] ].dateStreetSign, 'month');
				/*console.log('.' + tmpDateMonth.format('D-M-Y'));
				console.log('..');*/
				
				if (!maxFinalDate) maxFinalDate = tmpDateMonth;
				else maxFinalDate = moment.max(maxFinalDate, tmpDateMonth); /*final date valable*/
			}
		}

		initialDate = minDate;
		finalValableDate = maxFinalDate;
		min = 0;
		max = finalValableDate.diff(moment(), 'month');

		console.log('initialDate: ' + moment().format('D-M-Y'));
		console.log('finalValableDate: ' + finalValableDate.format('D-M-Y'));
		console.log('min: ' + min);
		console.log('max: ' + max);

		this.setState(
		{
			initialDate: moment(),
			finalValableDate: finalValableDate, 
			min: min,
			max: max,
			step: 1,
			value:[0, max],
			show: show
		});
	},

	onChange: function(e){
		console.log(e);
	},

	activeFilter: function(filterStrSign, checkboxFilter, diff){
		var streetSign = new StretSignFilter(filterStrSign);
		streetSign.setShow(checkboxFilter);
		if (diff && diff > 0) 
			streetSign.setDiff(diff);
		MapUtils.filterStreetSign(streetSign);
	},

	onNewDataFilterStreetSign: function(list){		
		this.setState({
			list: list
		});
	},

	render: function(){
		return (
			<FilterStreetSingComponent onChange={this.onChange} 
				initialDate={this.state.initialDate}
				finalValableDate={this.state.finalValableDate}
				min={this.state.min} 
				max={this.state.max} 
				step={this.state.step} 
				value={this.state.value} 
				show = {this.state.show} 
				activeFilter = {this.activeFilter}
				list = {this.state.list || {}} />
		);
	}
});

module.exports = FilterStreetSingContainer;