var React = require('react');
var ReactDOM = require('react-dom');

var ULComponent = require('./ULComponent.react');
var StateParkAlertInfoComponent = require('./StateParkAlertInfoComponent.react');

var EventStore = require('../stores/EventStore');
var ObjectStore = require('../stores/ObjectStore');
var EventActionCreator = require('../actions/EventActionCreator');
var WebApiUtils = require('../utils/WebApiUtils');

var TabsInformationAlertListComponent = React.createClass({
	displayName: 'TabsInformationAlertListComponent',

	getInitialState: function(){
		var events = EventStore.getEventsObject('alert', this.props.objectId);
		var object = ObjectStore.getObject(this.props.objectId);
		if (!Array.isArray(events)) events = [];
		return {
			events: events,
			lenEvents: events.length,
			lastEvent: null,
			info: object,
			updateCounter: false
		}
	},

	componentDidUpdate: function(){
		this.triggerResizeWindow();
	},

	componentDidMount: function(){
		EventStore.addNewEventListenner(this.onChangeEventCollection);
		EventStore.addEventListenner('changed_alert', this.onChangeEventCollection);
		ObjectStore.addChangeObjectListenner(this.onChangeObjectCollection);
	},

	shouldComponentUpdate: function(nextProps, nextState){
		if (this.props.active == false && nextProps.active == true) return true;
		if (nextState.events.length != this.state.lenEvents) return true;
		if (nextState.updateCounter) {
			nextState.updateCounter = false;
			return true;
		}
		return false;
	},
	
	onChangeEventCollection: function(){
		var events = EventStore.getEventsObject('alert', this.props.objectId)
		this.setState({
			events: events,
			lenEvents: events.length,
			lastEvent: events[ events.length - 1 ]
		});
	},

	onChangeObjectCollection: function(){
		/*console.log('onChangeObjectCollection - TabsInformationAlertListComponent');*/
		var object = ObjectStore.getObject(this.props.objectId);
		/*console.log(object);*/
		this.setState({
			info: object,
			updateCounter: true
		});
	},

	defaultValue: function(value){
		if (typeof value !== typeof undefined &&
			typeof value !== typeof null)
			return value;
		return 0;
	},

	moreEvent: function(){
		var date = 0;
		if (this.state.lastEvent != null){
			date = this.state.lastEvent.createdTime;
		}
		var option = {
			fn: EventActionCreator.receiveEventsHistory,
			typ: 'alert',
			date: date,
			objectID: this.props.objectId
		}
		WebApiUtils.getMoreEventsByObject(option);
	},

	triggerResizeWindow: function(){
		$(window).trigger('resize');
	},

	render: function(){

		/*if (this.props.active)*/
			return (
				<div role="tabpanel" className="tab-pane alerts" id={"alerts" + this.props.objectId}>
				    <div className="a-div-head">
			    		<div className="row">
			    			<div className="col-sm-4 col-md-4 col-lg-4">
			    				<div className="title-vide">Au Total</div>
			    				<span className="glyph-icon flaticon-shapes flaticon-red" aria-hidden="true"></span>
			    			</div>
			    			<div className="col-sm-4 col-md-4 col-lg-4 total">
			    				<div className="title-total">Au Total</div>
			    				<div className="circle-outside">
			    					<div className="circle-inside">
			    						<div className="text-total">{this.defaultValue(this.state.info.totalAlerts)}</div>
			    					</div>
			    				</div>
			    			</div>
			    			<div className="col-sm-4 col-md-4 col-lg-4 nosee">
			    				<div className="title-total">Non Lu</div>
			    				<div className="circle-outside">
			    					<div className="circle-inside">
			    						<div className="text-nosee">{this.defaultValue(this.state.info.alerts)}</div>
			    					</div>
			    				</div>
			    			</div>
			    		</div>
		    		</div>
		    		<div className="a-div-body">
				    	<ul className="event-alert StateParkAlertListComponent simple" id="StateParkAlertList">
				    		<li className="btnMoreEvent">
								<a href="#" onClick={this.moreEvent}>
									<h2>...</h2>
								</a>
							</li>
							<li className="divider"></li>
				    		<ULComponent.LiListEvent groupInfo={this.state.events} filtre = {this.props.filtre}>
				    			<StateParkAlertInfoComponent href={'javascript:'} iconClassName={'icon entypo user'}>						
				    			</StateParkAlertInfoComponent>
				    		</ULComponent.LiListEvent>
				    	</ul>
				    </div>	
		    	</div>	
			);
		/*else
			return (
				<ul className="simple">
				</ul>
			);*/
	}
});

module.exports = TabsInformationAlertListComponent;