var React = require('react');
var ReactDOM = require('react-dom');
var ULComponent = require('./ULComponent.react.js');

var AComponent = React.createClass({
	displayName: 'AComponent',

	getDefaultProps: function (){
		return {
			info: null,
			aClassName: 'simple',
			iconClassName: 'icon entypo picture'
		}
	},

	render: function(){
		if (this.props.info === null)
			return (
				<a href={this.props.href} className={this.props.aClassName}>
					<span className={this.props.iconClassName}></span>
					<span className="text">{this.props.children}</span>
				</a>	
			);
		return (
			<a href={this.props.href} className={this.props.aClassName}>
				<span className={this.props.iconClassName}></span>
				<span className="text">{this.props.info.name}</span>
			</a>
		);
	}
});

module.exports = AComponent;