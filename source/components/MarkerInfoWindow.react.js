var React = require('react');
var ReactDOM = require('react-dom');

var ObjectStore = require('../stores/ObjectStore');
var WebAPIUtils = require('../utils/WebAPIUtils');

var MapUtils = require('../utils/MapUtils');

var MarkerInfoWindow = React.createClass({
	displayName: 'MarkerInfoWindow',

	getInitialState: function(){
		return {
			loadedInformation: false
		}
	},

	componentDidMount: function(){
		if (!this.props.info.hasOwnProperty('societeName')){
			MarkerInfoWindow.prototype.WebAPIUtils.searchInfoObjectToTab(this.props.info.objetID, this.props.info.idParent);
			MarkerInfoWindow.prototype.ObjectStore.addListenner('newPropertyObject-' + this.props.info.objetID, this.onNewProperty);
		}
	},

	shouldComponentUpdate: function(s, p){
		console.log('shouldComponentUpdate - MarkerInfoWindow');
		return true;
	},

	openInfoObject: function(){
		$.when( MapUtils.closeGxMenu() ).then(function(v){
			if (!$('#gx-sidemenu-right').hasClass('opened')){
				$('.gx-menu.gx-trigger-right.entypo.list').trigger('click');
				$('.MarkersComponent a:first').trigger('click');
				setTimeout(function(){
					var id = '#MarkerInfoComponent' + this.props.info.objetID + ' a:first';
					$(id).trigger('click');
					var href = "#generalInformation" + this.props.info.objetID;
					var selector = '.TabsInformationComponent a[href="' +  href + '"]';
					$(selector).tab('show');
				}.bind(this), 250);
			}
		}.bind(this));
	},

	onNewProperty: function(){
		console.log('onNewProperty - MarkerInfoWindow');
		MarkerInfoWindow.prototype.ObjectStore.deleteListener('newPropertyObject-' + this.props.info.objetID, this.onNewProperty);
		this.props.info = MarkerInfoWindow.prototype.ObjectStore.getObject( this.props.info.objetID );
		this.setState({loadedInformation: true});
	},

	render: function(){
		return(
			<div className="MarkerInfoWindowComponent">
				<div></div>
				<div className="title">
					<span className="info">info</span>
					<a href="#" onClick={this.openInfoObject}>
						<span className="glyph-icon flaticon-circle-2" aria-hidden="true"></span>
					</a>	
				</div>
				<div className="subTitle">
					<div>{this.props.info.nameType}</div>
					<div>{this.props.info.name}</div>
				</div>
				<div className="img">
					<img className="center-block" src={'./' + this.props.info.image} />
				</div>
				<div className="body">
					<div>Localisation</div>
					<div>{this.props.info.adresse}</div>
					<div>Maintenance prévue</div>
					<div>19/03/2016</div>
				</div>
			</div>
		);
	}
});


MarkerInfoWindow.prototype.ObjectStore = ObjectStore;
MarkerInfoWindow.prototype.WebAPIUtils = WebAPIUtils;
module.exports = MarkerInfoWindow;