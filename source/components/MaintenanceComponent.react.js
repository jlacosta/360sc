var React = require('react');
var ReactDOM = require('react-dom');
var FilterStreetSingContainer = require('./FilterStreetSingContainer.react');

MaintenanceComponent = React.createClass({
	displayName: 'MaintenanceComponent',

	componentDidMount: function(){
	},

	getInitialState: function(){
		return {
			isStatusUp: false,
			isStatusMiddle: true,
			isStatusDown: true
		};
	},

	toggleVisibilitySlider: function(){
		$('.opt-slider-date').toggleClass('hidden');
	},

	toggleActive: function(){
		console.log('toggleActive');
	},

	render: function(){
		return(
			<li className="MaintenanceComponent" id="Modes">
				<a href="javascript:">
					<span className="icon entypo attach parentAttach">
						
					</span>
					<span className="text">Maintenance</span>
				</a>
				<ul className="simple">
					<FilterStreetSingContainer />
				</ul>
			</li>
		);
	}
});

module.exports = MaintenanceComponent;