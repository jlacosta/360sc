var renderGxMenuRight = function(){
	$('#gx-sidemenu-right').gxSideMenu({
		mode: 'normal', // normal | tiny
		interval: 300, // animations' interval
		direction: 'right', // left | right
		openOnHover: false, // true | false
		clickTrigger: true, // true | false
		followURLs: true, // true | false
		trigger: ".gx-trigger-right", // class or id of trigger element
		startFrom: 60, // start pixel from corner on hover trigger
		startClosed: true, // menu opens on document load
		scrolling: true, // menu scrollable (iScroll plugin needed!)
		urlBase: '/sidemenu/', // document base URL
		backText: 'Précedent', // Back button text
		onOpen: function() {
		}, // Open callback
		onClose: function() { } // Close callback
	});

}

var renderGxMenuLeft = function(){
	$('#gx-sidemenu').gxSideMenu({
		mode: 'normal', // normal | tiny
		interval: 300, // animations' interval
		direction: 'left', // left | right
		openOnHover: false, // true | false
		clickTrigger: true, // true | false
		followURLs: true, // true | false
		trigger: ".gx-trigger-left", // class or id of trigger element
		startFrom: 60, // start pixel from corner on hover trigger
		startClosed: true, // menu opens on document load
		scrolling: true, // menu scrollable (iScroll plugin needed!)
		urlBase: '/sidemenu/', // document base URL
		backText: 'Précedent', // Back button text
		onOpen: function() {
			
		}, // Open callback
		onClose: function() { } // Close callback
	});


}

var showScrollerWrapper = function(){
	/*$('pre.snippet').snippet("javascript", { style: 'ide-kdev' });
	$('pre.snippet-html').snippet("html", { style: 'ide-kdev' });*/
}

var show = function(id){
	$(id).show();
}

var hide = function(id){
	$(id).hide();
}


module.exports = {
	renderGxMenuRight: renderGxMenuRight,
	renderGxMenuLeft: renderGxMenuLeft,
	show: show,
	hide: hide
}