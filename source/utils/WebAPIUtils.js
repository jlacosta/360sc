var streamClient = require('./stream-info-client');
var ObejctActionCreator = require('../actions/ObjectActionCreator');
var EventActionCreator = require('../actions/EventActionCreator');

var initializeStreaming = function (option){
	streamClient.getEventsStream(EventActionCreator.receiveEventsStream);
	streamClient.getPropagatedEventsStream(EventActionCreator.receivePropagatedEventsStream);
}

var initializeObjects = function (option){
	streamClient.getObjects(ObejctActionCreator.receiveObjects);
}

var initializeEvents = function(option){
	streamClient.getEvents(EventActionCreator.receiveEventsHistory, 'alert', 0);
	streamClient.getEvents(EventActionCreator.receiveEventsHistory, 'evalmsg', 0);
	streamClient.getEvents(EventActionCreator.receiveEventsHistory, 'eval', 0);
}

var getMoreEvents = function(option){
	console.log(option);
	streamClient.getEvents(option.fn, option.typ, option.date);
}

var getMoreEventsByObject = function(option){
	console.log(option);
	streamClient.getEvents(option.fn, option.typ, option.date, option.objectID);
}

var dataObjectToNet = function(data){
	/*exitToNet(channel, data)*/
	streamClient.exitToNet('dataObject_Event', data);
}

var eventChangedToServer = function(data){
	console.log('eventChangedToServer');
	dataObjectToNet(data);
	streamClient.eventChangedToServer(data);
}

var autorisation = function(fn){
	streamClient.autorisation(fn);
}

var searchInfoObjectToTab = function(objectId, parentId){
	streamClient.getInfoToTab(ObejctActionCreator.receiveNewProperty, objectId, parentId);
}

var getInfoClient = function(fn){
	streamClient.getInfoClient(fn);
}

var getHistory = function(fn, data){
	streamClient.getHistory(fn, data);
}

module.exports = {
	getMoreEvents: getMoreEvents,
	getMoreEventsByObject: getMoreEventsByObject,
	initializeObjects: initializeObjects,
	initializeEvents: initializeEvents,
	initializeStreaming: initializeStreaming,
	dataObjectToNet: dataObjectToNet,
	eventChangedToServer: eventChangedToServer,
	autorisation: autorisation,
	searchInfoObjectToTab: searchInfoObjectToTab,
	getInfoClient: getInfoClient,
	getHistory: getHistory
}