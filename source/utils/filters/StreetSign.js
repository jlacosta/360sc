var moment = require('moment');
var _ = require('underscore');

/*
	var:
		streetSign:

*/

var Filter = function(StreetSign){
	this.iconRoot = ''; 
	this.streetSign = StreetSign;
	this.streetSign['nowDate'] = moment();
	// 001, 010, 100
	this.show = 7; // Is active all filters (111)
	this.deffTimeValable = 7; //Notification # month before   

	this.listFn = [];
	this.indexFn = 0;
	this.tmpState = {};

	this.setStreetSign = function(StreetSign){
		this.streetSign = StreetSign || false;
	}

	this.setShow = function(pshow){
		this.show = pshow;
	}

	this.setDiff = function(diff){
		this.deffTimeValable = diff;
	}	

	this.next = function(){
		if (this.indexFn < this.listFn.length - 1)
			return this.indexFn++;
		else{
			this.indexFn = 0;
			return false;
		}
		return this.listFn[ this.indexFn ] ;
	}

	this.fnStreetSign = function(data, pstatus){
		/*
			status:
			    uncheck:             don't check
				hide: 				 hide
				street-sign-default: vert     1
				street-sign-notif:   orange   2
				street-sign-alert:   red      4

		*/
		var status = pstatus || {};
		if (data.dateStreetSign <= 0          ||
			_.isNull(data.dateStreetSign)     ||
			_.isUndefined(data.dateStreetSign) ||
			_.isNaN(data.dateStreetSign) ){
			status.state = 'uncheck';
			status.valueState = -1;
			return status;
		}

		var initialDateFilter = moment(this.streetSign.initialDate);
		var initialDateData = moment(data.dateActive);
		var diff = this.deffTimeValable;
		var finalValableDate = moment(data.dateActive).add(data.dateStreetSign, 'month');
		
		/*
			To know is the data is between range:
				dateActiveOfData < finalDate(Filter)   ok
				dateActiveOfData > initialDate(Filter) x	
		*/
		if (initialDateData.diff(this.streetSign.finalDate, 'month') > 0){
			/*console.log('********************');
			console.log('INITIALDATE ->' + initialDateData.format('D-M-Y'));
			console.log('FINALDATE -> ' + this.streetSign.finalDate.format('D-M-Y'));
			console.log('DIFF -> ' + initialDateData.diff(this.streetSign.finalDate, 'month'));
			console.log('********************');*/
			status.state = 'hide';
			status.valueState = 0;
			return status;
		}else if (moment(this.streetSign.initialDate).diff(finalValableDate, 'month') >= 0){
			status.icon = 'red';
			status.state = 'street-sign-alert';
			/*console.log('********************');
			console.log('? ->' + moment(this.streetSign.initialDate).format('D-M-Y'));
			console.log('finalValableDate -> ' + finalValableDate.format('D-M-Y'));
			console.log('DIFF -> ' + moment(this.streetSign.initialDate).diff(finalValableDate, 'month'));
			console.log('********************');*/
			status.valueState = 4;
		}
		else{
			if (finalValableDate.diff(initialDateFilter, 'month') <= diff){
				status.icon = 'orange';
				status.state = 'street-sign-notif';
				status.valueState = 2;
				console.log('********************');
				console.log(moment(this.streetSign.initialDate).diff(finalValableDate, 'month'));
				console.log('INITIALDATE ->' + initialDateData.format('D-M-Y'));
				console.log('FINALDATE -> ' + this.streetSign.finalDate.format('D-M-Y'));
				console.log('DIFF -> ' + initialDateData.diff(this.streetSign.finalDate, 'month'));
				console.log('********************');
			}
			else{
				status.icon = 'vert';
				status.state = 'street-sign-default';
				/*console.log('********************');
				console.log('INITIALDATE ->' + initialDateData.format('D-M-Y'));
				console.log('FINALDATE -> ' + this.streetSign.finalDate.format('D-M-Y'));
				console.log('DIFF -> ' + initialDateData.diff(this.streetSign.finalDate, 'month'));
				console.log('********************');*/
				status.valueState = 1;
			}
		}

		status.icon = this.iconRoot + status.icon;
		return status;
	}

	this.fnShow = function(data, pstatus){
		/**
			state: 
				unknown 
		*/

		var status = pstatus || status;

		if (status.state == 'uncheck') return status;
		
		if ( (this.show & status.valueState) && _.isString(status.state) ){
			return status;
		}
		else if (! (this.show & status.valueState) ){
			status.valueState = 0;
			status.state = 'hide';
			return status;
		}

		status.state = 'unknown';
		return status;
	}

};

Filter.prototype.check  = function(data){
	var status = this.fnStreetSign(data, null);
	return this.fnShow(data, status);
}

module.exports = Filter;