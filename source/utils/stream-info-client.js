var Q = require('q');

var Stream = {
	socketio: null
}

var isEmpty = function (data){
	data = data.data;
	if (typeof data === typeof [])
		return (data.length > 0) ? false : true;
	if (typeof data === typeof {})
		return (Object.keys(data).length > 0) ? false : true;
}

var getObjects = function(fn){
	var deferred = Q.defer()
	var jqueryPromise = $.get('markers/pos', function(data){
		deferred.resolve(data);
		/*console.log(data);*/
	});
	var fake_fn = function(err, data){
		return fn(data, err);
	}

	return deferred.promise.nodeify(fake_fn);
}

var getEvents = function (fn, typ, end, objetID = null){
	console.log(end);
	var deferred = Q.defer();
	var url = typ + '/' + 'pagination';
	var jqueryPromise = $.post(url, { end: end, objetID: objetID }, function(data){
		data = {
			typ: typ,
			data: data
		}
		if (!isEmpty(data)){
			deferred.resolve(data);
			console.log('getEvents' + data.data.length);
		}
		else{
			console.log('getEvents Empty');
		}
	}).fail(function(e) {
	    console.log(e);
	});
	var fake_fn = function(err, data){
		return fn(data, err);
	}
	return deferred.promise.nodeify(fake_fn);
}

var getHistory = function(fn, data){
	var deferred = Q.defer();
	var jqueryPromise = $.post('history/pagination', { end: data.end, objectId: data.objectId }, function(data){
		if (!isEmpty(data)){
			deferred.resolve(data);
		}
	}).fail(function(e) {
	    console.log('error getHistory');
	    console.log(e);
	});
	var fake_fn = function(err, data){
		return fn(data, err);
	}
	return deferred.promise.nodeify(fake_fn);
}

var configIO = function(){
	if (Stream.socketio)
		return Stream.socketio;
	var options = {
		url: 'ws://82.97.21.144:10047'
		/*
			dev:  10041
			calif: 10044
			dev(Nginx): 10047			
		 */
	};
	Stream.socketio = io.connect(options.url);
	return Stream.socketio;
}

var getEventsStream = function(fn){
	var socketio = configIO();
	//change tous les mots a l'anglais
	socketio.on('alert', function(data){
		var newdata = {
			typ: 'alert',
			data: data.message[0]
		}
		fn(newdata);
	});

	socketio.on('evalmsg', function(data){
		var newdata = {
			typ: 'evalmsg',
			data: data.message[0]
		}
		fn(newdata);
	});

	socketio.on('eval', function(data){
		var newdata = {
			typ: 'eval',
			data: data.message[0]
		}
		/*console.log(data);*/
		fn(newdata);
	});
}

var getPropagatedEventsStream = function(fn){
	var socketio = configIO();

	socketio.on('propagation_events', function(data){
		var newdata = {
			typ: data.message.typ,
			data: data.message.data
		}
		fn(newdata);
	});
}

var exitToNet = function(channel, data){
	var socketio = configIO();
	console.log('exitToNet');
	socketio.emit(channel, data);	
}

var eventChangedToServer = function(data){
	//console.log(data.infoData);
	
	var url = 'eventChanged/' + data.typ;
	var jqueryPromise = $.post(url, { 'event': data.infoData }, function(res){
		console.log(res);
	}).fail(function(e) {
	    console.log('error eventChangedToServer');
	    console.log(e);
	});
}

var autorisation = function(fn){
	var deferred = Q.defer();
	var url = window.serverRoot + '/autorisation/url'; 
	var jqueryPromise = $.get(url, function(data){
		deferred.resolve(data);
	}).fail(function(e) {
	   console.log('error autorisation');
	    console.log(e);
	});

	var fake_fn = function(err, data){
		return fn(data, err);
	}

	return deferred.promise.nodeify(fake_fn);
}

var getInfoToTab = function(fn, objectId, parentId){
	/*console.log(objectId, parentId);*/
	var deferred = Q.defer()
	var jqueryPromise = $.post('information/object/tab', { 'objectId': objectId, 'parentId': parentId}, function(data){
		deferred.resolve(data[0]);
	}).fail(function(e) {
	    console.log('error getInfoToTab');
	    console.log(e);
	});

	var fake_fn = function(err, data){
		return fn(data, err);
	}

	return deferred.promise.nodeify(fake_fn);
}

var getInfoClient = function(fn){
	var deferred = Q.defer();
	var url = window.serverRoot + '/client/info'; 
	var jqueryPromise = $.get(url, function(data){
		deferred.resolve(data);
	}).fail(function(e) {
	    console.log('Error - getInfoClient');
	});

	var fake_fn = function(err, data){
		return fn(data, err);
	}

	return deferred.promise.nodeify(fake_fn);
}

module.exports = {
	getObjects: getObjects,
	getEventsStream: getEventsStream,
	getEvents: getEvents,
	exitToNet: exitToNet,
	eventChangedToServer: eventChangedToServer,
	autorisation: autorisation,
	getInfoToTab: getInfoToTab,
	getInfoClient: getInfoClient,
	getHistory: getHistory,
	getPropagatedEventsStream: getPropagatedEventsStream
}