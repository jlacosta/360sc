var Q = require('q');
var assign = require('object-assign');
var EventEmitter = require('events').EventEmitter;

var MapUtils = assign({
	callbackNames: {
		streetSign: 'STREET-SIGN'
	}
}, EventEmitter.prototype, {
	addNewFilterListener: function(callbackName, callback){
		this.on(callbackName, callback);
	}
});
utils.MapUtils = MapUtils;

var fixMapPosition = function (){
	var navH = $('#360-navbar').height();
	var winH = $(window).height();
	$('#map').css('top', navH);
	$('#map').css('height', winH - navH - 1);
}

var fixMapAndInit = function(){
	fixMapPosition();
	clientMap.initMap();
}

var onNewObjectToMap = function(objects){
	clientMap.newMarkers(objects);
}

var onNewEventStream = function(evnt){
	clientMap.newEventStream(evnt);
}

var onNewEventsStream = function(evnts){
	if (!Array.isArray(evnts)){
		onNewEventStream(evnts);
		return;
	}
	var lenEvents = evnts.length;
	for(var e=0; e<lenEvents; ++e)
		onNewEventStream(evnts[e]);
}

var openInfoWindow = function(content, objectId, info){
	utils.openInfoWindow(content, objectId, info);
}

var closeGxMenu = function(){
	var deferred = jQuery.Deferred();

	var listElem = $('.simple.active-sub');
	for (var i = listElem.length - 1; i >= 0; i--) {
		$(listElem[i]).find('a.back').trigger('click');
	};

	var notiDeff = function(){ 
		if (!$('#gx-sidemenu-right').hasClass('opened')){
			deferred.resolve('closed');
		}
		else
			setTimeout(notiDeff, 10);
	}

	if ($('#gx-sidemenu-right').hasClass('opened'))
		$.when( $('.gx-menu.gx-trigger-right.entypo.list').trigger('click')).then(
			notiDeff()
		);
	else
		deferred.resolve('close');

	return deferred.promise();
}

var closeInfoWindow = function(typ){
	var className = '.' + typ;
	clientMap.closeInfoWindowBy(className);
}

var filterStreetSign = function(filter){
	utils.filter('StreetSign', filter);
}

module.exports = {
	fixMapAndInit: fixMapAndInit,
	onNewObjectToMap: onNewObjectToMap,
	onNewEventsStream: onNewEventsStream,
	openInfoWindow: openInfoWindow,
	closeGxMenu: closeGxMenu,
	closeInfoWindow: closeInfoWindow,
	filterStreetSign: filterStreetSign,
	utils: MapUtils
}