var Dispatcher = require('flux').Dispatcher;
var assign = require('object-assign');
//var PayloadSources = require('../constants/PayloadSources');

var AppDispatcher = assign(new Dispatcher(), {
	typehandle: null,
	queueHandle: { server: [], storeInner: [], view: [] },
	
	push: function(typehandle, payload){
		this.queueHandle[typehandle].push(payload);
		if (this.typehandle == null) this.QueueDispatch();
	},

	QueueDispatch: function (){
		
	},

	_auxDispatch: function(payload){
		//console.log('_auxDispatch');
		var auxDispatch	= function(){
			if (!this.isDispatching()){
				this.dispatch(payload);
				return true;
			}
			else{
				setTimeout(auxDispatch.bind(this), 100);
			}	
		}
		auxDispatch.call(this);
	}, 

	handleServerAction: function(action) {
		if (!action.type) {
       		throw new Error('Empty action.type: you likely mistyped the action.');
     	}

		var payload = {
			//source: PayloadSources.SERVER_ACTION,
			source: 'SERVER_ACTION',
			action: action
		};

		/*this.push('server', payload);
		this.QueueDispatch();*/

		this.dispatch(payload);
	},

	handleStoreInnerAction: function(action) {
		if (!action.type) {
			throw new Error('Empty action.type: you likely mistyped the action.');
		}
		var payload = {
			//source: PayloadSources.SERVER_ACTION,
			source: 'SERVER_INNER_STORE',
			action: action
		};		

		/*this.push('storeInner', payload);
		this.QueueDispatch();*/

		this._auxDispatch(payload);
	},

   	handleViewAction: function(action) {
		if (!action.type) {
			throw new Error('Empty action.type: you likely mistyped the action.');
		}

		var payload = {
			//source: PayloadSources.VIEW_ACTION,
			source: 'VIEW_ACTION',
			action: action
		};

		/*this.push('view', payload);
		this.QueueDispatch();*/

		this._auxDispatch(payload);
    }
});

module.exports = AppDispatcher;