<?php 
/** 
* Multi Add Page
* @author: ROUMEGUE Jérémy - 360sc
* We include the header for regular page.
*/
include "./include/head2.php";
?>
		<div class="row">
		  <div class="columns large-12 small-6">
			<div class="panel">
				<p>
					Veuillez entrer une URLe précise.<br/>
					<?php if(isset($_POST['urle'])) {
						
						$var = explode('/', $_POST['urle']);
						
						if($var[0] == "360sc.yt" OR $var[0] == "localhost"){
							$key = get_client_key();
						} else {
							$key = get_domain_key($var[0]);
						}
						/**
						* Read and decrypt URL
						*/
						$cVigenere = new classVigenere($key);
						$short = $cVigenere->decrypt($var[count($var)-1]);
						
						echo " shorter : ".$short;

						/**
						* Display form on screen
						*/
						echo "<form id='edit_tags' name='tagList' action='".get_link()."edit/' method='POST'>";
						echo "<div class='form-group'><label for='newurls'>Veuillez entrer la nouvelle URL</label>
						<input id='newurls' type='text' name='newURLs' placeholder='nouvelle url de sortie'/></div>
						<input class='tag' type='hidden' name='URLe[]' value='".$short."'/>
						<input class='button' type='submit' value='Valider'>";
						echo "</form>";
						
					} else {
						/**
						* Display form on screen
						*/
						echo "<form id='edit_tags' name='tagList' action='".get_link()."search/' method='POST'>";
						echo "<div class='form-group'><label for='urle'>Veuillez entrer l'URL du tag sans le 'http://'</label>
						<input id='urle' type='text' name='urle' placeholder='url d entrée'/></div>
						<ul><input class='button' type='submit' value='Valider'>";
						echo "</form>";
					}
					?>
				</p>
			</div>
		  </div>		  
		</div>
<?php
/**
* We include the footer for regular page.
*/
include "./include/footer2.php";
?>