<?php 
/** 
* Multi Add Page
* @author: ROUMEGUE Jérémy - 360sc
* We include the header for regular page.
*/
include "./include/head2.php";
?>
	<div class="panel">
		<div class"large-12 small-8 columns">
		<?php
			if ($_SESSION['page'] != ""){
				$options="";
				$tags=array();
				$societes = getWrapSocietes($_SESSION['societe']);
				$societes[] = $_SESSION['societe']; 
				//algorithme pas optimise, il vaut mieux construire le sql statement dans le loop afin d'eviter une deuxieme sort
				foreach ($societes as $societe) {
					$bdd = connection_db();
					$sql = 'SELECT t.TagID, t.TagName, y.OriginalURL, t.YourlsID FROM 360sc_cms.tags as t INNER JOIN 360sc_iz.yourls as y INNER JOIN 360sc_cms.societe_tags AS st
					WHERE t.YourlsID = y.YourlsID AND t.YourlsID = st.yourlsID AND st.customersID = :societeId
					ORDER BY t.TagID ASC';
					$target = $bdd->prepare($sql);
					$target->bindValue(":societeId",$societe);
					$target->execute();
					while ( $row = $target->fetch()) { 
						$tags[$row['TagID']]=$row;
						
					}
				}
				ksort($tags);
				foreach ($tags as $tag) {
					$options.="<option value='". $tag['TagID'] ."'>". $tag['TagID']. ' | ' .$tag['TagName']. ' | '. $tag['OriginalURL']. "</option>";
				}

				
		?>		
		
				<form id="formChangeUrl" action="<?php echo get_link(); ?>outils/changerUrlTags/" method="post" enctype="multipart/form-data" style="position:relative">
					<div class="form-group">
						<label for="info">List de TagID | Nom | OriginalURl</label>
		<?php	
						echo "<select name='info' id='info'>";
							/*while ($row = $target->fetch()) {
								echo "										
									<option value='". $row['TagID'] ."'>". $row['TagID']. ' | ' .$row['TagName']. ' | '. $row['OriginalURL']. "</option>
								";	
							}*/ echo $options;
						echo "</select>";		
		?>			</div>
					<div class="form-group">
						<label for="OriginalURL">Nouvelle OriginalURL</label>
						<input id="originalURL" type="url" name="originalURL" value="http://" />	
					</div>
					
					<script>
						var d = function () {
							document.getElementById('originalURL').value = "http://www.360sc.yt/webApp/FingWa_MCInfo";
						}
						var envoyer = function(){
							var text = $('#originalURL').val();
							var re = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
							var isUrl = re.test(text);
							if (isUrl){
								
								$('#imageSuccess').css({'display': 'block'});
								$(document).foundation('success', 'reflow');					
								setTimeout(function(){
									$('#formChangeUrl').submit();
								}, 1500);
							}
							else{
								$('#default').before('<div id="alerte" data-alert class="alert-box alert radius">\
									Erreur, veuillez recommencer\
									<a href="#" class="close">&times;</a>\
								</div>');
								$(document).foundation('alert', 'reflow');
							}	
							return false;
						}
						
					</script>
					<div id='imageSuccess' data-alert class="alert-box success radius" style='display: none;'>	
					  Changement correctement effectué! Redirection
						  <img src="../../img/loader.GIF" width="25px" style="margin-left: 10px;">
						  <a href="#" class="close">&times;</a>
					</div>
					<input id="default"class="button" type="button" value="Default Url" onclick="d()" />					
					<div style = "position:absolute;right: 0;bottom:0">
						<a href = "<?php echo get_link();?>" class= "button" style = "text-decoration:none">Annuler</a>
						<input class="button" type="submit" value="Envoyer" onclick="return envoyer()" />
					</div> 
				</form>

		<?php
			}
			else if (isset($_POST['info'])){
				$bdd = connection_db();
				$sql = 'UPDATE 360sc_iz.yourls SET OriginalURL=:originalURL WHERE yourlsID=:yourlsID';
				$target = $bdd->prepare($sql);
				$target->bindValue(':originalURL', $_POST['originalURL'],  PDO::PARAM_STR);
				$porcions = explode(' | ', $_POST['info']);
				$sql = 'SELECT yourlsID FROM tags WHERE TagID = :tagID';
				$targetSelect = $bdd->prepare($sql);
				$targetSelect->bindValue(':tagID', $porcions[0], PDO::PARAM_INT);
				$targetSelect->execute();
				$rowSelect = $targetSelect->fetch();

				$target->bindValue(':yourlsID', $rowSelect['yourlsID']);
				$target->execute();


				$url = get_link()."outils/changerUrlTags/";
		?>		
				<span>Modification faite, la redirection se fera dans 3 secondes</span>
	 			<script language="javascript" type="text/javascript">
	 						setTimeout(function(){window.location.href="<?php echo $url; ?>";},3000);
	 						
				</script>
 
		<?php		
			}
		?>
		</div>	
	</div>
<?php
/**
* We include the footer for regular page.
*/
include "./include/footer2.php";
?>