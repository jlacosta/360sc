<?php 
/** 
* Import Page
* @author: ROUMEGUE Jérémy - 360sc
* We include the header for regular page.
*/
include "./include/head2.php";
?>
		<div class="row">
		  <div class="columns large-12 small-6">
			<div class="panel">
				<p>
					Bienvenue dans l'interface d'administration de IZ.<br/>
					<strong>Veuillez séléctionner un fichier à importer.</strong>
					<!-- Form for select the files to import -->
					<form action="<?php echo get_link(); ?>import/" method="post" enctype="multipart/form-data">
						<input type="hidden" name="MAX_FILE_SIZE" value="30000" />
						<input type="file" name="userfile" />
						<input class="medium button" type="submit" value="Envoyer" />
					</form>
				</p>
			</div>
		  </div>		  
		</div>
<?php
/**
* We include the footer for regular page.
*/
include "./include/footer2.php";
?>