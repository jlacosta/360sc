<?php 
/** 
* Multi Add Page
* @author: ROUMEGUE Jérémy - 360sc
* We include the header for regular page.
*/
include "./include/head2.php";
?>
		<div class="row">
		  <div class="columns large-12 small-6">
			<div class="panel">
				<p>
					Bienvenue dans l'interface d'administration de IZ.<br/>
					<!-- Form for encrypt an URLs in many tags -->
					<form action="<?php echo get_link(); ?>encrypt/multi/" method="post" enctype="multipart/form-data" >
						<div class="form-group">
							<label for="url">URL cible</label>
							<input id="url" type="text" name="url" value="http://"/>
						</div>
						<div class="form-group">
							<label for="desc">Nom du tag</label>
							<input id="desc" type="text" name="desc" />
						</div>
						<div class="form-group">
							<label for="nb_tags">Nombre de tags souhaités</label>
							<input id="nb_tags" type="text" name="nb_tags" value="1"/>
						</div>
						<input class="medium button" type="button" onclick="this.form.submit()" value="Envoyer" />
					</form>
				</p>
			</div>
		  </div>		  
		</div>
<?php
/**
* We include the footer for regular page.
*/
include "./include/footer2.php";
?>
