<?php 
/** 
* Multi Add Page
* @author: ROUMEGUE Jérémy - 360sc
* We include the header for regular page.
*/
include "./include/head2.php";
?>
		<div class="row">
		  <div class="columns large-12 small-6">
			<div class="panel">
				<p>
					Bienvenue dans l'interface d'administration de IZ.<br/>
					<!-- Form for encrypt an URLs in many tags -->
					<form action="<?php echo get_link(); ?>encrypt/" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label for="url">URL de redirection</label>
							<input id="url" type="text" name="url" />
						</div>
						<div class="form-group">
							<label for="desc">Nom du tag</label>
							<input id="desc" type="text" name="desc" />
						</div>
						<input class="medium button" type="submit" value="Envoyer" />
					</form>
				</p>
			</div>
		  </div>		  
		</div>
<?php
/**
* We include the footer for regular page.
*/
include "./include/footer2.php";
?>