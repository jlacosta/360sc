<?php 
/** 
* Multi Add Page
* @author: ROUMEGUE Jérémy - 360sc
* We include the header for regular page.
*/
include "./include/head2.php";
?>
		<div class="row">
		  <div class="columns large-12 small-6">
			<div class="panel">
				<p>
					Voici la liste de vos tags.<br/>
					<?php
						/**
						* We open the database for the SQL request.
						*/
						$bdd = connection_db();
						
						$query=$bdd->prepare('SELECT COUNT(*) AS nbr FROM customers_tags WHERE customersID =:id');
						$query->bindValue(':id',$_SESSION['client_id'], PDO::PARAM_INT);
						$query->execute();
						$nb_tags=($query->fetchColumn()==0)?true:false;
						$query->CloseCursor();
						
						if($nb_tags){
							?>
							<p>
							Vous n'avez aucun tag. Cliquez <a href="/contact/">ici</a> pour en commander.
							</p>
							<?php
						} else {
							/**
							* The SQL request
							*/
							$sql = 'SELECT t.TagID, t.TagName, t.TagLatitude, t.TagLongitude, t.description, t.isActive, y.ShortURL, y.OriginalURL FROM 360sc_iz.yourls AS y, tags AS t, customers_tags AS ct WHERE t.YourlsID=y.YourlsID
							AND ct.YourlsID = y.YourlsID AND ct.customersID = '.$_SESSION['client_id'].';';
							$target = $bdd->query($sql);
							
							/**
							* Display form on screen
							*/
							echo "<table>";
							echo "<tr>
							<th>ID</th>
							<th>Nom</th>
							<th>Description</th>
							<th>URL</th>
							<th>Latitude</th>
							<th>Longitude</th>
							<th>Actif</th>
							</tr>";
							
							/** 
							* Show ALL the tag
							*/
							while ($row = $target->fetch()) {
								echo '<tr id="'.$row['TagID'].'" class="tag-id">
								<td>'.$row['TagID'].'</td>
								<td>'.$row['TagName'].'</td>
								<td>'.$row['description'].'</td>
								<td>'.$row['OriginalURL'].'</td>
								<td>'.$row['TagLatitude'].'</td>
								<td>'.$row['TagLongitude'].'</td>
								<td>'.$row['isActive'].'</td>
								</tr>';
							}
							
							echo "</table>";
							
							/**
							* Closing database
							*/
							$target->closeCursor();
						}
					?>
				</p>
			</div>
		  </div>		  
		</div>
<?php
/**
* We include the footer for regular page.
*/
include "./include/footer2.php";
?>