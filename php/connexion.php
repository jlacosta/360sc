<?php
/**
* Init some basic value.
*/
$lvl=(isset($_SESSION['level']))?(int) $_SESSION['level']:5;
$lvl=(isset($_SESSION['societe']))?(int) $_SESSION['societe']:0;
$id=(isset($_SESSION['id']))?(int) $_SESSION['id']:-1;
$pseudo=(isset($_SESSION['login']))?$_SESSION['login']:'';
$mail=(isset($_SESSION['mail']))?$_SESSION['mail']:'';

include "./include/head2.php";

/**
* Define function.
*/
function erreur($err='') {
   $mess=($err!='')? $err:'Une erreur inconnue s\'est produite';
   exit('<p>'.$mess.'</p>
   <p>Cliquez <a href="./index.php">ici</a> pour revenir à la page d\'accueil</p></div></body></html>');
}

/**
* Database connexion
*/
$bdd = connection_db();
define('ERR_IS_CO','Vous ne pouvez pas accéder à cette page si vous n\'êtes pas connecté');

/**
* Check that input are not empty.
*/ 
if (!isset($_POST['login'])) {
    echo '<form method="post" action="'.get_link().'connexion/">
		<fieldset>
			<h3>Connexion</h3>
			<div class="group-label">
				<label for="login">Nom de compte :</label>
				<input name="login" type="text" id="login" />
			</div>
			<div class="group-label">
				<label for="password">Mot de Passe :</label>
				<input type="password" name="password" id="password" />
			</div>
		</fieldset>
		<p>
			<input class="button" type="submit" value="Connexion" />
		</p>
	</form>';
} else {
    $message='';
    if (empty($_POST['login']) || empty($_POST['password']) ) //Oublie d'un champ
    {
        $message = '<p>une erreur s\'est produite pendant votre identification.
		Vous devez remplir tous les champs</p>
		<p>Cliquez <a href="./connexion/">ici</a> pour revenir</p>';
    } else //On check le mot de passe
    {
		/**
		* Checking in base
		*/
        $query=$bdd->prepare('SELECT s.email, s.societe, s.password, s.CustomersID, s.level, s.login, d.adminMA, d.clientMA, d.type, 
		d.gestionTags, d.searchTags, d.addTag, d.editTags, d.addObject, d.editObject, d.createUser, d.export, d.societePF, d.SURLE
        FROM customers AS s, droit_client AS d WHERE s.login = :login AND d.idClient = s.CustomersID');
        $query->bindValue(':login',$_POST['login'], PDO::PARAM_STR);
        $query->execute();
        $data=$query->fetch();
		
		/**
		* Good catching, go to init all the value for the next. 
		*/
		if ($data['password'] == md5($_POST['password'])) // Acces OK !
		{
			$_SESSION['login'] = $data['login'];
			$_SESSION['societe'] = $data['societe'];
			$_SESSION['level'] = (isset($data['level']))?$data['level']:'5';
			$_SESSION['client_id'] = $data['CustomersID'];
			$_SESSION['mail'] = $data['email'];
			
			$_SESSION['adminMA'] = $data['adminMA'];
			$_SESSION['clientMA'] = $data['clientMA'];
			$_SESSION['gestionTags'] = $data['gestionTags'];
			$_SESSION['searchTags'] = $data['searchTags'];
			$_SESSION['addTag'] = $data['addTag'];
			$_SESSION['editTags'] = $data['editTags'];
			$_SESSION['addObject'] = $data['addObject'];
			$_SESSION['editObject'] = $data['editObject'];
			$_SESSION['createUser'] = $data['createUser'];
			$_SESSION['export'] = $data['export'];
			$_SESSION['societePF'] = $data['societePF'];
			$_SESSION['SURLE'] = $data['SURLE'];
			$_SESSION['type'] = $data['type'];
			
			$message = '<p>Votre connexion est un succes.<br /><br /><br /><br />
			Si vous n\'&ecirc;tes pas redirigé d\'ici 5 secondes, cliquez <a href="'.get_link().'">ici</a> 
			pour revenir à la page d accueil</p>';
		} else {// Else, show error!
			$message = '<p>Une erreur s\'est produite 
			pendant votre identification.<br /> Le mot de passe ou le pseudo 
				entré n\'est pas correcte.</p><p><br/><br/>
				Si vous n\'&ecirc;tes pas redirigé d\'ici 5 secondes, cliquez <a href="'.get_link().'">ici</a> 
			pour revenir à la page précédente
			<br />Ou cliquez <a href="../">ici</a> 
			pour revenir à la page d accueil</p>';
		}
		$query->CloseCursor();
    }
    
    echo $message;
    sleep(3);
    header('Location: '.get_link().'');

}

include "./include/footer2.php";
?>