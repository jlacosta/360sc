<?php

switch ($_SERVER['REQUEST_METHOD']) {
  case 'GET':

    require_once "php/Smarty/libs/Smarty.class.php";

    $smarty = new Smarty();
    $smarty->template_dir = "templates";
    $smarty->compile_dir = "templates_c";
    $smarty->caching = false;
    $smarty->left_delimiter = "<{";
    $smarty->right_delimiter = "}>";

    //récupérer les types d'objets
    $godRight = $_SESSION['level']==4?true:false;
    $owner = $_SESSION['societe'];
    $function = 'getObjectTypes';
    require_once "php/DAF/getObjectTypes.php";

    $objectTypes=array();
    foreach ($types as $type) {
      $objectTypes[$type['idType']] = $type['nameType'];
    }

    //récupérer les sociétés disponibles
    $societiesIds = getWrapSocietes($owner);
    $societiesIds[] = $owner;
    $function = "getSocietiesByIds";
    require_once "php/DAF/getSocieties.php";

    $dataSocieties = array();
    foreach ($societies as $societyId => $society) {
      $dataSocieties[$societyId] = $society['name'];
    }

    //nous n'avons que Tags comme MC pour l'instant
    $function = 'getOwnFreeTags';
    require_once "php/DAF/getTags.php";

    $freeMCs =array();
    foreach ($tags as $tagId => $tag) {
      $freeMCs[$tagId] = $tag['TagName'];
    }

    $smarty->assign("postPage",SERVERROOT.'/object');
    $smarty->assign("objectTypes",$objectTypes);
    $smarty->assign("societies",$dataSocieties);
    $smarty->assign("accountSociety",$owner);
    $smarty->assign("freeMCs",$freeMCs);
    $smarty->assign("serverRoot",SERVERROOT);
    $smarty->display("new-object.tpl");

  break;
  
  case 'POST':
    $function = 'newObject'; 
    require_once "php/DAF/setObjects.php";

    header("Location: ".SERVERROOT."/object?operation=$operation");
  break;

  default:
  
  break;
}






?>
