﻿<?php
include "./include/head2.php";

function erreur($err='') {
   $mess=($err!='')? $err:'Une erreur inconnue s\'est produite';
   exit('<p>'.$mess.'</p>
   <p>Cliquez <a href="./index.php">ici</a> pour revenir à la page d\'accueil</p>');
}
$bdd = connection_db();
define('ERR_IS_CO','Vous ne pouvez pas accéder à cette page si vous n\'êtes pas connecté');

if (empty($_POST['societeName'])) // Si on la variable est vide, on peut considérer qu'on est sur la page de formulaire
{

  ?>
<script type="text/javascript">

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) { 
                $("img.currentImg").attr('src', e.target.result);
                $("img.currentImg").css('display', 'inline-block'); 
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

  $(document).ready(function(){        

    $('input[type="file"]').change(function(){
      $("img").removeClass("currentImg");
      $(this).prev("img").addClass("currentImg");
      readURL(this);
    }); 

  });

</script>

  <?php 
    echo '<form method="post" action="'.get_link().'societe/" enctype="multipart/form-data">
		<fieldset>
			<h3>Entreprise</h3>
			<div class="group-label">
				<label for="societeName">* Nom de l\'entreprise :</label>
				<input name="societeName" type="text" id="societeName" />
			</div>
			<div class="group-label">
				<label for="alpha">* Alpha ID : <acronym title="Chaine de 15 caract&egrave;res"><span class="glyphicon glyphicon-question-sign"></span></acronym></label>
				<input name="alpha" type="text" id="alpha" />
			</div>
			<div class="group-label">
				<label for="url">* URL par defaut : <acronym title="URL de redirection par defaut dans les tags"><span class="glyphicon glyphicon-question-sign"></span></acronym></label>
				<input name="url" type="text" id="url" value="http://360sc.yt" />
			</div>
			<div class="group-label">
				<label for="fixe">* T&eacute;l&eacute;phone fixe :</label>
				<input name="fixe" type="text" id="fixe" />
			</div>';
			if($_SESSION['societePF']) {
				echo'<div class="group-label">
					<label>* Entreprise mère :</label>
					<select name="parent">';
						/**
						* Displaying society
						*/
				        $sql='SELECT ID, name FROM societe';
						$target = $bdd->query($sql);
						while ($row = $target->fetch()) {
							echo "<option value='".$row['ID']."'>".$row['name']."</option>";
						}
					echo'</select>
				</div>';
			}
			echo '
      <div class="group-label">
          <label for="newSocietyImage">Image de la société:</label>   
          <img id="imgSocietyPreview" src="#" alt="new image"/ style="display:none"/>
          <input type="file" name="newSocietyImage" id="newSocietyImage" />
      </div>
		</fieldset>
		<fieldset>
			<h3>Nom du compte administrateur</h3>
			<div class="group-label">
				<label for="login">* Nom du compte :</label>
				<input name="login" type="text" id="login" />
			</div>
			<div class="group-label">
				<label for="name">* Nom de l\'utilisateur :</label>
				<input name="name" type="text" id="name" />
			</div>
			<div class="group-label">
				<label for="password">* Mot de passe :</label>
				<input type="password" name="password" id="password" />
			</div>
			<div class="group-label">
				<label for="confirm">* Mot de passe :</label>
				<input type="password" name="confirm" id="confirm" />
			</div>';
			echo '
		</fieldset>
		<fieldset>
			<h3>Contact</h3>
			<div class="group-label">
				<label>* Droit du compte :</label>
				<ul class="multi-check">';
					echo ($_SESSION['adminMA'])?'<li class="checkbox-inline">
						<input type="checkbox" name="AAM" value="AAM" /> Administration <acronym title="Application Mobile">MA</acronym>
					</li>':'';
					echo ($_SESSION['clientMA'])?'<li class="checkbox-inline">
						<input type="checkbox" name="CAM" value="CAM" /> Client <acronym title="Application Mobile">MA</acronym>
					</li>':'';
					echo ($_SESSION['gestionTags'])?'<li class="checkbox-inline">
						<input type="checkbox" name="GMC" value="GMC" /> Gestion des <acronym title="Modules Communiquants">MC</acronym>
					</li>':'';
					echo ($_SESSION['searchTags'])?'<li class="checkbox-inline">
						<input type="checkbox" name="RMC" value="RMC" /> Recherche des <acronym title="Modules Communiquants">MC</acronym>
					</li>':'';
					echo ($_SESSION['addTag'])?'<li class="checkbox-inline">
						<input type="checkbox" name="AMC" value="AMC" /> Ajout de <acronym title="Module Communiquant">MC</acronym>
					</li>':'';
					echo ($_SESSION['societePF'])?'<li class="checkbox-inline">
						<input type="checkbox" name="PF" value="PF" /> Ajout de soci&eacute;t&eacute; 
					</li>':'';
				echo'</ul>
				<ul class="multi-check">';
					echo ($_SESSION['editTags'])?'<li class="checkbox-inline">
						<input type="checkbox" name="EMC" value="EMC" /> Edition des <acronym title="Modules Communiquants">MC</acronym>
					</li>':'';
					echo ($_SESSION['addObject'])?'<li class="checkbox-inline">
						<input type="checkbox" name="AO" value="AO" /> Ajout d&apos;objet
					</li>':'';
					echo ($_SESSION['editObject'])?'<li class="checkbox-inline">
						<input type="checkbox" name="EO" value="EO" /> Edition d&apos;objet
					</li>':'';
					echo ($_SESSION['createUser'])?'<li class="checkbox-inline">
						<input type="checkbox" name="CU" value="CU" /> Création d&apos;utilisateur
					</li>':'';
					echo ($_SESSION['export'])?'<li class="checkbox-inline">
						<input type="checkbox" name="E" value="E" /> Export
					</li>':'';
					echo ($_SESSION['SURLE'])?'<li class="checkbox-inline">
						<input type="checkbox" name="SURLE" value="SURLE" /> Voir les URLe
					</li>':'';
					echo ($_SESSION['type'])?'<li class="checkbox-inline">
						<input type="checkbox" name="T" value="T" /> Ajout Type d\'Objet
 					</li>':'';
				echo'</ul>
			</div>
			<div class="group-label">
				<label for="mobile">Téléphone Mobile :</label>
				<input type="text" name="mobile" id="mobile" />
			</div>
			<div class="group-label">
				<label for="email">* Email :</label>
				<input type="text" name="email" id="email" />
			</div>
      <div class="group-label">
          <label for="newAccountImage">Image du compte:</label>   
          <img id="imgAccountPreview" src="#" alt="new image"/ style="display:none"/>
          <input type="file" name="newAccountImage" id="newAccountImage" />
      </div>
		</fieldset>
		<p>Les champs précédés d\'un * sont obligatoires</p>
		<p>
			<input class="button" type="submit" value="Inscrire" />
		</p>
	</form>';
} else {
	/**
	* We open the URW algorithm.
	*/
	require_once "./algorithm/urw.php";
		
    $pseudo_erreur1 = NULL;
    $pseudo_erreur2 = NULL;
    $mail_erreur1 = NULL;
    $mail_erreur2 = NULL;
    $mdp_erreur = NULL;
    $fixe_erreur1 = NULL;
    $mobile_erreur1 = NULL;
    $mobile_erreur2 = NULL;
	
	$i = 0;
    $temps = time(); 
    $societeName=$_POST['societeName'];
    $alpha=$_POST['alpha'];
    $parent=isset($_POST['parent'])?$_POST['parent']:$_SESSION['societe'];
    $pseudo=$_POST['login'];
    $name=$_POST['name'];
    if(isset($_POST['mobile']) AND $_POST['mobile'] <> NULL){$mobile=$_POST['mobile'];} else {$mobile = NULL;}
    $fixe=$_POST['fixe'];
    $email=$_POST['email'];
    $pass = md5($_POST['password']);
    $confirm = md5($_POST['confirm']);
    $url=$_POST['url'];

    if(isset($_FILES["newSocietyImage"]) && $_FILES["newSocietyImage"]!=null && $_FILES["newSocietyImage"]["name"]!="" ){
      $target_dir = "public/img_profil/";
      $target_file = $target_dir . basename($_FILES["newSocietyImage"]["name"]);
      $file = $_FILES['newSocietyImage']['name'];
      move_uploaded_file($_FILES['newSocietyImage']['tmp_name'],realpath(dirname(dirname(__FILE__))).'/'.$target_file);   
      $societyImage=$target_file;
    }else{
      $societyImage=NULL;
    }
    if(isset($_FILES["newAccountImage"]) && $_FILES["newAccountImage"]!=null && $_FILES["newAccountImage"]["name"]!="" ){
      $target_dir = "public/img_profil/";
      $target_file = $target_dir . basename($_FILES["newAccountImage"]["name"]);
      $file = $_FILES['newAccountImage']['name'];
      move_uploaded_file($_FILES['newAccountImage']['tmp_name'],realpath(dirname(dirname(__FILE__))).'/'.$target_file);   
      $accountImage=$target_file;
    }else{
      $accountImage=NULL;
    }

    /**
	* Verification du nom de compte
	*/
    $query=$bdd->prepare('SELECT COUNT(*) AS nbr FROM customers WHERE login =:login');
    $query->bindValue(':login',$pseudo, PDO::PARAM_STR);
    $query->execute();
    $pseudo_free=($query->fetchColumn()==0)?1:0;
    $query->CloseCursor();
	
	/**
	* Verification du mail
	*/
	$query=$bdd->prepare('SELECT COUNT(*) AS nbr FROM customers WHERE email =:email');
    $query->bindValue(':email',$email, PDO::PARAM_STR);
    $query->execute();
    $mail_free=($query->fetchColumn()==0)?1:0;
    $query->CloseCursor();

    if(!$pseudo_free) {
        $pseudo_erreur1 = "Votre nom de compte est déjà utilisé par un membre";
        $i++;
    }
	
/*	if(!$mail_free) {
        $mail_erreur1 = "Le mail choisit est déjà utilisé, veuillez en choisir une autre";
		$i++;
    }*/
	
	if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
		$mail_erreur2 = "Le mail n'est pas au bon format. Veuillez recommencer";
		$i++;
	}
	/*
	if(isset($_POST['mobile']) AND $_POST['mobile'] <> NULL){
		if(!$mobile_free) { 
	        $mobile_erreur1 = "Le numéro de portable choisit est déjà utilisé, veuillez en choisir une autre";
			$i++;
	    }
		
		if(strlen($mobile) != 10) {
	        $mobile_erreur2 = "Le numéro de portable choisit n'a pas la bonne longueur, un numéro de téléphone est du type : '0123456789'";
			$i++;
	    }
	}
	
	if(strlen($fixe)!= 10) {
        $fixe_erreur1 = "Le numéro de fixe choisit n'a pas la bonne longueur, un numéro de téléphone est du type : '0123456789'";
		$i++;
    }*/

    if (strlen($pseudo) < 3 || strlen($pseudo) > 15) {
        $pseudo_erreur2 = "Votre nom de compte est soit trop grand, soit trop petit";
        $i++;
    }

    //Vérification du mdp
    if ($pass != $confirm || empty($confirm) || empty($pass)){
        $mdp_erreur = "Votre mot de passe et votre confirmation sont différents, ou sont vides";
        $i++;
    }	
	
	if ($i==0) {
		$bdd = connection_db(); 

		$query=$bdd->prepare('INSERT INTO societe (ID, name, alphaID, urlDefault, fixe, image)
        VALUES (NULL, :societeName, :alpha, :url, :fixe, :societyImage)');
		$query->bindValue(':societeName', $societeName, PDO::PARAM_STR);
		$query->bindValue(':alpha', $alpha, PDO::PARAM_INT);
		$query->bindValue(':url', $url, PDO::PARAM_STR);
		$query->bindValue(':fixe', $fixe, PDO::PARAM_STR);
    $query->bindValue(':societyImage', $societyImage, PDO::PARAM_STR);
        $query->execute();



        $sid=$bdd->lastInsertId();
        $query=$bdd->prepare('INSERT INTO 360sc_iz.customers (customersID, customersKey, customersDomain) VALUES (:id, :key, :domain)');
        $query->bindValue(':id',$sid, PDO::PARAM_INT);
        $query->bindValue(':key', 'fudjsoleudosndif', PDO::PARAM_STR);
        $query->bindValue(':domain', $url, PDO::PARAM_STR);
        $query->execute();

		
        $query=$bdd->prepare('INSERT INTO customers (CustomersID, `level`, Name, `password`, login, societe, email, mobile, image)
        VALUES (NULL, :niveau, :name, :pass, :login, :societe, :email, :mobile, :accountImage)');
		$query->bindValue(':niveau', 0, PDO::PARAM_INT);
		$query->bindValue(':name', $name, PDO::PARAM_STR);
		$query->bindValue(':pass', $pass, PDO::PARAM_STR);
		$query->bindValue(':login', $pseudo, PDO::PARAM_STR);
		$query->bindValue(':societe', $sid, PDO::PARAM_STR);
		$query->bindValue(':email', $email, PDO::PARAM_STR);
		$query->bindValue(':mobile', $mobile, PDO::PARAM_STR);
    $query->bindValue(':accountImage', $accountImage, PDO::PARAM_STR);
        $query->execute();


        
		/**
		* We check the new id
		*/
        $sql='SELECT CustomersID FROM customers WHERE login=\''.$pseudo.'\'';
		$target = $bdd->query($sql);
		while ($row = $target->fetch()) {
			$id = $row['CustomersID'];
		}
		


		/**
		* We check the new societeID
		*/
        $sql='SELECT ID FROM societe WHERE name=\''.$societeName.'\'';
		$target = $bdd->query($sql);
		while ($row = $target->fetch()) {
			$societeid = $row['ID'];
		}

		$query=$bdd->prepare('INSERT INTO societe_soussociete (societe, soussociete)
        VALUES (:idParent, :idFille)');
		$query->bindValue(':idParent', $parent, PDO::PARAM_INT);
		$query->bindValue(':idFille', $societeid, PDO::PARAM_INT);
        $query->execute();

        $query=$bdd->prepare('INSERT INTO droit_client (idClient, adminMA)
        VALUES (:idClient, 0)');
		$query->bindValue(':idClient', $id, PDO::PARAM_INT);
        $query->execute();
		
		if(isset($_POST['AAM'])){
			$sql = 'UPDATE droit_client SET adminMA=1 WHERE idClient=\''.$id.'\'';
			$target = $bdd->exec($sql);
		}
		if(isset($_POST['CAM'])){
			$sql = 'UPDATE droit_client SET clientMA=1 WHERE idClient=\''.$id.'\'';
			$target = $bdd->exec($sql);
		}
		if(isset($_POST['GMC'])){
			$sql = 'UPDATE droit_client SET gestionTags=1 WHERE idClient=\''.$id.'\'';
			$target = $bdd->exec($sql);
		}
		if(isset($_POST['RMC'])){
			$sql = 'UPDATE droit_client SET searchTags=1 WHERE idClient=\''.$id.'\'';
			$target = $bdd->exec($sql);
		}
		if(isset($_POST['AMC'])){
			$sql = 'UPDATE droit_client SET addTag=1 WHERE idClient=\''.$id.'\'';
			$target = $bdd->exec($sql);
		}
		if(isset($_POST['EMC'])){
			$sql = 'UPDATE droit_client SET editTags=1 WHERE idClient=\''.$id.'\'';
			$target = $bdd->exec($sql);
		}
		if(isset($_POST['AO'])){
			$sql = 'UPDATE droit_client SET addObject=1 WHERE idClient=\''.$id.'\'';
			$target = $bdd->exec($sql);
		}
		if(isset($_POST['EO'])){
			$sql = 'UPDATE droit_client SET editObject=1 WHERE idClient=\''.$id.'\'';
			$target = $bdd->exec($sql);
		}
		if(isset($_POST['CU'])){
			$sql = 'UPDATE droit_client SET createUser=1 WHERE idClient=\''.$id.'\'';
			$target = $bdd->exec($sql);
		}
		if(isset($_POST['E'])){
			$sql = 'UPDATE droit_client SET export=1 WHERE idClient=\''.$id.'\'';
			$target = $bdd->exec($sql);
		}
		if(isset($_POST['PF'])){
			$sql = 'UPDATE droit_client SET societePF=1 WHERE idClient=\''.$id.'\'';
			$target = $bdd->exec($sql);
		}
		if(isset($_POST['SURLE'])){
			$sql = 'UPDATE droit_client SET SURLE=1 WHERE idClient=\''.$id.'\'';
			$target = $bdd->exec($sql);
		}
		if(isset($_POST['T'])){
			$sql = 'UPDATE droit_client SET type=1 WHERE idClient=\''.$current_id.'\'';
			$target = $bdd->exec($sql);
		}
		
        $query->CloseCursor();

        //createAClient($pseudo,$id);

		echo '<p>Votre enregistrement est un succes.<br />Cliquez <a href="'.get_link().'">ici</a> 
			pour revenir à la page d accueil</p>';
    } else {
        echo'<h1>Inscription interrompue</h1>';
        echo'<p>Une ou plusieurs erreurs se sont produites pendant l incription</p>';
        echo'<p>'.$i.' erreur(s)</p>';
        echo'<p>'.$pseudo_erreur1.'</p>';
        echo'<p>'.$pseudo_erreur2.'</p>';
        echo'<p>'.$mdp_erreur.'</p>';
        echo'<p>'.$mail_erreur1.'</p>';
        echo'<p>'.$mail_erreur2.'</p>';
        echo'<p>'.$mobile_erreur1.'</p>';
        echo'<p>'.$mobile_erreur2.'</p>';
        echo'<p>'.$fixe_erreur1.'</p>';

        echo'<p>Cliquez <a href="./">ici</a> pour recommencer</p>';
    }
}

include "./include/footer2.php";
?>