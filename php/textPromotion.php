<?php 
/** 
* Multi Add Page
* @author: ROUMEGUE Jérémy - 360sc
* We include the header for regular page.
*/
include "./include/head2.php";
?>
	<div class="row">
		<?php
			if (!$_SESSION['page'] == ""){
		?>

				<div class"large-12 small-12 columns">
					<form action="<?php echo get_link(); ?>outils/textPromotion/" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label for="promotion">Text de promotion</label>
							<input id="promotion" type="text" name="promotion" />	
						</div>
						<input class="button" type="submit" value="Envoyer" />
					</form>
				</div>
		<?php
			}
			else {
				if (isset($_POST['promotion']) && $_POST['promotion'] != ""){
					$bdd = connection_db();
					$sql = 'INSERT INTO outils(page, info) VALUES(:page, :info)';
					$target = $bdd->prepare($sql);
					$target->bindValue(':page', 'connexion');
					$target->bindValue(':info', $_POST['promotion']);
					$target->execute();
				}
				$url = get_link()."outils/changerUrlTags/";
		?>
				<script language="javascript" type="text/javascript">
	 				window.location.href="<?php echo $url; ?>";								   
				</script>
		<?php		
			}
		?>		
	</div>
<?php
/**
* We include the footer for regular page.
*/
include "./include/footer2.php";
?>