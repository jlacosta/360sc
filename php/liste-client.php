<?php 
/** 
* Multi Add Page
* @author: ROUMEGUE Jérémy - 360sc
* We include the header for regular page.
*/
include "./include/head2.php";
?>
		<div class="row">
		  <div class="columns large-12 small-6">
			<div class="panel">
				<p>
					<?php
						
						if(isset($_POST['parent'])){
							
							foreach($_POST['selected'] as $idTags){
								$bdd = connection_db();
								$query=$bdd->prepare("UPDATE societe_tags SET customersID = :parent WHERE yourlsID = :idtags;");
								$query->bindValue(':parent',$_POST['parent'], PDO::PARAM_INT);
								$query->bindValue(':idtags',$idTags, PDO::PARAM_INT);
								$query->execute();
								$query->closeCursor();
							}
							echo '<p>Vos tags ont bien été distribués.<br />Cliquez <a href="'.get_link().'">ici</a> 
							pour revenir à la page d accueil</p>';
						} else {
							?>
							<?php
							 echo '<form method="post" action="'.get_link().'give/">
									<h3>Selectionez les tags que vous souhaitez donner.</h3>
									<div class="group-label">
										<label>* Liste des tags non attribués :</label>
									';

									$tags = array();
									$bdd = connection_db();
									/**
									* The SQL request
									*/
									if($_SESSION['level'] == "4") {
					/*					$query=$bdd->prepare('SELECT DISTINCT s.TagID, s.YourlsID, s.TagName, s.description
										FROM tags AS s, societe_tags
										WHERE s.YourlsID
										IN (
											SELECT yourlsID
											FROM societe_tags AS st
											WHERE st.customersID ='.$_SESSION['client_id'].'
										);');*/

									// get all children
										$societes = getWrapSocietes($_SESSION['societe']);
										// add itself into array
										$societes[] = $_SESSION['societe'];

										
										foreach ($societes as $societe) {
											selectTag($societe, $tags); 
										}
									}
									//$query->execute();

									/** 
									* Show ALL the object
									*/
									echo "<table>";
									echo "<tr>
									<th id='checkall'><input type='checkbox' value=''/></th>
									<th>ID</th>
									<th>Nom</th>
									<th>Description</th>
									</tr>";

									ksort($tags);

									foreach ($tags as $row) {
										echo '<tr id="'.$row['YourlsID'].'" class="tag-select">
										<td><input type="checkbox" name="selected[]" value="'.$row['YourlsID'].'"/></td>
										<td>'.$row['TagID'].'</td>
										<td>'.$row['TagName'].'</td>
										<td>'.$row['description'].'</td>
										</tr>';
									}
									
									/**
									* Display form on screen
									*/
									echo'</table></div>';

									/**
									* The SQL request
									*/
									if($_SESSION['level'] == "4") {
										$query=$bdd->prepare('SELECT DISTINCT * FROM societe AS s;');
									} else {
										$sql = 'SELECT DISTINCT s.name FROM societe AS s, societe_soussociete AS ss WHERE s.ID = ss.societe 
										AND s.ID = :id;';
										$query=$bdd->prepare($sql);
										$query->bindValue(':id',$_SESSION['societe'], PDO::PARAM_INT);
									}
									$query->execute();
									
									/**
									* Display form on screen
									*/
									echo'<div class="group-label">
										<label>* Entreprise :</label>
										<select name="parent">';
										/** 
										* Show ALL the object
										*/
										echo "<option value='".$_SESSION['societe']."' selected='selected'>Aucune</option>";
										while ($row = $query->fetch()) {
											echo "<option value='".$row['ID']."'>".$row['name']."</option>";
										}
									
										echo '</select>
									</div> 
								<p>Les champs précédés d\'un * sont obligatoires</p>
								<p>
									<input class="button" type="submit" value="Envoyer" />
								</p>
							</form>';
							/**
							* Closing database
							*/
							$query->closeCursor();
						}
					?>
				</p>
			</div>
		  </div>		  
		</div>
<?php
/**
* We include the footer for regular page.
*/
include "./include/footer2.php";
?>