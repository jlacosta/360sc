<?php
include "./include/head2.php";

/**
* Error message.
*/
function erreur($err='') {
   $mess=($err!='')? $err:'Une erreur inconnue s\'est produite';
   exit('<p>'.$mess.'</p>
   <p>Cliquez <a href="./index.php">ici</a> pour revenir à la page d\'accueil</p>');
}
$bdd = connection_db();
define('ERR_IS_CO','Vous ne pouvez pas accéder à cette page si vous n\'êtes pas connecté');

/**
* If "accountName" is empty, that's mean we're on form page.
*/
if (!isset($_POST['accountName']))
{
?>

<script type="text/javascript">

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#imgPreview').attr('src', e.target.result);
                $('#imgPreview').css('display', 'inline-block');
                $('#spanPreview').css('display', 'inline-block');
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

  $(document).ready(function(){ 
    var clients; 
    $.getJSON("<?php echo get_link();?>php/getClients.php",function(data){
      clients = data;
      for(var i=0;i<data.length;i++){ 
        var option = "<option value='"+data[i]['CustomersID']+"'>"+data[i]['Name']+"</option>";
        $("select#clientId").html($("select#clientId").html()+option);
      }

    });

    $.getJSON("<?php echo get_link();?>php/getSocietes.php",function(societes){
      for(var i=0;i<societes.length;i++){ 
        var option = "<option value='"+societes[i]['ID']+"'>"+societes[i]['name']+"</option>";
        $("select#societeName").html($("select#societeName").html()+option);
      }

    });


    $("select#clientId").change(function(){ 
         
      var selectedClient = $.grep(clients,function(client){
        return client.CustomersID==$("select#clientId").val();
      })[0]; 
      $("select#societeName").val(selectedClient['societe']);
      $("input#societeName").val(selectedClient['societeName']);
      $("input#mobile").val(selectedClient['mobile']);
      $("input#fixe").val(selectedClient['fixe']);
      $("input#email").val(selectedClient['email']);
      $("input#accountName").val(selectedClient['Name']);
      $("img#accountImage").attr('src',"<?php echo get_link();?>"+selectedClient['image']);


    });
 
    

    $('input#newAccountImage').change(function(){
        readURL(this);
    }); 

  });

</script>

<?php

	/**
	* We display the information from database
	*/
	$query=$bdd->prepare('SELECT c.*,s.name AS societeName FROM customers AS c INNER JOIN societe AS s  WHERE c.societe = s.ID AND customersID =:id');
	$query->bindValue(':id',$_SESSION['client_id'], PDO::PARAM_INT);
	$query->execute();
	while ($row = $query->fetch()) {
		echo '<form method="post" action="'.get_link().'gestion/" enctype="multipart/form-data">
			<fieldset>
				<h3>Identifiants</h3>';
        if ($_SESSION['level']==4) {
          echo'
          <div class="group-label">
            <label for="accountName">Nom de compte :</label>
            <select name="clientId" type="text" id="clientId"></select>
            <input name="accountName" type="hidden" id="accountName" value="'.$row['Name'].'"/>
          </div>';
        }else{
          echo'
          <div class="group-label">
            <label for="accountName">Nom de compte :</label>
            <input name="accountName" type="text" id="accountName" value="'.$row['Name'].'"/>
          </div>';

        }
        
        echo'
				<div class="group-label">
					<label for="password">Mot de Passe :</label>
					<input type="password" name="password" id="password" />
				</div>
				<div class="group-label">
					<label for="confirm">Confirmer le mot de passe :</label>
					<input type="password" name="confirm" id="confirm" />
				</div>
			</fieldset>
			<fieldset>
				<h3>Contact</h3>';
        if ($_SESSION['level']==4) {
          echo'
          <div class="group-label">
            <label for="societeName">Nom d\'entreprise :</label>
            <select name="societe" type="text" id="societeName"></select> 
          </div>';
        }else{
          echo'
            <div class="group-label">
                <label for="societeName">Nom d\'entreprise :</label>
                <input name="societeName" type="text" id="societeName" value="'.$row['societeName'].'" disabled/>
            </div>';

        }        
        echo    '<div class="group-label">
					<label for="mobile">Téléphone Mobile :</label>
					<input type="text" name="mobile" id="mobile" value="'.$row['mobile'].'"/>
				</div>
				<div class="group-label">
					<label for="fixe">Téléphone Fixe :</label>
					<input type="text" name="fixe" id="fixe" value="'.$row['fixe'].'"/>
				</div>
				<div class="group-label">
					<label for="email">Email :</label>
					<input type="text" name="email" id="email" value="'.$row['email'].'"/>
				</div>
                <div class="group-label">
                    <label for="accountImage">Image :</label>
                    <img id="accountImage" src="'.get_link().$row['image'].'" onerror="this.error=null;this.width=160;this.src=\''.get_link().'public/img_profil/profil_erreur.png'.'\'; "/>
                    <span id="spanPreview" style="white-space:pre-inline;display:none">   Remplace par  </span>
                    <img id="imgPreview" src="#" alt="new image"/ style="display:none"/>
                    <input type="file" name="newAccountImage" id="newAccountImage" />
                </div>
			</fieldset>
			<p>
				<input class="button" type="submit" value="Modifier" />
			</p>
		</form>';
	}
	$query->CloseCursor();
} else {
	/**
	* We open the URW algorithm.
	*/
	require_once "./algorithm/urw.php";
 
	/**
	* Check a random number between 10 and 20.
	*/
	$nb_urw = rand(10, 20);
	
	/**
	* Apply algorithm.
	*/
	$customerKey = urw($nb_urw);
		
    $pseudo_erreur1 = NULL;
    $pseudo_erreur2 = NULL;
    $mail_erreur1 = NULL;
    $mail_erreur2 = NULL;
    $mdp_erreur = NULL;
    $mobile_erreur1 = NULL;
    $mobile_erreur2 = NULL;
    $fixe_erreur1 = NULL;
	
	  $i = 0;
    $temps = time(); 
    $accountName=$_POST['accountName'];
    //$pseudo=$_POST['login'];
    if ($_SESSION['level']==4){
      $clientId = $_POST['clientId'];
    }else{
      $clientId = $_SESSION['client_id'];
    }
    
    //$societeName=$_POST['societeName'];
    //$niveau=$_POST['niveau'];
    $mobile=$_POST['mobile'];
    $fixe=$_POST['fixe'];
    $email=$_POST['email'];
    $pass = md5($_POST['password']);
    $confirm = md5($_POST['confirm']);

    /**
	* Verification du nom de compte
	*/
    /*$query=$bdd->prepare('SELECT COUNT(*) AS nbr FROM customers WHERE login =:login');
    $query->bindValue(':login',$pseudo, PDO::PARAM_STR);
    $query->execute();
    $pseudo_free=($query->fetchColumn()==0)?1:0;
    $query->CloseCursor();*/
	
	/**
	* Verification de la clé unique
	*/
/*	$query=$bdd->prepare('SELECT COUNT(*) AS nbr FROM customers WHERE CustomerKey =:customerKey');
    $query->bindValue(':customerKey',$customerKey, PDO::PARAM_STR);
    $query->execute();
    $key_free=($query->fetchColumn()==0)?1:0;
    $query->CloseCursor();
	*/
	/**
	* Verification du mail
	*/
/*	$query=$bdd->prepare('SELECT COUNT(*) AS nbr FROM customers WHERE email =:email');
    $query->bindValue(':email',$email, PDO::PARAM_STR);
    $query->execute();
    $mail_free=($query->fetchColumn()==0)?1:0;
    $query->CloseCursor();*/
	
	/**
	* Verification du mobile
	*/
/*	$query=$bdd->prepare('SELECT COUNT(*) AS nbr FROM customers WHERE mobile =:mobile');
    $query->bindValue(':mobile',$mobile, PDO::PARAM_STR);
    $query->execute();
    $mobile_free=($query->fetchColumn()==0)?1:0;
    $query->CloseCursor();*/


    /**
    * Definition des messages d'erreur
    */
    /*if(!$pseudo_free) {
        $pseudo_erreur1 = "Votre nom de compte est déjà utilisé par un membre";
        $i++;
    }*/
	
	/*if(!$key_free) {
		$customerKey = urw($nb_urw);
    }
	
	if(!$mail_free) {
        $mail_erreur1 = "Le mail choisit est déjà utilisé, veuillez en choisir une autre";
		$i++;
    }
	
	if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
		$mail_erreur2 = "Le mail n'est pas au bon format. Veuillez recommencer";
		$i++;
	}
	
	if(!$mobile_free) {
        $mobile_erreur1 = "Le numéro de portable choisit est déjà utilisé, veuillez en choisir une autre";
		$i++;
    }
	
	if(strlen($fixe)!= 10) {
        $fixe_erreur1 = "Le numéro de fixe choisit n'a pas la bonne longueur, un numéro de téléphone est du type : '0123456789'";
		$i++;
    }
	
	if(strlen($mobile) != 10) {
        $mobile_erreur2 = "Le numéro de portable choisit n'a pas la bonne longueur, un numéro de téléphone est du type : '0123456789'";
		$i++;
    }*/

    /*if (strlen($pseudo) < 3 || strlen($pseudo) > 15) {
        $pseudo_erreur2 = "Votre nom de compte est soit trop grand, soit trop petit";
        $i++;
    }*/

        
    //Vérification du mdp
    if($_SESSION['level']!=4){
        if ($pass != $confirm || empty($_POST['password']) || empty($_POST['confirm'])){
            $mdp_erreur = "Votre mot de passe et votre confirmation sont différents, ou sont vides";
            $i++;
        }    
    }else{ 
        if ($pass != $confirm){
                $mdp_erreur = "Votre mot de passe et votre confirmation sont différents";
                $i++;
            }
    }
	
	// S'il n'y a aucune erreur 
	if ($i==0) {
		$bdd = connection_db(); 
		
        $sql = "UPDATE customers SET Name ='$accountName',email='$email',fixe='$fixe', mobile= '$mobile' ";
        if(!empty($pass)) $sql.=",password='$pass' ";
        if(isset($_POST['societe'])) $sql.=",societe=".$_POST['societe'];
        if(isset($_FILES["newAccountImage"]) && $_FILES["newAccountImage"]!=null && $_FILES["newAccountImage"]["name"]!="" ){
            $target_dir = "public/img_profil/";
            $target_file = $target_dir . basename($_FILES["newAccountImage"]["name"]);
            $file = $_FILES['newAccountImage']['name'];
            move_uploaded_file($_FILES['newAccountImage']['tmp_name'],realpath(dirname(dirname(__FILE__))).'/'.$target_file);   
            $sql.=",image = '$target_file' ";
        }
        $sql.="  WHERE CustomersID = $clientId";
        $result = $bdd->exec($sql);

        echo 'Modification faite';

		/*
		$query->bindValue(':pass', $pass, PDO::PARAM_STR);
		$query->bindValue(':login', $pseudo, PDO::PARAM_STR);
		$query->bindValue(':email', $email, PDO::PARAM_STR);
		$query->bindValue(':fixe', $fixe, PDO::PARAM_STR);
		$query->bindValue(':mobile', $mobile, PDO::PARAM_STR);
        $query->execute();

        $query->CloseCursor();*/
        $url = get_link()."gestion/";
        ?>
        <script language="javascript" type="text/javascript">
                    window.location.href="<?php echo $url; ?>";                                
        </script>
        <?php
		//header(''.get_link().'gestion/');
    } else { //Il y a des erreurs, on les affiches
        echo'<h1>Inscription interrompue</h1>';
        echo'<p>Une ou plusieurs erreurs se sont produites pendant l incription</p>';
        echo'<p>'.$i.' erreur(s)</p>';
        echo'<p>'.$pseudo_erreur1.'</p>';
        echo'<p>'.$pseudo_erreur2.'</p>';
        echo'<p>'.$mdp_erreur.'</p>';
        echo'<p>'.$mail_erreur1.'</p>';
        echo'<p>'.$mail_erreur2.'</p>';
        echo'<p>'.$mobile_erreur1.'</p>';
        echo'<p>'.$mobile_erreur2.'</p>';
        echo'<p>'.$fixe_erreur1.'</p>';

        echo'<p>Cliquez <a href="./">ici</a> pour recommencer</p>';
    }
}

include "./include/footer2.php";
?>