<?php 
/** 
* Multi Add Page
* @author: ROUMEGUE Jérémy - 360sc
* We include the header for regular page.
*/
include "./include/head2.php";
?>
		<div class="row">
		  <div class="columns large-12 small-6">
			<div class="panel">

			<div id="firstModal" style="padding:0px !important;" class="reveal-modal" data-reveal aria-labelledby="firstModalTitle" aria-hidden="true" role="dialog">
					
				<div class="row">
					<input onclick="onValider()" style="margin-bottom:0px !important; width:100% !important;" class="button" type="button" value="Valider"/>
				</div>  
			<script>
				var Wrap = function (){			  
					this.installed = false;	
					var activeMap = function() {						
						if ($('#firstModal #map').length == 0)
							$('#firstModal').prepend('<div style="width: 100%; height:300px;;" id="map"></div>');

						this.wrapMap = function() {	
							$.__latlng = false;
							var lat = parseFloat($("#position").attr('value').split(', ')[0]);
							var lng = parseFloat($("#position").attr('value').split(', ')[1]);
							var google = new L.Google('ROAD');
							if (installed == false){
								this.map = L.map('map').setView([
										lat, 
										lng
									], 
								11);
								installed = true;
								this.map.addLayer(google);
	    						this.marker = L.marker([lat, lng], {draggable: true});
	    						$.__latlng = this.marker.getLatLng();
	    						this.marker.bindPopup(lat+', '+lng);
	    						this.marker.on('dragend', function(event){
	    							$.__latlng = this.getLatLng();
	    							this.setPopupContent(this.getLatLng().lat + ', ' + this.getLatLng().lng);
	    						});	
				   				this.marker.addTo(this.map);
							}
						}
						wrapMap = wrapMap.bind(this);						
						setTimeout(wrapMap, 500);
					}
					return activeMap;
				}
				var activeMap = Wrap();
				var onValider = function (){
					if ($.__latlng){
						$("#position").val($.__latlng.lat+', '+$.__latlng.lng);
					}
					$('#firstModal').foundation('reveal', 'close');
				}
			</script>
			  
			</div>
		
				<p>
					<?php
						if($_SESSION['page'] != "post-objet") {
							if(check_object_societe($id,$_SESSION['societe'])){

							echo "Modifier l'objet.<br/>";
							if($_SESSION['level']==4){
							?>

								<script type="text/javascript">
									function initMCs(){
										$("#MCchosen").html("<span>MCs attachés :</span><br/>");
										$("#MCfree").html("<span>MCs libres :</span><br/>");
										$("input[name='newTags']").remove();
										var societe = $("#proprietaire").val();
										$.getJSON("<?php echo get_link();?>php/getTags.php",{object:<?php echo $id;?>,societe:societe},function(data){
											data.objectTags.forEach(function(tag){
												$("#MCs").prepend("<input type='hidden' value='"+tag.TagID+"' name='newTags[]'/>");
												$("#MCchosen").append("<span class='tagChosen'>"+tag.TagID+" : "+tag.TagName+"</span>");
											});
											data.societeFreeTags.forEach(function(tag){
												$("#MCfree").append("<span class='tagFree'>"+tag.TagID+" : "+tag.TagName+"</span>");
											}); 
										});

									}
									$(document).ready(function(){

										initMCs();
										
										$("#MCchosen").on('click', 'span', function(){ 
											var tagId = $(this).html().split(" : ")[0];
											$(this).removeClass("tagChosen");
											$(this).addClass("tagFree"); 
											$("#MCfree").append($(this).clone());
											$(this).remove();
											$("input[type='hidden'][value='"+tagId+"']").remove();
										});
										$("#MCfree").on('click', 'span', function(){ 
											var tagId = $(this).html().split(" : ")[0];
											$(this).removeClass("tagFree");
											$(this).addClass("tagChosen"); 
											$("#MCchosen").append($(this).clone());
											$(this).remove();
											$("#MCs").prepend("<input type='hidden' value='"+tagId+"' name='newTags[]'/>");
										});

										$("select#proprietaire").change(function(){
											initMCs();
										});

									});

								</script>

							<?php
							}

							/**
							* We open the database for the SQL request.
							*/
							$bdd = connection_db();

							/**
							* The SQL request
							*/
							$target = $bdd->prepare('SELECT o.objetID, o.name, o.idParent, o.appairage, o.dateAppairage, o.description, o.active, 
									o.dateActive, o.descriptionPos,o.image,o.ficheProduit, o.objLat, o.objLong, o.adresse, o.alphaID, societe_objet.societeID
									FROM objet AS o, societe_objet  
									WHERE   o.objetID = :idObj  AND societe_objet.objetID = o.objetID'); 
							$target->bindValue(':idObj', $id, PDO::PARAM_STR);
							$target->execute();


							// Affichage du formulaire
							echo'<form action="'.get_link().'objet/'.$id.'" method="post" enctype="multipart/form-data">';
								while ($row = $target->fetch()) {
									echo '<label>Nom : </label><input name="name" value="'.$row['name'].'" value="'.$row['name'].'" type="text" />';
									function typeCmp($a,$b){
										return strcmp($a["nameType"], $b["nameType"]);
									}

									$ancestry = getAscendanceSocietes($_SESSION['societe']);
									$descendants = getWrapSocietes($_SESSION['societe']);
									$societes[] = $_SESSION['societe'];
									$societes = array_merge($societes,$ancestry,$descendants);
									$types =array();
									$bdd2 = connection_db(); 
									foreach ($societes as $societe) {
										$sql = "SELECT * FROM typeobjet WHERE proprietaire = $societe";
										$typeQuery = $bdd2->prepare($sql);
										$typeQuery->execute();
										while($type = $typeQuery->fetch()){
											$types[]=$type;
										}
									}
									usort($types,"typeCmp");  
									echo '<label for="idParent">Type d\'objet : </label>';
									echo "<select name='idParent' id='idParent'>";
									foreach ($types as $type) {
										echo "										
										<option value='". $type['idType'] ."'";
										if($type['idType']==$row['idParent']) echo "selected ";
										echo ">". $type['nameType']."</option>
										";	
									} 
									echo "</select>";
									if($_SESSION['level']==4){
										echo '<label>Propriétaire</label>';
										$sql = "SELECT ID,name FROM societe ";
										$allSocietesQuery = $bdd2->prepare($sql);
										$allSocietesQuery->execute();
										echo '<select name="proprietaire" id="proprietaire">';
										while($societe=$allSocietesQuery->fetch()){
											echo "<option value='".$societe['ID']."'".($societe['ID']==$row['societeID']?'selected':'')."  >".$societe['name']."</option>";
										}
										echo '</select>';
										echo '<a target="_blank" href="'.get_link().'outils/changerUrlTags/">Changer URL des tags </a>
													<div id="MCs" class="row">
														<div id="MCchosen" class=" columns large-6 small-6"><span>MCs attachés : </span><br/></div>
														<div id="MCfree" class=" columns large-6 small-6"><span>MCs libres: </span><br/></div>
													</div>';
									}
									

									echo '<label>Description : </label><input name="desc" value="'.$row['description'].'" type="text" />
									<label>AlphaID : </label><input name="alphaID" value="'.$row['alphaID'].'" type="text" '.($_SESSION['level']==4?'':'disabled').' />
									<label>Active : </label><input name="active" value="'.$row['active'].'" type="text" />
									<label>Date d\'activation (format: <i>yyyy-mm-dd</i>) :  </label><input name="dateActive" value="'.$row['dateActive'].'" type="text" />
									<label>Appairage : </label><input name="appairage" value="'.$row['appairage'].'" type="text" />
									<label>Date d\'appairage (format: <i>yyyy-mm-dd</i>) :  </label><input name="dateAppairage" value="'.$row['dateAppairage'].'" type="text" />
									<label>Position : </label>
										<input id="position" style="width:89.5% !important; display:inline !important;" name="latlng" value="'.$row['objLat']." , ".$row['objLong'].'" type="text" />
										<input onclick="activeMap()" data-reveal-id="firstModal"
											style="height:2.3125rem !important; padding: 0px !important; width:95.7969px !important;" 
											type="button" class="button" value="Changer" />										
									<label>Description position : </label><input name="descriptionPos" value="'.$row['descriptionPos'].'" type="text" />
										<label>Image de l\'objet : </label>';
										if($row['image']!=null) {echo '<img src="../'.$row['image'].'" height="50" width="60">';}
										echo ' <span id="spanPreview" style="white-space:pre-inline;display:none">   Remplace par  </span>
										<img id="imgPreview" src="#" alt="new image"/ style="display:none" height="50" width="60">
										<input name="imgObjet" type="file" id="imgObjet"/> ';
										echo"
										<script>
											function readURL(input) {

											    if (input.files && input.files[0]) {
											        var reader = new FileReader();

											        reader.onload = function (e) {
											            $('#imgPreview').attr('src', e.target.result);
											            $('#imgPreview').css('display', 'inline-block');
											            $('#spanPreview').css('display', 'inline-block');
											        }

											        reader.readAsDataURL(input.files[0]);
											    }
											}

											$('#imgObjet').change(function(){
											    readURL(this);
											});
										</script>";

										echo '
										<label>Fiche de produit de l\'objet : </label>
									';
										if(isset($row['ficheProduit']) && $row['ficheProduit']!=null) {echo '<embed src="../'.$row['ficheProduit'].'" >';}
										echo ' <input name="fpObjet" type="file" id="fpObjet"/> 
									';
								}

							echo'<input type="submit" class="button"/> <a href="'.get_link().'" class="button" style="text-decoration:none;float:right">Annuler</a></form>';

							}else{
								echo '<span> Vous n\'avez pas le droit de modifier cet objet</span>';
							}

						} else {
							/**
							* We open the database for the SQL request.
							*/
							$bdd = connection_db();

							//var_dump($_POST["latlng"]);

							if(isset($_POST['name']) && $_POST['name'] != NULL && $_POST['name'] != "" ) {
								/**
								* The SQL request
								*/
								$target = $bdd->prepare('UPDATE objet SET name = :name WHERE objetID = :idObj');
								$target->bindValue(':name', $_POST['name'], PDO::PARAM_STR);
								$target->bindValue(':idObj', $id, PDO::PARAM_STR);
								$target->execute();								
							}

							if(isset($_POST['idParent']) && $_POST['idParent']!=NULL && $_POST['idParent']!=""){
								$target = $bdd->prepare('UPDATE objet SET idParent = :idParent WHERE objetID = :idObj');
								$target->bindValue(':idParent', $_POST['idParent'], PDO::PARAM_INT);
								$target->bindValue(':idObj', $id, PDO::PARAM_INT);
								$target->execute();			
							}

							if(isset($_POST['proprietaire']) && $_POST['proprietaire']!=NULL && $_POST['proprietaire']!=""){
								//modifier la relation societe_objet
								$target = $bdd->prepare('UPDATE societe_objet SET societeID = :proprietaire WHERE objetID = :idObj');
								$target->bindValue(':proprietaire', $_POST['proprietaire'], PDO::PARAM_INT);
								$target->bindValue(':idObj', $id, PDO::PARAM_INT);
								$target->execute();			
								//modifier les MC reliant a l'objet
								$target = $bdd->prepare('SELECT * FROM tags,objet_tags AS ot WHERE tags.YourlsID = ot.yourlsID AND objetID = :idObj ');
								$target->bindValue(':idObj', $id, PDO::PARAM_INT);
								$target->execute();	
								$oldTagsList = array();
								while($tag=$target->fetch()){
									$oldTagsList[]=$tag['TagID'];
								}
								if(isset($_POST['newTags']) && !empty($_POST['newTags'])) $newTagsList = $_POST['newTags']; else $newTagsList = array();
								$addTagsList = array_diff($newTagsList,$oldTagsList);
								$removeTagsList = array_diff($oldTagsList,$newTagsList);
								$changeOwnerTagsList = array_intersect($oldTagsList,$newTagsList); 

								foreach ($removeTagsList as $tagId) {
									//desassocier les tags et l'objet
									$target = $bdd->prepare('DELETE FROM objet_tags WHERE yourlsID = (SELECT YourlsID FROM tags WHERE TagID = :tagId)');
									$target->bindValue(':tagId', $tagId, PDO::PARAM_INT);
									$target->execute();	
									//indiquer de le tag n'est plus	en appairage
									$target = $bdd->prepare('UPDATE tags SET appairer = 0 WHERE TagID = :tagId');
									$target->bindValue(':tagId', $tagId, PDO::PARAM_INT);
									$target->execute();	
								}
								foreach ($changeOwnerTagsList as $tagId) {
									//desassocier les tags et la societe
									$target = $bdd->prepare('UPDATE societe_tags SET customersID = :proprietaire WHERE yourlsID IN (SELECT yourlsID FROM objet_tags WHERE objetID = :idObj) ');
									$target->bindValue(':proprietaire', $_POST['proprietaire'], PDO::PARAM_INT);
									$target->bindValue(':idObj', $id, PDO::PARAM_INT);
									$target->execute();	
								}
								foreach ($addTagsList as $tagId) {
									//chercher yourlsID du tag
									$target = $bdd->prepare('SELECT YourlsID FROM tags WHERE TagID = '.$tagId);
									$target->execute();	
									if($yourlsId = $target->fetch()){
										//ajouter les relations entre les tags et l'objet
										$target = $bdd->prepare('INSERT INTO objet_tags(yourlsID,objetID) VALUES (:yourlsID,:idObj)');
										$target->bindValue(':yourlsID', $yourlsId['YourlsID'], PDO::PARAM_INT);
										$target->bindValue(':idObj', $id, PDO::PARAM_INT);
										$target->execute();
										//indiquer de le tag est 	en appairage
										$target = $bdd->prepare('UPDATE tags SET appairer = 1 WHERE TagID = :tagId');
										$target->bindValue(':tagId', $tagId, PDO::PARAM_INT);
										$target->execute();	
									}
									
								}
								if(!isset($_POST['newTags'])){
									//tous les tags desattaches
									$target = $bdd->prepare('UPDATE societe_tags SET customersID = :proprietaire WHERE yourlsID IN (SELECT yourlsID FROM objet_tags WHERE objetID = :idObj) ');

								}

							}
							

							if(isset($_POST['alphaID']) && $_POST['alphaID']!=NULL && $_POST['alphaID']!=""){
								$target = $bdd->prepare('UPDATE objet SET alphaID = :alphaID WHERE objetID = :idObj');
								$target->bindValue(':alphaID', $_POST['alphaID'], PDO::PARAM_STR);
								$target->bindValue(':idObj', $id, PDO::PARAM_INT);
								$target->execute();			
							}

							if (isset($_POST['latlng']) && $_POST['latlng'] != NULL && $_POST['latlng'] != ""){

								$latlng = explode(', ', $_POST['latlng']);
								$target = $bdd->prepare('UPDATE objet SET objLat = :lat, objLong = :lng WHERE objetID = :idObj');
								$target->bindValue(':lat', $latlng[0], PDO::PARAM_STR);
								$target->bindValue(':idObj', $id, PDO::PARAM_STR);
								$target->bindValue(':lng', $latlng[1], PDO::PARAM_STR);
								$target->execute();
								/*echo "$lat, $lng";		*/						
							}

							if(isset($_POST['desc']) && $_POST['desc'] != NULL && $_POST['desc'] != "" ) {
								/**
								* The SQL request
								*/
								$target = $bdd->prepare('UPDATE objet SET description = :description WHERE objetID = :idObj');
								$target->bindValue(':description', $_POST['desc'], PDO::PARAM_STR);
								$target->bindValue(':idObj', $id, PDO::PARAM_STR);
								$target->execute();
								
							}

							if(isset($_POST['active']) && $_POST['active'] != NULL && $_POST['active'] != "" ) {
								/**
								* The SQL request
								*/
								$target = $bdd->prepare('UPDATE objet SET active = :active WHERE objetID = :idObj');
								$target->bindValue(':active', $_POST['active'], PDO::PARAM_STR);
								$target->bindValue(':idObj', $id, PDO::PARAM_STR);
								$target->execute();
								
							}

							if(isset($_POST['dateActive']) && $_POST['dateActive'] != NULL && $_POST['dateActive'] != "" ) {
								/**
								* The SQL request
								*/
								$target = $bdd->prepare('UPDATE objet SET dateActive = :dateActive WHERE objetID = :idObj');
								$target->bindValue(':dateActive', $_POST['dateActive'], PDO::PARAM_STR);
								$target->bindValue(':idObj', $id, PDO::PARAM_STR);
								$target->execute();
								
							}

							if(isset($_POST['appairage']) && $_POST['appairage'] != NULL && $_POST['appairage'] != "" ) {
								/**
								* The SQL request
								*/
								$target = $bdd->prepare('UPDATE objet SET appairage = :appairage WHERE objetID = :idObj');
								$target->bindValue(':appairage', $_POST['appairage'], PDO::PARAM_STR);
								$target->bindValue(':idObj', $id, PDO::PARAM_STR);
								$target->execute();
								
							}

							if(isset($_POST['dateAppairage']) && $_POST['dateAppairage'] != NULL && $_POST['dateAppairage'] != "" ) {
								/**
								* The SQL request
								*/
								$target = $bdd->prepare('UPDATE objet SET dateAppairage = :dateAppairage WHERE objetID = :idObj');
								$target->bindValue(':dateAppairage', $_POST['dateAppairage'], PDO::PARAM_STR);
								$target->bindValue(':idObj', $id, PDO::PARAM_STR);
								$target->execute();
								
							}

							if(isset($_POST['descriptionPos']) && $_POST['descriptionPos'] != NULL && $_POST['descriptionPos'] != "" ) {
								/**
								* The SQL request
								*/
								$target = $bdd->prepare('UPDATE objet SET descriptionPos = :descriptionPos WHERE objetID = :idObj');
								$target->bindValue(':descriptionPos', $_POST['descriptionPos'], PDO::PARAM_STR);
								$target->bindValue(':idObj', $id, PDO::PARAM_STR);
								$target->execute();
							}

							if(isset($_FILES["imgObjet"]) && $_FILES["imgObjet"]!=null && $_FILES["imgObjet"]["name"]!="" ){
								/**
								* The SQL request
								*/

								$target_dir = "/public/img_objet/";
								$target_file = $target_dir . basename($_FILES["imgObjet"]["name"]);

								$file = $_FILES['imgObjet']['name'];
								move_uploaded_file($_FILES['imgObjet']['tmp_name'],realpath(dirname(dirname(__FILE__))).$target_file);   
								//var_dump($_FILES);

								//echo $_FILES['imgObjet']['tmp_name'].'<br/>';
								//echo realpath(dirname(dirname(__FILE__))).$target_file;


								$target = $bdd->prepare('UPDATE objet SET image = :image WHERE objetID = :idObj');
								$target->bindValue(':image', $target_file, PDO::PARAM_STR);
								$target->bindValue(':idObj', $id, PDO::PARAM_STR);
								$target->execute();
								/*echo "imgObjet\n";
								var_dump($target->errorInfo());*/
							}

							if(isset($_FILES["fpObjet"]) && $_FILES["fpObjet"]!=null && $_FILES["fpObjet"]["name"]!="" ){
								/**
								* The SQL request
								*/

								$target_dir = "/public/ficheProduit_objet/";
								$target_file = $target_dir . basename($_FILES["fpObjet"]["name"]);

								$file = $_FILES['fpObjet']['name'];
								move_uploaded_file($_FILES['fpObjet']['tmp_name'],realpath(dirname(dirname(__FILE__))).$target_file);   
								  


								$target = $bdd->prepare('UPDATE objet SET ficheProduit = :ficheProduit WHERE objetID = :idObj');
								$target->bindValue(':ficheProduit', $target_file, PDO::PARAM_STR);
								$target->bindValue(':idObj', $id, PDO::PARAM_STR);
								$target->execute();
								
							}
							$url = get_link()."edit-objet/";
							echo "Modification effectuée<br/>";
							echo "Retourner à la page d'avant dans 1 seconde";
							/*ob_start();
								header('Location: '. get_link()."edit-objet/".'');
							ob_end_flush();*/
							//$_SESSION['page']='';
							/*echo "Modification effectuée<br/>";
							echo "Retourner à la page d'avant dans 1 seconde";*/
							?>
				 			
 							<script language="javascript" type="text/javascript">
				 						setTimeout(function(){window.location.href="<?php echo $url; ?>";},1000);								   
							</script>
							<?php
						}
					?>
				</p>
			</div>
		  </div>		  
		</div>
<?php
/**
* We include the footer for regular page.
*/
include "./include/footer2.php";
?>