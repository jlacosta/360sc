<?php 
if(($_SESSION['page'] != "SansConnect") && ($_SESSION['connect'] === false)){
	$_SESSION['page'] = "connexion";
	$_SESSION['redirect'] = true;
	header('Location: '.get_link().'connexion/');
	exit();
}
		
echo "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
<html ng-app='Demo'>
	<head>
		<meta charset='UTF-8' />
		<title>360SmartMachine - ".$_SESSION['page']."</title>
		<meta name='description' content='Page d'administration de 360SmartMachine.' />
		<meta name='viewport' content='width=device-width, initial-scale=1.0' />

		<meta property='og:title' content=360SmartMachine' />

		<link rel='shortcut icon' type='image/png' href='".get_link()."img/favicon.png'>

		<link rel='pingback' href='http://www.blocparc.fr/xmlrpc.php' />
		<!--[if lt IE 9]>
			<link rel='stylesheet' type='text/css' href='http://www.blocparc.fr/wp-content/themes/flatbox/ie.css' />
		<![endif]-->	
		<!--[if IE]> <link href='css/ie.css' type='text/css' rel='stylesheet'> <![endif]-->
		<!-- begin JS -->
		<!-- end JS -->
		<meta name='viewport' content='width=device-width, initial-scale=1.0' />
		<link rel='stylesheet' href='".get_link()."css/bootstrap.min.css' />
		<link rel='stylesheet' href='".get_link()."css/foundation.css?0.0.0.0.1' />
		<link href='http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet'>
		<link rel='stylesheet' href='".get_link()."css/responsive-tables.css' />
		<script src='".get_link()."js/vendor/modernizr.js'></script>
		<!-- <script type='text/javascript' src='".get_link()."include/data.json'></script> -->
		<style type='text/css'>
		
			/**/
			.has-form {
			  position: absolute;
			  top: 0;
			  left: 6rem;
			  min-width: 14rem; }
			  @media only screen and (max-width: 40em) {
			    .has-form {
			      min-width: 10rem;
			      top: 0.5rem; }
			      .has-form .button {
			        height: 1.85rem; } }
			/**/

        .left-off-canvas-menu{
        	width: 14.2rem !important;
        }
        ul.off-canvas-list li label {
        	text-transform: capitalize !important;
    	}
			/* Set a size for our map container, the MabBox will take up 100% of this container */
			#map {
				width: 100%;
				height: 92.5% !important;
				position: absolute;
			}
			.contentPopup {
				text-align: center;						
			}
			.actions {
				margin:0 0 15px;
			}
			.actionPopup-L {
				float: left;
			}
			.actionPopup-R {
				float: right; 
			}
			.name{
				border-bottom: 10px !important;
				border-bottom-color: black;
			}
			.hidePopup{
				display: none;
			}
		</style>

	
		<!-- Css PopUP -->
		<style>
			.myrow {
				padding: 0px;
				margin: 0px;
			}
			.switch input:checked + label.labelGreen {
				background: #95c11f !important;  
			}
			.rowPopup, rowPopup row {
				padding-left: -15;
				padding-right: -15;
			}
		</style>

		<style>
			.iconPopup{
				position: relative; 
			    margin-left: 7px;
				float: left;
			}
			.css-wrapp{
				position: relative; 
			    width: 35px; 
				float: left;
			}
			.css-circle-up{
				position: relative; 
			    height: 10px; 
			    width: 9px; 
			    text-align: center;
			    vertical-align: text-top; 
			    display: table;
			    float: right;
			}
			span.up{
			     display: table-cell;
			     vertical-align: middle;
			     font-size: 10px !important;
			     color:black !important;
			}
		</style>

		<style>
			
			.circle{
				position: relative; 
			    width: 55px; 
			    height: 55px;
			    border-radius: 50%;
			    background-color: black ;
		     	text-align: center;
		      	display: table;
			}
			.circleBorderBlack{
				position: relative; 
			    width: 53px; 
			    height: 53px;
			    border-radius: 50%;
			    background-color: #c8c6c7;
			    text-align: center;
		      	display: table;
		      	top: 1px;
    			left: 1px;
			}
			span.viewCircle{
				display: table-cell;
		     	vertical-align: middle;
		     	font-size: 25px !important;
		     	color:black !important;
			}
			span.textViewHistMessage{
				font-size: 15px !important;

			}
			span.iconMViewHistMessage{
				position: relative;
				font-size: 60px !important;
				top: 23px !important;
				text-align: center;
				display: table-cell;
			}
			.iconMViewAlerte{
				position: relative;
				font-size: 72px !important;
				top: 16px !important;
				text-align: center;
				display: table-cell;
			}
			.cirlceLeftCenter{
				margin-left:15px !important;
			}
			div.divColumnViewHistMessage{
				padding-right: 0px !important;
				padding-left: 0px !important;
				display: inline-table;
			}
		</style>
		<style>
			#historiqueList-Messages .accordion .accordion-navigation > a,
			#historiqueList-Alertes .accordion .accordion-navigation > a,
			#historiqueList-Evaluations .accordion .accordion-navigation > a,
			#markerList .accordion .accordion-navigation > a, 
			#etatParc .accordion .accordion-navigation > a,
			#markerList .tab-title a,
			#MessageEtat,
			#EtatAlerte,
			#EvaluationEtat{
				padding: 0px !important;
			}
			#historiqueList-Messages .accordion, 
			#historiqueList-Alertes .accordion,
			#historiqueList-Evaluations .accordion,
			#markerList .accordion,
			#etatParc .accordion{
				margin-left: 0px !important;
			}
			.clear{
				padding: 0px !important;
				border: none !important;
				background: #f93C32 !important;
			}
			.switch label.clear:after{
				height: 0.5rem !important;
				width: 0.5rem !important;
			}
			.switch input:checked + label.clear:after{
				left: 1.25rem !important;
			}
		</style>
		<style>
			.a-wrap{
				display:inline !important;
				color: black !important;
				padding: 0px !important;
			}
			p.withoutBottom{
				margin-bottom:0px !important;
				padding-bottom: 2px !important;
				background-color:white;
			}
			.tabs-content{
				margin-bottom: 0px !important;
			}

			#markerList::-webkit-scrollbar-track {
		      background-color: #b46868;
			}

			
			.right-off-canvas-menu li.backAll{
				display: inline-block !important;
				float: right !important;

			}
			.right-off-canvas-menu li.back{
				display: inline-block !important;
			}
		</style>

		<script src='https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

		<script src='".get_link()."js/bower_components/js/jqueryUtilities.js'></script>
			
		<script type='text/javascript' src='".get_link()."js/tooltip.js'></script>

    	
		<script type='text/javascript' src='".get_link()."js/fuzzyset.js'></script>
		<script type='text/javascript' src='".get_link()."js/underscore.js'></script>
		

		<script src='".get_link()."js/bower_components/js/angular.min.js'></script>
		<script src='".get_link()."js/bower_components/js/leaflet.js'></script>
		<script src='".get_link()."js/bower_components/js/index.js'></script>
		<script src='".get_link()."js/bower_components/js/leaflet.markercluster.js'></script>
		<script src='".get_link()."js/bower_components/js/angular-leaflet-directive.js'></script>
		<script src='".get_link()."js/bower_components/js/leaflet.awesome-markers.js'></script>
		<script src='".get_link()."js/bower_components/js/leaflet.extra-markers.js'></script>
		<script src='".get_link()."js/bower_components/js/angular-websocket.js'></script>

		<link rel='stylesheet' href='".get_link()."js/bower_components/css/leaflet.css'/>
		<link rel='stylesheet' href='".get_link()."js/bower_components/css/MarkerCluster.css' />
		<link rel='stylesheet' href='".get_link()."js/bower_components/css/MarkerCluster.Default.css' />
		<link rel='stylesheet' href='".get_link()."js/bower_components/css/font-awesome.css'/>
		<link rel='stylesheet' href='".get_link()."js/bower_components/css/leaflet.extra-markers.css'/>
	
		
		<script src='https://cdn.socket.io/socket.io-1.2.0.js'></script>

		<!-- Module App AngularJs -->
		<script src='".get_link()."js/bower_components/js/factory.js'></script>
		<script src='".get_link()."js/bower_components/js/filters.js'></script>
		<script src='".get_link()."js/bower_components/js/countActionsService.js'></script>
		<script src='".get_link()."js/bower_components/js/templatesHTMLService.js'></script>
		<script src='".get_link()."js/bower_components/js/markersController.js'></script>
		<script src='".get_link()."js/bower_components/js/infoShowController.js'></script>
		<script src='".get_link()."js/bower_components/js/app.js'></script>		

		<!-- Google -->
		<script src='".get_link()."js/bower_components/js/Google.js'></script>
		<script src='https://maps.googleapis.com/maps/api/js?sensor=false&KEY=AIzaSyAeaVmn26e5tMD1kYr68MiLIii9jG91rdQ'></script>

		

		<link rel='stylesheet' href='".get_link()."js/bower_components/css/demoMPB.css' />

	</head>
	<body class=";
	echo isset($_SESSION['page'])?$_SESSION['page']:'connexion';
	echo ">";
?>
<?php 
echo"
	<script> $('#Etat').css('height', 200 + 'px');</script>
	<div class='off-canvas-wrap' data-offcanvas>
		  	<div class='inner-wrap'>
				<nav class='tab-bar' style='background-color:#c4d600'>";
					if($_SESSION['page'] == "accueil") {
						echo "<section class='right-small'>
							<a class='right-off-canvas-toggle menu-icon' href='#'><span></span></a>
						</section>";
					}
				    echo "
				    <section ng-controller='etatDuParkController' class='middle tab-bar-section'>
				    	<h1> 
				    	<img src='".get_link()."img/head-logo.svg' style='height:2.8125rem;margin-right:5px'/>
				    		<a href='#' style='position:relative;   text-decoration: none; color:rgb(218, 218, 218);' onclick='ovrirOffEtatDuParc()'>
				    			
					    		<span ng-if='!!alerteAPriseEncompte'>
					    			<style>
					    				.bell{
					    					font-size: 22px;;
					    				}
					    			</style>
					    			<span aria-hidden='true' ng-style='couleurClocheRouge' class='glyphicon glyphicon-bell bell'  style='position:relative;top:5px;background-color:#303228;padding:5px 6px 5px 5px;border-radius:50%'></span>
					    			<span style=' color:black' ng-bind='evaluationAPriseEncompte + messageAPriseEncompte + alerteAPriseEncompte'></span>
					    		</span>
					    		<span ng-if='!alerteAPriseEncompte && !!messageAPriseEncompte && !!evaluationAPriseEncompte'>
					    			<style>
					    				.bell{
					    					font-size: 22px;;
					    				}
					    			</style>
					    			<span aria-hidden='true' ng-style='couleurClocheVerte' class='glyphicon glyphicon-bell bell'  style='position:relative;top:5px;background-color:#303228;padding:5px 6px 5px 5px;border-radius:50%'></span>
					    			<span style='p ' ng-bind='evaluationAPriseEncompte + messageAPriseEncompte + alerteAPriseEncompte'></span>
					    		</span>
					    		<span ng-if='!alerteAPriseEncompte && !messageAPriseEncompte && !evaluationAPriseEncompte'>
					    			<style>
					    				.bell{
					    					font-size: 22px;;
					    				}
					    			</style>
					    			<span aria-hidden='true' ng-style='couleurClocheGris' class='glyphicon glyphicon-bell bell'  style='position:relative;top:5px;background-color:#303228;padding:5px 6px 5px 5px;border-radius:50%'></span>
					    			<span style=' ' ng-bind='evaluationAPriseEncompte + messageAPriseEncompte + alerteAPriseEncompte'></span>
					    		</span>
				    		</a>
				    		<a href='#' id='glyphiconSearchAdresse' ng-click='glyphiconSearchAdresse()'>
				    			<span id='glyphiconSearchAdresseIcon' class='glyphicon glyphicon-search'></span>
				    		</a>
				    	</h1>					    		
				    </section>";
				    if($_SESSION['page'] != "FingWa_MCInfo") {
					    echo "<section class='left-small'>
						  <a class='left-off-canvas-toggle menu-icon' href='#'><span></span></a>
					 	   </section>
							</nav>
							";	
					}	
		if 	($_SESSION['page'] != "SansConnect"){
			echo "
			<aside class='left-off-canvas-menu'>
			 <ul class='off-canvas-list'>
				<li>
					<a class='left-off-canvas-toggle'><span aria-hidden='true' class='glyphicon glyphicon-chevron-left'></span> Menu</a>
				</li> 
			 	<li><img  src='".$_SESSION['client_image']."' style='width:100%' /></li>
				<li>
           <a style='text-decoration:none'>
          <span aria-hidden='true' class='glyphicon glyphicon-chevron-left' style='color: transparent !important;'></span>  					 
					<span style='font-size:18px; font-style:italic'>Utilisateur</span>  <br/> 
					<span aria-hidden='true' class='glyphicon glyphicon-chevron-left' style='color: transparent !important;'></span> 
					<span id='nav-client-name' style='width: 100%;font-size:25px;color:#C4D600;'>".$_SESSION['client_name']."</span></a>
				</li> 
				<li><a href='".get_link()."'><span aria-hidden='true' class='glyphicon glyphicon-home'></span>Accueil</a></li>
				";
				echo"<li class='has-submenu'><a href='#'><span aria-hidden='true' class='glyphicon glyphicon-tags'></span> MCs</a>
					<ul class='left-submenu'>
						<li class='back'>
							<a href='#'>Précédent</a>
						</li>
						<li>
							<label>MCs</label>
						</li>";
						echo (isset($_SESSION['client_id']) AND $_SESSION['addTag'])?"<li><a href='".get_link()."adds/'><span aria-hidden='true' class='glyphicon glyphicon-plus'></span>Ajouter des MCs</a></li>":"";
						echo (isset($_SESSION['client_id']) AND $_SESSION['searchTags'])?"<li><a href='".get_link()."search/'><span aria-hidden='true' class='glyphicon glyphicon-search'></span>Chercher un MC</a></li>":"";
						echo (isset($_SESSION['client_id']) AND $_SESSION['export'])?"<li><a href='".get_link()."export/'><span aria-hidden='true' class='glyphicon glyphicon-open'></span> Exporter les MCs</a></li>":"";
						echo "<li><a href='".get_link()."tags/'><span aria-hidden='true' class='glyphicon glyphicon-tags'></span> Liste des tags</a></li>";
						echo (isset($_SESSION['client_id']) AND $_SESSION['editTags'])?"<li><a href='".get_link()."edit/'><span aria-hidden='true' class='glyphicon glyphicon-pencil'></span> Modifier un Mc</a></li>":"";
						echo (isset($_SESSION['client_id']) AND $_SESSION['level']==4)?"<li><a href='".get_link()."give/'><span aria-hidden='true' class='glyphicon glyphicon-send'></span>Donner des MCs</a></li>":"";
						echo (isset($_SESSION['client_id']) AND $_SESSION['level']==4)?"<li><a href='".get_link()."delete/'><span aria-hidden='true' class='glyphicon glyphicon-remove'></span>Supprimer un MC</a></li>":"";
						echo (isset($_SESSION['client_id']) AND $_SESSION['SURLE'])?"<li><a href='".get_link()."urle/'><span aria-hidden='true' class='glyphicon glyphicon-link'></span>Liste des URLe</a></li>":"";
					echo "</ul>
				</li>
				<li class='has-submenu'><a href='#'><span aria-hidden='true' class='glyphicon glyphicon-list'></span> Objets</a>
					<ul class='left-submenu'>
						<li class='back'>
							<a href='#'>Précédent</a>
						</li>
						<li>
							<label>Objets</label>
						</li>";
						echo (isset($_SESSION['client_id']) AND $_SESSION['addObject'])?"<li><a href='".get_link()."objet/'><span aria-hidden='true' class='glyphicon glyphicon-scissors'></span> Créer un objet</a></li>":"";
						echo (isset($_SESSION['client_id']) AND $_SESSION['editObject'])?"<li><a href='".get_link()."edit-objet/'><span aria-hidden='true' class='glyphicon glyphicon-edit'></span> Modifier un objet</a></li>":"";
						echo (isset($_SESSION['client_id']) AND $_SESSION['type'])?"<li><a href='".get_link()."type/'><span aria-hidden='true' class='glyphicon glyphicon-plus'></span> Ajouter un type d'objet</a></li>":"";
						echo "<li><a href='".get_link()."objets/'><span aria-hidden='true' class='glyphicon glyphicon-list'></span> Liste des objets</a></li>";
					echo "</ul>
				</li>
				<li class='has-submenu'><a href='#'><span aria-hidden='true' class='glyphicon glyphicon-cog'></span> Compte Utilisateur</a>
					<ul class='left-submenu'>
						<li class='back'>
							<a href='#'>Précédent</a>
						</li>
						<li>
							<label>Compte Utilisateur</label>
						</li>";
						echo "<li><a href='".get_link()."gestion/'><span aria-hidden='true' class='glyphicon glyphicon-cog'></span> Gestion de compte</a></li>";
						echo (isset($_SESSION['client_id']) AND $_SESSION['createUser'])?"<li><a href='".get_link()."register/'><span aria-hidden='true' class='glyphicon glyphicon-user'></span> Ajouter un utilisateur</a></li>":"";
					echo "</ul>
				</li>";
				echo (isset($_SESSION['client_id']) AND $_SESSION['editObject'])?"<li><a href='".get_link()."societe/'><span aria-hidden='true' class='glyphicon glyphicon-briefcase'></span> Cr&eacute;er une entreprise</a></li>":"";
				echo "<li><a href='".get_link()."contact/'><span aria-hidden='true' class='glyphicon glyphicon-envelope'></span>Contact</a></li>";
				echo "<li><a href='".get_link()."outils/changerUrlTags/'>
							<span aria-hidden='true' class='glyphicon glyphicon-tag'></span>
							Changer l'URL d'un MC
					      </a>
					  </li>";
				echo (isset($_SESSION['client_id']) AND strlen($_SESSION['client_id']) != 0)?"<li><a href='".get_link()."deconnexion/'><span aria-hidden='true' class='glyphicon glyphicon-off'></span>Deconnexion</a></li>":"<li><a href='".get_link()."connexion/'><span aria-hidden='true' class='glyphicon glyphicon-user'></span>Connexion</a></li>";
				echo "
				<li id='logo360_gray' style='text-align:center;margin:auto;width: 30%;'><img  src='".get_link()."img/logo-color-simple.png'  /></li>
			  </ul>
			</aside>";
		}	
			if($_SESSION['page'] == "accueil") {
				echo "
<aside class='right-off-canvas-menu'>
<ul class='off-canvas-list'>
	<li>
		<li class='back'>
				<a   style='cursor: default;text-decoration:none;background: transparent;border-bottom: none;padding: 0.3rem 0.9375rem;'>&nbsp;</a>
		</li>
		<li class='backAll'>
		<a href='#' onclick='fermerTousRightMenu()'>
			<span aria-hidden='true' class='glyphicon glyphicon-remove'></span> 
		</a>
		</li>
	</li>
	<li>
			<label style='text-transform: uppercase !important;text-align: center !important;font-size: 1.2rem !important;padding: 7px 0px !important;color: #C4D600; !important;'>Options Generale</label>
	</li>
	<li class='has-submenu'>
		<a href='#'><span aria-hidden='true' ng-style='couleurClocheGris' class='glyphicon glyphicon-bell bell' style='font-size:16px;left:-5px;margin-right:0'></span>Etat Du Parc</a>
		<ul ng-controller='etatDuParkController' id='etatParc' class='right-submenu'>
			<li class='back'>
				<a href='#'>Précédent</a>
			</li>
			<li class='backAll'><a href='#' onclick='fermerTousRightMenu()'>
        		<span aria-hidden='true' class='glyphicon glyphicon-remove'></span></a>
      </li>
			<li>
			<label>Etat Du Parc</label>
			</li>
			<ul class='accordion' data-accordion role='tablist'>
			 	<li class='accordion-navigation'>
		    		<a href='#EtatAlerte' role='tab' id='EtatAlerte-heading' style='padding-left: 10px;'
		    		aria-controls='EtatAlerte'>
		    			<label  ng-style='{ \"color\": alerteAPriseEncompte > 0 ? \"#ff6155\" : \"\"}'>
		    				<span aria-hidden='true' style='color:#ff6155;' class='glyphicon glyphicon-alert'></span>
							{{ alerteAPriseEncompte }} Alerte(s)
						</label>	
		    		</a>
			    	<div id='EtatAlerte' class='content' role='tabpanel' aria-labelledby='EtatAlerte-heading'
			    	style='overflow-y : auto;'>
		    			<ul class='accordion' data-accordion role='tablist'>
		    			 	<li class='accordion-navigation' ng-repeat='m in action.alerte | convertToArray | orderBy : \"-createdTime\"' ng-if='m.prisEnCompte == 0 ' >
		    		    		<a ng-href='#alerteEtat-{{ m.actionID }}' role='tab' ng-attr-id='alerteEtat-{{ m.actionID }}-heading' 
		    		    		ng-attr-aria-controls='alerteEtat-{{ m.actionID }}'>
		    		    			<label ng-if='!!m.messageAlerte' 
		    		    			ng-style='{\"text-transform\": \"capitalize\", \"font-size\": \"12px\", \"color\": m.prisEnCompte == 1 ? \"#999999\": \"#dadada\" }'> 
			    		    			{{ m.objetName }}
			    		    			<i class='fi-alert' style='color:#ff6155; float:right; font-size: 18px; margin-top:1px; margin-left: 2px;' ></i>
			    		    			<span aria-hidden='true' style='color:#dadada; margin-right:5px; margin-top: 3px; float:right; font-size:15px;' class='glyphicon glyphicon-envelope'></span> 
			    		    			<span style='font-size:9px; text-transform: lowercase; font-style: italic;'> 
			    		    				{{ m.createdTime.slice(0, 10) }} à {{ m.createdTime.slice(11, 19) }} 
			    		    			</span> 	
		    		    			</label>
		    		    			<label ng-if='!!!m.messageAlerte' ng-style='{\"text-transform\": \"capitalize\", \"font-size\": \"12px\", \"color\": m.prisEnCompte == 1 ? \"#999999\": \"#dadada\" }'> 
			    		    			{{ m.objetName }}
			    		    			<i class='fi-alert' style='color:#ff6155; float:right; font-size: 18px; margin-top:1px; margin-left: 2px;' ></i>
			    		    			<span style='margin-right:5px; font-size:9px; text-transform: lowercase; font-style: italic;'> 
			    		    				{{ m.createdTime.slice(0, 10) }} à {{ m.createdTime.slice(11, 19) }} 
			    		    			</span> 	
		    		    			</label>
		    		    		</a>
		    			    	<div ng-attr-id='alerteEtat-{{ m.actionID }}' class='content' role='tabpanel'
		    			    	ng-attr-aria-labelledby='alerteEtat-{{ m.actionID }}-heading'
		    			    	style='background-color:#c6c6c6; padding:0px;'>
		    			    		<div class='row' style='padding: 10px 0px; background-color:#dadada; margin-bottom:5px;'>
		    			    			<span aria-hidden='true' style='font-size: 1rem;' class='right glyphicon glyphicon-share-alt'></span>
		    			    			<a class='a-wrap' href='#' ng-click='centreMap(m.actionID, m._type)'>	
		    			    				<span aria-hidden='true' style='font-size: 1rem; padding-left:7px;' class='right glyphicon glyphicon-screenshot'></span>
		    			    			</a>
		    			    			<div style='margin-bottom:0px;' class='switch round right'>
									 		<input id='etat-{{ m.actionID }}' type='checkbox' onclick='prisEnCompteAlerte_(this)'>
									 		<label style='height: 1rem; width:2rem;' class='clear labelGreen' for='etat-{{ m.actionID }}'></label>
										</div>
		    			    		</div>
		    			    		<p class='withoutBottom' style='padding: 0 0.9375rem;'> 
		    			    			<span style='font-weight:bold;'>
		    			    				{{ m.Name }} :
		    			    				<span ng-if='m.messageAlerte == \"\"'>Sans Message</span>
		    			    			</span> 
		    			    			<span>
		    			    				{{ m.messageEvaluation || m.messageAlerte }}
		    			    			</span>	
		    			    		</p>
		    			    	</div>
		    			  	</li>
		    			</ul> 
			    	</div>
			  	</li>
			  	<li class='accordion-navigation'>
		    		<a href='#MessageEtat' role='tab' id='MessageEtat-heading' style='padding-left: 10px;'
		    			aria-controls='MessageEtat'>
		    			<label ng-style='{ \"color\": messageAPriseEncompte > 0 ? \"#ff6155\" : \"\"}'>
		    				<span aria-hidden='true' style='color:#95c11f;' class='glyphicon glyphicon-envelope'></span>
							{{ messageAPriseEncompte }} Message(s)
						</label>	
		    		</a>
			    	<div id='MessageEtat' class='content' role='tabpanel' aria-labelledby='MessageEtat-heading'
			    	style='overflow-y : scroll; '>
		    			<ul class='accordion' data-accordion role='tablist'>
		    			 	<li class='accordion-navigation' ng-repeat='m in action.message | convertToArray | orderBy : \"-createdTime\"' ng-if='m.prisEnCompte == 0'>
		    		    		<a ng-href='#messageEtat-{{ m.actionID }}' role='tab' ng-attr-id='messageEtat-{{ m.actionID }}-heading' 
		    		    		ng-attr-aria-controls='messageEtat-{{ m.actionID }}'>
		    		    			<label ng-if='m.prisEnCompte == 0' 
		    		    			style='text-transform: capitalize; font-size:12px; color:#dadada;'> 
			    		    			{{ m.objetName }} 
			    		    			<span style='font-size:9px; text-transform: lowercase; font-style: italic;'> 
			    		    				{{ m.createdTime.slice(0, 10) }} à {{ m.createdTime.slice(11, 19) }}
			    		    				<span aria-hidden='true' style='color:#dadada; margin-top: 3px; float:right; font-size:15px;' class='glyphicon glyphicon-envelope'></span> 
			    		    				<i class='fi-alert' style='color:#ff6155; float:right; font-size: 18px; margin-right:5px; margin-top:1px; margin-left: 2px;' ></i>
			    		    			</span> 	
		    		    			</label>
		    		    			<label ng-if='m.prisEnCompte == 1' style='text-transform: capitalize; font-size:12px; color:#999999;'> 
			    		    			{{ m.objetName }} 
			    		    			<span style='font-size:9px; text-transform: lowercase; font-style: italic;'> 
			    		    				{{ m.createdTime.slice(0, 10) }} à {{ m.createdTime.slice(11, 19) }} 
			    		    			</span> 	
		    		    			</label>
		    		    		</a>
		    			    	<div ng-attr-id='messageEtat-{{ m.actionID }}' class='content' role='tabpanel'
		    			    	ng-attr-aria-labelledby='messageEtat-{{ m.actionID }}-heading'
		    			    	style='background-color:#c6c6c6; padding:0px;'>
		    			    		<div class='row' style='padding: 10px 0px; background-color:#dadada; margin-bottom:5px;'>
		    			    			<span aria-hidden='true' style='font-size: 1rem;' class='right glyphicon glyphicon-share-alt'></span>
		    			    			<a class='a-wrap' href='#' ng-click='centreMap(m.actionID, m._type)'>	
		    			    				<span aria-hidden='true' style='font-size: 1rem; padding-left:7px;' class='right glyphicon glyphicon-screenshot'></span>
		    			    			</a>
		    			    			<div style='margin-bottom:0px;' class='switch round right'>
									 		<input id='etat-{{ m.actionID }}' type='checkbox' onclick='prisEnCompteEvaluation_(this)'>
									 		<label style='height: 1rem; width:2rem;' class='clear labelGreen' for='etat-{{ m.actionID }}'></label>
										</div>
		    			    		</div>
		    			    		<p class='withoutBottom' style='padding: 0 0.9375rem;'> 
		    			    			<span style='font-weight:bold;'>
		    			    				{{ m.Name }} :
		    			    			</span> 
		    			    			<span>
		    			    				{{ m.messageEvaluation || m.messageAlerte }}
		    			    			</span>	
		    			    		</p>
		    			    	</div>
		    			  	</li>
		    			</ul> 
			    	</div>
			  	</li>
			  	<li class='accordion-navigation'>
			  		<a href='#EvaluationEtat' role='tab' id='EvaluationEtat-heading' style='padding-left: 10px;'
		    			aria-controls='EvaluationEtat'>
		    			<label ng-style='{ \"color\": evaluationAPriseEncompte > 0 ? \"#ff6155\" : \"\"}'>
		    				<img src='img/smile-happy.svg' style='position:relative; top: -2px; margin-right: 5px; width: 16px; height: 16px;'>
							{{ evaluationAPriseEncompte }} Evaluation(s)
						</label>
		    		</a>
		    		<div id='EvaluationEtat' role='tabpanel' class='content' aria-labelledby='EvaluationEtat-heading'
		    		style='overflow-y:scroll;'>
		    			<ul class='accordion' data-accordion role='tablist'>
			    			 	<li class='accordion-navigation' ng-repeat='m in action.evaluation | convertToArray | orderBy : \"-createdTime\"' ng-if='m.prisEnCompte == 0'>
			    		    		<a ng-href='#TabEvaluations-{{ m.actionID }}' role='tab' ng-attr-id='TabEvaluations-{{ m.actionID }}-heading' 
			    		    		ng-attr-aria-controls='TabEvaluations-{{ m.actionID }}'>
			    		    			<label ng-style='{\"text-transform\": \"capitalize\", \"font-size\": \"12px\", \"color\": m.prisEnCompte == 1 ? \"#999999\": \"#dadada\" }'> 
			    		    			{{ m.objetName }} 
			    		    			<span ng-controller='etatDuParkController' style=' font-size:9px; text-transform: lowercase; font-style: italic;'> 			    		    				 
			    		    				{{ m.createdTime.slice(0, 10) }} à {{ m.createdTime.slice(11, 19) }}  
			    		    			</span>
												<span style='float:right;'>
			    		    					<img ng-if='m.evaluation == 3' src='img/smile-happy.svg' style='width: 20px; height: 20px;'>
			    		    					<img ng-if='m.evaluation == 2' src='img/smile-mid.svg' style='width: 20px; height: 20px;'>
			    		    					<img ng-if='m.evaluation == 1' src='img/smile-sad.svg' style='width: 20px; height: 20px;'>
			    		    				</span>	
			    		    				<span ng-if='!!m.messageEvaluation' aria-hidden='true' ng-style='{\"color\": m.prisEnCompte == 1 ? \"#999999\": \"#dadada\", \"margin-top\": \"3px\", \"float\":\"right\", \"font-size\":\"15px\"}' class='glyphicon glyphicon-envelope'></span> 
			    		    			</span> 	
		    		    			</label>
			    		    		</a>
			    			    	<div ng-attr-id='TabEvaluations-{{ m.actionID }}' class='content' role='tabpanel'
			    			    	ng-attr-aria-labelledby='TabEvaluations-{{ m.actionID }}-heading'
			    			    	style='background-color:#c6c6c6; padding:0px;'>
			    			    		<div class='row' style='padding: 10px 0px; background-color:#dadada; margin-bottom:5px;'>
			    			    			<span aria-hidden='true' style='font-size: 1rem;' class='right glyphicon glyphicon-share-alt'></span>
			    			    			<a class='a-wrap' href='#' ng-click='centreMap(m.actionID, m._type)'>	
			    			    				<span aria-hidden='true' style='font-size: 1rem; padding-left:7px;' class='right glyphicon glyphicon-screenshot'></span>
			    			    			</a>
			    			    			<div style='margin-bottom:0px;' class='switch round right'>
										 		<input id='inputtabEvaluation-{{ m.actionID }}' ng-checked='m.prisEnCompte === 1 ? \"checked\": \"\"' type='checkbox' onclick='prisEnCompteEvaluation1_(this)'>
										 		<label style='height: 1rem; width:2rem;' class='clear labelGreen' for='inputtabEvaluation-{{ m.actionID }}'></label>
											</div>
			    			    		</div>
			    			    		<p class='withoutBottom' style='padding: 0 0.9375rem;'> 
			    			    			<span style='font-weight:bold;'>
			    			    				{{ m.Name }} :
			    			    			</span> 
			    			    			<span>
			    			    				{{ m.messageEvaluation || m.messageAlerte }}
			    			    			</span>	
			    			    		</p>
			    			    	</div>
			    			  	</li>
			    			</ul> 
		    		</div>
			  	</li>
			</ul>		
		</ul>
	</li>
	<li class='has-submenu'>
		<a href='#'><img src='img/objets.svg' style='position:relative; left: -5px; width: 16px; height: 16px;'>Objets</a>
		<ul id='markerList' class='right-submenu'>
			<li class='back'> <a href='#'>Précédent</a> </li>
			<li class='backAll'><a href='#' onclick='fermerTousRightMenu()'>
        		<span aria-hidden='true' class='glyphicon glyphicon-remove'></span></a>
        	</li>
			<li> <label>Markers

			</label>
			</li>
			<li ng-repeat='ma in markers' class='has-submenu'>
				<a href='#'>{{ ma.name }}</a>
				<ul ng-attr-id='markerShowProperties-{{ ma.objetID }}'  class='right-submenu'>
					<li class='back'>
						<a href='#'>Précédent</a>
					</li>
					<li class='backAll'>
						<a href='#' onclick='fermerTousRightMenu()' style='padding: 0.66667rem !important; background: none !important;'>
		        			<span aria-hidden='true' class='glyphicon glyphicon-remove'></span>
		        		</a>
		        	</li>
					<li>
						<style>
							a.disabled{
								pointer-events: none;
								opacity: 0;
							}
						</style>
						<label style='position:relative'>{{ ma.name }}
							<a href='#' ng-click='centreMapMerker(ma.objetID)' style='position:relative;top:4px; float:right; border-top:none;padding:0;'><span aria-hidden='true' class='glyphicon glyphicon-screenshot'></span></a>
							<a ng-href='".get_link()."objet/{{ ma.objetID }}' target=_blank  ".(!$_SESSION['editObject']?'class="disabled"':'')."  style='position:relative; top:4px; float:right;border-top:none;padding:0;'><span aria-hidden='true' class='glyphicon glyphicon-cog'></span></a>

						</label>
					</li>
					<ul class='tabs' role='tablist' data-tab'>
						<!--Tab Info-->
					 	<li class='tab-title active'>
				  			<a ng-href='#tab-{{ ma.objetID  }}-info' ng-attr-id='a_tab-{{ ma.objetID  }}-info' onclick='openCloseTabs(this)'>
				  				Infos
				  			</a>
					 	</li>
					 	<!--Tab Alertes-->
					  	<li class='tab-title'>
				  			<a ng-href='#tab-{{ ma.objetID }}-alerte' ng-attr-id='a_tab-{{ ma.objetID  }}-alerte' onclick='openCloseTabs(this)'>
 								Alertes
				  			</a>
					 	</li>
						<!--Tab Messages-->
					  	<li class='tab-title'>
				  			<a ng-href='#tab-{{ ma.objetID }}-message' ng-attr-id='a_tab-{{ ma.objetID  }}-message' onclick='openCloseTabs(this)'>
				  				Messages
				  			</a>
					 	</li>
					 	<!--Tab Evaluations-->
					  	<li class='tab-title' >
				  			<a ng-href='#tab-{{ ma.objetID }}-evaluation' ng-attr-id='a_tab-{{ ma.objetID  }}-evaluation' onclick='openCloseTabs(this)'>
				  				Avis
				  			</a>
					 	</li>		
					 	<!--Tab Maintanance-->
					  	<li class='tab-title'>
				  			<a ng-href='#tab-{{ ma.objetID }}-maintanance' ng-attr-id='a_tab-{{ ma.objetID  }}-maintanance' onclick='openCloseTabs(this)'>
				  				Maintenances
				  			</a>
					 	</li>		
			 			<!--Tab histotique-->
			 		  	<li class='tab-title'>
			 	  			<a ng-href='#tab-{{ ma.objetID }}-historique' ng-attr-id='a_tab-{{ ma.objetID  }}-historique' onclick='openCloseTabs(this)'>
			 	  				Historique
			 	  			</a>
			 		 	</li>	 	
					</ul>
					<div class='tabs-content tabs-content-objets' style=''>
						<!--Section Info-->

						<section id='infoSection' style='background-color:#c8c6c7 !important;' class='content active tabs info' ng-attr-id='tab-{{ ma.objetID }}-info'>
							<span style='position:relative; padding:0 2px;'>
								<img ng-src='{{ ma.info.image || ma.info.typeImage }}' alt='Non Image' style='width:100%;'>
							</span>
							<label>Propiétaire</label>
							<label>{{ma.info.societeName}}</label>
							<label>Type</label>
							<label>{{ma.info.nameType}}</label>
							<label>Reference</label>
							<label>{{ma.info.alphaID}}</label>
							<label>Description</label>
							<label>{{ma.info.description}}</label>
							<label>Date de mise en service</label>
							<label style='background-color: #d1a195 !important;'>{{ ma.info.dateActive }}</label>
							<label>Localisation</label>
							<label> 
								<address style='margin-bottom: 0px !important;'>
								{{ ma.info.adresse }}
								</address>
							 </label>
							<label >Latitude</label>
							<label>{{ ma.info.objLat }}</label>
							<label>Longitude</label>
							<label>{{ ma.info.objLong }}</label>

							
							<style>
								#ficheTechnique:hover, #ficheTechnique:focus{
									text-decoration: none;
								}
							</style>
							<div ng-attr-id='ficheproduitModal-{{ ma.info.objetID }}' class='reveal-modal' data-reveal aria-labelledby='firstModalTitle' aria-hidden='true' role='dialog'>
								<embed style='width:100%; max-height: 50% !important; height: 20rem;' ng-src='{{ ma.info.ficheProduit }}'>
							</div>	
							<a id='ficheTechnique' ng-href='{{ ma.info.ficheProduit }}' target='_blank' style='color:black;text-align:center;color:#dbdbdb; border-bottom:0px !important'>
								<span aria-hidden='true' style='font-size:15px; color:#dbdbdb;' class='glyphicon glyphicon-plus'></span>
								Fiche Technique
							</a>					
						</section>
						<!-- Maintanance -->
						<style>
							section.maintanance > label:nth-child(odd){
								background-color: #c8c6c7 !important;
								font-family: 'Myriad Pro Bold' !important;
								font-size: 1.1rem !important;
							}
							section.maintanance > label:nth-child(even){
								background-color: #dbdbdb !important;
							}
							section.maintanance > label{
								color: black !important;
								border-top: 0px !important;
							}
						</style>
						<section ng-controller='InfoTabController' class='content maintanance' ng-attr-id='tab-{{ ma.objetID }}-maintanance'
						style='padding:0px !important;'>
							<label>Prochaine Maintenance</label>
							<label style='background-color:#d1a195 !important;'>{{ pDate }}</label>
							<label>Derniére Maintenance</label>
							<label>17/9/2015</label>
							<label>Reference</label>
							<label>Fréquence de la Maintenance</label>
							<style>
								.inputTabMaintenanceInline > input{
									display: inline-block;
									height:1.8rem !important;
									padding-top: 2.5px !important;
									margin-right: 2.9px !important;
								}
							</style>
							<label class='inputTabMaintenanceInline'>
								<input type='number' style='width: 12% !important;' ng-model='fJour' min='0' max='31'>Jours</input>
								<input type='number' style='width: 12% !important;' ng-model='fMois' min='0' max='12'>Mois</input>
								<input  type='number' style='width: 12% !important;' ng-model='fAns' min='0'>Ans</input>
								<a href='#' ng-click='dd()' style='margin-left: 5px !important; display:inline; width: 20% !important;' >Actualiser</a>
							</label>
							<ul class='accordion tabHistoriqueScroll' data-accordion role='tablist'>
			    			 	<li class='accordion-navigation'>
			    		    		<a href='#TabMaintenance' role='tab' id='TabMaintenance-heading' 
			    		    		aria-controls='TabMaintenance'>
			    		    			<label style='text-transform: capitalize; font-size: 12px; color: #dadada;'> 
				    		    			Maintenance 3
				    		    			<span style='font-size:9px; text-transform: lowercase; font-style: italic;'> 
											17/9/2015					    		    				
				    		    			</span> 	
			    		    			</label>
			    		    		</a>
			    			    	<div id='TabMaintenance' class='content' role='tabpanel'
			    			    	aria-labelledby='TabMaintenance-heading'
			    			    	style='background-color:#c6c6c6; padding:0px;'>
			    			    		<div class='row' style='padding: 10px 0px; background-color:#dadada; margin-bottom:5px;'>
			    			    			<span aria-hidden='true' style='font-size: 1rem;' class='right glyphicon glyphicon-share-alt'></span>
			    			    			<a class='a-wrap' href='#'>	
			    			    				<span aria-hidden='true' style='font-size: 1rem; padding-left:7px;' class='right glyphicon glyphicon-screenshot'></span>
			    			    			</a>
			    			    			<div style='margin-bottom:0px;' class='switch round right'>
										 		<input id='inputTabMaintenance' type='checkbox' >
										 		<label style='height: 1rem; width:2rem;' class='clear labelGreen' for='inputTabMaintenance'></label>
											</div>
			    			    		</div>
			    			    		<p class='withoutBottom' style='padding: 0 0.9375rem;'> 
			    			    			<span style='font-weight:bold;'>
			    			    				Utilisateur :
			    			    			</span> 
			    			    			<span>
			    			    				Ce despositif est super
			    			    			</span>	
			    			    		</p>
			    			    	</div>
			    			  	</li>
			    			  	<li class='accordion-navigation'>
			    		    		<a href='#TabMaintenance2' role='tab' id='TabMaintenance2-heading' 
			    		    		aria-controls='TabMaintenance2'>
			    		    			<label style='text-transform: capitalize; font-size: 12px; color: #dadada;'> 
				    		    			Maintenance 2
				    		    			<span style='font-size:9px; text-transform: lowercase; font-style: italic;'> 
											17/9/2014					    		    				
				    		    			</span> 	
			    		    			</label>
			    		    		</a>
			    			    	<div id='TabMaintenance2' class='content' role='tabpanel'
			    			    	aria-labelledby='TabMaintenance2-heading'
			    			    	style='background-color:#c6c6c6; padding:0px;'>
			    			    		<div class='row' style='padding: 10px 0px; background-color:#dadada; margin-bottom:5px;'>
			    			    			<span aria-hidden='true' style='font-size: 1rem;' class='right glyphicon glyphicon-share-alt'></span>
			    			    			<a class='a-wrap' href='#'>	
			    			    				<span aria-hidden='true' style='font-size: 1rem; padding-left:7px;' class='right glyphicon glyphicon-screenshot'></span>
			    			    			</a>
			    			    			<div style='margin-bottom:0px;' class='switch round right'>
										 		<input id='inputTabMaintenance2' type='checkbox'>
										 		<label style='height: 1rem; width:2rem;' class='clear labelGreen' for='inputTabMaintenance2'></label>
											</div>
			    			    		</div>
			    			    		<p class='withoutBottom' style='padding: 0 0.9375rem;'> 
			    			    			<span style='font-weight:bold;'>
			    			    				Utilisateur :
			    			    			</span> 
			    			    			<span>
			    			    				Ce despositif est super
			    			    			</span>	
			    			    		</p>
			    			    	</div>
			    			  	</li>
			    			  	<li class='accordion-navigation'>
			    		    		<a href='#TabMaintenance3' role='tab' id='TabMaintenance3-heading' 
			    		    		aria-controls='TabMaintenance3'>
			    		    			<label style='text-transform: capitalize; font-size: 12px; color: #dadada;'> 
				    		    			Maintenance 1
				    		    			<span style='font-size:9px; text-transform: lowercase; font-style: italic;'> 
											17/9/2013					    		    				
				    		    			</span> 	
			    		    			</label>
			    		    		</a>
			    			    	<div id='TabMaintenance3' class='content' role='tabpanel'
			    			    	aria-labelledby='TabMaintenance-heading'
			    			    	style='background-color:#c6c6c6; padding:0px;'>
			    			    		<div class='row' style='padding: 10px 0px; background-color:#dadada; margin-bottom:5px;'>
			    			    			<span aria-hidden='true' style='font-size: 1rem;' class='right glyphicon glyphicon-share-alt'></span>
			    			    			<a class='a-wrap' href='#'>	
			    			    				<span aria-hidden='true' style='font-size: 1rem; padding-left:7px;' class='right glyphicon glyphicon-screenshot'></span>
			    			    			</a>
			    			    			<div style='margin-bottom:0px;' class='switch round right'>
										 		<input id='inputTabMaintenance' type='checkbox'>
										 		<label style='height: 1rem; width:2rem;' class='clear labelGreen' for='inputTabMaintenance'></label>
											</div>
			    			    		</div>
			    			    		<p class='withoutBottom' style='padding: 0 0.9375rem;'> 
			    			    			<span style='font-weight:bold;'>
			    			    				Utilisateur :
			    			    			</span> 
			    			    			<span>
			    			    				Ce despositif est super
			    			    			</span>	
			    			    		</p>
			    			    	</div>
			    			  	</li>
			    			</ul> 
						</section>
						<!--Section Messages-->
						<section ng-controller='InfoTabController' class='content' ng-attr-id='tab-{{ ma.objetID }}-message'
						style='padding:0px !important;'>			
							<div class='row' style='background-color: #c8c6c7; padding-bottom: 12px;'>
								<div class='small-4 large-4 columns divColumnViewHistMessage' style=''>
									<span class='textViewHistMessage' style='margin-top:5px;'></span>
									<span aria-hidden='true' style='color:#313133;' class='glyphicon glyphicon-envelope iconMViewHistMessage'></span>		
								</div>						
								<div ng-controller='etatDuParkController' class='small-4 large-4  columns divColumnViewHistMessage'>
										<span class='textViewHistMessage cirlceLeftCenter'>Au total</span>
										<div class='circle cirlceLeftCenter'>
											<div class='circleBorderBlack'>	
												<span class='viewCircle'>{{ data[ma.objetID].message.message }}</span>						
											</div>	
										</div>
								</div>
								<div ng-controller='etatDuParkController' class='small-4 large-4  columns divColumnViewHistMessage'>	
									<span class='textViewHistMessage cirlceLeftCenter' style='margin-top:5px;'>Non Lu</span>
									<div style='background-color:#95c11f;' class='circle cirlceLeftCenter'>
										<span class='viewCircle' style='font-weight: bold;'>{{ data[ma.objetID].message.totalMessage }}</span>						
									</div>				
								</div>
								<div class='small-6 large-6 columns'>
									
								</div>
							</div>
							<ul class='accordion tabMessagesScroll' data-accordion role='tablist'>
			    			 	<li class='accordion-navigation' ng-repeat='m in action.message | convertToArray | orderBy : \"-createdTime\"' ng-if='m.objetID == ma.objetID'>
			    		    		<a ng-href='#TabMessages-{{ m.actionID }}' role='tab' ng-attr-id='TabMessages-{{ m.actionID }}-heading' 
			    		    		ng-attr-aria-controls='TabMessages-{{ m.actionID }}'>
			    		    			<label ng-style='{\"text-transform\": \"capitalize\", \"font-size\": \"12px\", \"color\": m.prisEnCompte == 1 ? \"#999999\": \"#dadada\" }'> 
			    		    			<span style='  text-transform: lowercase; '> 
			    		    				{{ m.createdTime.slice(0, 10) }} à {{ m.createdTime.slice(11, 19) }}
			    		    				<span aria-hidden='true'
			    		    				 ng-style='{\"color\": m.prisEnCompte == 1 ? \"#999999\": \"#dadada\", \"margin-top\": \"3px\", \"float\":\"right\", \"font-size\":\"15px\"}'
			    		    				 class='glyphicon glyphicon-envelope'></span>
			    		    				 <i ng-if='!!m.messageAlerte' class='fi-alert' style='margin-right: 5px; color:#ff6155; float:right; font-size: 18px; margin-top:1px; margin-left: 2px;' ></i>
			    		    			</span> 	
		    		    			</label>
			    		    		</a>
			    			    	<div ng-attr-id='TabMessages-{{ m.actionID }}' class='content' role='tabpanel'
			    			    	ng-attr-aria-labelledby='TabMessages-{{ m.actionID }}-heading'
			    			    	style='background-color:#c6c6c6; padding:0px;'>
			    			    		<div class='row' style='padding: 10px 0px; background-color:#dadada; margin-bottom:5px;'>
			    			    			<span aria-hidden='true' style='font-size: 1rem;' class='right glyphicon glyphicon-share-alt'></span>
			    			    			<a class='a-wrap' href='#' ng-click='centreMap(m.actionID, m._type)'>	
			    			    				<span aria-hidden='true' style='font-size: 1rem; padding-left:7px;' class='right glyphicon glyphicon-screenshot'></span>
			    			    			</a>
			    			    			<div style='margin-bottom:0px;' class='switch round right'>
										 		<input id='inputTabMessages-{{ m.actionID }}' ng-checked='m.prisEnCompte === 1 ? \"checked\": \"\"' type='checkbox' onclick='prisEnCompteEvaluation_(this)'>
										 		<label style='height: 1rem; width:2rem;' class='clear labelGreen' for='inputTabMessages-{{ m.actionID }}'></label>
											</div>
			    			    		</div>
			    			    		<p class='withoutBottom' style='padding: 0 0.9375rem;'> 
			    			    			<span style='font-weight:bold;'>
			    			    				{{ m.Name }} :
			    			    			</span> 
			    			    			<span>
			    			    				{{ m.messageEvaluation || m.messageAlerte }}
			    			    			</span>	
			    			    		</p>
			    			    	</div>
			    			  	</li>
			    			</ul> 
						</section>
						<!--Section Alertes-->
						<section ng-controller='InfoTabController' class='content' ng-attr-id='tab-{{ ma.objetID }}-alerte'
						style='padding:0px !important;'>			
							<div class='row' style='background-color: #c8c6c7; padding-bottom: 12px; '>
								<div class='small-4 large-4 columns divColumnViewHistMessage' style=''>
									<span class='textViewHistMessage' style='margin-top:5px;'></span>
									
									
									<span class='iconMViewAlerte'>
										<i class='fi-alert' style='color:#313133'></i>
									</span>		
								</div>						
								<div ng-controller='etatDuParkController' class='small-4 large-4  columns divColumnViewHistMessage'>
									<span class='textViewHistMessage cirlceLeftCenter'>Au total</span>
									<div class='circle cirlceLeftCenter'>
										<div class='circleBorderBlack'>	
											<span class='viewCircle'>{{ data[ma.objetID].alerte.alerte }}</span>						
										</div>	
									</div>
								</div>
								<div ng-controller='etatDuParkController' class='small-4 large-4  columns divColumnViewHistMessage'>	
									<span class='textViewHistMessage cirlceLeftCenter' style='margin-top:5px;'>Non Lu</span>
									<div style='background-color:#95c11f;' class='circle cirlceLeftCenter'>
										<span class='viewCircle' style='font-weight: bold;'>{{ data[ma.objetID].alerte.totalAlerte }}</span>						
									</div>				
								</div>
								<div class='small-6 large-6 columns'>
									
								</div>
							</div>
							<ul class='accordion tabAlerteScroll' data-accordion role='tablist'>
			    			 	<li class='accordion-navigation' ng-repeat='m in action.alerte | convertToArray | orderBy : \"-createdTime\"'  ng-if='m.objetID == ma.objetID'>
			    		    		<a ng-href='#TabAlertes-{{ m.actionID }}' role='tab' ng-attr-id='TabAlertes-{{ m.actionID }}-heading' 
			    		    		ng-attr-aria-controls='TabAlertes-{{ m.actionID }}'>
			    		    			<label ng-style='{\"text-transform\": \"capitalize\", \"font-size\": \"12px\", \"color\": m.prisEnCompte == 1 ? \"#999999\": \"#ff6155\" }'> 
				    		    			<i class='fi-alert' style='color:#ff6155; float:right; font-size: 18px; margin-top:1px; margin-left: 2px;' ></i>
				    		    			<span style='  text-transform: lowercase; '> 
				    		    				{{ m.createdTime.slice(0, 10) }} à {{ m.createdTime.slice(11, 19) }}
				    		    				<span ng-if='!!m.messageAlerte' aria-hidden='true' ng-style='{\"color\": m.prisEnCompte == 1 ? \"#999999\": \"#dadada\", \"margin-top\": \"3px\", \"float\":\"right\", \"font-size\":\"15px\"}' class='glyphicon glyphicon-envelope'></span> 
				    		    			</span> 	
			    		    			</label>    		    			
			    		    		</a>
			    			    	<div ng-attr-id='TabAlertes-{{ m.actionID }}' class='content' role='tabpanel'
			    			    	ng-attr-aria-labelledby='TabAlertes-{{ m.actionID }}-heading'
			    			    	style='background-color:#c6c6c6; padding:0px;'>
			    			    		<div class='row' style='padding: 10px 0px; background-color:#dadada; margin-bottom:5px;'>
			    			    			<span aria-hidden='true' style='font-size: 1rem;' class='right glyphicon glyphicon-share-alt'></span>
			    			    			<a class='a-wrap' href='#' ng-click='centreMap(m.actionID, m._type)'>	
			    			    				<span aria-hidden='true' style='font-size: 1rem; padding-left:7px;' class='right glyphicon glyphicon-screenshot'></span>
			    			    			</a>
			    			    			<div style='margin-bottom:0px;' class='switch round right'>
										 		<input id='inputTabAlertes-{{ m.actionID }}' type='checkbox' ng-checked='m.prisEnCompte === 1 ? \"checked\": \"\"' onclick='prisEnCompteAlerte_(this)'>
										 		<label style='height: 1rem; width:2rem;' class='clear labelGreen' for='inputTabAlertes-{{ m.actionID }}'></label>
											</div>
			    			    		</div>
			    			    		<p class='withoutBottom' style='padding: 0 0.9375rem;'> 
			    			    			<span style='font-weight:bold;'>
			    			    				{{ m.Name }} :
			    			    			</span> 
			    			    			<span>
			    			    				{{ m.messageEvaluation || m.messageAlerte }}
			    			    			</span>	
			    			    		</p>
			    			    	</div>
			    			  	</li>
			    			</ul> 
						</section>
						<!--Section Historique-->
						<section ng-controller='InfoTabController' class='content' ng-attr-id='tab-{{ ma.objetID }}-historique'
						style='padding:0px !important;'>			
							<ul class='accordion tabHistoriqueScroll' data-accordion role='tablist'>
			    			 	<li class='accordion-navigation' ng-repeat='m in systemActions | convertToArray | orderBy : \"-createdTime\"' ng-if='m.objetID == ma.objetID'>
			    		    		<a ng-href='#TabAlertes-{{ m.actionID }}' role='tab' ng-attr-id='TabAlertes-{{ m.actionID }}-heading' 
			    		    		ng-attr-aria-controls='TabAlertes-{{ m.actionID }}'>
			    		    			<label
			    		    			ng-style='{\"text-transform\": \"capitalize\", \"font-size\": \"12px\", \"color\": m.prisEnCompte == 1 ? \"#999999\": \"#dadada\" }'> 
				    		    			<span style='  text-transform: lowercase; '> 
				    		    				{{ m.createdTime.slice(0, 10) }} à {{ m.createdTime.slice(11, 19) }}
				    		    				<span ng-if='m._type == \"message\"' aria-hidden='true' ng-style='{\"color\": m.prisEnCompte == 1 ? \"#999999\": \"#dadada\", \"margin-top\": \"3px\", \"float\":\"right\", \"font-size\":\"15px\"}' class='glyphicon glyphicon-envelope'></span> 
				    		    			</span>
				    		    			<i ng-if='m._type == \"alerte\"' class='fi-alert' style='color:#ff6155; float:right; font-size: 18px; margin-top:1px; margin-left: 2px;margin-right: 5px;' ></i>
				    		    			<span ng-if='m._type == \"evaluation\"' style='float:right;margin-right: 3px;'>
			    		    					<img ng-if='m.evaluation == 3' src='img/smile-happy.svg' style='width: 20px; height: 20px;'>
			    		    					<img ng-if='m.evaluation == 2' src='img/smile-mid.svg' style='width: 20px; height: 20px;'>
			    		    					<img ng-if='m.evaluation == 1' src='img/smile-sad.svg' style='width: 20px; height: 20px;'>
			    		    				</span>	
			    		    			</label>
			    		    		</a>
			    			    	<div ng-attr-id='TabAlertes-{{ m.actionID }}' class='content' role='tabpanel'
			    			    	ng-attr-aria-labelledby='TabAlertes-{{ m.actionID }}-heading'
			    			    	style='background-color:#c6c6c6; padding:0px;'>
			    			    		<div class='row' style='padding: 10px 0px; background-color:#dadada; margin-bottom:5px;'>
			    			    			<span aria-hidden='true' style='font-size: 1rem;' class='right glyphicon glyphicon-share-alt'></span>
			    			    			<a class='a-wrap' href='#' ng-click='centreMap(m.actionID, m._type)'>	
			    			    				<span aria-hidden='true' style='font-size: 1rem; padding-left:7px;' class='right glyphicon glyphicon-screenshot'></span>
			    			    			</a>
			    			    			<div style='margin-bottom:0px;' class='switch round right'>
										 		<input id='inputTabHAlertes-{{ m.actionID }}' ng-checked='m.prisEnCompte === 1 ? \"checked\": \"\"' type='checkbox' onclick='prisEnCompteAlerte_(this)' ng-if='m._type == \"alerte\"'>
										 		<label style='height: 1rem; width:2rem;' class='clear labelGreen' for='inputTabHAlertes-{{ m.actionID }}' ng-if='m._type == \"alerte\"'></label>

										 		<input id='inputTabHMessages-{{ m.actionID }}' ng-checked='m.prisEnCompte === 1 ? \"checked\": \"\"' type='checkbox' onclick='prisEnCompteEvaluation_(this)' ng-if='m._type == \"message\"'>
										 		<label style='height: 1rem; width:2rem;' class='clear labelGreen' for='inputTabHMessages-{{ m.actionID }}' ng-if='m._type == \"message\"'></label>

										 		<input id='inputTabHEvaluations-{{ m.actionID }}' ng-checked='m.prisEnCompte === 1 ? \"checked\": \"\"' type='checkbox' onclick='prisEnCompteEvaluation1_(this)' ng-if='m._type == \"evaluation\"'>
										 		<label style='height: 1rem; width:2rem;' class='clear labelGreen' for='inputTabHEvaluations-{{ m.actionID }}' ng-if='m._type == \"evaluation\"'></label>
											</div>
			    			    		</div>
			    			    		<p class='withoutBottom' style='padding: 0 0.9375rem;'> 
			    			    			<span style='font-weight:bold;'>
			    			    				{{ m.Name }} :
			    			    			</span> 
			    			    			<span>
			    			    				{{ m.messageEvaluation || m.messageAlerte }}
			    			    			</span>	
			    			    		</p>
			    			    	</div>
			    			  	</li>
			    			</ul> 
						</section >
						<!-- Evaluation -->
						<section ng-controller='InfoTabController' class='content' ng-attr-id='tab-{{ ma.objetID }}-evaluation'
						style='padding:0px !important;'>
							<div class='row' style='background-color: #c8c6c7; padding-bottom: 12px; '>
								<div ng-controller='etatDuParkController' class='small-3 large-3 columns' style='padding-top:20px !important;'>
									<img style='width:60px; height:60px; display:block; margin:0 auto;'  ng-src='img/{{ image }}' alt=''>
									<span ng-bind='evaluationDataXMarker[ma.objetID][\"total\"]' style='display:block;text-align:center'></span>
								</div>	
								<div class='small-1 large-1 columns' style='padding-top: 9px; padding-bottom: 5px;'>
									<img style='width:30px; height:30px; display:table-cell; align:center;'  ng-src='img/smile-happy.svg' alt=''>
									<img style='width:30px; height:30px; display:table-cell; align:center;'  ng-src='img/smile-mid.svg' alt=''>
									<img style='width:30px; height:30px; display:table-cell; align:center;'  ng-src='img/smile-sad.svg' alt=''>
								</div>
								<div ng-controller='etatDuParkController' class='small-5 large-5 columns' style='width: 43.66667% !important; padding-top: 9px; padding-bottom: 5px; 
								padding-right: 0px !important; padding-left: 0px;'>
									<style>
										._content{
											width:0%;
											height:26px;
											background-color: #c4d600;
										}
										._mid{
											width:0%;
											height:26px;
											background-color: #f1c800;
										}
										._sad{
											width:0%;
											height:26px;
											background-color: #fc604d;
										}
										.barPadding{
											margin-bottom: 5.5px !important;
										} 
									</style>
									<div class='_content barPadding' ng-style='{\"width\": (evaluationDataXMarker[ma.objetID][3]*100)/evaluationDataXMarker[ma.objetID][\"total\"]}'>
												<span ng-bind='evaluationDataXMarker[ma.objetID][3]' ng-style='{\"margin-left\": (evaluationDataXMarker[ma.objetID][3]*100)/evaluationDataXMarker[ma.objetID][\"total\"]+3}'></span>				
									</div>
									<div class='_mid barPadding' ng-style='{\"width\": (evaluationDataXMarker[ma.objetID][2]*100)/evaluationDataXMarker[ma.objetID][\"total\"]}'>
												<span ng-bind='evaluationDataXMarker[ma.objetID][2]' ng-style='{\"margin-left\": (evaluationDataXMarker[ma.objetID][2]*100)/evaluationDataXMarker[ma.objetID][\"total\"]+3}'></span>								
									</div>	
									<div class='_sad' ng-style='{\"width\": (evaluationDataXMarker[ma.objetID][1]*100)/evaluationDataXMarker[ma.objetID][\"total\"]}'>
												<span ng-bind='evaluationDataXMarker[ma.objetID][1]' ng-style='{\"margin-left\": (evaluationDataXMarker[ma.objetID][1]*100)/evaluationDataXMarker[ma.objetID][\"total\"]+3}'></span>								
									</div>
								</div>					
								<div ng-controller='etatDuParkController' class='small-1 large-1  columns divColumnViewHistMessage'>	
								</div>
							</div>
							<ul class='accordion tabEvaluationScroll' data-accordion role='tablist'>
			    			 	<li class='accordion-navigation' ng-repeat='m in action.evaluation | convertToArray | orderBy : \"-createdTime\"' ng-if='m.objetID == ma.objetID'>
			    		    		<a ng-href='#TabEvaluations-{{ m.actionID }}' role='tab' ng-attr-id='TabEvaluations-{{ m.actionID }}-heading' 
			    		    		ng-attr-aria-controls='TabEvaluations-{{ m.actionID }}'>
			    		    			<label
			    		    			ng-style='{\"text-transform\": \"capitalize\", \"font-size\": \"12px\", \"color\": m.prisEnCompte == 1 ? \"#999999\": \"#dadada\" }'> 
			    		    			<span ng-controller='etatDuParkController' style='  text-transform: lowercase; '> 
			    		    				{{ m.createdTime.slice(0, 10) }} à {{ m.createdTime.slice(11, 19) }}
											<span style='float:right;'>
			    		    					<img ng-if='m.evaluation == 3' src='img/smile-happy.svg' style='width: 20px; height: 20px;'>
			    		    					<img ng-if='m.evaluation == 2' src='img/smile-mid.svg' style='width: 20px; height: 20px;'>
			    		    					<img ng-if='m.evaluation == 1' src='img/smile-sad.svg' style='width: 20px; height: 20px;'>
			    		    				</span>	
			    		    				<span ng-if='!!m.messageEvaluation' aria-hidden='true' ng-style='{\"color\": m.prisEnCompte == 1 ? \"#999999\": \"#dadada\", \"margin-top\": \"3px\", \"float\":\"right\", \"font-size\":\"15px\"}' class='glyphicon glyphicon-envelope'></span> 
			    		    			</span> 	
		    		    			</label>
			    		    		</a>
			    			    	<div ng-attr-id='TabEvaluations-{{ m.actionID }}' class='content' role='tabpanel'
			    			    	ng-attr-aria-labelledby='TabEvaluations-{{ m.actionID }}-heading'
			    			    	style='background-color:#c6c6c6; padding:0px;'>
			    			    		<div class='row' style='padding: 10px 0px; background-color:#dadada; margin-bottom:5px;'>
			    			    			<span aria-hidden='true' style='font-size: 1rem;' class='right glyphicon glyphicon-share-alt'></span>
			    			    			<a class='a-wrap' href='#' ng-click='centreMap(m.actionID, m._type)'>	
			    			    				<span aria-hidden='true' style='font-size: 1rem; padding-left:7px;' class='right glyphicon glyphicon-screenshot'></span>
			    			    			</a>
			    			    			<div style='margin-bottom:0px;' class='switch round right'>
										 		<input id='inputtabEvaluation-{{ m.actionID }}' ng-checked='m.prisEnCompte === 1 ? \"checked\": \"\"' type='checkbox' onclick='prisEnCompteEvaluation1_(this)'>
										 		<label style='height: 1rem; width:2rem;' class='clear labelGreen' for='inputtabEvaluation-{{ m.actionID }}'></label>
											</div>
			    			    		</div>
			    			    		<p class='withoutBottom' style='padding: 0 0.9375rem;'> 
			    			    			<span style='font-weight:bold;'>
			    			    				{{ m.Name }} :
			    			    			</span> 
			    			    			<span>
			    			    				{{ m.messageEvaluation || m.messageAlerte }}
			    			    			</span>	
			    			    		</p>
			    			    	</div>
			    			  	</li>
			    			</ul> 
						</section>

						<div  style='display: block; clear: both;'></div>					
					</div>
				</ul>
			</li>
		</ul>
	</li>
	<li class='has-submenu'>
		<a href='#'><img src='img/history.svg' style='position:relative;   left: -5px; width: 16px; height: 16px;'>Historique d'actions</a>
		<ul id='historiqueList' class='right-submenu'>
			<li class='back'> <a href='#'>Précédent</a> </li>
			<li class='backAll'><a href='#' onclick='fermerTousRightMenu()'>
        		<span aria-hidden='true' class='glyphicon glyphicon-remove'></span></a>
        	</li>
			<li> <label>Historique d'actions</label> </li>
			<li class='has-submenu'>
				<a href='#'><span aria-hidden='true' style='color:#ff6155;' class='glyphicon glyphicon-alert'></span>Alertes</a>
				<ul id='historiqueList-Alertes'  class='right-submenu'>
					<li class='back'>
						<a href='#'>Précédent</a>
					</li>
					<li class='backAll'><a href='#' onclick='fermerTousRightMenu()'>
		        		<span aria-hidden='true' class='glyphicon glyphicon-remove'></span></a>
		        	</li>
					<li>
						<label>HistoriqueAlertes</label>
					</li>
					<ul class='accordion' data-accordion role='tablist'>
	    			 	<li class='accordion-navigation' ng-repeat='m in action.alerte | convertToArray | orderBy : \"-createdTime\"' ng-if='m._type == \"alerte\" ' >
	    		    		<a ng-href='#HistoriqueAlertes-{{ m.actionID }}' role='tab' ng-attr-id='HistoriqueAlertes-{{ m.actionID }}-heading' 
	    		    		ng-attr-aria-controls='HistoriqueAlertes-{{ m.actionID }}'>
	    		    			<label ng-if='!!m.messageAlerte' 
	    		    			ng-style='{\"text-transform\": \"capitalize\", \"font-size\": \"12px\", \"color\": m.prisEnCompte == 1 ? \"#999999\": \"#dadada\" }'> 
		    		    			{{ m.objetName }} 
		    		    			<span aria-hidden='true' style='color:#dadada; margin-top: 3px; float:right; font-size:15px;' class='glyphicon glyphicon-envelope'></span> 
		    		    			<span style='font-size:9px; text-transform: lowercase; font-style: italic;'> 
		    		    				{{ m.createdTime.slice(0, 10) }} à {{ m.createdTime.slice(11, 19) }} 
		    		    			</span> 	
	    		    			</label>
	    		    			<label ng-if='!!!m.messageAlerte' ng-style='{\"text-transform\": \"capitalize\", \"font-size\": \"12px\", \"color\": m.prisEnCompte == 1 ? \"#999999\": \"#dadada\" }'> 
		    		    			{{ m.objetName }} 
		    		    			<span style='font-size:9px; text-transform: lowercase; font-style: italic;'> 
		    		    				{{ m.createdTime.slice(0, 10) }} à {{ m.createdTime.slice(11, 19) }} 
		    		    			</span> 	
	    		    			</label>
	    		    		</a>
	    			    	<div ng-attr-id='HistoriqueAlertes-{{ m.actionID }}' class='content' role='tabpanel'
	    			    	ng-attr-aria-labelledby='HistoriqueAlertes-{{ m.actionID }}-heading'
	    			    	style='background-color:#c6c6c6; padding:0px;'>
	    			    		<div class='row' style='padding: 10px 0px; background-color:#dadada; margin-bottom:5px;'>
	    			    			<span aria-hidden='true' style='font-size: 1rem;' class='right glyphicon glyphicon-share-alt'></span>
	    			    			<a class='a-wrap' href='#' ng-click='centreMap(m.actionID, m._type)'>	
	    			    				<span aria-hidden='true' style='font-size: 1rem; padding-left:7px;' class='right glyphicon glyphicon-screenshot'></span>
	    			    			</a>
	    			    			<div style='margin-bottom:0px;' class='switch round right'>
								 		<input id='inputHistoriqueAlertes-{{ m.actionID }}' ng-checked='m.prisEnCompte === 1 ? \"checked\": \"\"' type='checkbox' onclick='prisEnCompteAlerte_(this)'>
								 		<label style='height: 1rem; width:2rem;' class='clear labelGreen' for='inputHistoriqueAlertes-{{ m.actionID }}'></label>
									</div>
	    			    		</div>
	    			    		<p class='withoutBottom' style='padding: 0 0.9375rem;'> 
	    			    			<span style='font-weight:bold;'>
	    			    				{{ m.Name }} :
	    			    				<span ng-if='!!!m.messageAlerte'>Sans Message</span>
	    			    			</span> 
	    			    			<span>
	    			    				{{ m.messageEvaluation || m.messageAlerte }}
	    			    			</span>	
	    			    		</p>
	    			    	</div>
	    			  	</li>
	    			</ul> 
				</ul>	
			</li>
			<li class='has-submenu'>
				<a href='#'><span aria-hidden='true' style='color:#95c11f;' class='glyphicon glyphicon-envelope'></span>Messages</a>
				<ul id='historiqueList-Messages'  class='right-submenu'>
					<li class='back'>
						<a href='#'>Précédent</a>
					</li>
					<li class='backAll'><a href='#' onclick='fermerTousRightMenu()'>
		        		<span aria-hidden='true' class='glyphicon glyphicon-remove'></span></a>
		        	</li>
					<li>
						<label>HistoriqueMessages</label>
					</li>
					<ul class='accordion' data-accordion role='tablist'>
	    			 	<li class='accordion-navigation' ng-repeat='m in action.message | convertToArray | orderBy : \"-createdTime\"' ng-if='m._type != \"alerte\"'>
	    		    		<a ng-href='#HistoriqueMessages-{{ m.actionID }}' role='tab' ng-attr-id='HistoriqueMessages-{{ m.actionID }}-heading' 
	    		    		ng-attr-aria-controls='HistoriqueMessages-{{ m.actionID }}'>
	    		    			<label ng-if='m.prisEnCompte == 0' 
	    		    			style='text-transform: capitalize; font-size:12px; color:#dadada;'> 
		    		    			{{ m.objetName }} 
		    		    			<span style='font-size:9px; text-transform: lowercase; font-style: italic;'> 
		    		    				{{ m.createdTime.slice(0, 10) }} à {{ m.createdTime.slice(11, 19) }}
		    		    				<span aria-hidden='true' style='color:#dadada; margin-top: 3px; float:right; font-size:15px;' class='glyphicon glyphicon-envelope'></span> 
		    		    			</span> 	
	    		    			</label>
	    		    			<label ng-if='m.prisEnCompte == 1' style='text-transform: capitalize; font-size:12px; color:#999999;'> 
		    		    			{{ m.objetName }} 
		    		    			<span style='font-size:9px; text-transform: lowercase; font-style: italic;'> 
		    		    				{{ m.createdTime.slice(0, 10) }} à {{ m.createdTime.slice(11, 19) }} 
		    		    				<span aria-hidden='true' style='color:#dadada; margin-top: 3px; float:right; font-size:15px;' class='glyphicon glyphicon-envelope'></span> 
		    		    			</span> 	
	    		    			</label>
	    		    		</a>
	    			    	<div ng-attr-id='HistoriqueMessages-{{ m.actionID }}' class='content' role='tabpanel'
	    			    	ng-attr-aria-labelledby='HistoriqueMessages-{{ m.actionID }}-heading'
	    			    	style='background-color:#c6c6c6; padding:0px;'>
	    			    		<div class='row' style='padding: 10px 0px; background-color:#dadada; margin-bottom:5px;'>
	    			    			<span aria-hidden='true' style='font-size: 1rem;' class='right glyphicon glyphicon-share-alt'></span>
	    			    			<a class='a-wrap' href='#' ng-click='centreMap(m.actionID, m._type)'>	
	    			    				<span aria-hidden='true' style='font-size: 1rem; padding-left:7px;' class='right glyphicon glyphicon-screenshot'></span>
	    			    			</a>
	    			    			<div style='margin-bottom:0px;' class='switch round right'>
								 		<input id='inputHistoriqueMessages-{{ m.actionID }}' ng-checked='m.prisEnCompte === 1 ? \"checked\": \"\"' type='checkbox' onclick='prisEnCompteEvaluation_(this)'>
								 		<label style='height: 1rem; width:2rem;' class='clear labelGreen' for='inputHistoriqueMessages-{{ m.actionID }}'></label>
									</div>
	    			    		</div>
	    			    		<p class='withoutBottom' style='padding: 0 0.9375rem;'> 
	    			    			<span style='font-weight:bold;'>
	    			    				{{ m.Name }} :
	    			    			</span> 
	    			    			<span>
	    			    				{{ m.messageEvaluation || m.messageAlerte }}
	    			    			</span>	
	    			    		</p>
	    			    	</div>
	    			  	</li>
	    			</ul> 
				</ul>	
			</li>
			<li class='has-submenu'>
				<a href='#'><img src='img/smile-happy.svg' style='position:relative; top: -2px; margin-right: 5px; width: 16px; height: 16px;'>Evaluations</a>
				<ul id='historiqueList-Evaluations'  class='right-submenu'>
					<li class='back'>
						<a href='#'>Précédent</a>
					</li>
					<li class='backAll'><a href='#' onclick='fermerTousRightMenu()'>
		        		<span aria-hidden='true' class='glyphicon glyphicon-remove'></span></a>
		        	</li>
					<li>
						<label>HistoriqueEvaluations</label>
					</li>
					<ul class='accordion tabEvaluationScroll' data-accordion role='tablist'>
	    			 	<li class='accordion-navigation' ng-repeat='m in action.evaluation | convertToArray | orderBy : \"-createdTime\"'>
	    		    		<a ng-href='#HistoriqueEvaluations-{{ m.actionID }}' role='tab' ng-attr-id='HistoriqueEvaluations-{{ m.actionID }}-heading' 
	    		    		ng-attr-aria-controls='HistoriqueEvaluations-{{ m.actionID }}'>
	    		    			<label
	    		    			ng-style='{\"text-transform\": \"capitalize\", \"font-size\": \"12px\", \"color\": m.prisEnCompte == 1 ? \"#999999\": \"#dadada\" }'> 
	    		    			{{ m.objetName }} 
	    		    			<span ng-controller='etatDuParkController' style='font-size:9px; text-transform: lowercase; font-style: italic;'> 
	    		    				{{ m.createdTime.slice(0, 10) }} à {{ m.createdTime.slice(11, 19) }}
									<span style='float:right;'>
	    		    					<img ng-if='m.evaluation == 3' src='img/smile-happy.svg' style='width: 20px; height: 20px;'>
	    		    					<img ng-if='m.evaluation == 2' src='img/smile-mid.svg' style='width: 20px; height: 20px;'>
	    		    					<img ng-if='m.evaluation == 1' src='img/smile-sad.svg' style='width: 20px; height: 20px;'>
	    		    				</span>	
	    		    				<span ng-if='!!m.messageEvaluation' aria-hidden='true' ng-style='{\"color\": m.prisEnCompte == 1 ? \"#999999\": \"#dadada\", \"margin-top\": \"3px\", \"float\":\"right\", \"font-size\":\"15px\"}' class='glyphicon glyphicon-envelope'></span> 
	    		    			</span> 	
    		    			</label>
	    		    		</a>
	    			    	<div ng-attr-id='HistoriqueEvaluations-{{ m.actionID }}' class='content' role='tabpanel'
	    			    	ng-attr-aria-labelledby='HistoriqueEvaluations-{{ m.actionID }}-heading'
	    			    	style='background-color:#c6c6c6; padding:0px;'>
	    			    		<div class='row' style='padding: 10px 0px; background-color:#dadada; margin-bottom:5px;'>
	    			    			<span aria-hidden='true' style='font-size: 1rem;' class='right glyphicon glyphicon-share-alt'></span>
	    			    			<a class='a-wrap' href='#' ng-click='centreMap(m.actionID, m._type)'>	
	    			    				<span aria-hidden='true' style='font-size: 1rem; padding-left:7px;' class='right glyphicon glyphicon-screenshot'></span>
	    			    			</a>
	    			    			<div style='margin-bottom:0px;' class='switch round right'>
								 		<input id='HistoriqueEvaluations-{{ m.actionID }}' ng-checked='m.prisEnCompte === 1 ? \"checked\": \"\"' type='checkbox' onclick='prisEnCompteEvaluation_(this)'>
								 		<label style='height: 1rem; width:2rem;' class='clear labelGreen' for='HistoriqueEvaluations-{{ m.actionID }}'></label>
									</div>
	    			    		</div>
	    			    		<p class='withoutBottom' style='padding: 0 0.9375rem;'> 
	    			    			<span style='font-weight:bold;'>
	    			    				{{ m.Name }} :
	    			    			</span> 
	    			    			<span>
	    			    				{{ m.messageEvaluation || m.messageAlerte }}
	    			    			</span>	
	    			    		</p>
	    			    	</div>
	    			  	</li>
	    			</ul> 				
				</ul>	
			</li>
		</ul>
	</li>
</ul>
</aside>
		<script>
		
			self.google = google;
		</script>	
";
			}
			?>
		<span ng-controller="markerController">
			<leaflet class="panel" id="map" controls="controls" event-broadcast="events" lf-center="center" controls="controls" markers="markers" layers="layers"></leaflet>
		</span>
		<div class="row" id="mapCAdresse" ng-controller="markerController">
			<form style="height:90%; width:100%; display:none;">		    
          		<input style="margin-bottom: 0px; width:60% !important; float:left; height: 100% !important; display:inline !important;" type="text" placeholder="Chercher une Adresse">
         		<a style="margin-bottom: 0px; width: 40% !important; float:left; height: 100% !important; text-align: center;" href="#" class="button postfix" ng-click="rechercheAdresse()">
         			Chercher
         		</a>
		    </form>
		</div>
	</div>
	  
	<?php
	/**
	* Check if was a previous operation.
	*/
		if(isset($_SESSION['success'])){
			if($_SESSION['success']){
			/**
			* If operation and succes
			*/
	?>
		<script>
		$(document).ready(function(){
			goodalert();
		});</script>
	<?php
			} else {
			/**
			* If operation and fail.
			*/
	?>
		<script>$(document).ready(function(){
			badalert();
		});</script>
	<?php
			}
			/**
			* Reset success
			*/
			$_SESSION['success'] = null;
		}
	?>

<?php
echo"

		  </div>
		</div>
		<!-- For Foundation Icons, put this in your head -->		
		<script src='js/foundation.min.js'></script>
		<script type='text/javascript' src='".get_link()."js/foundation/foundation.accordion.js'></script>
		
		<script type='text/javascript' src='".get_link()."js/foundation/responsive-tables.js'></script>
    	<script type='text/javascript' src='".get_link()."js/vendor/modernizr.js'></script>
		<script src='js/style.js'></script>
		<script>

		  if(window.location.hash){
    $('dl.tabs dd a').each(function(){
        var hash = '#' + $(this).attr('href').split('#')[1];
        if(hash == window.location.hash){
            $(this).click();
        }
    });         
}			


		  $('#EtatAlerte').css({'max-height': window.innerHeight - 165 + 28.6 + 'px'});
		  $('#MessageEtat').css({'max-height': window.innerHeight - 130 + 'px'});
		  $('section.tabs').css({
		  	'max-height': window.innerHeight - 128 + 'px',
		  	'overflow-y':'auto', 'overflow-x': 'hidden'
		  });
		  
			$('.tabHistoriqueScroll').css({
		  		'max-height': window.innerHeight - 97 + 'px',
		  		'overflow-y':'auto', 'overflow-x': 'hidden',
	  		});

			$('.tabMessagesScroll').css({
		  		'max-height': window.innerHeight - 207 + 'px',
		  		'overflow-y':'auto', 'overflow-x': 'hidden'
	  		});
			$('.tabAlertesScroll').css({
		  		'max-height': window.innerHeight - 207 + 'px',
		  		'overflow-y':'auto', 'overflow-x': 'hidden'
	  		});
			$('.tabEvaluationScroll').css({
		  		'max-height': window.innerHeight - 213 + 'px',
		  		'overflow-y':'auto', 'overflow-x': 'hidden'
	  		});
			$('.tabAlerteScroll').css({
		  		'max-height': window.innerHeight - 190 + 'px',
		  		'overflow-y':'auto', 'overflow-x': 'hidden'
	  		});

			
		  $( window ).resize(function() {
		  	$('.tabEvaluationScroll').css({
		  		'max-height': window.innerHeight - 213 + 'px',
		  		'overflow-y':'auto', 'overflow-x': 'hidden'
	  		});			

		  	$('.tabAlertesScroll').css({
		  		'max-height': window.innerHeight - 207 + 'px',
		  		'overflow-y':'auto', 'overflow-x': 'hidden'
	  		});
		  	$('.tabMessagesScroll').css({
		  		'max-height': window.innerHeight - 207 + 'px',
		  		'overflow-y':'auto', 'overflow-x': 'hidden'
	  		});
			  	$('section.tabs').css({
			  	'max-height': window.innerHeight - 128 + 'px',
			  	'overflow-y':'auto', 'overflow-x': 'hidden'
			  });
			 
			 $('.tabHistoriqueScroll').css({
		  		'max-height': window.innerHeight - 97 + 'px',
		  		'overflow-y':'auto', 'overflow-x': 'hidden',
	  		});
			$('#EtatAlerte').css({'max-height': window.innerHeight - 165 + 28.6 + 'px'});
		  $('#MessageEtat').css({'max-height': window.innerHeight - 165 + 'px'});
		  $('section.tabs').css({
		  	'max-height': window.innerHeight - 128 + 'px',
		  	'overflow-y':'auto', 'overflow-x': 'hidden'
		  });
 			
			   
		  });
		</script>
  </body>
</html>";
?>