<?php

switch($_SERVER['REQUEST_METHOD']){
  case "GET":

  require_once "php/Smarty/libs/Smarty.class.php";

  $smarty = new Smarty();
  $smarty->template_dir = "templates";
  $smarty->compile_dir = "templates_c";
  $smarty->caching = false;
  $smarty->left_delimiter = "<{";
  $smarty->right_delimiter= "}>";

  $owner = $_SESSION['societe'];

  //récupérer les sociétés disponibles
  $societiesIds = getWrapSocietes($owner);
  $societiesIds[] = $owner;
  $function = "getSocietiesByIds";
  require_once "php/DAF/getSocieties.php";

  $dataSocieties = array();
  foreach ($societies as $societyId => $society) {
    $dataSocieties[$societyId] = $society['name'];
  }

  $smarty->assign("postPage",SERVERROOT.'/account');
  $smarty->assign("societies",$dataSocieties);
  $smarty->assign("serverRoot",SERVERROOT);

  $smarty->display("new-account.tpl");

  break;
  case "POST":
    $function = 'newAccount';
    require_once "php/DAF/setAccounts.php";

    header("Location: ".SERVERROOT."/account?operation=$operation");


  break;
}

?>