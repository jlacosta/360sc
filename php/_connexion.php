﻿<?php
/**
* Init some basic value.
*/
$lvl=(isset($_SESSION['level']))?(int) $_SESSION['level']:5;
$lvl=(isset($_SESSION['societe']))?(int) $_SESSION['societe']:0;
$id=(isset($_SESSION['id']))?(int) $_SESSION['id']:-1;
$pseudo=(isset($_SESSION['login']))?$_SESSION['login']:'';
$mail=(isset($_SESSION['mail']))?$_SESSION['mail']:'';

if(!($_SERVER['REQUEST_URI'] == "".get_link()."_connexion/" OR $_SERVER['REQUEST_URI'] == "".get_link()."_connexion")){
	if($_SESSION['connect'] === $_SESSION['redirect']){
		$_SESSION['redirect'] = true;
		header('Location: '.get_link().'_connexion/');
		exit();
	}
} else if (isset($_SESSION['client_id']) AND strlen($_SESSION['client_id']) != 0 AND $_SESSION['client_id'] != null) {
	header('Location: '.get_link().'');
}

?>

<?php

echo "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
<html>
	<head>
		<meta charset='UTF-8' />
		<title>360SmartMachine - ".$_SESSION['page']."</title>
		<meta name='description' content='Page d'administration de 360SmartMachine.' />
		<meta name='viewport' content='width=device-width, initial-scale=1.0' />

		<meta property='og:title' content='360SmartMachine' />

		<link rel='pingback' href='http://www.blocparc.fr/xmlrpc.php' />
		<!--[if lt IE 9]>
			<link rel='stylesheet' type='text/css' href='http://www.blocparc.fr/wp-content/themes/flatbox/ie.css' />
		<![endif]-->	
		<!--[if IE]> <link href='".get_link()."css/ie.css' type='text/css' rel='stylesheet'> <![endif]-->
		<!-- begin JS -->
		<!-- end JS -->
		<meta name='viewport' content='width=device-width, initial-scale=1.0' />
		<link rel='stylesheet' href='".get_link()."css/bootstrap.min.css' />
		<link rel='stylesheet' href='".get_link()."css/foundation.css?0.0.0.0.1' />
		<script src='https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js'
		></script>
		<script src='".get_link()."js/vendor/modernizr.js'></script>
		<script type='text/javascript'>
			var get_link = function(){
				return '".get_link()."';
			}
		</script>

		<style>
			#map { height: 100%; width:100%;}
			div.leaflet-control-zoom.leaflet-bar.leaflet-control{
				display: none !important;
				z-index:-1;
			}
			form{
				position: relative;
			}
			.wrapForm{
				position: absolute;
				top:0px;
				z-index: 5;
			}
			.wrap{
				position:absolute;
				width: 100%;
				height: 100%;
				z-index: 2;
				opacity: 0.4;
				background: rgba(0, 0, 0, 0.6);
				top: 0px;
			}
		</style>
		
		<link rel='stylesheet' href='http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.css' />
		<script src='http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.js'></script>

		<body class=";echo isset($_SESSION['page'])?$_SESSION['page']:'connexion';echo">";
?>
		<div id='map'></div>
		<div class='wrap'>
		</div>
		<script>
			var map = L.map('map').setView([43.503474, 6.485704], 18);
			L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
			    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
			    maxZoom: 18,
			    id: 'jlacosta.04d51441',
			    accessToken: 'pk.eyJ1IjoiamxhY29zdGEiLCJhIjoiMzg1MTkxNzM3ZmI2NzJkM2E5NzcyOTMwMWQ1MjU2NGEifQ.dYPfXlspQrxUmOiRP4bdFQ'
			}).addTo(map);
			map.dragging.disable();
			map.touchZoom.disable();
			map.doubleClickZoom.disable();
			map.scrollWheelZoom.disable();

			// Disable tap handler, if present.
			if (map.tap) map.tap.disable();
		</script>
<?php
		if(isset($_SESSION['success'])){
			if($_SESSION['success']){
?>
				<script>
					jQuery(document).ready(function(){
						goodalert();
					});
				</script>
<?php
			} else {
?>
				<script>
					jQuery(document).ready(function(){
						badalert();
					});
				</script>
<?php
			}
				$_SESSION['success'] = false;
		}

?>


<?php

	/**
	* Define function.
	*/
	function erreur($err='') {
	   $mess=($err!='')? $err:'Une erreur inconnue s\'est produite';
	   exit('<p>'.$mess.'</p>
	   <p>Cliquez <a href="./index.php">ici</a> pour revenir à la page d\'accueil</p></div></body></html>');
	}

	/**
	* Database connexion
	*/
	$bdd = connection_db();
	define('ERR_IS_CO','Vous ne pouvez pas accéder à cette page si vous n\'êtes pas connecté');

	/**
	* Check that input are not empty.
	*/ 
	if (!isset($_POST['login'])) {
?>	
	<!-- Body Connexion -->
	<style>
		.bodyConnexion{
			position: absolute;
			top: 0px;
			width: 100%;
			z-index: 5;
		}
		.bodyConnexion h1{
			float: left;
			color: white;
			margin-top: 41px;
			margin-right: 15px;
		}
		.bodyConnexion p{
			color: #c4d600;
			font-size: 125px;
			margin-top: -38px;
		}

		.marginLeft-2{
			margin-left: 2% !important;
		}

		form input.button{
			font-size: 18px !important;
			background-color: #c4d600 !important;
			color: white !important;
			font-weight: bold;
			padding: 2rem 2rem 1.8rem 2rem !important;
		}
		img{
			width: 16rem;
		}
		input[type="text"], input[type="password"]{
		  background: white;
		  padding: 10px !important;
		  border-color:0px !important;
		 -webkit-box-shadow: none;
			-moz-box-shadow: none;
			box-shadow: none;
		  border-bottom: solid 2px #c9c9c9 !important;
		  transition: border 0.3s !important;
		  border-style: none !important;
		  border-width: 0px !important;
		  margin: 0 0 0.2rem 0 !important;
		}
		input:-webkit-autofill {
 	     -webkit-box-shadow: 0 0 0px 1000px white inset;
		}
		input[type="password"]{
			margin-top: 7px !important;
		}
		.formConnexion{
			margin-top: 40px !important;
		}
		.bodyConnexion{
			margin-top: 5rem;
		}

	</style>
	<script>
		
	</script>
	<div class='bodyConnexion'>
		<div class="row marginLeft-2">
			<div class="small-2 large-2 columns">
				<img class='img360' src="../img/logo360.jpg" alt="Logo 360SmartMachine">
			</div>
			<div class="small-10 large-10 columns">
				<h1> Welcome to the 360 Smart Machine </h1>
			</div>
		</div>

		<div class="formConnexion">
			<?php
		 	echo '<form method="post" action="'.get_link().'_connexion/">';
			?>	
			<div class="row marginLeft-2">
					<div class="small-2 large-2 columns">
						<input id="connexion" class="button" type="submit" value="Connexion" />
					</div>
					<div class="small-10 large-10 columns">
						<input name="login" type="text" id="login" placeholder="Nom du compte" />
						<input type="password" name="password" id="password" placeholder="Mot de passe"/>
					</div>
			</div>
			</form>	
		</div>
	</div>
<?php	
	} else {
    	$message='';
	    if (empty($_POST['login']) || empty($_POST['password']) ) //Oublie d'un champ
	    {
	        $message = '<p>une erreur s\'est produite pendant votre identification.
			Vous devez remplir tous les champs</p>
			<p>Cliquez <a href="./connexion/">ici</a> pour revenir</p>';
   		}
   		else //On check le mot de passe
   		{
			/**
			* Checking in base
			*/
	        $query=$bdd->prepare('SELECT s.email, s.societe, s.password, s.CustomersID, s.Name, s.level, s.login, d.adminMA, d.clientMA, d.type, 
			d.gestionTags, d.searchTags, d.addTag, d.editTags, d.addObject, d.editObject, d.createUser, d.export, d.societePF, d.SURLE
	        FROM customers AS s, droit_client AS d WHERE s.login = :login AND d.idClient = s.CustomersID');
	        $query->bindValue(':login',$_POST['login'], PDO::PARAM_STR);
	        $query->execute();
	        $data=$query->fetch();
		
			/**
			* Good catching, go to init all the value for the next. 
			*/
			if ($data['password'] == md5($_POST['password'])) // Acces OK !
			{
				$_SESSION['login'] = $data['login'];
				$_SESSION['societe'] = $data['societe'];
				$_SESSION['level'] = (isset($data['level']))?$data['level']:'5';
				$_SESSION['client_id'] = $data['CustomersID'];
      	$_SESSION['client_name'] = $data['Name'];
				$_SESSION['mail'] = $data['email'];
				$_SESSION['client_image'] = get_customer_image($data['CustomersID']);
				
				$_SESSION['adminMA'] = $data['adminMA'];
				$_SESSION['clientMA'] = $data['clientMA'];
				$_SESSION['gestionTags'] = $data['gestionTags'];
				$_SESSION['searchTags'] = $data['searchTags'];
				$_SESSION['addTag'] = $data['addTag'];
				$_SESSION['editTags'] = $data['editTags'];
				$_SESSION['addObject'] = $data['addObject'];
				$_SESSION['editObject'] = $data['editObject'];
				$_SESSION['createUser'] = $data['createUser'];
				$_SESSION['export'] = $data['export'];
				$_SESSION['societePF'] = $data['societePF'];
				$_SESSION['SURLE'] = $data['SURLE'];
				$_SESSION['type'] = $data['type'];
				
				$message = '<p>Votre connexion est un succes.<br /><br /><br /><br />
				Si vous n\'&ecirc;tes pas redirigé d\'ici 5 secondes, cliquez <a href="'.get_link().'">ici</a> 
				pour revenir à la page d accueil</p>';
			} else {// Else, show error!
				$message = '<p>Une erreur s\'est produite 
				pendant votre identification.<br /> Le mot de passe ou le pseudo 
					entré n\'est pas correcte.</p><p><br/><br/>
					Si vous n\'&ecirc;tes pas redirigé d\'ici 5 secondes, cliquez <a href="'.get_link().'">ici</a> 
				pour revenir à la page précédente
				<br />Ou cliquez <a href="../">ici</a> 
				pour revenir à la page d accueil</p>';
			}
			$query->CloseCursor();
    	}
    
	    echo $message;
	    sleep(3);
	    header('Location: '.get_link().'');

	}

	include "./include/footer2.php";
?>