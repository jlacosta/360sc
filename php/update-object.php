<?php

switch($_SERVER['REQUEST_METHOD']){
  case 'GET':

  require_once "php/Smarty/libs/Smarty.class.php";
  $smarty = new Smarty();
  $smarty->template_dir = "templates";
  $smarty->compile_dir = "templates_c";
  $smarty->caching = false;
  $smarty->left_delimiter = "<{";
  $smarty->right_delimiter = "}>";

  //récupérer l'objet à modifier
  $function = "getObjectById";
  require_once "php/DAF/getObjects.php";
  $object['latlng']=$object['objLat'].' , '.$object['objLong'];
  //récupérer les types d'objets
  $godRight = $_SESSION['level']==4?true:false;
  $owner = $_SESSION['societe'];
  $function = 'getObjectTypes';
  require_once "php/DAF/getObjectTypes.php";
  $objectTypes=array();
  foreach ($types as $type) {
    $objectTypes[$type['idType']] = $type['nameType'];
  }
  //récupérer les sociétés disponibles
  $societiesIds = getWrapSocietes($owner);
  $societiesIds[] = $owner;
  $function = "getSocietiesByIds";
  require_once "php/DAF/getSocieties.php";
  $dataSocieties = array();
  foreach ($societies as $societyId => $society) {
    $dataSocieties[$societyId] = $society['name'];
  }
  $smarty->assign("postPage",SERVERROOT.'/object/'.$objectId);
  $smarty->assign("objectTypes",$objectTypes); 
  $smarty->assign("object",$object);
  $smarty->assign("societies",$dataSocieties);
  $smarty->assign("godRight",$godRight);
  $smarty->assign("serverRoot",SERVERROOT);
  $smarty->display("edit-object.tpl");
  break;

  case 'POST':
    $function = 'setObject'; 
    require_once "php/DAF/setObjects.php";

    header("Location: ".SERVERROOT."/object/$objectId?operation=$operation");


  break;
}

?>