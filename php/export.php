﻿<?php 
/** 
* Multi Add Page
* @author: ROUMEGUE Jérémy - 360sc
* We include the header for regular page.
*/
include "./include/head2.php";
?>
<div class="row">
		  <div class="columns large-12 small-6">
			<div class="panel">
				<p>
					<?php
						/**
						* If the user confirm that he want to export the tags to excel format
						*/
						if(isset($_POST['confirm']) AND $_POST['confirm'] == "true")  {
							echo "<span class='alert-box radius'>Votre opération a été effectuée avec succès </span>";
							$_SESSION['confirm'] = false;
						
						/**
						* Else, we show the form.
						*/
						} else {
					?>
						<span>Récupérer les données de vos Modules Communicants au format '.xls'<br/></span>
						<form action="<?php echo get_link(); ?>export/" method="post" enctype="multipart/form-data">
						<?php if($_SESSION['level'] == 4) { 
							$pdo = connection_db();
							$societes = $pdo->prepare('SELECT ID,name FROM societe');
							$societes->execute();
							?>
							<div class="form-group">
								<label for="id">Client à visualiser</label>
								<select id="id" class="medium" name="id" >
							<?php 
								echo '<option value="0">All</option>';
								while($societe=$societes->fetch()){
									echo '<option value="'.$societe['ID'].'">'.$societe['name'].'</option>';
								}
							?>
								</select>
							</div>
						<?php } ?>
							<input class="medium button" name="confirm" type="hidden" value="true" />
							<input class="medium button" type="submit" value="Confirmer" />
						</form>
					<?php
						}
					?>
				</p>
			</div>
		  </div>		  
		</div>
<?php
/**
* We include the footer for regular page.
*/
include "./include/footer2.php";
?>
