<?php

switch ($_SERVER['REQUEST_METHOD']) {
  case 'GET':

    require_once "php/Smarty/libs/Smarty.class.php";
    require_once "php/Smarty/libs/SmartyPaginate.class.php";

    $smarty = new Smarty();
    $smarty->template_dir = "templates";
    $smarty->compile_dir = "templates_c";
    $smarty->caching = false;
    $smarty->left_delimiter = "<{";
    $smarty->right_delimiter = "}>";
     // required connect
    SmartyPaginate::connect("MCs");
    // set items per page
    if(isset($_GET['limit']) && is_numeric($_GET['limit']) && $_GET['limit']>0)
      SmartyPaginate::setLimit(round($_GET['limit']),"MCs");
    else
      SmartyPaginate::setLimit(25,"MCs");

    //pour l'instant on n'a que les tags comme MC
    //if(isset($_GET['getTags'])){
      $function = "getTagsPage";
      $owner = $_SESSION['societe'];
      $godRight = $_SESSION['level']==4?true:false;
      $paginateId = "MCs";
      require_once "php/DAF/getTags.php";
    //}

    $MCs=array();
    $MCs=array_merge($MCs,$tags);


    $smarty->assign("godRight",$godRight);
    $smarty->assign("serverRoot",SERVERROOT); 
    $smarty->assign("MCs",$MCs);  
    SmartyPaginate::assign($smarty,"paginate","MCs");
    $smarty->display("list-MCs.tpl");
  break;

  case 'POST':
  break;
}


?>