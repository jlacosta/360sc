<?php

switch ($_SERVER['REQUEST_METHOD']) {
  case 'GET':

    require_once "php/Smarty/libs/Smarty.class.php";

    $smarty = new Smarty();
    $smarty->template_dir = "templates";
    $smarty->compile_dir = "templates_c";
    $smarty->caching = false;
    $smarty->left_delimiter = "<{";
    $smarty->right_delimiter = "}>";

    $smarty->assign("postPage",SERVERROOT.'/addMCs');
    $smarty->assign("godRight",$_SESSION['level']==4?true:false);
    $smarty->assign("serverRoot",SERVERROOT);
    $smarty->display("new-MCs.tpl");

  break;
  
  case 'POST':
    $function = 'newTags';
    if(isset($_POST['societe'])){ 
      $owner = $_POST['societe'];
    }else{
      $owner = $_SESSION['societe'];
    }
    // Laisser une couche flexible 'MC' qui est le parent du tag
    require_once "php/DAF/setTags.php";

    header("Location: ".SERVERROOT."/addMCs?operation=$operation");
  break;

  default:
  
  break;
}


?>
