<?php 
/** 
* Multi Add Page
* @author: Jorge Luis - 360sc
* We include the header for regular page.
*/
$_SESSION['page'] = "SansConnect";
if(($_SESSION['page'] != "SansConnect") && ($_SESSION['connect'] === false)){
	$_SESSION['page'] = "connexion";
	$_SESSION['redirect'] = true;
	header('Location: '.get_link().'connexion/');
	exit();
}
?>
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no">
	<meta name="viewport" content="height=device-height, user-scalable=no">
	<title></title>
	<style type="text/css">
		.animationLoad {
			position: absolute;
			left: calc(50% - 150px);
			top: calc(50% - 15%);
		  	width: 300px;
		}
		div.label{
			position: relative;
			text-transform: uppercase;
			text-align: center;
		}
		.sk-three-bounce {
		  margin: 40px auto;
		  width: 80px;
		  text-align: center; }
		  .sk-three-bounce .sk-child {
		    width: 20px;
		    height: 20px;
		    background-color: #333;
		    border-radius: 100%;
		    display: inline-block;
		    -webkit-animation: sk-three-bounce 1.4s ease-in-out 0s infinite both;
		            animation: sk-three-bounce 1.4s ease-in-out 0s infinite both; }
		  .sk-three-bounce .sk-bounce1 {
		    -webkit-animation-delay: -0.32s;
		            animation-delay: -0.32s; }
		  .sk-three-bounce .sk-bounce2 {
		    -webkit-animation-delay: -0.16s;
		            animation-delay: -0.16s; }

		@-webkit-keyframes sk-three-bounce {
		  0%, 80%, 100% {
		    -webkit-transform: scale(0);
		            transform: scale(0); }
		  40% {
		    -webkit-transform: scale(1);
		            transform: scale(1); } }

		@keyframes sk-three-bounce {
		  0%, 80%, 100% {
		    -webkit-transform: scale(0);
		            transform: scale(0); }
		  40% {
		    -webkit-transform: scale(1);
		            transform: scale(1); } }

	</style>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script type="text/javascript">
		function resize(){
		  $('#iframe').each(function(){
		    $(this).css( 'height',  window.innerHeight - 5 + 'px' );
		  });
		}
		function load(){
			resize();
			$('.animationLoad').css('display', 'none');
			 $('#iframe').css('display', 'block');		
			 //reload the width pour qu'il soit adapte au device
			 $("meta[name=viewport]").attr('content', 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0');
		}
		$(window).on('resize', resize);
		$(window).on('load', load);
	</script>
</head>
<body style="padding:0px; border:0px; margin:0px;">
	<div class="animationLoad">
		<div class="label"> 360SmartConnect </div> 	
		<div class="sk-three-bounce">
	      <div class="sk-child sk-bounce1"></div>
	      <div class="sk-child sk-bounce2"></div>
	      <div class="sk-child sk-bounce3"></div>
	    </div>
	</div>
	<?php 
		/*
		Essayer savoir s'il est valide l'adresse
		function validURL($urL){
			if (!parse_url($urL)['scheme']){
				$urL = 'http://'.$urL;
				echo $url." ".'dans';
			}
			return $urL;
		}*/
	echo'
		<iframe 
			id="iframe" style="display:none; padding:0px; border:0px; margin:0px; width: 100%;" 
			frameborder="0" 
			src="'.$url.'"></iframe>';
	?>		
</body>
</html>