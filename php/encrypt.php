<?php

$bdd = connection_db();

require_once "./algorithm/urw.php";

if(isset($_POST['societe'])){
	$societe = $_POST['societe'];
} else {
	$societe = $_SESSION['societe'];
}
for($i=0;$i<$_POST['nb_tags'];$i++){
	/**
	* Check a random number between 5 and 15.
	*/
	$nb_urw = rand(5, 15);
	
	/**
	* Apply algorithm
	*/
	$urwe = urw($nb_urw);
	
	/**
	* We insert the new URL for the new tag. 
	*/
	$bdd->exec('INSERT INTO 360sc_iz.yourls(ShortURL, OriginalURL) VALUES(\''.$urwe.'\',\''.trim($_POST['url']).'\')');
	
	/**
	* We prepare SQL request for making an URLe.
	*/
	$sql = 'SELECT YourlsID FROM 360sc_iz.yourls WHERE ShortURL=\''.$urwe.'\'';
	$target   = $bdd->query($sql);
	
	/**
	* We check the new YourlsID
	*/
	while ($row = $target->fetch()) {
		$YourlsID = $row['YourlsID'];
	}
	
	/**
	* We add the new tag in database.
	*/
	$bdd = connection_db();

	$bdd->exec('INSERT INTO 360sc_cms.tags(TagID, UniqueTagID, TagName, YourlsID, appairer, isActive, isTracked, isMultiLangue, description, typeTag) 
				VALUES(NULL, NULL, \''.$_POST['desc'].'\', '.$YourlsID.', 0, 0, 0, 0, NULL, NULL);');

	$bdd = connection_db();
	$bdd->exec('INSERT INTO societe_tags(customersID, yourlsID) VALUES(\''.$societe.'\',\''.$YourlsID.'\')');
	
	// -----------------------------------------------------------------------------------------
	// -----------------------------------------------------------------------------------------
	// -----------------------------------------------------------------------------------------

	/**
	* We encrypt the ShortURL to make an URLe
	*/
	$cVigenere = new classVigenere(get_client_key());
	$realestate = $cVigenere->encrypt(trim($urwe));

	/**
	* If no error occurred, say it was a success.
	*/
	$_SESSION['success'] = true;

	/**
	* Redirect to say that it was a success.
	*/
	//TODO
	//createOnetag(,$_POST['name'],$_POST['url'],$_SESSION['client_id']);
	
	if(!(isset($_POST['societe']))){
		header('Location: '.get_link().'urle/');
	} else {
		$urlf = get_societe_domain($societe).'/'.$realestate;
		$result = '{"data": "'.$urlf.'", "yourlsID": "'.$YourlsID.'"}';
		echo $result;
	}
}
?>