<?php 

require_once('admin/DBconnector.php');

 $permissions = array();
if(isset($_POST['function'])) $function = $_POST['function'];
if(!empty($_POST['account'])) $accountId = $_POST['account'];
if(isset($_POST['login'])) $login = $_POST['login'];
if(isset($_POST['name'])) $name = $_POST['name'];
if(isset($_POST['society'])) $society = $_POST['society'];
if(!empty($_POST['password'])) $password = md5($_POST['password']);
if(!empty($_POST['newPassword'])) $newPassword = md5($_POST['newPassword']);
if(isset($_POST['mobile'])) $mobile = $_POST['mobile'];
if(isset($_POST['fixed'])) $fixed = $_POST['fixed'];
if(isset($_POST['email'])) $email = $_POST['email'];
if(isset($_POST['permissions'])) $permissions = $_POST['permissions'];
if(!empty($_FILES['newAccountImage']) && !empty($_FILES['newAccountImage']['name'])){
  $target_dir = "public/img_profil/";
  $target_file = $target_dir . basename($_FILES["newAccountImage"]["name"]);
  $file = $_FILES['newAccountImage']['name'];
  move_uploaded_file($_FILES['newAccountImage']['tmp_name'],realpath(dirname(dirname(dirname(__FILE__)))).'/'.$target_file);  
  $image=$target_file; 
}  

try {

  switch ($function){
    case 'newAccount':  
      $accountId = newAccount($pdo,$login,$name,$society,$password,$email);
      $varArray=array();
      if(isset($image)) $varArray['image']="'".$image."'";
      if(isset($mobile)) $varArray['mobile']="'".$mobile."'";
      if(isset($fixed)) $varArray['fixe']="'".$fixed."'";
      setAccount($pdo,$accountId,$varArray);
      setPermissions($pdo,$accountId,$permissions);
      //var_dump($permissions);
    break; 
    case 'setAccount':
      $varArray=array();
      if(isset($name)) $varArray['name'] = "'$name'";
      if(isset($image)) $varArray['image'] = "'$image'";
      if(isset($mobile)) $varArray['mobile'] = "'$mobile'";
      if(isset($fixed)) $varArray['fixe'] = "'$fixed'";
      if(isset($email)) $varArray['email'] = "'$email'";
      if(isset($newPassword)) $varArray['password'] = "'$newPassword'";
      if(isset($society)) $varArray['societe'] = $society;
      setAccount($pdo,$accountId,$varArray);
    break;
  }

  $operation = 'success';
} catch (Exception $e){
  $operation = 'failure';
}





function newAccount($pdo,$login,$name,$society,$password,$email){

  $query=$pdo->prepare('INSERT INTO customers(Name, login, password, email, societe) 
            VALUES ( :name, :login, :password, :email, :society)');
  $query->bindValue(':name',$name,PDO::PARAM_STR);
  $query->bindValue(':login',$login,PDO::PARAM_STR);
  $query->bindValue(':password',$password,PDO::PARAM_STR);
  $query->bindValue(':society',$society,PDO::PARAM_INT);
  $query->bindValue(':email',$email,PDO::PARAM_STR);
  $query->execute();
  $newAccountId = $pdo->lastInsertId();

  $pdo->exec("INSERT INTO droit_client(idClient) VALUES ($newAccountId)");
  return $newAccountId;
}

function setAccount($pdo,$id,$varArray){

  $sql = 'UPDATE customers SET ';
  $sqlArray = array();
  foreach ($varArray as $column => $value) {
    $sqlArray[] = $column.' = '.$value;
  }
  $sql.= implode(',',$sqlArray);
  $sql.= ' WHERE CustomersID = '.$id;
  $query = $pdo->prepare($sql);
  $query->execute();

}

function setPermissions($pdo,$id,$permissions){
  if(!empty($permissions)){
    $setStrArray = array();
    foreach ($permissions as $permission) {
      $setStrArray[] = $permission.' = 1';
    }  
    $sql = 'UPDATE droit_client SET '.implode(',',$setStrArray).' WHERE idClient = '.$id;
    $query = $pdo->prepare($sql);
    $query->execute();

  }
  
  
}

?>
