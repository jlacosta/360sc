<?php 

require_once('admin/DBconnector.php');
require_once("algorithm/outils.php");
 
if(isset($_POST['function'])) $function = $_POST['function'];
if(isset($_POST['owner'])) $owner = $_POST['owner']; 
if(isset($_POST['tagId'])) $tagId = $_POST['tagId']; 
if(isset($_POST['godRight'])) $godRight = $_POST['godRight'];

try {

  switch ($function){
    case 'getObjectTypes': 
        $types = getObjectTypes($pdo,$owner);

    break; 
    case 'getObjectTypeById':
       $type = getObjectTypeById($pdo, $typeId);
    break;
  }

  $operation = 'success';
} catch (Exception $e){
  $operation = 'failure';
}

    function getObjectTypes($pdo,$owner) { 
      
      $ancestry = getAscendanceSocietes($owner);
      $descendants = getWrapSocietes($owner);
      $societies[] = $owner;
      $societies = array_merge($societies,$ancestry,$descendants);

      $types =array();
      foreach ($societies as $society) {
        $sql = "SELECT * FROM typeobjet WHERE proprietaire = $society";
        $target = $pdo->prepare($sql);
        $target->execute();
        while($type = $target->fetch()){
          $types[]=$type;
        }
      }
      $sql = "SELECT * FROM typeobjet WHERE proprietaire IS NULL";
      $commonTypes = $pdo->prepare($sql);
      $commonTypes->execute();
      while($commonType=$commonTypes->fetch()){
        $types[]=$commonType;
      }
      usort($types,"typeCmp");  

      return $types;
    }

    function getAllObjectTypes($pdo){
      $types= array();
      $sql = 'SELECT * FROM typeobjet';
      $target = $pdo->prepare($sql); 
      $target->execute();
      
      while ($type = $target->fetch(PDO::FETCH_ASSOC)) { 
        // besoin d'index pour convenir le format des arrays
        $types[]=$type;
      }   
      return $types;
    }



    function getObjectTypeById($pdo,$id) {
      $type = null;
      $sql = 'SELECT * FROM typeobjet WHERE idType=:typeId';
      $target = $pdo->prepare($sql);
      $target->bindValue(':typeId', $id);
      $target->execute();
      while ($type = $target->fetch(PDO::FETCH_ASSOC)) {  
         
      }
      return $type;
    }

 
    function typeCmp($a,$b){
      return strcmp($a["nameType"], $b["nameType"]);
    }

?>
