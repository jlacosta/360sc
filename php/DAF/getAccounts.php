<?php 

require_once('admin/DBconnector.php');
require_once("algorithm/outils.php");
 
if(isset($_POST['function'])) $function = $_POST['function'];
if(isset($_POST['owner'])) $owner = $_POST['owner']; 
if(isset($_POST['accountId'])) $accountId = $_POST['accountId']; 

try {

  switch ($function){ 
    case 'getAccountById':
       $account = getAccountById($pdo, $accountId);
    break;
    case 'getAllAccounts':
      $accounts = getAllAccounts($pdo);
    break;
  }

  $operation = 'success';
} catch (Exception $e){
  $operation = 'failure';
}

    


     function getAccountById($pdo,$id) {
      $account = null;
      $sql = 'SELECT c.*, s.name AS societyName FROM customers AS c,societe AS s WHERE c.societe = s.ID AND CustomersID=:accountId';
      $target = $pdo->prepare($sql);
      $target->bindValue(':accountId', $id); 
      $target->execute();
      while ($row = $target->fetch(PDO::FETCH_ASSOC)) {  
          $account = $row;
      }
      return $account;
    }

    function getALLAccounts($pdo){
      $accounts = array();
      $sql = "SELECT c.*, s.name AS societyName FROM customers AS c,societe AS s WHERE c.societe = s.ID ORDER BY CustomersID ASC";
      $target = $pdo->prepare($sql);
      $target->execute();
      while ($row = $target->fetch(PDO::FETCH_ASSOC)) {
        $accounts[$row['CustomersID']] = $row;
      }
      return $accounts;

    }
 

?>
