<?php 

require_once('admin/DBconnector.php');

 
if(isset($_POST['function'])) $function = $_POST['function'];
if(isset($_POST['name'])) $name = $_POST['name'];
if(isset($_POST['alpha'])) $alpha = $_POST['alpha'];
if(!empty($_FILES['imgObjType']) && !empty($_FILES['imgObjType']['name'])){
  $target_dir = "public/img_objet/";
  $target_file = $target_dir . basename($_FILES["imgObjType"]["name"]);
  $file = $_FILES['imgObjType']['name'];
  move_uploaded_file($_FILES['imgObjType']['tmp_name'],realpath(dirname(dirname(dirname(__FILE__)))).'/'.$target_file);  
  $image=$target_file; 
}  
if(isset($_POST['desc'])) $desc = $_POST['desc'];
if(isset($_POST['owner'])) $owner = $_POST['owner'];

try {

  switch ($function){
    case 'newObjectType':
      if(isset($image))
        newObjectType($pdo,$name,$alpha,$desc,$owner,$image);
      else
        newObjectType($pdo,$name,$alpha,$desc,$owner);
    break; 
    case 'setObjectType':
      $varArray=array();
      if(isset($name)) $varArray['nameType'] = $name;
      if(isset($alpha)) $varArray['alphaID'] = $alpha;
      if(isset($image)) $varArray['image'] = $image;
      if(isset($desc)) $varArray['description'] = $desc;
      if(isset($owner)) $varArray['proprietaire'] = $owner;
      setObjectType($pdo,$id,$varArray);
    break;
  }

  $operation = 'success';
} catch (Exception $e){
  $operation = 'failure';
}





function newObjectType($pdo,$name,$alpha,$desc,$owner,$image=NULL){
  $query=$pdo->prepare('INSERT INTO typeobjet(idType, nameType, alphaID, image, description, proprietaire) 
            VALUES (:typeID, :name, :alpha, :image, :desc, :prop)');
  $query->bindValue(':typeID',NULL,PDO::PARAM_INT);
  $query->bindValue(':name',$name,PDO::PARAM_STR);
  $query->bindValue(':alpha',$alpha,PDO::PARAM_STR);
  $query->bindValue(':image',$image,PDO::PARAM_STR);
  $query->bindValue(':desc',$desc,PDO::PARAM_STR);
  $query->bindValue(':prop',$owner,PDO::PARAM_INT);
  $query->execute();

}

function setObjectType($pdo,$id,$varArray){

  $sql = 'UPDATE typeobjet SET ';
  $sqlArray = array();
  foreach ($varArray as $column => $value) {
    $sqlArray[] = $column.' = '.$value;
  }
  $sql.= implode(',',$sqlArray);
  $sql.= ' WHERE idType = '.$id;
  $query = $pdo->prepare($sql);
  $query->execute();

}


?>
