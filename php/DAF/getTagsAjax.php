<?php

 

$results = array();


if (isset($_GET['object']) && $_GET['object']!=NULL && $_GET['object']!=""){
  $results['objectTags']=getTagsByObject($_GET['object']);
}
if (isset($_GET['societe']) && $_GET['societe']!=NULL && $_GET['societe']!=""){
  $results['societeFreeTags'] = getFreeTagsBySociety($_GET['societe']);
}else{
  $results['allTags']=getAllTags();
}

echo json_encode($results);




function getDBConnection(){
  try {
    return new PDO("mysql:host=localhost;port=3306;dbname=360sc_cms;charset=utf8", "root", "");
  } catch (Exception $e){
    die('Erreur : ' . $e->getMessage());
  }
}

function getFreeTagsBySociety($society){
  $result = array();
  $pdo = getDBConnection();
  $sql = "SELECT *  FROM tags AS t, societe_tags AS st WHERE  t.appairer = 0 AND t.YourlsID = st.yourlsID AND st.customersID = ".$society;  
  $query = $pdo->query($sql);
  while($row=$query->fetch()){
    $result[]=$row;
  }
  $query->closeCursor();
  return $result;
}

function getTagsByObject($object){
  $result = array();
  $pdo = getDBConnection();
  $sql = "SELECT *  FROM tags AS t, objet_tags AS ot WHERE  t.YourlsID = ot.yourlsID AND ot.objetID = ".$object;  
  $query = $pdo->query($sql);
  while($row=$query->fetch()){
    $result[]=$row;
  }
  $query->closeCursor();
  return $result;
}

function getAllTags(){
  $result = array();
  $pdo = getDBConnection();
  $sql = "SELECT * FROM tags";  
  $query = $pdo->query($sql);
  while($row=$query->fetch()){
    $result[]=$row;
  }
  $query->closeCursor();
  return $result;
}



?>