<?php 

require_once('admin/DBconnector.php');
 
if(isset($_POST['function'])) $function = $_POST['function'];
if(isset($_POST['societiesIds'])) $societiesIds = $_POST['societiesIds'];  

try {

  switch ($function){ 
    case 'getSocietiesByIds':
      $societies = getSocietiesByIds($pdo, $societiesIds);
    break;
    case 'getAllSocieties':
      $societies = getAllSocieties($pdo);
    break;
  }

  $operation = 'success';
} catch (Exception $e){
  $operation = 'failure';
}

    function getSocietiesByIds($pdo,$ids) {

      $sql = "SELECT * FROM societe WHERE ID IN (".implode(',',$ids).") ORDER BY ID ASC ";
      $target = $pdo->prepare($sql);
      $target->execute();
      $societies = array();
      while ($row = $target->fetch(PDO::FETCH_ASSOC)) {
        $societies[$row['ID']] = $row;
      }
      return $societies;
    }

    function getAllSocieties($pdo){
      $sql = "SELECT * FROM societe ORDER BY ID ASC";
      $target = $pdo->prepare($sql);
      $target->execute();
      $societies = array();
      while ($row = $target->fetch(PDO::FETCH_ASSOC)) {
        $societies[$row['ID']] = $row;
      }
      return $societies;
    } 


?>
