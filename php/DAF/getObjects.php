<?php 

require_once('admin/DBconnector.php');
 
if(isset($_POST['function'])) $function = $_POST['function'];
if(isset($_POST['owner'])) $owner = $_POST['owner']; 
if(isset($_POST['objectId'])) $objectId = $_POST['objectId']; 
if(isset($_POST['objectIds'])) $objectIds = $_POST['objectIds']; 
if(isset($_POST['godRight'])) $godRight = $_POST['godRight'];
if(!isset($paginateId)) $paginateId = "objects";

try {

  switch ($function){

    case 'getObjectsPage':
      if(isset($godRight) && $godRight)
        $objects = getAllObjectsPage($pdo,$paginateId);
      else
        $objects = getObjectsPage($pdo,$paginateId,$owner);
    break;  
    case 'getObjectsByIdsPage':
       $objects = getObjectsByIdsPage($pdo,$paginateId, $objectIds);
    break;
    case 'getObjectById':
       $object = getObjectById($pdo, $objectId);
    break;
  }

  $operation = 'success';
} catch (Exception $e){
  $operation = 'failure';
}

    function getObjectsByIdsPage($pdo,$paginateId,$objectIds) {
      $objects = array();
      $sql="SELECT o.*,so.societeID FROM objet AS o,societe_objet AS so WHERE so.objetID = o.objetID AND o.objetID IN (".implode(',',$objectIds).")";
      $target = $pdo->prepare($sql);
      $target->execute();
      while($row = $target->fetch(PDO::FETCH_ASSOC)){
        $objects[$row['objetID']] = $row; 
      }
    
      ksort($objects);

      SmartyPaginate::setTotal(count($objects),$paginateId);

      $subObjects = array_slice($objects, SmartyPaginate::getCurrentIndex($paginateId),SmartyPaginate::getLimit($paginateId));
       
      return $subObjects;
    }

    function getObjectsPage($pdo,$paginateId,$owner) {
      $objects = array();
       // get all children
      $societies = getWrapSocietes($owner);
      // add itself into array
      $societies[] = $_SESSION['societe'];

      $sql="SELECT o.*,so.societeID FROM objet AS o,societe_objet AS so WHERE o.objetID = so.objetID AND so.societeID IN (".implode(',',$societies).")";
      $target = $pdo->prepare($sql);
      $target->execute();
      while($row = $target->fetch(PDO::FETCH_ASSOC)){
        $objects[$row['objetID']] = $row; 
      }
    
      ksort($objects);

      SmartyPaginate::setTotal(count($objects),$paginateId);

      $subObjects = array_slice($objects, SmartyPaginate::getCurrentIndex($paginateId),SmartyPaginate::getLimit($paginateId));
       
      return $subObjects;
    }



    function getAllObjectsPage($pdo,$paginateId){
      $objects = array();
      $sql="SELECT o.*,so.societeID FROM objet AS o,societe_objet AS so WHERE so.objetID = o.objetID ORDER BY o.objetID ";
      $target = $pdo->prepare($sql);
      $target->execute();
      while($row = $target->fetch(PDO::FETCH_ASSOC)){
        $objects[$row['objetID']] = $row; 
      }
    
      SmartyPaginate::setTotal(count($objects),$paginateId);

      $subObjects = array_slice($objects, SmartyPaginate::getCurrentIndex($paginateId),SmartyPaginate::getLimit($paginateId));
       
      return $subObjects;
    }
 

     function getObjectById($pdo,$id) {
      $object = null;
      $sql = 'SELECT o.*,so.societeID FROM objet AS o,societe_objet AS so WHERE so.objetID = o.objetID AND o.objetID = :objectId';
      $target = $pdo->prepare($sql);
      $target->bindValue(':objectId', $id);
      $target->execute();
      while ($row = $target->fetch(PDO::FETCH_ASSOC)) {  
        $object = $row; 
      }
      return $object;
    }
 


?>
