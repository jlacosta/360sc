<?php 

require_once('admin/DBconnector.php');
require_once("algorithm/urw.php");
 
if(isset($_POST['function'])) $function = $_POST['function'];
if(isset($_POST['nb_tags'])) $nb_tags = $_POST['nb_tags'];
if(isset($_POST['url'])) $url = $_POST['url'];
if(isset($_POST['tagName'])) $tagName = $_POST['tagName'];
if(isset($_POST['owner'])) $owner = $_POST['owner'];
if(isset($_POST['desc'])) $desc = $_POST['desc'];
if(isset($_POST['active'])) $active = 1; else $active = 0;
if(isset($_POST['distributedTags'])) $distributedTags = $_POST['distributedTags'];

try {

  switch ($function){
    case 'newTags':
    for ($i=0; $i < $nb_tags; $i++) { 
      newTag($pdo, $url, $tagName,$owner);
    }
    break; 
    case 'setTag':
      $varArray=array();
      if(isset($tagName)) $varArray['TagName'] = "'$tagName'";
      if(isset($desc)) $varArray['description'] = "'$desc'";
      if(isset($url)) $varArray['OriginalURL'] = "'$url'";
      if(isset($owner)) $varArray['proprietaire'] = $owner;
      if(isset($active)) $varArray['isActive'] = $active;
      setTag($pdo,$tagId,$varArray);
    break;
    case 'redistributeTags':
      redistributeTags($pdo,$distributedTags,$owner);
    break;
    case 'removeTag':
      removeTag($pdo,$tagId);
    break;
  }

  $operation = 'success';
} catch (Exception $e){
  $operation = 'failure';
}



function newTag($pdo,$url,$tagName,$owner){

  //Check a random number between 5 and 15.
  $nb_urw = rand(5, 15);
  //Apply algorithm
  $urwe = urw($nb_urw);
  //We insert the new URL for the new tag. 
  $pdo->exec('INSERT INTO 360sc_iz.yourls(ShortURL, OriginalURL) VALUES(\''.$urwe.'\',\''.trim($url).'\')');
  //We check the new YourlsID
  $yourlsID = $pdo->lastInsertId();
  //We add the new tag in database.
  $query = $pdo->prepare('INSERT INTO 360sc_cms.tags(TagID, TagName, YourlsID) VALUES(:tagId, :tagName, :yourlsID)'); 
  $query->bindValue(':tagId',NULL,PDO::PARAM_INT);
  $query->bindValue(':tagName',$tagName,PDO::PARAM_STR);
  $query->bindValue(':yourlsID',$yourlsID,PDO::PARAM_INT);
  $query->execute();
 
  $pdo->exec('INSERT INTO societe_tags(customersID, yourlsID) VALUES('.$owner.','.$yourlsID.')');
  
  
}

function setTag($pdo,$id,$varArray){
  if($varArray!=null || count($varArray)!=0){
    $sql = 'UPDATE tags SET ';
    $sqlArray = array();
    foreach ($varArray as $column => $value) {
      switch ($column) {
        case 'proprietaire':
          $editSocieteSql="UPDATE societe_tags SET customersID = $value WHERE yourlsID = (SELECT YourlsID FROM tags WHERE TagID = $id)";
          $pdo->exec($editSocieteSql);
        break;
        case 'OriginalURL':
          $editUrlSql="UPDATE 360sc_iz.yourls SET OriginalURL = $value WHERE YourlsID = (SELECT YourlsID FROM 360sc_cms.tags WHERE TagID = $id)";
          $pdo->exec($editUrlSql);
        break;
        default:
          $sqlArray[] = $column.' = '.$value;  
        break;
      }

    }
    $sql.= implode(',',$sqlArray);
    $sql.= ' WHERE TagID = '.$id;
    $query = $pdo->prepare($sql);
    var_dump($varArray);
    $query->execute();  
  }

}

function distributeTags($pdo,$ids,$owner){
  $yourlsIdSql = "SELECT YourlsID FROM tags WHERE TagID IN (".implode(',',$ids).")";
  $yourlsIdTarget = $pdo->prepare($yourlsIdSql);
  $yourlsIdTarget->execute();
  $yourlsIds = $yourlsIdTarget->fetchAll(PDO::FETCH_COLUMN);
  $insertSql = "INSERT INTO societe_tags(yourlsID,customersID) VALUES(:id,:owner)";
  $insertTarget = $pdo->prepare($insertSql);
  foreach ($yourlsIds as $yourlsId) {
    $insertTarget->execute(array(":id"=>$yourlsId,":owner"=>$owner));
  }
}

function redistributeTags($pdo,$ids,$owner){
  $yourlsIdSql = "SELECT YourlsID FROM tags WHERE TagID IN (".implode(',',$ids).")";
  $yourlsIdTarget = $pdo->prepare($yourlsIdSql);
  $yourlsIdTarget->execute();
  $yourlsIds = $yourlsIdTarget->fetchAll(PDO::FETCH_COLUMN);
  $updateSql = "UPDATE societe_tags SET customersID = $owner WHERE yourlsID IN (".implode(',',$yourlsIds).")";
  $updateTarget = $pdo->prepare($updateSql); 
  $updateTarget->execute();
}

function removeTag($pdo,$id){
  $yourlsIdSql = "SELECT YourlsID FROM tags WHERE TagID = $id";
  $yourlsIdTarget = $pdo->prepare($yourlsIdSql);
  $yourlsIdTarget->execute();
  $yourlsId = $yourlsIdTarget->fetch(PDO::FETCH_ASSOC)['YourlsID'];
  if(!empty($yourlsId)){
    $deleteCmsSql = "DELETE FROM 360sc_cms.tags WHERE YourlsID = $yourlsId";
    $pdo->exec($deleteCmsSql);
    $deleteIzSql = "DELETE FROM 360sc_iz.yourls WHERE YourlsID = $yourlsId";
    $pdo->exec($deleteIzSql);
  }
}


?>
