<?php 

require_once('admin/DBconnector.php');

 
if(isset($_POST['function'])) $function = $_POST['function'];
if(isset($_POST['societyName'])) $societyName = $_POST['societyName'];
if(isset($_POST['alpha'])) $alpha = $_POST['alpha'];
if(isset($_POST['url'])) $url = $_POST['url'];
if(isset($_POST['fixed'])) $fixed = $_POST['fixed'];
if(isset($_POST['parent'])) $parent = $_POST['parent'];
if(!empty($_FILES['newSocietyImage']) && !empty($_FILES['newSocietyImage']['name'])){
  $target_dir = "public/img_profil/";
  $target_file = $target_dir . basename($_FILES["newSocietyImage"]["name"]);
  $file = $_FILES['newSocietyImage']['name'];
  move_uploaded_file($_FILES['newSocietyImage']['tmp_name'],realpath(dirname(dirname(dirname(__FILE__)))).'/'.$target_file);  
  $societyImage=$target_file; 
}  

try {

  switch ($function){
    case 'newSociety':  
        $societyId = newSociety($pdo,$societyName,$alpha,$url,$fixed);
        $varArray=array();
        if(isset($societyImage)) $varArray['image']="'".$societyImage."'";
        if(isset($webSite)) $varArray['webSite']="'".$webSite."'";
        setSociety($pdo,$societyId,$varArray);
        if(isset($parent) && is_numeric($parent)){
          $pdo->exec("INSERT INTO societe_soussociete(societe,soussociete) VALUES ($parent,$societyId)");
        }
    break; 
    case 'setSociety':
      $varArray=array();
/*    if(isset($societyName)) $varArray['name'] = $societyName;
      if(isset($alpha)) $varArray['alphaID'] = $alpha;
      if(isset($url)) $varArray['urlDefault'] = $url;
      if(isset($fixed)) $varArray['fixe'] = $fixed;
      if(isset($societyImage)) $varArray['image'] = $societyImage;*/
      setSociety($pdo,$id,$varArray);
    break;
  }

  $operation = 'success';
} catch (Exception $e){
  $operation = 'failure';
}





function newSociety($pdo,$name,$alpha,$url,$fixed){

  $query=$pdo->prepare('INSERT INTO societe( name, alphaID, urlDefault, fixe) 
            VALUES ( :name, :alpha, :url, :fixed)');
  $query->bindValue(':name',$name,PDO::PARAM_STR);
  $query->bindValue(':alpha',$alpha,PDO::PARAM_STR);
  $query->bindValue(':url',$url,PDO::PARAM_STR);
  $query->bindValue(':fixed',$fixed,PDO::PARAM_STR);
  $query->execute();
  $newSocietyId = $pdo->lastInsertId();

  $query=$pdo->prepare('INSERT INTO 360sc_iz.customers (customersID, customersKey, customersDomain) VALUES (:id, :key, :domain)');
  $query->bindValue(':id',$newSocietyId, PDO::PARAM_INT);
  $query->bindValue(':key', 'fudjsoleudosndif', PDO::PARAM_STR);
  $query->bindValue(':domain', $url, PDO::PARAM_STR);
  $query->execute();
  return $newSocietyId;
}

function setSociety($pdo,$id,$varArray){

  $sql = 'UPDATE societe SET ';
  $sqlArray = array();
  foreach ($varArray as $column => $value) {
    $sqlArray[] = $column.' = '.$value;
  }
  $sql.= implode(',',$sqlArray);
  $sql.= ' WHERE ID = '.$id;
  $query = $pdo->prepare($sql);
  $query->execute();

}


?>
