<?php 


require_once('admin/DBconnector.php');
require_once("algorithm/urw.php");
require_once("algorithm/vigenereCypher.php");
 
 
if(isset($_POST['function'])) $function = $_POST['function'];
if(isset($_POST['owner'])) $owner = $_POST['owner']; 
if(isset($_POST['tagId'])) $tagId = $_POST['tagId']; 
if(isset($_POST['godRight'])) $godRight = $_POST['godRight'];
if(!isset($paginateId)) $paginateId = 'urles';

try {

  switch ($function){
    case 'getTagsPage':
      if(isset($godRight) && $godRight)
        $tags = getAllTagsPage($pdo,$paginateId);
      else
        $tags = getTagsPage($pdo,$paginateId,$owner);
    break; 
    case 'getTags':
      $tags = getTags($pdo,$owner);
    break;
    case 'getOwnFreeTags':
      $tags = getOwnFreeTags($pdo,$owner);
    break;
    case 'getTagById':
       $tag = getTagById($pdo, $tagId);
    break;
    case 'getNotAffectedTags':
      $tags = getNotAffectedTags($pdo);
    break;
     
  }

  $operation = 'success';
} catch (Exception $e){
  $operation = 'failure';
}

    function getTagsPage($pdo,$paginateId,$owner) {
      $tags = array();
      // get all children
      $societies = getWrapSocietes($owner);
      // add itself into array
      $societies[] = $owner;
      $time_array=array();
      $tags = pickTags($pdo,$societies);

    
      ksort($tags);

      SmartyPaginate::setTotal(count($tags),$paginateId);

      $subTags = array_slice($tags, SmartyPaginate::getCurrentIndex($paginateId),SmartyPaginate::getLimit($paginateId));
      //on n'ajoute l'urle que pour les tags a afficher parce que l'algorithme d'encryptage prend beaucoup de temps
      foreach ($subTags as $tagId => &$tag) {
        $cVigenere = new classVigenere($tag['customersKey']);
        $realestate = $tag['customersDomain'].'/'.$cVigenere->encrypt(trim($tag['ShortURL']));
        if(isset($realestate))
          $tag['urle'] = $realestate;
        else
          $tag['urle'] = null;
      }
      unset($tag);
      return $subTags;
    }


    function getTags($pdo,$owner) {
      $tags = array();
      // get all children
      $societies = getWrapSocietes($owner);
      // add itself into array
      $societies[] = $owner;
      $time_array=array();
      $tags = pickTags($pdo,$societies);
    
      ksort($tags);
 
       foreach ($tags as $tagId => &$tag) {
        $cVigenere = new classVigenere($tag['customersKey']);
        $realestate = $tag['customersDomain'].'/'.$cVigenere->encrypt(trim($tag['ShortURL']));
        if(isset($realestate))
          $tag['urle'] = $realestate;
        else
          $tag['urle'] = null;
      }
      unset($tag);
      return $tags;
    }

    function getAllTagsPage($pdo,$paginateId){
      $tags= array();
      $sql = 'SELECT  t.*, y.ShortURL, y.OriginalURL, c.customersKey,c.customersDomain  , st.customersID, s.name
              FROM tags AS t,  360sc_iz.yourls AS y, 360sc_cms.societe_tags AS st, 360sc_iz.customers AS c, 360sc_cms.societe AS s
              WHERE t.YourlsID = y.YourlsID AND c.customersID = st.customersID AND st.yourlsID = t.YourlsID AND c.customersID = s.ID
              ORDER BY TagID';
      $target = $pdo->prepare($sql); 
      $target->execute();
      
      while ($row = $target->fetch(PDO::FETCH_ASSOC)) { 
        // besoin d'index pour convenir le format des arrays
        $tags[$row['TagID']] = $row; 
      }  

      SmartyPaginate::setTotal(count($tags),$paginateId);
      $subTags = array_slice($tags, SmartyPaginate::getCurrentIndex($paginateId),SmartyPaginate::getLimit($paginateId));
      //on n'ajoute l'urle que pour les tags a afficher parce que l'algorithme d'encryptage prend beaucoup de temps
      foreach ($subTags as $tagId => &$tag) {
        $cVigenere = new classVigenere($tag['customersKey']);
        $realestate = $tag['customersDomain'].'/'.$cVigenere->encrypt(trim($tag['ShortURL']));
        if(isset($realestate))
          $tag['urle'] = $realestate;
        else
          $tag['urle'] = null;
      }
      unset($tag);
      return $subTags;
    }

    function getAllTags($pdo,$paginateId){
      $tags= array();
      $sql = 'SELECT  t.*, y.ShortURL, y.OriginalURL, c.customersKey,c.customersDomain  , st.customersID, s.name
              FROM tags AS t,  360sc_iz.yourls AS y, 360sc_cms.societe_tags AS st, 360sc_iz.customers AS c , 360sc_cms.societe AS s
              WHERE t.YourlsID = y.YourlsID AND c.customersID = st.customersID AND st.yourlsID = t.YourlsID AND c.customersID = s.ID
              ORDER BY TagID';
      $target = $pdo->prepare($sql); 
      $target->execute();
      
      while ($row = $target->fetch(PDO::FETCH_ASSOC)) { 
        // besoin d'index pour convenir le format des arrays
        $tags[$row['TagID']] = $row; 
      }  
       foreach ($tags as $tagId => &$tag) {
        $cVigenere = new classVigenere($tag['customersKey']);
        $realestate = $tag['customersDomain'].'/'.$cVigenere->encrypt(trim($tag['ShortURL']));
        if(isset($realestate))
          $tag['urle'] = $realestate;
        else
          $tag['urle'] = null;
      }
      unset($tag);
      return $tags;
    } 

    function getOwnFreeTags($pdo,$owner){  

        $sql = "SELECT * FROM tags,societe_tags WHERE tags.YourlsID = societe_tags.yourlsID AND tags.appairer = 0 AND societe_tags.customersID = $owner ORDER BY TagID ASC ";
        $target  = $pdo->prepare($sql);
        $target->execute();
        $tags = array();
        while($row= $target->fetch(PDO::FETCH_ASSOC)){
          $tags[$row['TagID']] = $row;
        }

        return $tags;

    }

    function getNotAffectedTags($pdo){
      $tags= array();
      $sql = 'SELECT DISTINCT t.*, y.ShortURL, y.OriginalURL 
              FROM tags AS t,  360sc_iz.yourls AS y, 360sc_cms.societe_tags AS st
              WHERE t.YourlsID = y.YourlsID AND  t.YourlsID NOT IN (SELECT DISTINCT yourlsID FROM societe_tags)
              ORDER BY TagID';
      $target = $pdo->prepare($sql); 
      $target->execute();
      
      while ($row = $target->fetch(PDO::FETCH_ASSOC)) { 
        // besoin d'index pour convenir le format des arrays
        $tags[$row['TagID']] = $row; 
      }   
      return $tags;
    }

     function getTagById($pdo,$id) {
      $tag = null;
      $sql = 'SELECT t.*, y.ShortURL, y.OriginalURL, c.customersKey,c.customersDomain , st.customersID , s.name 
              FROM 360sc_iz.yourls AS y, 360sc_cms.tags AS t, 360sc_cms.societe_tags AS st, 360sc_iz.customers AS c, 360sc_cms.societe AS s
              WHERE t.YourlsID=y.YourlsID AND st.yourlsID = y.YourlsID AND  st.customersID = c.customersID  AND c.customersID = s.ID AND t.TagID = :tagId';
      $target = $pdo->prepare($sql);
      $target->bindValue(':tagId', $id);
      $target->execute();
      while ($row = $target->fetch(PDO::FETCH_ASSOC)) {  
        $tag = $row; 
        $cVigenere = new classVigenere($tag['customersKey']);
        $realestate = $tag['customersDomain'].'/'.$cVigenere->encrypt(trim($tag['ShortURL']));
        if(isset($realestate))
          $tag['urle'] = $realestate;
        else
          $tag['urle'] = null;
      }
      return $tag;
    }
 
     
    function getTagsByObject($pdo,$object){
      $tags = array();
      $sql = "SELECT t.*, y.ShortURL, y.OriginalURL, c.customersKey,c.customersDomain , st.customersID  
              FROM tags AS t, objet_tags AS ot,360sc_iz.yourls AS y, 360sc_cms.societe_tags AS st, 360sc_iz.customers AS c 
              WHERE  t.YourlsID = ot.yourlsID AND 
                     t.YourlsID = y.YourlsID AND 
                     c.customersID = st.customersID AND 
                     st.yourlsID = t.YourlsID AND 
                     ot.objetID = ".$object;  
      $query = $pdo->query($sql);
      while($row=$query->fetch()){
        $tags[$row['TagID']]=$row;
      }
      return $tags;
    }

    function getTagsBySociety($pdo,$society,$pairingState=null){
      switch($pairingState){
        case null:
          $pairingStr = '';
        break;
        case 1:
          $pairingStr = ' AND  t.appairer = 1';
        break;
        case 0:
          $pairingStr = ' AND  t.appairer = 0';
        break;
      }

      $tags = array();
      $sql = "SELECT t.*,y.ShortURL, y.OriginalURL, c.customersKey, c.customersDomain , st.customersID 
              FROM tags AS t, societe_tags AS st,360sc_iz.yourls AS y, 360sc_iz.customers AS c 
              WHERE  t.YourlsID = y.YourlsID AND 
                     c.customersID = st.customersID AND 
                     st.yourlsID = t.YourlsID AND
                     t.YourlsID = st.yourlsID AND 
                     st.customersID = ".$society.$pairingStr;  
      $query = $pdo->query($sql);
      while($row=$query->fetch()){
        $tags[$row['TagID']]=$row;
      }
      return $tags;
    }



    function pickTags($pdo,$societies){
      $tags= array();
      $sql = 'SELECT  t.*, y.ShortURL, y.OriginalURL, c.customersKey,c.customersDomain , st.customersID , s.name
              FROM tags AS t,  360sc_iz.yourls AS y, 360sc_cms.societe_tags AS st, 360sc_iz.customers AS c , 360sc_cms.societe AS s
              WHERE t.YourlsID = y.YourlsID AND c.customersID = st.customersID AND c.customersID = s.ID AND st.yourlsID = t.YourlsID AND c.customersID IN ('.implode(',',$societies).')
              ORDER BY TagID ';
      $target = $pdo->prepare($sql); 
      $target->execute();
      while ($row = $target->fetch(PDO::FETCH_ASSOC)) { 
        // besoin d'index pour convenir le format des arrays
        $tags[$row['TagID']] = $row; 
      }  
      return $tags;
    }



?>
