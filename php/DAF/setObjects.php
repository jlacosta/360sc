<?php 

require_once('admin/DBconnector.php');

 
if(!empty($_POST['function'])) $function = $_POST['function'];
if(!empty($_POST['name'])) $name = $_POST['name'];
if(!empty($_POST['idType'])) $objectType = $_POST['idType'];
if (!empty($_POST['date-streetSign']))
  $dateStreetSign = $_POST['date-streetSign'];
if(!empty($_POST['alphaId'])) $alphaId = $_POST['alphaId'];
if(!empty($_POST['desc'])) $desc = $_POST['desc'];
if(!empty($_POST['owner'])) $owner = $_POST['owner'];
if(isset($_POST['active'])) $active = $_POST['active'];
if(!empty($_POST['dateActive'])) $dateActive = $_POST['dateActive'];
if(isset($_POST['appairage'])) $appairage = $_POST['appairage'];
if(!empty($_POST['dateAppairage'])) $dateAppairage = $_POST['dateAppairage'];
if(!empty($_POST['descPosition'])) $descPosition = $_POST['descPosition']; 
if(!empty($_FILES['imgObject']) && !empty($_FILES['imgObject']['name'])){
  $target_dir = "public/img_objet/";
  $target_file = $target_dir . basename($_FILES["imgObject"]["name"]);
  $file = $_FILES['imgObject']['name'];
  move_uploaded_file($_FILES['imgObject']['tmp_name'],realpath(dirname(dirname(dirname(__FILE__)))).'/'.$target_file);  
  $image=$target_file; 
}  
if(!empty($_FILES['fpObjet']) && !empty($_FILES['fpObjet']['name'])){
  $target_dir = "public/ficheProduit_objet/";
  $target_file = $target_dir . basename($_FILES["fpObjet"]["name"]);
  $file = $_FILES['fpObjet']['name'];
  move_uploaded_file($_FILES['fpObjet']['tmp_name'],realpath(dirname(dirname(dirname(__FILE__)))).'/'.$target_file);  
  $productFile=$target_file; 
}  
if(!empty($_POST['latlng'])) {
  $latlng = explode(',',$_POST['latlng']);
}

if(!empty($_POST['appairageMCs'])){
  $appairageMCs = $_POST['appairageMCs'];
}

if(!empty($_POST['newMCs']) || !empty($_POST['oldMCs'])){
  if(!empty($_POST['newMCs'])) $newMCsList = $_POST['newMCs']; else $newMCsList = array();
  if(!empty($_POST['oldMCs'])) $oldMCsList = $_POST['oldMCs']; else $oldMCsList = array();
                
  $addMCsList = array_diff($newMCsList,$oldMCsList);
  $removeMCsList = array_diff($oldMCsList,$newMCsList);
  $changeOwnerTagsList = array_intersect($oldMCsList,$newMCsList); 

}



try {

  switch ($function){
    case 'newObject':
      $objectId = '';

      if (isset($dateStreetSign))
        $objectId = newObjectStreetSign($pdo,$name,$alphaId,$objectType,$_SESSION['client_id'],$desc,$owner, $dateStreetSign);
      else 
        $objectId = newObject($pdo,$name,$alphaId,$objectType,$_SESSION['client_id'],$desc,$owner);
      $varArray=array();
      if(isset($image)) $varArray['image']="'".$image."'";
      if(isset($latlng)) {
        $varArray['objLat']=$latlng[0];
        $varArray['objLong']=$latlng[1];
      }

      //pour l'instant, nous n'avons que les tags comme MC
      //dans le future, le type de mc et son id doit etre passes
      //ex. $appairageMCs[i] = list($MCType,$MCId)
      if(isset($appairageMCs)) $varArray['appairageMCs'] = $appairageMCs;
      setObject($pdo,$objectId,$varArray);    
    break; 
    case 'setObject':
      $varArray=array();
      if(isset($name)) $varArray['name'] = "'$name'";
      if(isset($objectType)) $varArray['idParent'] = $objectType;
      if(isset($alphaId)) $varArray['alphaID'] = "'$alphaId'";
      if(isset($image)) $varArray['image'] = "'$image'";
      if(isset($productFile)) $varArray['ficheProduit'] = "'$productFile'";
      if(isset($desc)) $varArray['description'] = "'$desc'";
      if(isset($owner)) $varArray['proprietaire'] = $owner;
      if(isset($active)) $varArray['active'] = $active;
      if(isset($dateActive)) $varArray['dateActive'] = "'$dateActive'";
      if(isset($appairage)) $varArray['appairage'] = $appairage;
      if(isset($dateAppairage)) $varArray['dateAppairage'] = "'$dateAppairage'";
      if(isset($descPosition)) $varArray['descriptionPos'] = "'$descPosition'";
      if(isset($latlng)) {
        $varArray['objLat']=$latlng[0];
        $varArray['objLong']=$latlng[1];
      }
      if(isset($appairageMCs)) $varArray['appairageMCs'] = $appairageMCs;
      if(!empty($addMCsList)) $varArray['appairageMCs'] = $addMCsList;
      if(!empty($removeMCsList)) $varArray['removeMCsList'] = $removeMCsList;
      if(!empty($changeOwnerTagsList)) $varArray['changeOwnerTagsList'] = $changeOwnerTagsList;
      


      setObject($pdo,$objectId,$varArray);
    break;
  }

  $operation = 'success';
} catch (Exception $e){
  $operation = 'failure';
}





function newObject($pdo,$name,$alphaId,$objectType,$respId,$desc,$owner){

  $sql = 'SELECT Name FROM customers WHERE CustomersID='.$respId;
  $target   = $pdo->query($sql); 
  while ($row = $target->fetch()) {
    $resp = $row['Name'];
  }
  if(!isset($resp)){
    exit("Account not validated");
  }else{

        $sql = 'INSERT INTO objet(  name, idParent,  respoAppairage, description,alphaID)
        VALUES ( :name, :idParent,  :respoAppairage, :description, :alphaID)';
        
        $query = $pdo->prepare($sql);
        $query->bindValue(':name',$name,PDO::PARAM_STR);
        $query->bindValue(':idParent',$objectType,PDO::PARAM_INT);
        $query->bindValue(':respoAppairage',$resp,PDO::PARAM_STR);
        $query->bindValue(':description',$desc,PDO::PARAM_STR);
        $query->bindValue(':alphaID',$alphaId, PDO::PARAM_STR);
        $query->execute();

        $newObjectId = $pdo->lastInsertId();

        $target = $pdo->prepare('INSERT INTO societe_objet(societeID, objetID) VALUES (:societe, :objetID)');
        $target->bindValue(':societe', $owner, PDO::PARAM_INT);
        $target->bindValue(':objetID', $newObjectId, PDO::PARAM_INT);
        $target->execute();

        return $newObjectId;

  }
}

function newObjectStreetSign($pdo,$name,$alphaId,$objectType,$respId,$desc,$owner, $dateStreetSign){

  $sql = 'SELECT Name FROM customers WHERE CustomersID='.$respId;
  $target   = $pdo->query($sql); 
  while ($row = $target->fetch()) {
    $resp = $row['Name'];
  }
  if(!isset($resp)){
    exit("Account not validated");
  }else{

        $sql = 'INSERT INTO objet(  name, idParent,  respoAppairage, description,alphaID, dateStreetSign)
        VALUES (  :name, :idParent,  :respoAppairage, :description, :alphaID, :dateStreetSign)';
        
        $query = $pdo->prepare($sql);
        $query->bindValue(':name',$name,PDO::PARAM_STR);
        $query->bindValue(':idParent',$objectType,PDO::PARAM_INT);
        $query->bindValue(':respoAppairage',$resp,PDO::PARAM_STR);
        $query->bindValue(':description',$desc,PDO::PARAM_STR);
        $query->bindValue(':alphaID',$alphaId, PDO::PARAM_STR);
        $query->bindValue(':dateStreetSign',$dateStreetSign, PDO::PARAM_INT);
        $query->execute();

        $newObjectId = $pdo->lastInsertId();

        $target = $pdo->prepare('INSERT INTO societe_objet(societeID, objetID) VALUES (:societe, :objetID)');
        $target->bindValue(':societe', $owner, PDO::PARAM_INT);
        $target->bindValue(':objetID', $newObjectId, PDO::PARAM_INT);
        $target->execute();

        return $newObjectId;

  }
}

function setObject($pdo,$id,$varArray){
  if($varArray!=null || count($varArray)!=0){
    $sql = 'UPDATE objet SET ';
    $sqlArray = array();
    foreach ($varArray as $column => $value) {
      switch ($column) {
        case 'proprietaire':
          $editSocietySql="UPDATE societe_objet SET societeID = $value WHERE objetID = $id";
          $pdo->exec($editSocietySql);
        break;
        case 'appairageMCs':
          $tagExisting = false;
          foreach ($value as $MCId) {   
            $tagQuery = $pdo->prepare("SELECT * FROM tags WHERE TagID = $MCId");
            $tagQuery->execute();
            if($tag=$tagQuery->fetch()){
              $tagExisting = true;    
              $insertObjectTagQuery = $pdo->prepare("INSERT INTO objet_tags(yourlsID,objetID) VALUES(".$tag['YourlsID'].", $id)");
              $insertObjectTagQuery->execute();
              $updateTagQuery = $pdo->prepare("UPDATE tags SET appairer = 1 WHERE TagID = $MCId"); 
              $updateTagQuery->execute();               
            }
          }
          if($tagExisting){ 
            $updateObjectQuery = $pdo->prepare("UPDATE objet SET appairage = 1, dateAppairage = NOW() WHERE objetID = $id");
            $updateObjectQuery->execute();          
          }

        break;
        case 'removeMCsList' :
          foreach ($value as $MCId) {
            //desassocier les tags et l'objet
            $target = $pdo->prepare('DELETE FROM objet_tags WHERE yourlsID = (SELECT YourlsID FROM tags WHERE TagID = :MCId)');
            $target->bindValue(':MCId', $MCId, PDO::PARAM_INT);
            $target->execute(); 
            //indiquer de le tag n'est plus en appairage
            $target = $pdo->prepare('UPDATE tags SET appairer = 0 WHERE TagID = :MCId');
            $target->bindValue(':MCId', $MCId, PDO::PARAM_INT);
            $target->execute(); 
          }
        break;
        case 'changeOwnerTagsList' :
          foreach ($value as $MCId) {
            //desassocier les tags et la societe
            $target = $pdo->prepare('UPDATE societe_tags SET customersID = :owner WHERE yourlsID IN (SELECT yourlsID FROM objet_tags WHERE objetID = :idObj) ');
            $target->bindValue(':owner', $_POST['owner'], PDO::PARAM_INT);
            $target->bindValue(':idObj', $id, PDO::PARAM_INT);
            $target->execute(); 
          }
        break;
        default:
          $sqlArray[] = $column.' = '.$value;  
        break;
      }

    }
    $sql.= implode(',',$sqlArray);
    $sql.= ' WHERE objetID = '.$id;
    $query = $pdo->prepare($sql);
    $query->execute();  
  }
}


?>
