<?php
// Couleur du texte des champs si erreur saisie utilisateur
$color_font_warn="#FF0000";
// Couleur de fond des champs si erreur saisie utilisateur
$color_form_warn="#FFCC66";
// Le mail a été passé ou non
$ok_mail=false;
// Message d'erreur
$erreur="";

// Ne rien modifier ci-dessous si vous n’êtes pas certain de ce que vous faites !
if(isset($_POST['submit'])){
	// Nettoyage des entrées
	while(list($var,$val)=each($_POST)){
	if(!is_array($val)){
		$$var=strip_tags($val);
	}else{
		while(list($arvar,$arval)=each($val)){
				$$var[$arvar]=strip_tags($arval);
			}
		}
	}
	// Formatage des entrées
	// Verification des champs
	$f_1=$_POST['f_1'];
	$f_2=$_POST['f_2'];

	if(strlen($f_1)<2){
		$erreur.="<li><span class='txterror'>Le champ 'Nombre de tags souhaités' est vide ou incomplet.</span>";
		$errf_1=1;
	}
	if(strlen($f_2)<2){
		$erreur.="<li><span class='txterror'>Le champ 'Commentaires' est vide ou incomplet.</span>";
		$errf_2=1;
	}
	if($erreur==""){
		// Création du message
		/*$titre="Message de 360sc";
		$tete="From:360sc-contact@gmail.com\n";
		$corps="Nombre de tags souhaités : ".$f_1."\n";
		$corps.="Commentaires : ".$f_2."\n";
		if(mail("roumegue.jeremy@gmail.com", $titre, stripslashes($corps), $tete)){
			$ok_mail=true;
		} else {
			$erreur.="<li><span class='txterror'>Une erreur est survenue lors de l'envoi du message, veuillez refaire une tentative.</span>";
		}*/
		//Message

		$to      = 'rolland.melet@gmail.com';
		$subject = 'Demande de tags du client '.$_SESSION['societe'];
		$message = 'nombre de tag souhaités : '.$f_1."\n";
		$message.= "Commentaires : ".$f_2."\n";
		$headers = 'From: '.$_SESSION['mail']."\r\n".'Reply-To: '.$_SESSION['mail']."\r\n".'X-Mailer: PHP/'.phpversion();

		$ok_mail=mail($to, $subject, $message, $headers);
	}
}
include "./include/head2.php";
?>
<div class="row">
	<div class="columns large-12 small-6">
		<div class="panel">
			<p>
				<h3>Formulaire de contact.</h3><br/>
				<?php if($ok_mail){ ?>
					<table width='100%' border='0' cellspacing='1' cellpadding='1'>
						<tr><td><span class='txtform'>Le message ci-dessous nous a bien été transmis, et nous vous en remercions.</span></td></tr>
						<tr><td>&nbsp;</td></tr>
						<tr><td><tt><?php echo nl2br(stripslashes($message));?></tt></td></tr>
						<tr><td>&nbsp;</td></tr>
						<tr><td><span class='txtform'>Nous allons y donner suite dans les meilleurs délais.<br>A bientôt.</span></td></tr>
					</table>
				<?php } else { ?>
				<form action='<?php echo get_link(); ?>contact/' method='post' name='Form'>
					<table width='100%' border='0' cellspacing='1' cellpadding='1'>
						<?php if($erreur){ ?><tr><td colspan='2' bgcolor='red'><span class='txterror'><font color='white'><b>&nbsp;ERREUR, votre message n'a pas été transmis</b></font></span></td></tr><tr><td colspan='2'><ul><?php echo $erreur; ?></ul></td></tr><?php } ?>
						<tr><td colspan='2'><span class='txterror'>Les champs marqués d'un * sont obligatoires</span></td></tr>
						<tr><td class="form-group"><label for='f_1'>Nombre de tags souhaités* :</label><input type="text" id="f_1" name='f_1'/></td></tr>
						<tr><td class="form-group"><label for='f_2'>Commentaires* :</label><textarea id="f_2" name='f_2' rows='6' cols='40'></textarea></td></tr>
						<tr><td><input class="button" type='submit' name='submit' value='Envoyer'></td></tr>
					</table>
				</form>
				<?php } ?>
			</p>
		</div>
	</div>		  
</div>
<?php
/**
* We include the specific footer for home page.
*/
include "./include/footer2.php";
?>