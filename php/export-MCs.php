<?php

switch($_SERVER['REQUEST_METHOD']){
  case 'GET':

  require_once "php/Smarty/libs/Smarty.class.php";
  $smarty = new Smarty();
  $smarty->template_dir = "templates";
  $smarty->compile_dir = "templates_c";
  $smarty->caching = false;
  $smarty->left_delimiter = "<{";
  $smarty->right_delimiter = "}>";
 

   //récupérer les sociétés disponibles
  $owner = $_SESSION['societe'];
  $societiesIds = getWrapSocietes($owner);
  $societiesIds[] = $owner;
  $function = "getSocietiesByIds";
  require_once "php/DAF/getSocieties.php";
  $dataSocieties = array();
  foreach ($societies as $societyId => $society) {
    $dataSocieties[$societyId] = $society['name'];
  }

  $smarty->assign("postPage",SERVERROOT.'/export-MCs/');
  $smarty->assign("societyName",array_values($dataSocieties)[0] );
  $smarty->assign("societies",$dataSocieties); 
  $smarty->assign("serverRoot",SERVERROOT);
  $smarty->display("export-MCs.tpl");
  break;

  case 'POST': 

    require_once "php/PHPExcel.php";
    require_once "php/PHPExcel/IOFactory.php";

    $clientKey = get_client_key();

    /**
      * Making basic object and init them.
      */
    $classeur = new PHPExcel;
    $classeur->getProperties()->setCreator("360sc");
    $classeur->setActiveSheetIndex(0);
    $feuille=$classeur->getActiveSheet();

    $feuille->setTitle('Tags');
      //Making the title rows
    $feuille->setCellValueByColumnAndRow(0, 1, 'MCID');
    $feuille->setCellValueByColumnAndRow(1, 1, 'Nom du MC');
    $feuille->setCellValueByColumnAndRow(2, 1, 'Description du MC');
    $feuille->setCellValueByColumnAndRow(3, 1, 'URLe complet');
    $feuille->setCellValueByColumnAndRow(4, 1, 'OriginalURL');
    $feuille->setCellValueByColumnAndRow(5, 1, 'Nom de la societe');
    $feuille->setCellValueByColumnAndRow(6, 1, 'UniqueTagID');

      //il faut détecter les type des MCs
    $function = 'getTags';
    require_once "php/DAF/getTags.php";

    $MCs=array();
    $MCs=array_merge($MCs,$tags);
    $i=2;
    foreach ($tags as $tagId => $tag) { 
      $feuille->setCellValueByColumnAndRow(0,$i, $tag['TagID']);
      $feuille->setCellValueByColumnAndRow(1, $i, $tag['TagName']);
      $feuille->setCellValueByColumnAndRow(2, $i, $tag['description']);
          // Making URLe
      $cVigenere = new classVigenere($clientKey);
      $realestate = $tag['customersDomain'].'/'.$cVigenere->encrypt(trim($tag['ShortURL']));
      $feuille->setCellValueByColumnAndRow(3, $i, $realestate);

      $feuille->setCellValueByColumnAndRow(5, $i, $tag['name']);        
      $feuille->setCellValueByColumnAndRow(4, $i, $tag['OriginalURL']);
      $feuille->setCellValueByColumnAndRow(6, $i, $tag['UniqueTagID']);
      $i++;
    }

      //Making more readable

    $feuille->getColumnDimension('B')->setWidth(20);
    $feuille->getColumnDimension('C')->setWidth(20);
    $feuille->getColumnDimension('D')->setWidth(20);
    $feuille->getColumnDimension('E')->setWidth(20);
    $feuille->getColumnDimension('F')->setWidth(20);
    $feuille->getColumnDimension('G')->setWidth(20); 
 
    if(!empty($_POST['societyName'])) 
      $societyName=$_POST['societyName'].'-';
    else
      $societyName='';
      /**
      * Making a files to be export and download.
      */
      header("Content-Type: application/x-msexcel");
      header('Content-Disposition: attachment;filename="'.$societyName.'MCs.xlsx"');
      $writer = PHPExcel_IOFactory::createWriter($classeur, 'Excel2007'); 
      $writer->save('php://output');
      header("Location: ".SERVERROOT."/export-MCs");

  break;
}

?>