<?php 
/** 
* Multi Add Page
* @author: ROUMEGUE Jérémy - 360sc
* We include the header for regular page.
*/
include "./include/head2.php";
?>
		<div class="row">
		  <div class="columns large-12 small-6">
			<div class="panel">
				<p>
					
					<?php
						/**
						* We open the database for the SQL request.
						*/
						$bdd = connection_db();
						if($_SESSION['level'] == "4") {
							$query=$bdd->prepare('SELECT COUNT(*) AS nbr FROM societe_objet');
						} else {
							$query=$bdd->prepare('SELECT COUNT(*) AS nbr FROM societe_objet WHERE societeID =:id');
							$query->bindValue(':id',$_SESSION['societe'], PDO::PARAM_INT);
						}
						$query->execute();
						$nb_tags=($query->fetchColumn()==0)?true:false;
						$query->CloseCursor();
						
						if($nb_tags){
							?>
							<p>
								Vous n'avez aucun objet. Cliquez <a href="/objet/">ici</a> pour en créer.
							</p>
							<?php
						} else {
							?>
							<p>
								Voici la liste de vos objets.<br/>
							</p>
							<?php

							/**
							* The SQL request
							*/
							if($_SESSION['level'] == "4") {
								$query=$bdd->prepare('SELECT DISTINCT * FROM objet AS o, societe_objet AS co WHERE o.objetID = co.objetID;');
							} else {
								$sql = 'SELECT DISTINCT * FROM objet AS o, societe_objet AS co WHERE co.societeID=:id 
								AND o.objetID = co.objetID;';
								$query=$bdd->prepare($sql);
								$query->bindValue(':id',$_SESSION['societe'], PDO::PARAM_INT);
							}
							$query->execute();
							
							/**
							* Display form on screen
							*/
							echo "<table>";
							echo "<tr>
							<th>Nom</th>
							<th>Description</th>
							<th>Description Position</th>
							<th>Apparé</th>
							<th>Date Apparage</th>
							<th>Activé</th>
							<th>Date d'Activation</th>
							<th>Latitude</th>
							<th>Longitude</th>
							</tr>";
							
							/** 
							* Show ALL the object
							*/
							while ($row = $query->fetch()) {
								echo '<tr id="'.$row['objetID'].'" class="objet-id">
								<td>'.$row['name'].'</td>
								<td>'.$row['description'].'</td>
								<td>'.$row['descriptionPos'].'</td>
								<td>'.$row['appairage'].'</td>
								<td>'.$row['dateAppairage'].'</td>
								<td>'.$row['active'].'</td>
								<td>'.$row['dateActive'].'</td>
								<td>'.$row['objLat'].'</td>
								<td>'.$row['objLong'].'</td>
								</tr>';
							}
							
							echo "</table>";
							
							/**
							* Closing database
							*/
							$query->closeCursor();
						}
					?>
				</p>
			</div>
		  </div>		  
		</div>
<?php
/**
* We include the footer for regular page.
*/
include "./include/footer2.php";
?>