<?php 
/** 
* Multi Add Page
* @author: ROUMEGUE Jérémy - 360sc
* We include the header for regular page.
*/
include "./include/head2.php";
?>
		<div class="row">
		  <div class="columns large-12 small-6">
			<div class="panel">
				<p>
					Voici les URLe libres dont vous disposez.<br/>
					<?php 
						/**
						* We open the database for the SQL request.
						*/
						$bdd = connection_db();

						$clientKey = get_client_key();

						/**
						* Prepare SQL request
						*/
						$sql = 'SELECT t.TagID, t.TagName, t.UniqueTagID,y.ShortURL, y.OriginalURL 
						FROM 360sc_iz.yourls AS y, tags AS t, societe_tags AS ct WHERE t.YourlsID=y.YourlsID
						AND y.YourlsID = ct.yourlsID';
						$target = $bdd->query($sql);
						

						/**
						* Display form on screen
						*/
						echo "<table>";
						echo "
						<tr>
						<th colspan=5>Tags Appair&eacute;s</td>
						</tr>
						<tr>
						<th>ID</th>
						<th>Name</th>
						<th>ShortURL</th>
						<th>URL de sortie</th>
						<th>UID</th>
						</tr>";

						/**
						* Making the table.
						*/
						$i=1;
						while ($row = $target->fetch()) {
							echo "<tr><td>".$row['TagID']."</td>";
							echo "<td>".$row['TagName']."</td>";
								$cVigenere = new classVigenere($clientKey);
								$realestate = get_client_domain().'/'.$cVigenere->encrypt(trim($row['ShortURL']));
							echo "<td>".$realestate."</td>";
							echo "<td>".$row['OriginalURL']."</td>";
							echo "<td>".$row['UniqueTagID']."</td></tr>";
						}
						
						echo "</table>";

						/**
						* Closing database.
						*/
						$target->closeCursor();
					?>
				</p>
			</div>
		  </div>		  
		</div>
<?php
/**
* We include the footer for regular page.
*/
include "./include/footer2.php";
?>