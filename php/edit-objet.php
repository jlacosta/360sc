<?php 
/** 
* Multi Add Page
* @author: ROUMEGUE Jérémy - 360sc
* We include the header for regular page.
*/
include "./include/head2.php";
?>
		<div class="row">
		  <div class="columns large-12 small-6">
			<div class="panel">
				<p>
					Voici la liste de vos tags.<br/>
					<?php
						/**
						* We open the database for the SQL request.
						*/
						$bdd = connection_db();
						
						$query=$bdd->prepare('SELECT COUNT(*) AS nbr FROM customers_objet WHERE customersID =:id');
						$query->bindValue(':id',$_SESSION['client_id'], PDO::PARAM_INT);
						$query->execute();
						$nb_tags=($query->fetchColumn()==0)?true:false;
						$query->CloseCursor();
						
						if($nb_tags){
							?>
							<p>
							Vous n'avez aucun objet.
							</p>
							<?php
						} else {
							/**
							* The SQL request
							*/
							$sql = 'SELECT o.objetID, o.name, o.appairage, o.dateAppairage, o.description, o.active, 
							o.dateActive, o.descriptionPos
							FROM objet AS o, customers_objet AS co 
							WHERE co.objetID = o.objetID AND co.customersID = '.$_SESSION['client_id'].';';
							$target = $bdd->query($sql);
							
							/**
							* Display form on screen
							*/
							echo "<table id='object'>";
							echo "<tr>
							<th>Nom</th>
							<th>Description</th>
							<th>Active</th>
							<th>Date Active</th>
							<th>Appairage</th>
							<th>Date Appairage</th>
							<th>Description Position</th>
							</tr>";
							
							/** 
							* Show ALL the tag
							*/
							while ($row = $target->fetch()) {
								echo '<tr id="'.$row['objetID'].'" class="tag-id">
								<td>'.$row['name'].'</td>
								<td>'.$row['description'].'</td>
								<td>'.$row['active'].'</td>
								<td>'.$row['dateActive'].'</td>
								<td>'.$row['appairage'].'</td>
								<td>'.$row['dateAppairage'].'</td>
								<td>'.$row['descriptionPos'].'</td>
								</tr>';
							}
							
							echo "</table>";
							
							/**
							* Closing database
							*/
							$target->closeCursor();
						}
					?>
				</p>
			</div>
		  </div>		  
		</div>
<?php
/**
* We include the footer for regular page.
*/
include "./include/footer2.php";
?>