<?php

switch ($_SERVER['REQUEST_METHOD']) {
  case 'GET':

    require_once "php/Smarty/libs/Smarty.class.php";
    require_once "php/Smarty/libs/SmartyPaginate.class.php";

    $smarty = new Smarty();
    $smarty->template_dir = "templates";
    $smarty->compile_dir = "templates_c";
    $smarty->caching = false;
    $smarty->left_delimiter = "<{";
    $smarty->right_delimiter = "}>";
     // required connect
    SmartyPaginate::connect("objects");
    // set items per page
    if(isset($_GET['limit']) && is_numeric($_GET['limit']) && $_GET['limit']>0)
      SmartyPaginate::setLimit(round($_GET['limit']),"objects");
    else
      SmartyPaginate::setLimit(25,"objects");

    $function = "getObjectsPage";//$function = "getObjectsByIdsPage";
    $objectIds = $_SESSION['objets'];// si $_SESSION['objets'] ne fonctionne pas, c'est dangereux
    $godRight = $_SESSION['level']==4?true:false;
    require_once "php/DAF/getObjects.php";


    $smarty->assign("godRight",$godRight);
    $smarty->assign("serverRoot",SERVERROOT); 
    $smarty->assign("objects",$objects);  
    SmartyPaginate::assign($smarty,"paginate","objects");
    $smarty->display("list-objects.tpl");
  break;

  case 'POST':
  break;
}


?>