<?php

switch ($_SERVER['REQUEST_METHOD']) {
  case 'GET':

    require_once "php/Smarty/libs/Smarty.class.php";

    $smarty = new Smarty();
    $smarty->template_dir = "templates";
    $smarty->compile_dir = "templates_c";
    $smarty->caching = false;
    $smarty->left_delimiter = "<{";
    $smarty->right_delimiter = "}>";

    $smarty->assign("postPage",SERVERROOT.'/type');
    $smarty->assign("godRight",$_SESSION['level']==4?true:false);
    $smarty->assign("serverRoot",SERVERROOT);
    $smarty->display("new-objectType.tpl");

  break;
  
  case 'POST':
    $function = 'newObjectType';
    if(isset($_POST['commonType'])) {
      $owner = null;
    }else{
      $owner = $_SESSION['societe'];
    }
    require_once "php/DAF/setObjectTypes.php";

    header("Location: ".SERVERROOT."/type?operation=$operation");
  break;

  default:
  
  break;
}


?>
