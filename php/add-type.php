<?php 
/** 
* Multi Add Page
* @author: ROUMEGUE Jérémy - 360sc
* We include the header for regular page.
*/
include "./include/head2.php";
?>
		<div class="row">
		  <div class="columns large-12 small-6">
			<div class="panel">
			<?php if(!(isset($_POST['name']))){?>
				<p>
					Bienvenue dans l'interface d'administration de IZ.<br/>
					<!-- Form for encrypt an URLs in many tags -->
					<form action="<?php echo get_link(); ?>type/" method="post">
						<div class="form-group">
							<label for="name">Nom du type</label>
							<input id="name" type="text" name="name" />
						</div>
						<div class="form-group">
							<label for="alpha">References externes du type d'objet</label>
							<input id="alpha" type="text" name="alpha" />
						</div>
						<div class="form-group">
							<label for="desc">Description du type</label>
							<input id="desc" type="text" name="desc" />
						</div>
						<?php if($_SESSION['level']==4){?>
						<div class="form-group">
							<input id="commonType" type="checkbox" name="commonType" />
							<label for="commonType">Type commun</label>							
						</div>
						<?php }?>
						<input class="medium button" type="submit" value="Envoyer" />
					</form>
				</p>
			<?php } else {
				if(isset($_POST['commonType'])) {
						$prop = null;
					} else {
						$prop = $_SESSION['societe'];
					}
				$bdd = connection_db();

				$query=$bdd->prepare('INSERT INTO typeobjet(idType, nameType, alphaID, image, description, proprietaire) 
									VALUES (:typeID, :name, :alpha, :image, :desc, :prop)');
				$query->bindValue(':typeID',NULL,PDO::PARAM_INT);
				$query->bindValue(':name',$_POST['name'],PDO::PARAM_STR);
				$query->bindValue(':alpha',$_POST['alpha'],PDO::PARAM_STR);
				$query->bindValue(':image',NULL,PDO::PARAM_STR);
				$query->bindValue(':desc',$_POST['desc'],PDO::PARAM_STR);
				$query->bindValue(':prop',$prop);
				$query->execute();
				
				//createOneObject($_POST['name'], $_SESSION['client_id']);
				//
				echo '<p>Le type a bien été ajouté.<br />Cliquez <a href="'.get_link().'">ici</a> 
							pour revenir à la page d accueil</p>';
			}
			?>
			</div>
		  </div>		  
		</div>
<?php
/**
* We include the footer for regular page.
*/
include "./include/footer2.php";
?>