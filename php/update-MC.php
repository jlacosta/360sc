<?php

switch($_SERVER['REQUEST_METHOD']){
  case 'GET':

  require_once "php/Smarty/libs/Smarty.class.php";
  $smarty = new Smarty();
  $smarty->template_dir = "templates";
  $smarty->compile_dir = "templates_c";
  $smarty->caching = false;
  $smarty->left_delimiter = "<{";
  $smarty->right_delimiter = "}>";

  //récupérer le MC à modifier
  //il faut détecter le type de MC
  //ici on a que tag
  $function = "getTagById";
  $tagId=$MCId;
  require_once "php/DAF/getTags.php";
  //unifier le nom des champs
  $MC = array();
  $MC['name']=$tag['TagName'];
  $MC['description']=$tag['description'];
  $MC['url']=$tag['OriginalURL'];
  $MC['active']=$tag['isActive'];
  $MC['societyId']=$tag['customersID']; 

   //récupérer les sociétés disponibles
  $owner = $_SESSION['societe'];
  $societiesIds = getWrapSocietes($owner);
  $societiesIds[] = $owner;
  $function = "getSocietiesByIds";
  require_once "php/DAF/getSocieties.php";
  $dataSocieties = array();
  foreach ($societies as $societyId => $society) {
    $dataSocieties[$societyId] = $society['name'];
  }

  $smarty->assign("postPage",SERVERROOT.'/MC/'.$MCId);
  //on a que tag pour l'instant
  $smarty->assign("MC",$MC);
  $smarty->assign("societies",$dataSocieties); 
  $smarty->assign("serverRoot",SERVERROOT);
  $smarty->display("edit-MC.tpl");
  break;

  case 'POST': 
    
    //il faut détecter le type de MC
    $function = 'setTag';
    $tagId = $MCId;
    $tagName = $_POST['name'];


    require_once "php/DAF/setTags.php";

    header("Location: ".SERVERROOT."/MC/$MCId?operation=$operation");


  break;
}

?>