<?php 

	if(($_SESSION['page'] != "SansConnect") && ($_SESSION['connect'] === false)){
		$_SESSION['page'] = "connexion";
		$_SESSION['redirect'] = true;
		header('Location: '.get_link().'connexion/');
		exit();
	}

?>	

<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
<html ng-app="application">
<head>
	<meta charset='UTF-8' />

	<?php 
		echo "<title>360SmartMachine - ".$_SESSION['page']."</title>";
	?>

	<meta name="description" content="" />
	<meta name="author"			content="" />
	<meta name="publisher"		content="Rolland Mellet" />
	<meta name="keywords"		content="" />
	<meta name='viewport' content='width=device-width, initial-scale=1.0' />
	<meta property="og:title" content="360SmartMachine" />
	<meta name='viewport' content='width=device-width, initial-scale=1.0' />
	
	<?php
		echo "<link rel='shortcut icon' type='image/png' href='".get_link()."img/favicon.png'>";
	?>
	<link rel='pingback' href='http://www.blocparc.fr/xmlrpc.php' />

	<?php 
		echo "
			<link rel='stylesheet' href='".get_link()."js/bower_components/files/css/bootstrap.min.css' />
			<link rel='stylesheet' href='".get_link()."js/bower_components/css/leaflet.css'/>
			<link rel='stylesheet' href='".get_link()."js/bower_components/css/MarkerCluster.css' />
			<link rel='stylesheet' href='".get_link()."js/bower_components/css/MarkerCluster.Default.css' />
			<link rel='stylesheet' href='".get_link()."js/bower_components/css/font-awesome.css'/>
			<link rel='stylesheet' href='".get_link()."js/bower_components/css/leaflet.extra-markers.css'/>
			<link rel='stylesheet' href='".get_link()."js/bower_components/css/leaflet.fullscreen.css'/>
			
			<link rel='stylesheet' href='".get_link()."js/bower_components/files/css/entypo.css'/>
			
			<link rel='stylesheet' href='".get_link()."js/bower_components/files/css/gx-sidemenu-light.css'/>
			<link rel='stylesheet' href='".get_link()."js/bower_components/files/assets/jquery.snippet.css'/>

			<link rel='stylesheet' href='".get_link()."js/bower_components/files/css/m360sc.css'/>
		";
	?>

	<!-- <script src='https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> -->

	<script src='https://cdn.socket.io/socket.io-1.2.0.js'></script>
	<script src='https://maps.googleapis.com/maps/api/js?sensor=false&KEY=AIzaSyAeaVmn26e5tMD1kYr68MiLIii9jG91rdQ'></script>

	<?php 
		echo "

			<script src='".get_link()."js/bower_components/files/js/jquery.js'></script>

			<script src='".get_link()."js/vendor/modernizr.js'></script>
			<script type='text/javascript' src='".get_link()."js/fuzzyset.js'></script>
			<script type='text/javascript' src='".get_link()."js/underscore.js'></script>

			<script src='".get_link()."js/bower_components/files/js/iscroll.js'></script>
			<script src='".get_link()."js/bower_components/files/js/gx.sidemenu.js'></script>
			<script src='".get_link()."js/bower_components/files/assets/jquery.snippet.js'></script>

			<script src='".get_link()."js/bower_components/js/angular.min.js'></script>
			<script src='".get_link()."js/bower_components/js/bindonce.js'></script>
			<script src='".get_link()."js/bower_components/js/leaflet.js'></script>
			<script src='".get_link()."js/bower_components/js/Leaflet.fullscreen.js'></script>
			<script src='".get_link()."js/bower_components/js/Google.js'></script>
			<script src='".get_link()."js/bower_components/js/index.js'></script>
			<script src='".get_link()."js/bower_components/js/leaflet.markercluster.js'></script>
			<script src='".get_link()."js/bower_components/js/angular-leaflet-directive.js'></script>
			<script src='".get_link()."js/bower_components/js/leaflet.awesome-markers.js'></script>
			<script src='".get_link()."js/bower_components/js/leaflet.extra-markers.js'></script>
			<script src='".get_link()."js/bower_components/js/angular-socket.js'></script>

			<script src='".get_link()."js/bower_components/files/js/m360sc.js'></script>
		";
	?>

	<!-- Module App AngularJs -->
	<?php 

		echo "<script src='".get_link()."js/bower_components/files/js/angularApp/services/DomService.js'></script>";
		echo "<script src='".get_link()."js/bower_components/files/js/angularApp/factories/ObjectsMapFactory.js'></script>";
		echo "<script src='".get_link()."js/bower_components/files/js/angularApp/services/ObjectsService.js'></script>";
		echo "<script src='".get_link()."js/bower_components/files/js/angularApp/controllers/ObjectsController.js'></script>";
		echo "<script src='".get_link()."js/bower_components/files/js/angularApp/app.js'></script>";
		
	?>

</head>
<body ng-controller="ObjectController">
<!-- 
	<a href="javascript:" class="gx-menu gx-trigger-left entypo list"></a>
			<a href="javascript:" class="gx-menu gx-trigger-right entypo list"></a> -->

	<nav id="360-navbar" class="navbar navbar-default navbar-fixed-top">
		<a href="javascript:" class="gx-menu gx-trigger-left entypo list"></a>
		<a href="javascript:" class="gx-menu gx-trigger-right entypo list"></a>
		<div class="container">
			<h2>360 Smart Connect</h2>
		</div>
	</nav>

	<leaflet class="panel" id="map" controls="controls" event-broadcast="events" lf-center="center"  markers="markers" layers="layers"></leaflet>


	<div id="scroller">
		<div id="wrapper" class="waitsObjects">
			<div>
				<nav class="sidebars">
					<!-- side menu right -->
					<div id="gx-sidemenu-right" style="z-index: 9999">
						<div class="gx-sidemenu-inner">
							<div class="scroll">
								<ul class="gx-menu">
									<li class="back">
										<a href="javascript:" class="gx-trigger-right">
											<span class="icon entypo chevron-left"></span>
											<span class="text">Back</span>
										</a>
									</li>
									<li class="facebook">
										<a href="https://www.facebook.com/gexasoftware.hu" target="_blank">
											<span class="icon entypo-social facebook"></span>
											<span class="text">Facebook</span>
										</a>
									</li>
									<li class="facebook">
										<a href="https://www.facebook.com/gexasoftware.hu" target="_blank">
											<span class="icon entypo-social facebook"></span>
											<span class="text">Facebook</span>
										</a>
									</li>
									<li class="facebook">
										<a href="https://www.facebook.com/gexasoftware.hu" target="_blank">
											<span class="icon entypo-social facebook"></span>
											<span class="text">Facebook</span>
										</a>
									</li>
									<li class="facebook">
										<a href="https://www.facebook.com/gexasoftware.hu" target="_blank">
											<span class="icon entypo-social facebook"></span>
											<span class="text">Facebook</span>
										</a>
									</li>
									<li class="facebook">
										<a href="https://www.facebook.com/gexasoftware.hu" target="_blank">
											<span class="icon entypo-social facebook"></span>
											<span class="text">Facebook</span>
										</a>
									</li>
									<li class="facebook">
										<a href="https://www.facebook.com/gexasoftware.hu" target="_blank">
											<span class="icon entypo-social facebook"></span>
											<span class="text">Facebook</span>
										</a>
									</li>
									<li class="facebook">
										<a href="https://www.facebook.com/gexasoftware.hu" target="_blank">
											<span class="icon entypo-social facebook"></span>
											<span class="text">Facebook</span>
										</a>
									</li>
									<li class="facebook">
										<a href="https://www.facebook.com/gexasoftware.hu" target="_blank">
											<span class="icon entypo-social facebook"></span>
											<span class="text">Facebook</span>
										</a>
									</li>
									<li class="facebook">
										<a href="https://www.facebook.com/gexasoftware.hu" target="_blank">
											<span class="icon entypo-social facebook"></span>
											<span class="text">Facebook</span>
										</a>
									</li>
									<li class="facebook">
										<a href="https://www.facebook.com/gexasoftware.hu" target="_blank">
											<span class="icon entypo-social facebook"></span>
											<span class="text">Facebook</span>
										</a>
									</li>
									<li class="facebook">
										<a href="https://www.facebook.com/gexasoftware.hu" target="_blank">
											<span class="icon entypo-social facebook"></span>
											<span class="text">Facebook</span>
										</a>
									</li>
									<li class="facebook">
										<a href="https://www.facebook.com/gexasoftware.hu" target="_blank">
											<span class="icon entypo-social facebook"></span>
											<span class="text">Facebook</span>
										</a>
									</li>
									<li class="facebook">
										<a href="https://www.facebook.com/gexasoftware.hu" target="_blank">
											<span class="icon entypo-social facebook"></span>
											<span class="text">Facebook</span>
										</a>
									</li>
									<li class="facebook">
										<a href="https://www.facebook.com/gexasoftware.hu" target="_blank">
											<span class="icon entypo-social facebook"></span>
											<span class="text">Facebook</span>
										</a>
									</li>
									<li class="pinterest">
										<a href="javascript:">
											<span class="icon entypo-social pinterest"></span>
											<span class="text">Pinterest</span>
										</a>
									</li>
									<li class="linkedin">
										<a href="javascript:">
											<span class="icon entypo-social linkedin"></span>
											<span class="text">LinkedIn</span>
										</a>
									</li>
									<li class="skype">
										<a href="javascript:">
											<span class="icon entypo-social skype"></span>
											<span class="text">Skype</span>
										</a>
									</li>
									<li class="google">
										<a href="javascript:">
											<span class="icon entypo-social googleplus"></span>
											<span class="text">Google+</span>
										</a>
									</li>
									<li class="twitter">
										<a href="javascript:">
											<span class="icon entypo-social twitter"></span>
											<span class="text">Twitter</span>
										</a>
									</li>
									<li class="soundcloud">
										<a href="javascript:">
											<span class="icon entypo-social soundcloud"></span>
											<span class="text">SoundCloud</span>
										</a>
									</li>
									<li class="dropbox">
										<a href="javascript:">
											<span class="icon entypo-social dropbox"></span>
											<span class="text">DropBox</span>
										</a>
									</li>
									<li class="paypal">
										<a href="javascript:">
											<span class="icon entypo-social paypal"></span>
											<span class="text">PayPal</span>
										</a>
									</li>
								</ul>
							</div>	
						</div>
					</div>
					<!-- side menu left -->
					<div id="gx-sidemenu" style="z-index: 9998">
						<div class="gx-sidemenu-inner" id="gx-sidemenu-inner-1">
							<div class="scroll">
								<ul class="gx-menu">
									<li class="back">
										<a href="javascript:" class="gx-trigger-left">
											<span class="icon entypo chevron-left"></span>
											<span class="text">Back</span>
										</a>
									</li>
									<li class="news">
										<a href="#news">
											<span class="icon entypo newspaper"></span>
											<span class="text">Objets</span>
										</a>
										<ul>
											<li>
												<a href="javascript:">
													<span class="icon entypo pencil"></span>
													<span class="text">Name Objet</span>
												</a>
												<ul>
													<li>
														<a href="?article=false">
															<span class="icon entypo list"></span>
															<span class="text">Next level...</span>
														</a>
													</li>
													<li>
														<a href="?article=true">
															<span class="icon entypo video"></span>
															<span class="text">Another sub element...</span>
														</a>
													</li>
												</ul>
											</li>
											<li bindonce ng-repeat="object in objects">
												<a href="javascript:" bo-if="renderGxMenu($last)">
													<span class="icon entypo location"></span>
													<span bo-text="object.name" class="text"></span>
												</a>
											</li>
										</ul>
									</li>
									<li class="gallery">
										<a href="?action=galleries">
											<span class="icon entypo picture"></span>
											<span class="text">Galleries</span>
										</a>
									</li>
									<li class="users">
										<a href="javascript:">
											<span class="icon entypo users"></span>
											<span class="text">Users</span>
										</a>
										<ul>
											<li>
												<a href="?login=true" target="_blank">
													<span class="icon entypo user"></span>
													<span class="text">Login</span>
												</a>
											</li>
										</ul>
									</li>
									<li class="category">
										<a href="javascript:">
											<span class="icon entypo folder"></span>
											<span class="text">Categories</span>
										</a>
									</li>
									<li class="category">
										<a href="javascript:">
											<span class="icon entypo folder"></span>
											<span class="text">Categories</span>
										</a>
									</li>
									<li class="category">
										<a href="javascript:">
											<span class="icon entypo folder"></span>
											<span class="text">Categories</span>
										</a>
									</li>
									<li class="category">
										<a href="javascript:">
											<span class="icon entypo folder"></span>
											<span class="text">Categories</span>
										</a>
									</li>

									<li class="category">
										<a href="javascript:">
											<span class="icon entypo folder"></span>
											<span class="text">Categories</span>
										</a>
									</li>
									<li class="category">
										<a href="javascript:">
											<span class="icon entypo folder"></span>
											<span class="text">Categories</span>
										</a>
									</li>
									<li class="category">
										<a href="javascript:">
											<span class="icon entypo folder"></span>
											<span class="text">Categories</span>
										</a>
									</li>
									<li class="category">
										<a href="javascript:">
											<span class="icon entypo folder"></span>
											<span class="text">Categories</span>
										</a>
									</li>
									<li class="category">
										<a href="javascript:">
											<span class="icon entypo folder"></span>
											<span class="text">Categories</span>
										</a>
									</li>
									<li class="category">
										<a href="javascript:">
											<span class="icon entypo folder"></span>
											<span class="text">Categories</span>
										</a>
									</li>
									<li class="category">
										<a href="javascript:">
											<span class="icon entypo folder"></span>
											<span class="text">Categories</span>
										</a>
									</li>
									<li class="category">
										<a href="javascript:">
											<span class="icon entypo folder"></span>
											<span class="text">Categories</span>
										</a>
									</li>
									<li class="category">
										<a href="javascript:">
											<span class="icon entypo folder"></span>
											<span class="text">Categories</span>
										</a>
									</li>
									<li class="category">
										<a href="javascript:">
											<span class="icon entypo folder"></span>
											<span class="text">Categories</span>
										</a>
									</li>
								</ul>
							</div>
						</div>

						<div id="gx-sidemenu-login">
							<!-- <div class="divider"></div>
							<h2>
								<span class="icon entypo user"></span>
								<span class="text">Welcome, !</span>
							</h2>
							<div class="divider"></div>
							<a href="?edit=profile" class="login-btn">Edit profile</a>
							<div class="divider"></div>
							<a href="?logout" class="login-btn">Log out...</a>
							<div class="divider"></div> -->
						</div>
					</div>
				</nav>
			</div>
		</div>
	</div>	
	
	<!-- <script type="text/javascript" src="./build/bundle.js"></script> -->
</body>
</html>