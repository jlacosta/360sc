<?php 
/** 
* Multi Add Page
* @author: ROUMEGUE Jérémy - 360sc
* We include the header for regular page.
*/
include "./include/head2.php";
?>
		<div class="row">
		  <div class="columns large-12 small-6">
			<div class="panel">
				<p>
					<?php
						if($_SESSION['page'] != ""){

							echo "Modifier le tags.<br/>";
							/**
							* We open the database for the SQL request.
							*/
							$bdd = connection_db();
							if($_SESSION['level'] == "4") {
								$query=$bdd->prepare('SELECT COUNT(*) AS nbr FROM societe_tags');
							} else {
								$query=$bdd->prepare('SELECT COUNT(*) AS nbr FROM societe_tags WHERE customersID =:id');
								$query->bindValue(':id',$_SESSION['societe'], PDO::PARAM_INT);
							}
							
							$query->execute();
							$nb_tags=($query->fetchColumn()==0)?true:false;
							$query->CloseCursor();
							
							if($nb_tags){
								?>
								<p>
								Vous n'avez aucun tag. Cliquez <a href="/contact/">ici</a> pour en commander.
								</p>
								<?php
							} else {
								?>
								<?php
								/**
								* The SQL request
								*/
								if($_SESSION['level'] == "4") {
									$sql = 'SELECT t.TagID, t.TagName, t.description, t.isActive, y.ShortURL, y.OriginalURL FROM 360sc_iz.yourls 
									AS y, tags AS t, societe_tags AS ct WHERE t.YourlsID=y.YourlsID AND ct.yourlsID = t.YourlsID ;';
								} else {
									$sql = 'SELECT t.TagID, t.TagName, t.description, t.isActive, y.ShortURL, y.OriginalURL FROM 360sc_iz.yourls AS y, tags AS t, societe_tags AS ct 
									WHERE t.YourlsID=y.YourlsID AND ct.yourlsID = t.YourlsID AND ct.customersID = '.$_SESSION['societe'].';';
								}
								$target = $bdd->query($sql);
								
								/**
								* Display form on screen
								*/
								echo "<form method='post' action='".get_link()."edit/'>";
								
								/** 
								* Show ALL the tag
								*/
								while ($row = $target->fetch()) {
									if($row['TagID'] == $id) {
										echo '
										<div class="group-label">
											<label>Nom : </label><input name="TagName" value="'.$row['TagName'].'" type="text" />
										</div>
										<div class="group-label">
											<label>Description : </label><input name="description" value="'.$row['description'].'" type="text" />
										</div>
										<div class="group-label">
											<label>URL de sortie: </label><input name="OriginalURL" value="'.$row['OriginalURL'].'" type="text" />
										</div>
										<div class="group-label">
											<label>Active : </label><input name="isActive" disabled/>
										</div>';
									}
								}
								
								echo "<input class='button' type='sumbit' value='Envoyer'/></form>";
								
								/**
								* Closing database
								*/
								$target->closeCursor();
							}
						} else {
							/**
							* We open the database for the SQL request.
							*/
							$bdd = connection_db();

							if(isset($_POST['TagName']) && $_POST['TagName'] != NULL && $_POST['TagName'] != "" ) {
								/**
								* The SQL request
								*/
								$target = $bdd->prepare('UPDATE tags SET TagName = :TagName WHERE TagID = :TagID');
								$target->bindValue(':TagName', $_POST['TagName'], PDO::PARAM_STR);
								$target->bindValue(':TagID', $id, PDO::PARAM_STR);
								$target->execute();
							}

							if(isset($_POST['description']) && $_POST['description'] != NULL && $_POST['description'] != "" ) {
								/**
								* The SQL request
								*/
								$target = $bdd->prepare('UPDATE tags SET description = :description WHERE TagID = :TagID');
								$target->bindValue(':description', $_POST['description'], PDO::PARAM_STR);
								$target->bindValue(':TagID', $id, PDO::PARAM_STR);
								$target->execute();
							}

							if(isset($_POST['OriginalURL']) && $_POST['OriginalURL'] != NULL && $_POST['OriginalURL'] != "" ) {
								/**
								* The SQL request
								*/
								$target = $bdd->prepare('UPDATE tags SET OriginalURL = :OriginalURL WHERE TagID = :TagID');
								$target->bindValue(':OriginalURL', $_POST['OriginalURL'], PDO::PARAM_STR);
								$target->bindValue(':TagID', $id, PDO::PARAM_STR);
								$target->execute();
							}

							if(isset($_POST['isActive']) && $_POST['isActive'] != NULL && $_POST['isActive'] != "" ) {
								/**
								* The SQL request
								*/
								$target = $bdd->prepare('UPDATE tags SET isActive = :isActive WHERE objetID = :TagID');
								$target->bindValue(':isActive', $_POST['isActive'], PDO::PARAM_STR);
								$target->bindValue(':TagID', $id, PDO::PARAM_STR);
								$target->execute();
							}

							header('Location: '.get_link().'');
						}
					?>
				</p>
			</div>
		  </div>		  
		</div>
<?php
/**
* We include the footer for regular page.
*/
include "./include/footer2.php";
?>