<?php 
/** 
* Multi Add Page
* @author: Jorge Luis - 360sc
* We include the header for regular page.
*/
include "./include/head.php";
?>
<br>
<br>
<div class="row">
	<div class"large-12 small-12 columns">
	<div class="panel">
	<?php
		/**
		* We open the database for the SQL request.
		*/
		$bdd = connection_db();
		/*For take yourlsID*/
		$target = $bdd->prepare('SELECT YourlsID FROM 360sc_iz.yourls WHERE ShortURL=:url');
		$target->bindValue(':url', $key, PDO::PARAM_STR);
		$target->execute();
		$yourlsID = $target->fetch()['YourlsID'];
		/*With yourlsID, to take le customersID*/
		$target = $bdd->prepare('SELECT customersID FROM societe_tags WHERE yourlsID=:id');
		$target->bindValue(':id', $yourlsID, PDO::PARAM_INT);
		$target->execute();
		$customersID = $target->fetch()['customersID'];
		/*With le customersID, to take le name the societe*/
		$target = $bdd->prepare('SELECT name FROM societe WHERE id=:id');
		$target->bindValue(':id', $customersID, PDO::PARAM_INT);
		$target->execute();
		$nameSociete = $target->fetch()['name'];
		/*With the yourlsID, to take the informations of tags*/
		$target = $bdd->prepare('SELECT * FROM tags WHERE yourlsID=:yourlsID');
		$target->bindValue(':yourlsID', $yourlsID, PDO::PARAM_INT);
		$target->execute();
		$tag = $target->fetch();
		/*OriginalUrl*/
		$target = $bdd->prepare('SELECT OriginalURL FROM 360sc_iz.yourls WHERE ShortURL=:url');
		$target->execute(array('url'=>$key));
		$originalUrl = $target->fetch()['OriginalURL'];

		/**
		* Display form on screen
		*/


		echo'
			<table style="margin: auto;">
				<tdbody>
					<tr>
						<th>Societe</th>
					</tr> 
					<tr>
						<td>'.(isset($nameSociete) && $nameSociete ? $nameSociete : 'vide').'</td>
					</tr>
					<tr>
						<th>Tag Name</th>
					</tr> 
					<tr>
						<td>'.(isset($tag['TagName']) && $tag['TagName'] ? $tag['TagName'] : 'vide').'</td>
					</tr>
					<tr>
						<th>TagID</th>
					</tr> 
					<tr>
						<td>'.$tag['TagID'].'</td>
					</tr>
					<tr>
						<th>UniqueTagID</th>
					</tr> 
					<tr>
						<td>'.$tag['UniqueTagID'].'</td>
					</tr>
				</tdbody>										
		';
		/**
		* Closing database
		*/
		$target->closeCursor();
	?>
	</div>
	</div>
</div>
<?php
/**
* We include the footer for regular page.
*/
include "./include/footer2.php";
?>