<?php

switch($_SERVER['REQUEST_METHOD']){
  case "GET":

  require_once "php/Smarty/libs/Smarty.class.php";

  $smarty = new Smarty();
  $smarty->template_dir = "templates";
  $smarty->compile_dir = "templates_c";
  $smarty->caching = false;
  $smarty->left_delimiter = "<{";
  $smarty->right_delimiter= "}>";

  $owner = $_SESSION['societe'];
  $godRight = $_SESSION['level']==4?true:false;

  
  $smarty->assign("serverRoot",SERVERROOT);

  if($godRight){

    //récupérer tous les comptes
    $function = "getAllAccounts";
    require_once "php/DAF/getAccounts.php";

    //récupérer toutes les sociétés   
    $function = "getAllSocieties";
    require_once "php/DAF/getSocieties.php";

    $dataSocieties = array();
    foreach ($societies as $societyId => $society) {
      $dataSocieties[$societyId] = $society['name'];
    }

    $dataAccounts =array();
    $societyId = null;
    $currentAccount = null;
    foreach ($accounts as $key => $account){
      $dataAccounts[$key] = $account['Name'];
      if($key==$accountId) $currentAccount = $account;
    }

    $smarty->assign("societies",$dataSocieties);
    $smarty->assign("accounts",$dataAccounts);
    $smarty->assign("accountId",$accountId);
    $smarty->assign("account",$currentAccount);
    $smarty->display("edit-account-god.tpl");

  }else{



    if($_SESSION['client_id']==$accountId){

          //récupérer le compte à modifier
    $function = "getAccountById";
    require_once "php/DAF/getAccounts.php";

      $smarty->assign("postPage",SERVERROOT.'/account/'.$accountId);
      $smarty->assign("account",$account);

      $smarty->display("edit-account.tpl");

    }else{

      $smarty->display("no-permission-page.tpl");  
    }

  }


  break;
  case "POST":
    $godRight = $_SESSION['level']==4?true:false;
    $operation = "success";
    if(!$godRight){
      if(!empty($_POST['password']) && !empty($_POST['confirm'])){ // si on veut changer le mot de passe
        $password = $_POST['password'];
        if (!checkPassword($_SESSION['client_id'],$password))  $operation = "failure";
      }
      if ($operation != "failure") {
        $function = 'setAccount';
        require_once "php/DAF/setAccounts.php";
      }
      
    }else{
      $function = 'setAccount';
      require_once "php/DAF/setAccounts.php";
    }
    header("Location: ".SERVERROOT."/account/$accountId?operation=$operation");
 
    


  break;
}

?>