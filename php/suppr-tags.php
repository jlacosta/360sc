<?php 
/** 
* Multi Add Page
* @author: ROUMEGUE Jérémy - 360sc
* We include the header for regular page.
*/
include "./include/head2.php";
?>
		<div class="row">
		  <div class="columns large-12 small-6">
			<div class="panel">
				<p>
					<?php
						if($_SESSION['page'] == "delete") {
							$bdd = connection_db();
							$query=$bdd->prepare("DELETE FROM tags WHERE TagID = ".$_POST['identifiant'].";");
							$query->execute();
							echo '<p>La suppression du tag à bien été faites.<br />Cliquez <a href="'.get_link().'">ici</a> 
							pour revenir à la page d accueil</p>';
						} else if(isset($id)){
							echo "<form method='post' action='".get_link()."delete-tag/' enctype='multipart/form-data'>
									<p>Etes-vous sur de vouloir supprimer le tag ?</p>
									<p>
										<input type='hidden' value=".$id." name='identifiant'/>
										<input class='button' type='submit' value='Oui' />
										<input class='button' type='cancel' value='Non' />
									</p>";
							echo "</form>";
						} else {
							/**
							* We open the database for the SQL request.
							*/
							$bdd = connection_db();
							
							if($_SESSION['level'] == "4") {
								$query=$bdd->prepare('SELECT COUNT(*) AS nbr FROM societe_tags');
							} else {
								$query=$bdd->prepare('SELECT COUNT(*) AS nbr FROM societe_tags WHERE customersID =:id');
								$query->bindValue(':id',$_SESSION['societe'], PDO::PARAM_INT);
							}

							$query->execute();
							$nb_tags=($query->fetchColumn()==0)?true:false;
							$query->CloseCursor();
							
							if($nb_tags){
								?>
								<p>
								Vous n'avez aucun tag. Cliquez <a href="/contact/">ici</a> pour en commander.
								</p>
								<?php
							} else {
								?>
								<p>
								Voici la liste de vos tags.<br/>
								</p>
								<?php
								/**
								* The SQL request
								*/
								if($_SESSION['level'] == "4") {
									$sql = 'SELECT t.TagID, t.TagName, t.description, t.isActive, y.ShortURL, y.OriginalURL FROM 360sc_iz.yourls 
									AS y, tags AS t, societe_tags AS ct WHERE t.YourlsID=y.YourlsID AND ct.yourlsID = y.YourlsID ;';
								} else {
									$sql = 'SELECT t.TagID, t.TagName, t.description, t.isActive, y.ShortURL, y.OriginalURL FROM 360sc_iz.yourls AS y, tags AS t, societe_tags AS ct 
									WHERE t.YourlsID=y.YourlsID AND ct.yourlsID = y.YourlsID AND ct.customersID = '.$_SESSION['societe'].';';
								}
								$target = $bdd->query($sql);
								
								/**
								* Display form on screen
								*/
								echo "<table>";
								echo "<tr>
								<th>ID</th>
								<th>Nom</th>
								<th>Description</th>
								<th>URL</th>
								<th>Actif</th>
								</tr>";
								
								/** 
								* Show ALL the tag
								*/
								while ($row = $target->fetch()) {
									echo '<tr id="'.$row['TagID'].'" class="del-t">
									<td>'.$row['TagID'].'</td>
									<td>'.$row['TagName'].'</td>
									<td>'.$row['description'].'</td>
									<td>'.$row['OriginalURL'].'</td>
									<td>'.$row['isActive'].'</td>
									</tr>';
								}
								
								echo "</table>";
								
								/**
								* Closing database
								*/
								$target->closeCursor();
							}
						}
					?>
				</p>
			</div>
		  </div>		  
		</div>
<?php
/**
* We include the footer for regular page.
*/
include "./include/footer2.php";
?>