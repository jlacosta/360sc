<?php

switch ($_SERVER['REQUEST_METHOD']) {
  case 'GET':

    require_once "php/Smarty/libs/Smarty.class.php";
    require_once "php/Smarty/libs/SmartyPaginate.class.php";

    $smarty = new Smarty();
    $smarty->template_dir = "templates";
    $smarty->compile_dir = "templates_c";
    $smarty->caching = false;
    $smarty->left_delimiter = "<{";
    $smarty->right_delimiter = "}>";
     // required connect
    SmartyPaginate::connect("urles");
    // set items per page
    if(isset($_GET['limit']) && is_numeric($_GET['limit']) && $_GET['limit']>0)
      SmartyPaginate::setLimit(round($_GET['limit']),"urles");
    else
      SmartyPaginate::setLimit(25,"urles");
    $function = 'getTagsPage';
    $owner = $_SESSION['societe'];
    $godRight = $_SESSION['level']==4?true:false;
    $paginateId = "urles";
    require_once "php/DAF/getTags.php";
    $smarty->assign("godRight",$godRight);
    $smarty->assign("serverRoot",SERVERROOT);
    /*
    code d'unir les MCs (tag, etc...)
    */
    $smarty->assign("MCs",$tags); // we have only tags as MC at the moment
    SmartyPaginate::assign($smarty,"paginate","urles");
    $smarty->display("list-urles.tpl");

  break;
  
  case 'POST':
    $function = 'newTags';
    if(isset($_POST['societe'])){ 
      $owner = $_POST['societe'];
    }else{
      $owner = $_SESSION['societe'];
    }
    // Laisser une couche flexible 'MC' qui est le parent du tag
    require_once "php/DAF/setTag.php";

    header("Location: ".SERVERROOT."/addMCs?operation=$operation");
  break;

  default:
  
  break;
}


?>
