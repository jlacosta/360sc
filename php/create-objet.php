<?php 
/** 
* Multi Add Page
* @author: ROUMEGUE Jérémy - 360sc
* We include the header for regular page.
*/
include "./include/head2.php";
?>

<div class="row">

	<div id="firstModal" style="padding:0px !important;" class="reveal-modal" data-reveal aria-labelledby="firstModalTitle" aria-hidden="true" role="dialog">
		<div class="row">
			<div style="position: absolute; top:7rem; left:15px; width: 18rem; height:150px;">
				<form>
					<div class="row">
						<div class="small-12 large-12 columns">
							<div class="row">
								<div class="small-5 large-5 columns" style="padding: 0rem 0rem !important;">
									<input id="modallatitud" type="text" value="0" placeholder="Latidude">
								</div>
								<div class="small-5 large-5 columns" style="padding: 0rem 0rem !important;">
									<input id="modallongitud" type="text" value="0" placeholder="Longitud">
								</div>
								<div class="small-2 large-2 columns" style="padding: 0rem 0rem !important;">
									<a href="#" onclick="changerParCoord()" class="button postfix" style="padding:0px 0px 0px 0px !important;">Voir</a>
								</div>
							</div>
						</div>
						<div class="small-12 large-12 columns">
							<div class="row">
								<div class="small-12 large-12 columns" style="padding: 0rem 0rem !important;">
									<input type="text" id="modaladresse" placeholder="Adresse">
								</div>
								<div class="small-10 large-10 columns" style="padding: 0rem 0rem !important;">
									<input type="text" id="modalpays" placeholder="Etat / Pays">
								</div>
								<div class="small-2 large-2 columns" style="padding: 0rem 0rem !important;">
									<a href="#" onclick="changerParAdress()" class="button postfix" style="padding:0px 0px 0px 0px !important;">Voir</a>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
			<input onclick="onValider()" style="margin-bottom:0px !important; width:100% !important;" class="button" type="button" value="Valider"/>
		</div>  
	</div>	
	<script>
	var gMap;
	var gMarker;

	var Wrap = function (){			  
		this.installed = false;	
		var activeMap = function() {						
			if ($('#firstModal #map').length == 0)
				$('#firstModal').prepend('<div style="width: 100%; height:450px;" id="map"></div>');

			this.wrapMap = function() {	
				$.__latlng = false;
				var lat = 43.503474;
				var lng =  6.485704;
				var google = new L.Google('ROAD');
				if (installed == false){
					gMap = this.map = L.map('map').setView([
						lat, 
						lng
						], 
						11);
					installed = true;
					this.map.addLayer(google);
					gMarker = this.marker = L.marker([lat, lng], {draggable: true});
					$.__latlng = this.marker.getLatLng();
					this.marker.bindPopup(lat+', '+lng);
					this.marker.on('dragend', function(event){
						$.__latlng = this.getLatLng();
						this.setPopupContent(this.getLatLng().lat + ', ' + this.getLatLng().lng);
					});	
					this.marker.addTo(this.map);
				}
			}
			wrapMap = wrapMap.bind(this);						
			setTimeout(wrapMap, 500);
		}
		return activeMap;
	}
	
	var activeMap = Wrap();

	var onValider = function (){
		if ($.__latlng){
			$("#position").val($.__latlng.lat+', '+$.__latlng.lng);
		}
		$('#firstModal').foundation('reveal', 'close');
	}

	var changerParCoord = function(){
		console.log(gMarker.getLatLng());
		var lng = parseFloat($("#modallongitud").val());
		var lat = parseFloat($("#modallatitud").val());
		gMarker.setLatLng([lat, lng]);

		$.__latlng = this.marker.getLatLng();
		
		gMap.panTo(gMarker.getLatLng());
		gMap.setZoom( gMap.getMaxZoom() );
	}

	var changerParAdress = function(){
		console.log(gMarker.getLatLng());
		var adresse = $("#modaladresse").val();
		var pays = $("#modalpays").val();
		var finalAdresse = adresse + ' ' + pays;

		var geocoder = new self.google.maps.Geocoder();
		geocoder.geocode( { 'address': finalAdresse}, function (results, status){
			if (status == self.google.maps.GeocoderStatus.OK) {
	           	console.log(results[0].geometry.location.lat());
	            console.log(results[0].geometry.location.lng());

	            var lat = parseFloat(results[0].geometry.location.lat());
	            var lng = parseFloat(results[0].geometry.location.lng());

	            gMarker.setLatLng([lat, lng]);

	            $.__latlng = this.marker.getLatLng();

				gMap.panTo(gMarker.getLatLng());
				gMap.setZoom( gMap.getMaxZoom() );
	          	
	        }
		}); 
	}

	</script>	

	<div class="columns large-12 small-6">
		<div class="panel">
			<?php if(!(isset($_POST['name']))){
				?>
				<p>
					Bienvenue dans l'interface d'administration de IZ.<br/>
					<!-- Form for encrypt an URLs in many tags -->
					<form action="<?php echo get_link(); ?>objet/" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label for="name">Nom de l'objet</label>
							<input id="name" type="text" name="name" />
						</div>
						<div class="form-group">
							<label for="parent">Type d'objet :</label>
							<?php
							function typeCmp($a,$b){
								return strcmp($a["nameType"], $b["nameType"]);
							}

							$ancestry = getAscendanceSocietes($_SESSION['societe']);
							$descendants = getWrapSocietes($_SESSION['societe']);
							$societes[] = $_SESSION['societe'];
							$societes = array_merge($societes,$ancestry,$descendants);
							$types =array();
							$bdd = connection_db(); 
							foreach ($societes as $societe) {
								$sql = "SELECT * FROM typeobjet WHERE proprietaire = $societe";
								$target = $bdd->prepare($sql);
								$target->execute();
								while($type = $target->fetch()){
									$types[]=$type;
								}
							}
							$sql = "SELECT * FROM typeobjet WHERE proprietaire IS NULL";
							$commonTypes = $bdd->prepare($sql);
							$commonTypes->execute();
							while($commonType=$commonTypes->fetch()){
								$types[]=$commonType;
							}
							usort($types,"typeCmp");  
							echo "<select name='idParent' id='idParent'>";
							foreach ($types as $type) {
								echo "										
								<option value='". $type['idType'] ."'>". $type['nameType']."</option>
								";	
							} 
							echo "</select>";
							?>	
						</div>
						<div>
							<label for="alphaid">*Alpha ID :</label>
							<input type="text" name="alphaid" id="alphaid">
						</div>
						<div class="form-group">
							<label for="lat">*Position</label>
							<input id="position" style="width:89.5% !important; display:inline !important;" name="latlng" type="text" />
							<input onclick="activeMap()" data-reveal-id="firstModal"
							style="height:2.3125rem !important; padding: 0px !important; width:95.7969px !important;"  
							type="button" class="button" value="Ajouter" /> 
						</div>
						<div class="form-group">
							<label for="proprietaire">Propiétaire</label>
							<?php
							$bdd = connection_db();
							$societes = $descendants;
							$societes[] = $_SESSION['societe'];
							echo "<select id='proprietaire' name='proprietaire'>";
							foreach ($societes as $societe) {
								$sql = "SELECT name, ID FROM societe WHERE ID = $societe ORDER BY ID ASC ";
								$target  = $bdd->prepare($sql);
								$target->execute();
								while ($row = $target->fetch()) {
									echo "<option ";
									if ($row['ID']==$_SESSION['societe']) echo  ' selected ';
									echo "value='". $row['ID'] ."'>".$row['name']."</option>";	
								}
							}


							echo "</select>";
							?>	
						</div>
						<div>
							<label for="fichier">Image de profil :</label>
							<input type="file" name="image" id="fichier">
						</div>
						<div class="form-group">
							<label for="desc">Description de l'objet</label>
							<input id="desc" type="text" name="desc" />
						</div>
						<div class="form-group">
							<label  >associer MC:</label>
							<?php
							$bdd = connection_db(); 
							$societe  = $_SESSION['societe']; 

							$sql = "SELECT * FROM tags,societe_tags WHERE tags.YourlsID = societe_tags.yourlsID AND tags.appairer = 0 AND societe_tags.customersID = $societe ORDER BY TagID ASC ";
							$target  = $bdd->prepare($sql);
							$target->execute();
							$i=0;
							while ($row = $target->fetch()) {
								if($i % 4 === 0) {echo "<div class='row'>";}
								echo "<div class='columns large-3 small-3 end'>
								<input type='checkbox' value='". $row['TagID'] ."' name='appairageTags[]' id='Tag_".$row['TagID']."'/>
								<label for='Tag_".$row['TagID']."'>".$row['TagName']."</label>
								</div>";
								if($i % 4 === 3) {echo "</div>";}
								$i++;
							} 
							if($i % 4){echo "</div>";}
							

							?>	
						</div>
						<input class="medium button" type="submit" value="Envoyer" />
					</form>
				</p>
				<?php 
			} 
			else {
				$bdd = connection_db();
				/**
				* We prepare SQL request for making an URLe.
				*/
				$sql = 'SELECT Name FROM customers WHERE CustomersID='.$_SESSION['client_id'].'';
				$target   = $bdd->query($sql);
				/**
				* We check the new YourlsID
				*/
				while ($row = $target->fetch()) {
					$name = $row['Name'];
				}

				
				

				$bdd = connection_db();
				$sql = 'INSERT INTO objet(image, name, idParent, appairage, dateAppairage, respoAppairage, description, active, dateActive, respoActive, objLat, objLong, adresse, descriptionPos, alphaID, comment)
				VALUES (:image, :name, :idParent, :appairage, :dateAppairage, :respoAppairage, :description, :active, :dateActive, :respoActive, :objLat, :objLong, :adresse, :descriptionPos, :alphaID, :comment)';
				
				$query = $bdd->prepare($sql);
				//$query->bindValue(':objetID', NULL,PDO::PARAM_INT);
				$query->bindValue(':image',NULL,PDO::PARAM_STR);
				$query->bindValue(':name',$_POST['name'],PDO::PARAM_STR);
				$query->bindValue(':idParent',$_POST['idParent'],PDO::PARAM_INT);
				$query->bindValue(':appairage',0,PDO::PARAM_INT);
				$query->bindValue(':dateAppairage',NULL,PDO::PARAM_STR);
				$query->bindValue(':respoAppairage',$name,PDO::PARAM_STR);
				$query->bindValue(':description',$_POST['desc'],PDO::PARAM_STR);
				$query->bindValue(':active',0,PDO::PARAM_INT);
				$query->bindValue(':dateActive',NULL,PDO::PARAM_STR);
				$query->bindValue(':respoActive',NULL,PDO::PARAM_STR);


				$latlng = explode(', ', $_POST['latlng']);
				$query->bindValue(':objLat', $latlng[0], PDO::PARAM_INT);
				$query->bindValue(':objLong', $latlng[1], PDO::PARAM_INT);
				
				$query->bindValue(':adresse',NULL,PDO::PARAM_STR);
				$query->bindValue(':descriptionPos',NULL,PDO::PARAM_STR);
				$query->bindValue(':alphaID', $_POST['alphaid'], PDO::PARAM_STR);
				$query->bindValue(':comment',NULL,PDO::PARAM_STR);
				$query->execute();



				$target = $bdd->prepare('SELECT o.objetID FROM objet as o ORDER BY o.objetID DESC LIMIT 1');
				$target->execute();
				
				while ($row = $target->fetch()) {
					$objetID = $row['objetID'];
				}


				//createOneObject($_POST['name'], $_SESSION['client_id']);
				$target = $bdd->prepare('INSERT INTO societe_objet(societeID, objetID) VALUES (:societe, :objetID)');
				$target->bindValue(':societe', $_POST['proprietaire'], PDO::PARAM_INT);
				$target->bindValue(':objetID', $objetID, PDO::PARAM_INT);
				$target->execute();

				if(isset($_POST['appairageTags']) && sizeof($_POST['appairageTags']!=0)){
					$tagExisting = false;
					foreach ($_POST['appairageTags'] as $tagId) {		
						$tagQuery = $bdd->prepare("SELECT * FROM tags WHERE TagID = $tagId");
						$tagQuery->execute();
						if($tag=$tagQuery->fetch()){
							$tagExisting = true;		
							$insertObjectTagQuery = $bdd->prepare("INSERT INTO objet_tags(yourlsID,objetID) VALUES(".$tag['YourlsID'].", $objetID)");
							$insertObjectTagQuery->execute();
							$updateTagQuery = $bdd->prepare("UPDATE tags SET appairer = 1 WHERE TagID = $tagId");	
							$updateTagQuery->execute();								
						}
					}
					if($tagExisting){
						$updateObjectQuery = $bdd->prepare("UPDATE objet SET appairage = 1, dateAppairage = NOW() WHERE objetID = $objetID");
						$updateObjectQuery->execute();							
					}
				}

				/*ob_start();
		       
		       	header('Location:'.get_link().'objet/');
		       
		       	ob_end_flush();*/
		       	$url = get_link()."objet/";
		       	?>
		       	<script language="javascript" type="text/javascript">
 						 window.location.href="<?php echo $url; ?>";								   
 						</script>

 						<?php       	
 					}
 					?>
 				</div>
 			</div>		  
 		</div>
 		<?php
/**
* We include the footer for regular page.
*/
include "./include/footer2.php";
?>