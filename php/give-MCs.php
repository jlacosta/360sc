<?php

switch ($_SERVER['REQUEST_METHOD']) {
  case 'GET':

    require_once "php/Smarty/libs/Smarty.class.php";
    require_once "php/Smarty/libs/SmartyPaginate.class.php";

    $smarty = new Smarty();
    $smarty->template_dir = "templates";
    $smarty->compile_dir = "templates_c";
    $smarty->caching = false;
    $smarty->left_delimiter = "<{";
    $smarty->right_delimiter = "}>";

 
    // required connect
    SmartyPaginate::connect("gMCs");
    // set items per page
    if(isset($_GET['limit']) && is_numeric($_GET['limit']) && $_GET['limit']>0)
      SmartyPaginate::setLimit(round($_GET['limit']),"gMCs");
    else
      SmartyPaginate::setLimit(25,"gMCs"); 

     //pour l'instant on n'a que les tags comme MC
    //if(isset($_GET['getTags'])){
      $function = "getTagsPage";
      $owner = $_SESSION['societe'];
      $godRight = $_SESSION['level']==4?true:false;
      $paginateId = "gMCs";
      require_once "php/DAF/getTags.php";
    //}

    $MCs=array();
    $MCs=array_merge($MCs,$tags);


    //récupérer les sociétés disponibles
    $societiesIds = getWrapSocietes($owner);
    $societiesIds[] = $owner;
    $function = "getSocietiesByIds";
    require_once "php/DAF/getSocieties.php";

    $dataSocieties = array();
    foreach ($societies as $societyId => $society) {
      $dataSocieties[$societyId] = $society['name'];
    }

    $smarty->assign("postPage",SERVERROOT.'/redistribute');
    $smarty->assign("MCs",$MCs);  
    $smarty->assign("societies",$dataSocieties);  
    $smarty->assign("accountSociety",$owner);  
    $smarty->assign("godRight",$_SESSION['level']==4?true:false);
    $smarty->assign("serverRoot",SERVERROOT);
    $smarty->display("give-MCs.tpl");

  break;
  
  case 'POST':
    $function = 'redistributeTags';
    $distributedTags = array();
    if(isset( $_POST['selectedMCs'])) $distributedTags = $_POST['selectedMCs']; 
      
    // Laisser une couche flexible 'MC' qui est le parent du tag*/
    require_once "php/DAF/setTags.php";

    header("Location: ".SERVERROOT."/redistribute?operation=$operation");
  break;

  default:
  
  break;
}


?>
